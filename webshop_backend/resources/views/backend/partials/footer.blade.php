{{--page_footer_start--}}
<div class="page-footer">
    <div class="page-footer-inner"> 2020 &copy;
        <a href="http://perfectsocket.com/" title="Posetite nas sajt" target="_blank"> Perfect Socket</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
{{--page_footer_end--}}