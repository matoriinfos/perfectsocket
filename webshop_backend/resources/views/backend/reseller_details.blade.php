@extends('backend.main')

@section('content')
    <div class="page-content">
        <div class="row">
            @if (Session::has('account_type_changed'))
                <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ Session::get('account_type_changed') }}</strong>
                </div>
            @endif
                @if (Session::has('reseller_changed'))
                    <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ Session::get('reseller_changed') }}</strong>
                    </div>
                @endif
                @if (Session::has('password_reset'))
                    <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ Session::get('password_reset') }}</strong>
                    </div>
                @endif

            {{--<div class="row">--}}
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <br>
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-present font-green"></i>
                            <span class="caption-subject font-green sbold uppercase"> Prikaz podataka o preprodavcu </span>
                        </div>

                            @if($reseller->account_type == 'user')
                                <a style="float:right; margin-left: 5px;" href="{{ route('resellers.update', $reseller->id) }}"  class="btn btn-success"
                                   onclick="event.preventDefault();
                                   document.getElementById('account').value = 'reseller';
                                   document.getElementById('action').value = '-1';
                                    document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-ok"></i>&nbsp;
                                    <span class="title">Potvrdi status preprodavca</span>
                                </a>
                            @else
                                <a style="float:right; margin-left: 5px;" href="{{ route('resellers.update', $reseller->id) }}"  class="btn btn-danger"
                                   onclick="event.preventDefault();
                                   document.getElementById('account').value = 'user';
                                   document.getElementById('action').value = '-1';
                                  document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-remove"></i>&nbsp;
                                    <span class="title">Ponisti status preprodavca</span>
                                </a>
                            @endif
                        <a style="float:right; margin-left: 5px;" href="{{ route('resellers.destroy', $reseller->id) }}"  class="btn btn-danger"
                           onclick="event.preventDefault();
                                  document.getElementById('delete-form').submit();">
                            <i class="glyphicon glyphicon-remove"></i>&nbsp;
                            <span class="title">Obrisi preprodavca</span>
                        </a>

                        <form id="update-form" action="{{ route('resellers.update', $reseller->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="account" id="account" value="-1">
                        </form>
                        <form id="delete-form" action="{{ route('resellers.destroy', $reseller->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('delete')
                        </form>

                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active col-md-4 col-md-offset-4 "  id="tab_1_1">
                                <form role="form" action="{{ route('resellers.update', $reseller->id) }}" method="post" data-parsley-validate>
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label class="control-label">Ime</label>
                                        <input type="text"  name="name" class="form-control" value="{{$reseller->name}}" required /> </div>
                                    <div class="form-group">
                                        <label class="control-label">Prezime</label>
                                        <input type="text" name="surname" value="{{$reseller->surname}}" class="form-control" required/> </div>
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" name="email" value="{{$reseller->email}}" class="form-control" required /> </div>
                                    <div class="form-group">
                                        <label class="control-label">Adresa</label>
                                        <input type="text" name="address" value="{{$reseller->address}}" class="form-control" required/> </div>
                                    <div class="form-group">
                                        <label class="control-label">Stan</label>
                                        <input type="text" name="apartment" value="{{$reseller->apartment}}" class="form-control" /></div>
                                    <div class="form-group">
                                        <label class="control-label">Grad</label>
                                        <input type="text" name="city" value="{{$reseller->city}}" class="form-control" required/></div>
                                    <div class="form-group">
                                        <label class="control-label">Postanski broj</label>
                                        <input type="text" name="post_number" value="{{$reseller->post_number}}" class="form-control" /></div>
                                    <div class="form-group">
                                        <label class="control-label">Telefon</label>
                                        <input type="text" name="telephone" value="{{$reseller->telephone}}" class="form-control" required  /> </div>
                                    <input type="hidden" name="account_type" value="{{$reseller->account_type}}" class="form-control" required  />
                                    @if($reseller->account_type == 'reseller')
                                    <div class="form-group">
                                        <label class="control-label">Popust</label>
                                        <select name="discount" id="discount" class="form-control">
                                            <option value="1" @if($reseller->discount == '1')  {{'selected'}}@endif>0 %</option>
                                            <option value="0.95" @if($reseller->discount == '0.95')  {{'selected'}}@endif>5 %</option>
                                            <option value="0.90" @if($reseller->discount == '0.90')  {{'selected'}}@endif>10 %</option>
                                            <option value="0.85" @if($reseller->discount == '0.85')  {{'selected'}}@endif>15 %</option>
                                            <option value="0.80" @if($reseller->discount == '0.80')  {{'selected'}}@endif>20 %</option>
                                        </select>
                                    </div>
                                    @endif
                                    <input type="hidden" name="action" id="action" value="update">

                                    <div class="margiv-top-10">
                                        <button  type="submit" class="btn btn-success">
                                            <i class="fa fa-check"></i> Sačuvaj</button>
                                        <a href="{{ route('resellers.edit',$reseller->id) }}" class="btn default"> Odustani </a>
                                    </div>

                                    <div class="margiv-top-10">
                                        <a href="{{ route('resellers.show',$reseller->id) }}" class="btn btn-primary form-control" style="margin-top: 5px;"> Resetovanje lozinke </a>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
<script>
    function showDiscount() {
        document.getElementById('dugmePopust').style.display="none";
        document.getElementById('izborPopusta').style.display= "block";
    }

    function dodajPopust() {
        var popust = document.getElementById('popust').value;
        $('$iznos_popusta').val(popust);
        alert(document.getElementById('iznos_popusta').value);

    }


</script>