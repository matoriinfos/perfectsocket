@extends('backend.main')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i>Pregled statistike prodaje proizvoda od {{ Carbon\Carbon::parse($from)->format('d-m-Y') }} do {{ Carbon\Carbon::parse($to)->format('d-m-Y') }}.godine </div>
                    </div>
                    <div class="portlet-body" >

                        <div class="table-container">
                            <div class="col-md-4 col-md-offset-4"><h4><u>Serija proizvoda: {{$serija}}</u></h4></div>
                            <table id="orders_table" class="display" cellspacing="0" width="100%">
                                <thead>
                                  <th>ID</th>
                                  <th>kat_br</th>
                                  <th>Proizvod</th>
                                  <th>Opis</th>
                                  <th>Kolicina</th>
                                </thead>
                                <tbody>
                                @foreach($izborKorisnika as $izbor)
                                    <tr>
                                        <td style="vertical-align:middle">{{$loop->iteration}}</td>
                                        <td style="vertical-align:middle">
                                            {{$izbor['kat_br']}}
                                        </td>
                                        <td style="vertical-align:middle">
                                          @if (strpos($izbor['kratak_opis'], "Ram")!==false || strpos($izbor['kratak_opis'], "Maska")!==false|| strpos($izbor['kratak_opis'], "Frame")!==false|| strpos($izbor['kratak_opis'], "Kit")!==false)
                                              <img class="rounding" src="{{$putanja.'okviri/'.$izbor['kat_br'].'.png' }}" alt="" style=" height:60px;">
                                          @else
                                            @if (strpos($izbor['kat_br'], "+")!==false)
                                              <?php
                                               $slika = substr($izbor['kat_br'], strpos($izbor['kat_br'], "+")+1);
                                              ?>
                                              <img class="rounding" src="{{$putanja.''.$slika.'.jpg' }}" alt="" style=" height:60px;">
                                            @else
                                              @if ($serija=='PRIMERA' || $serija=='PRIMERA kompleti'|| $serija=='NILOE' )
                                                <img class="rounding" src="{{$putanja.''.$izbor['kat_br'].'.png' }}" alt="" style=" height:60px;">
                                              @endif
                                                <img class="rounding" src="{{$putanja.''.$izbor['kat_br'].'.jpg' }}" alt="" style=" height:60px;">
                                            @endif

                                          @endif
                                        </td>
                                        <td style="vertical-align:middle">{{$izbor['kratak_opis']}}</td>
                                        <td style="vertical-align:middle">{{$izbor['kolicina_kreiranja']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <br>
                            <hr>
                            <div class="col-md-offset-3 col-md-9">
                                <a type="button" href="{{route('statistics.create')}}" class="btn red">
                                    <i class="fa fa-repeat"></i> Ponovi proračun</a>
                                <a type="button" href="{{route('orders.index')}}" class="btn default">Početna</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
    </div>

@endsection
