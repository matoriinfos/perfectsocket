@extends('backend.main')

@section('content')

    <div class="page-content">
        <div class="row">
            @if (Session::has('order_status_changed'))
                <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{ Session::get('order_status_changed') }}</strong>
                </div>
            @endif
                @if (Session::has('success_preprodavac'))
                    <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ Session::get('success_preprodavac') }}</strong>
                    </div>
                @endif
                @if (Session::has('success_delete'))
                    <div style="text-align: center" class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ Session::get('success_delete') }}</strong>
                    </div>
                @endif
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-green"></i>
                            <span class="caption-subject font-green sbold uppercase"> Pregled preprodavaca </span>
                        </div>
                    </div>
                    <div class="portlet-body" >
                        <div class="table-container">
                            <table id="orders_table" class="display" cellspacing="0" width="100%">
                                <thead>
                                <th style="width:10px">#</th>
                                <th>Ime i prezime</th>
                                <th>Email</th>
                                <th>Adresa</th>
                                <th>Grad</th>
                                <th style="width:100px">Status naloga</th>
                                <th style="width:100px">Popust</th>
                                <th>Detalji </th>
                                </thead>

                                <tbody>
                                @foreach ($resellers as $reseller)
                                    <tr>
                                        <td style="vertical-align:middle">{{ $reseller->id }}</td>
                                        <td style="vertical-align:middle">{{ $reseller->name }} {{ $reseller->surname }}</td>
                                        <td style="vertical-align:middle">{{ $reseller->email }}</td>
                                        <td style="vertical-align:middle">{{ $reseller->address }}/{{ $reseller->apartment }}</td>
                                        <td style="vertical-align:middle">{{ $reseller->city }}</td>
                                        @if($reseller->account_type == 'user')
                                            <td style="vertical-align:middle;"><h4><span class="label label-lg label-success">Korisnik</span></h4></td>
                                        @elseif($reseller->account_type == 'reseller')
                                            <td style="vertical-align:middle;"><h4><span style="border-radius: 25px;" class="label label-primary">Preprodavac</span></h4></td>
                                        @endif

                                        @if($reseller->discount == '1')
                                            <td style="vertical-align:middle;"><h4><span class="label label-lg label-success">/</span></h4></td>
                                        @else
                                            <td style="vertical-align:middle;"><h4><span style="border-radius: 25px;" class="label label-primary">{{(1 - $reseller->discount) * 100}} %</span></h4></td>
                                        @endif

                                        <td style="vertical-align:middle;">
                                            <a href="{{route('resellers.edit',$reseller->id)}}" style="border-radius: 25px;" class="btn btn-primary">Detalji</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection