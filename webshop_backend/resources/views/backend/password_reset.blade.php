@extends('backend.main')

@section('content')

    <div class="page-content">
        <div class="row">
            @if (Session::has('success_password'))
                <div class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Uspešno:</strong> {{ Session::get('success_password') }}
                </div>
            @endif
            @if (Session::has('success_user'))
                <div class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Uspešno:</strong> {{ Session::get('success_user') }}
                </div>
            @endif
            @if (Session::has('error_password'))
                <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Greška: </strong> {{ Session::get('error_password') }}
                </div>
            @endif
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-green"></i>
                            <span class="caption-subject font-green sbold uppercase"> Resetovanje lozinke  korisnika {{$reseller->name}} {{$reseller->surname}} </span>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">

                        <div class="tab-pane col-md-4 active col-md-offset-4" id="tab_1_3">
                            <form action="{{route('resellers.update', $reseller->id)}}" method="post" data-parsley-validate >
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <input type="hidden" name="pass" value="pass"> </div>
                                <div class="form-group">
                                    <label class="control-label">Nova lozinka <i style="margin-left:20px;"class="icon2 glyphicon glyphicon-eye-open"></i></label>
                                    <input name="newpassword" id="txtNewPassword" type="password" class="form-control password2" required />
                                </div>
                                <div class="margin-top-10">
                                    <button  type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i> Sačuvaj</button>
                                    <a href="{{ route('resellers.edit',$reseller->id) }}" class="btn default"> Nazad </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
