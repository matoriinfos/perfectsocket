<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ponuda broj <strong>{{$order->id}} </strong></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        body {
            font-family: DejaVu Sans;
            font-size: 12px;
        }

        .column {
            float: left;
            width: 50%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .row {
            padding: 20px;
        }
    </style>
</head>
<body>
<div class="well">
    <img src="images/logo.png" class="img-responsive" alt="" width="150px">
    <p>PerfectSocket</p>
    <p>Ulica i broj</p>
    <p>Maticni broj: xxxxxxx PIB: xxxxxxxxx</p>
    <p>Rn. XXX-00000XXXXX-XXX</p>
    <p>Telefon: 06X-XXX XXX E-mail: info@perfectsocket.com</p>
</div>
<div class="row">
    <div class="column">
        <p>Mesto izdavanja računa: Beograd</p>
        <p>Datum izdavanja računa:{{ "  " . Carbon\Carbon::parse($order->created_at)->format('d.m.Y') }}</p>
        <p>Datum prometa dobara i usluga: {{ "  " . Carbon\Carbon::parse($order->created_at)->format('d.m.Y') }}</p>
        <br>
        <h4>RACUN BR. {{$order->order_id}}</h4>
    </div>
    <div class="column">
        <h5>Podaci o kupcu:</h5>
        <p>Ime Prezime: {{ $order->name }}&nbsp;{{ $order->surname }}</p>
        <p>E-mail: {{ $order->email }}</p>
        <p>Telefon: {{ $order->telephone }}</p>
        <p>Adresa za dostavu:</p>
        <p>{{ $order->shipping_address." "}} {{ $order->shipping_apartment." " }}
            , {{ $order->post_number }} {{ $order->shipping_city }}</p>
    </div>
</div>
<div>
    <h4>Prikaz porudžbine</h4>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Konfiguracija</th>
            <th style="width: 50px">Kol</th>
            <th style="width: 80px">Svega</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($orderItems as $orderItem)
            <tr>
                <td style="width: 20px;">{{ $i++ }}</td>
                <td style="vertical-align:middle">
                    <?php
                    // echo "<h4 style='padding-bottom: 10px'><i>" . $orderItem->configuration->name . "</i></h4>";
                    //ispisi proizvode  konfiguracije
                    $proizvodi = $orderItem->configuration->configurationItems;
                    $redniBroj = 1;
                    foreach ($proizvodi as $proizvod) {
//                        echo number_format((float)((float)$proizvod->product->price *100/120), 2);
//                        echo $proizvod->product->price;
                        echo "<h6 style='text-align: left; padding-left: 5px'>" . $redniBroj . ".   <b>" . $proizvod->product->description_en . "</b>  &nbsp;&nbsp;&nbsp;    " . $proizvod->product->reference_number . "&nbsp;&nbsp;&nbsp;   "  .str_replace(".",",",$proizvod->product->price). "   RSD  &nbsp;&nbsp;&nbsp; </h6>";
                        //  echo "<div style='width: 30px; height: 30px; padding-left: 10px'><img src='".$proizvod->product->image_src . "' class='slika_". $proizvod->product->image. "' style='width: 100%; height: 100%' ></div><br>";
                        $redniBroj++;

                    }
                    ?>
                </td>
                <td>{{$orderItem->quantity }}</td>
                <td>{{number_format((float)( $orderItem->item_price * $orderItem->quantity * 100/120), 2, ',', '.')." RSD"}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="row">
    <div class="column">

    </div>
    <div class="column">

        <table style="width:100%">
            <tr>
                <th>Ukupno</th>
                <td>{{number_format((float)($order->total_price * 100/120), 2, ',', '.')." RSD"}}</td>
            </tr>
            @if($order->discount>0)
                <tr>
                    <th>Cena sa popustom</th>
                    <td>{{number_format((float)($order->discounted_price * 100/120), 2, ',', '.')." RSD"}}</td>
                </tr>
            @endif
            <tr>
                <th>PDV</th>
                <td>{{number_format((float)($order->total_price * 20/120), 2, ',', '.')." RSD"}} </td>
            </tr>
            @if($order->discount>0)
                <tr>
                    <th>Za uplatu</th>
                    <td>{{ number_format($order->discounted_price, 2, ',', '.') .' RSD' }}</td>
                </tr>
            @else
                <tr>
                    <th>Za uplatu</th>
                    <td>{{ number_format($order->total_price, 2, ',', '.') .' RSD' }}</td>
                </tr>
            @endif

        </table>

        {{--@if(empty($order->discount))--}}
        {{--<h3 style="font-size: 18px; padding: 10px" class="label label-success pull-right">--}}
        {{--PDV:&nbsp;<b>{{number_format((float)($order->total_price * 20/120), 2)." RSD"}} </b></h3><br>--}}

        {{--<h3 style="font-size: 18px; padding: 10px" class="label label-success pull-right">--}}
        {{--Ukupno (sa PDV-om):&nbsp;<b>{{ $order->total_price.' RSD' }}</b></h3>--}}

        {{--@else--}}
        {{--<h3 style="font-size: 18px; padding: 10px" class="label label-success pull-right">--}}
        {{--Ukupno:&nbsp;<b>{{ $order->total_price.' RSD' }}--}}
        {{--<br>Snizeno:&nbsp;{{ $order->discounted_price.' RSD' }}--}}
        {{--</b></h3>--}}
        {{--@endif--}}
    </div>
</div>


</body>
</html>
