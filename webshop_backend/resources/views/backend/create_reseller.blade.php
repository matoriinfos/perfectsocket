@extends('backend.main')
@section('content')
    <div class="page-content">
        <div class="row">
            @if (Session::has('success_password'))
                <div class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Uspešno:</strong> {{ Session::get('success_password') }}
                </div>
            @endif
            @if (Session::has('success_user'))
                <div class="alert alert-success col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Uspešno:</strong> {{ Session::get('success_user') }}
                </div>
            @endif
            @if (Session::has('error_password'))
                <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>Greška: </strong> {{ Session::get('error_password') }}
                </div>
            @endif
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-green"></i>
                            <span class="caption-subject font-green sbold uppercase"> Dodavanje novog preprodavca </span>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active col-md-4 col-md-offset-4 "  id="tab_1_1">
                            <form role="form" action="{{route('resellers.store')}}" method="post" data-parsley-validate>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label">Ime</label>
                                    <input type="text"  name="name" class="form-control" value="" required /> </div>
                                <div class="form-group">
                                    <label class="control-label">Prezime</label>
                                    <input type="text" name="surname" value="" class="form-control" required/> </div>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="email" value="" class="form-control" required /> </div>
                                <div class="form-group">
                                    <label class="control-label">Adresa</label>
                                    <input type="text" name="address" value="" class="form-control" required/> </div>
                                <div class="form-group">
                                    <label class="control-label">Stan</label>
                                    <input type="text" name="apartment" value="" class="form-control" /></div>
                                <div class="form-group">
                                    <label class="control-label">Grad</label>
                                    <input type="text" name="city" value="" class="form-control" required/></div>
                                <div class="form-group">
                                    <label class="control-label">Poštanski broj</label>
                                    <input type="text" name="post_number" value="" class="form-control" required/></div>
                                <div class="form-group">
                                    <label class="control-label">Telefon</label>
                                    <input type="text" name="telephone" value="" class="form-control" required  /> </div>
                                <div class="form-group">
                                    <label class="control-label">Popust</label>
                                    <select name="discount" id="discount" class="form-control">
                                        <option value="1">0 %</option>
                                        <option value="0.95">5 %</option>
                                        <option value="0.90">10 %</option>
                                        <option value="0.85">15 %</option>
                                        <option value="0.80">20 %</option>
                                    </select>
                                </div>


                                <div class="margiv-top-10">
                                    <button  type="submit" class="btn btn-success">
                                        <i class="fa fa-check"></i> Sačuvaj</button>
                                    <a href="{{ route('resellers.index') }}" class="btn default"> Nazad </a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
