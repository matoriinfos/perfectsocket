@extends('backend.main')

@section('content')
    <div class="page-content">
        <div class="row">

            {{--<div class="row">--}}
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <br>
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-present font-green"></i>
                            <span class="caption-subject font-green sbold uppercase"> Prikaz stavki porudžbine </span>
                        </div>
                        @if($order->order_status == 0)

                            <a style="float:right;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-success"
                               onclick="event.preventDefault();
                            document.getElementById('update-form').submit();">
                                <i class="glyphicon glyphicon-ok"></i>&nbsp;
                                <span class="title">Evidentiraj porudžbinu?</span>
                            </a>
                        @if(empty($order->discount))
                            <button style="float:right; margin-right: 5px;" id="dugmePopust"  class="btn btn-primary"
                               onclick="showDiscount();">
                                <i class="fa fa-gift"></i>
                                <span class="title">Dodaj popust</span>
                            </button>
                        @else
                                <button style="float:right; margin-right: 5px;" id="dugmePopust"  class="btn btn-primary"
                                        onclick="showDiscount();">
                                    <i class="fa fa-gift"></i>
                                    <span class="title">Popust {{(1- $order->discount)*100}} %</span>
                                </button>
                            @endif
                            <div style="float:right; margin-right: 5px; display: none;" id="izborPopusta" class="">
                                Izaberite popust:
                                <form role="form" id="popust-form" action="{{route('setdiscount', $order->id)}}" method="post">
                                    {{ csrf_field() }}


                            <select name="popust" id="popust" onchange=" event.preventDefault(); document.getElementById('popust-form').submit();  ">
                                <option value="0" @if($order->discount == '0') {{'selected'}} @endif>0 %</option>
                                <option value="0.95" @if($order->discount == '0.95') {{'selected'}} @endif>5 %</option>
                                <option value="0.90" @if($order->discount == '0.90') {{'selected'}} @endif>10 %</option>
                                <option value="0.85" @if($order->discount == '0.85') {{'selected'}} @endif>15 %</option>
                                <option value="0.80" @if($order->discount == '0.80') {{'selected'}} @endif>20 %</option>
                            </select>
                                </form>
                            </div>


                        @elseif($order->order_status == 1)
                            @if($order->payment_status == 0)
                                <a style="float:right; margin-left: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-success"
                                   onclick="event.preventDefault();
                                   document.getElementById('placanje').value = 2;
                                    document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-ok"></i>&nbsp;
                                    <span class="title">Potvrdi plaćanje</span>
                                </a>
                            @else
                                <a style="float:right; margin-left: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-danger"
                                   onclick="event.preventDefault();
                                   document.getElementById('placanje').value = 0;
                                  document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-remove"></i>&nbsp;
                                    <span class="title">Poništi plaćanje</span>
                                </a>
                            @endif
                            <a style="float:right;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-success"
                               onclick="event.preventDefault();
                               document.getElementById('action').value = 2;
                            document.getElementById('update-form').submit();">
                                <i class="glyphicon glyphicon-ok"></i>&nbsp;
                                <span class="title">Potvrdi slanje</span>
                            </a>

                            <a style="float:right; margin-right: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-danger"
                               onclick="event.preventDefault();
                            document.getElementById('action').value = 0;
                            document.getElementById('update-form').submit();">
                                <i class="glyphicon glyphicon-remove"></i>
                                <span class="title">Poništi evidentiranje</span>
                            </a>


                        @elseif($order->order_status == 2)
                            @if($order->payment_status == 0)
                                <a style="float:right; margin-left: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-success"
                                   onclick="event.preventDefault();
                                   document.getElementById('placanje').value = 2;
                                    document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-ok"></i>&nbsp;
                                    <span class="title">Potvrdi plaćanje</span>
                                </a>
                            @else
                                <a style="float:right; margin-left: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-danger"
                                   onclick="event.preventDefault();
                                   document.getElementById('placanje').value = 0;
                                  document.getElementById('update-form').submit();">
                                    <i class="glyphicon glyphicon-remove"></i>&nbsp;
                                    <span class="title">Poništi plaćanje</span>
                                </a>
                            @endif
                            <a style="float:right; margin-right: 5px;" href="{{ route('orders.update', $order->id) }}"  class="btn btn-danger"
                               onclick="event.preventDefault();
                            document.getElementById('action').value = 1;
                            document.getElementById('update-form').submit();">
                                <i class="glyphicon glyphicon-remove"></i>
                                <span class="title">Poništi slanje</span>
                            </a>
                        @endif
                        <form id="update-form" action="{{ route('orders.update', $order->id) }}" method="POST" style="display: none;">
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="action" id="action" value="-1">
                            <input type="hidden" name="placanje" id="placanje" value="-1">
                        </form>

                    </div>
                    <div class="portlet-body">
                        <div class="table-container">
                            <table id="order_details_table" class="display" cellspacing="0" width="100%">
                                <thead>
                                <th>#</th>
                                <th>Konfiguracija</th>
                                <th>Količina</th>
                                <th>Cena + PDV</th>
                                </thead>
                                <tbody>
                                @foreach ($orderItems as $orderItem)
                                    <tr>
                                        <td style="width: 20px;">{{ $i++ }}</td>
                                        <td style="vertical-align:middle">
                                            <?php
                                            //echo "<h4 style='padding-bottom: 10px'><i>" . $orderItem->configuration->name . "</i></h4>";
                                            //ispisi proizvode  konfiguracije
                                            $proizvodi = $orderItem->configuration->configurationItems;
                                            $redniBroj = 1;
                                            foreach ($proizvodi as $proizvod) {

                                                echo  "<h6 style='text-align: left; padding-left: 5px'>".$redniBroj . ".   <b>" . $proizvod->product->description_en ."</b>  &nbsp;&nbsp;&nbsp;    " . $proizvod->product->reference_number . "&nbsp;&nbsp;&nbsp;   " .str_replace(".",",",$proizvod->product->price)   ."   RSD  &nbsp;&nbsp;&nbsp; </h6>";
                                              //  echo "<div style='width: 30px; height: 30px; padding-left: 10px'><img src='".$proizvod->product->image_src . "' class='slika_". $proizvod->product->image. "' style='width: 100%; height: 100%' ></div><br>";
                                                $redniBroj++;

                                            }

                                            ?>

                                        </td>
                                        <td>{{$orderItem->quantity }}</td>

                                        <td>{{ number_format(($orderItem->item_price * $orderItem->quantity), 2, ',', '.')  . ' RSD' }}</td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="col-md-8"></div>
                        <div class="col-md-4">
                            @if(empty($order->discount))
                            <h3 style="font-size: 18px; padding: 10px" class="label label-success pull-right">Ukupno:&nbsp;<b>{{ number_format($order->total_price, 2, ',', '.') .' RSD' }}</b></h3>
                            @else
                                <h3 style="font-size: 18px; padding: 10px" class="label label-success pull-right">Ukupno:&nbsp;<b>{{ number_format($order->total_price, 2, ',', '.') .' RSD' }} <br>Snizeno:&nbsp;{{ number_format($order->discounted_price, 2, ',', '.') .' RSD' }} </b></h3>
                             @endif
                        </div>
                        <br><br><br>
                        <div class="col-md-5 col-sm-12">
                            <br><br>
                            <div class="caption" style="font-size: 18px; padding-left: 10px">
                                <i class="icon-user font-green"></i>&nbsp;
                                <span class="caption-subject font-green sbold uppercase"> Podaci o kupcu </span>
                            </div>
                            <br>
                            <table class="order_details table table-bordered table-responsive">
                                <thead>
                                <th>Ime i prezime</th>
                                <th>Telefon</th>
                                <th>Email</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->name }}&nbsp;{{ $order->surname }}</td>
                                    <td>{{ $order->telephone }}</td>
                                    <td>{{ $order->email }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-7 col-sm-12">
                            <br><br>
                            <div class="caption" style="font-size: 18px; padding-left: 10px">
                                <i class="icon-map font-green"></i>&nbsp;
                                <span class="caption-subject font-green sbold uppercase"> Podaci za dostavu </span>
                            </div>
                            <br>
                            <table class="order_details table table-bordered table-responsive">
                                <thead>
                                <th>Adresa</th>
                                <th>Stan</th>
                                <th>Grad</th>
                                <th>Napomena</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $order->shipping_address}}</td>
                                    <td>{{ $order->shipping_apartment }}</td>
                                    <td>{{ $order->shipping_city }}</td>
                                    <td>{{ $order->additional_note }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
<script>
    function showDiscount() {
    document.getElementById('dugmePopust').style.display="none";
    document.getElementById('izborPopusta').style.display= "block";
    }

    function dodajPopust() {
        var popust = document.getElementById('popust').value;
       $('$iznos_popusta').val(popust);
       alert(document.getElementById('iznos_popusta').value);

    }


</script>
