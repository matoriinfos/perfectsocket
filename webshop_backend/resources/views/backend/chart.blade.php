@extends('backend.main')

@section('content')

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home"></i> Početna strana </div>
                    </div>
                    <div class="portlet-body form">
                        <div style="padding-top: 20px" id="orders_chart"></div>
                        <?= Lava::render('LineChart', 'OrdersChart', 'orders_chart') ?>

                        <div style="padding-top: 20px" id="perf_div"></div>
                        <?= Lava::render('ColumnChart', 'Finances', 'perf_div') ?>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
    </div>

@endsection
