<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
</head>
<body>
     <h4>Poštovani {{$order->name.' '.$order->surname }} </h4>
    <p>Vaša porudžbina pod brojem {{$order->order_id}} je evidentirana, a oprema je na lageru!</p>
    <p>Oprema će vam biti isporučena za dva dana od trenutka prijema Vaše uplate.</p><br>
    
    <p><strong>Podaci za uplatu:</strong></p>
    <p>Naziv primaoca: PerfectSocket</p>
    <p>Račun primaoca: xxx - 000000xxxxxx - xxx</p>
    <p>Iznos: (ovde ide ukupan iznos iz ponude, uvećan za PDV)</p>
    <p>Svrha plaćanja: Kupovina opreme - {{$order->order_id}}</p><br>
    
    <p>U prilogu Vam šaljemo predračun.</p>
</body>
</html>