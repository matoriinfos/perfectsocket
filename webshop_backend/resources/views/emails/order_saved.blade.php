<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
</head>
<body>

<h4>Poštovani {{$order->name.' '.$order->surname }}, </h4>
<p>Dobili smo Vašu porudžbinu koja ima broj:<strong> {{$order->order_id}}</strong></p>
<p>Uskoro ćete dobiti email sa potvrdom Vaše porudžbine.</p>
<p>Zahvaljujemo Vam se na ukazanom poverenju i radujemo ponovnoj saradnji😊</p><br>
<h4>Srdačno, tim PerfectSocket.com</h4>


</body>
</html>