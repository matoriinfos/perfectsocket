<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as' => 'index', 'uses' => 'backend\ChartController@index']);


Auth::routes();

//user Profile
Route::get('/profile', ['as' => 'profile', 'uses' => 'backend\ProfilesController@show']);
Route::post('/updateprofile', ['as' => 'updateprofile', 'uses' => 'backend\ProfilesController@updateprofile']);
Route::post('/updatepassword', ['as' => 'updatepassword', 'uses' => 'backend\ProfilesController@updatepassword']);

// orders
Route::resource('orders', 'backend\OrderController');
Route::get('/send_email', 'backend\EmailController@send_email')->name('send_email');

//resellers
// orders
Route::resource('resellers', 'backend\ResellerController');

//dodavanje popusta
Route::post('/setdiscount/{id}',['as' => 'setdiscount', 'uses' => 'backend\PopustController@setdiscount']);

//order_items
Route::resource('order_items', 'backend\OrderItemController');
// generate bills
Route::get('/export_excel', 'backend\OrderController@export_excel')->name('Export_excel');
Route::get('/export_word', 'backend\OrderController@export_word')->name('Export_word');
Route::get('/export_pdf/{id}', 'backend\OrderController@export_pdf')->name('Export_pdf');

// statistics
Route::resource('statistics', 'backend\StatisticController');

//charts home
Route::resource('charts','backend\ChartController');

