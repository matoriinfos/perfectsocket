<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{

    public function orderItems(){
        return $this->hasMany('App\OrderItem');
    }

    public function configurationItems(){
        return $this->hasMany('App\ConfigurationItem');
    }




}
