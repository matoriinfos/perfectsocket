<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigurationItem extends Model
{
    protected $table = 'configuration_items';

    public function configuration(){
        return $this->belongsTo('App\Configuration');
    }
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
