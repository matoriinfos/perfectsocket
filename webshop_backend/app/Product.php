<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    public function configurationItems(){
        return $this->hasMany('App\ConfigurationItem');
    }
}
