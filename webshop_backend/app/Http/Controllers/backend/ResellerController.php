<?php

namespace App\Http\Controllers\backend;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ResellerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resellers = Customer::all();
     //   $orders = Order::orderBy('id', 'desc')->get();
        return view('backend.resellers')
            ->withResellers($resellers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.create_reseller');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reseller = new Customer;

        $reseller->name = $request->name;
        $reseller->surname = $request->surname;
        $reseller->email = $request->email;
        $password = sha1('Password');
        $reseller->password = $password;
        $reseller->address = $request->address;
        $reseller->apartment = $request->apartment;
        $reseller->city = $request->city;
        $reseller->post_number = $request->post_number;
        $reseller->telephone = $request->telephone;
        $reseller->account_type = 'reseller';
        $reseller->discount = $request->discount;

        $reseller->save();

        Session::flash('success_preprodavac', 'Preprodavac je uspesno sacuvan!');

        return redirect()->route('resellers.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reseller = Customer::find($id);
        return view('backend.password_reset')
            ->withReseller($reseller);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reseller = Customer::find($id);
        return view('backend.reseller_details')
            ->withReseller($reseller);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reseller = Customer::find($id);

        if ($request->pass == 'pass')
        {
            $pass = sha1($request->newpassword);
            $reseller->password = $pass;

            $reseller->save();
            Session::flash('password_reset', 'Lozinka za korisnika (' . $reseller->name . ' ' . $reseller->surname . ') je resetovana!');
            return redirect()->route('resellers.edit', $id);
        }

        if ($request->action == 'update')
        {
            $reseller->name = $request->name;
            $reseller->surname = $request->surname;
            $reseller->email = $request->email;
            $reseller->address = $request->address;
            $reseller->apartment = $request->apartment;
            $reseller->city = $request->city;
            $reseller->post_number = $request->post_number;
            $reseller->telephone = $request->telephone;
            $reseller->account_type = $request->account_type;

            if($reseller->account_type == 'reseller')
            $reseller->discount = $request->discount;
            else
            $reseller->discount = 1;
            $reseller->save();
            Session::flash('reseller_changed', 'Podaci za nalog (' . $reseller->name . ' ' . $reseller->surname . ') su azurirani!');
        }

        if($request->account) {

            $account_type = $request->account;
            if ($account_type == 'reseller') {
                Customer::where('id', $id)->update(['account_type' => 'reseller']);
                Session::flash('account_type_changed', 'Korisniku (' . $reseller->name . ' ' . $reseller->surname . ') je potvrdjen status preprodavca!');
            } elseif ($account_type == 'user') {
                Customer::where('id', $id)->update(['account_type' => 'user','discount' => '1']);
                Session::flash('account_type_changed', 'Korisniku (' . $reseller->name . ' ' . $reseller->surname . ') je ponisten status preprodavca!');
            }
            echo $request->account_type;
        }


        return redirect()->route('resellers.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reseller = Customer::find($id);
        $reseller->delete();
        Session::flash('success_delete', 'Preprodavac je uspesno obrisan!');
        return redirect()->route('resellers.index');
    }
}
