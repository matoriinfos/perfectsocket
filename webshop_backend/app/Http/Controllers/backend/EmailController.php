<?php

namespace App\Http\Controllers\backend;

use App\Mail\NewOrder;
use App\Mail\OrderConfirm;
use App\Mail\OrderSaved;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use Mail;
use Session;
use PDF;

class EmailController extends Controller
{
    public function send_email(Request $request){
        $order_id =  $request->order_id;
        $order = Order::where('order_id',$order_id)->first();

        Mail::to($order->email)->send(new OrderSaved($order));
        Mail::to('perfect.socket@gmail.com')->send(new NewOrder($order));

        return 1;
    }
}
