<?php

namespace App\Http\Controllers\backend;

use App\OrderItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Configuration;
use Session;

class StatisticController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $serija = $request->serija;
        $from_date = new \DateTime($from);
        $to_date = new \DateTime($to);
        $server = 'http://localhost/perfectsocketgitlab/webshop/';
        if ($to_date < $from_date){
            Session::flash('date_format_incorrect', 'Vremenski period nije validan!');
            return redirect()->route('statistics.create');
        }

        //Experience
        if ($serija == 1) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_experience')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'EXPERIENCE';
          $putanja = $server. 'wp-content/themes/momentous-lite/experience/slike/';
          //dd($izbor_korisnika);
        }
        //interio
        if ($serija == 2) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_interio')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'INTERIO';
          $putanja = $server. 'wp-content/themes/momentous-lite/interio/slike/';
          //dd($izbor_korisnika);
        }
        //primera
        if ($serija == 3) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_primera')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'PRIMERA';
          $putanja = $server. 'wp-content/themes/momentous-lite/primera/slike/';
          //dd($izbor_korisnika);
        }
        //primera kompleti
        if ($serija == 4) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_primera_komplet')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'PRIMERA kompleti';
          $putanja = $server. 'wp-content/themes/momentous-lite/primera/slike/';
          //dd($izbor_korisnika);
        }
        //livinglight
        if ($serija == 5) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_livinglight')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'LIVING LIGHT';
          $putanja = $server. 'wp-content/themes/momentous-lite/livinglight_3/slike/';
          //dd($izbor_korisnika);
        }
        if ($serija == 6) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_livinglight_air')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'LIVING LIGHT AIR';
          $putanja = $server. 'wp-content/themes/momentous-lite/livinglight_air/slike/';
          //dd($izbor_korisnika);
        }

        //niloe
        if ($serija == 7) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_niloe')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'NILOE';
          $putanja = $server. 'wp-content/themes/momentous-lite/niloe/slike/';
          //dd($izbor_korisnika);
        }
        //niloe
        if ($serija == 8) {
          $izbor_korisnika = DB::table('ws_izbor_korisnika_visio')->whereBetween('created_at', array($from, $to))->distinct()->get();
          $serija = 'VISIO';
          $putanja = $server. 'wp-content/themes/momentous-lite/visio/slike/';
          //dd($izbor_korisnika);
        }

        // $number_of_orders = Order::whereBetween('updated_at', array($from, $to))->count();
        // if ($number_of_orders ==0){
        //     Session::flash('no_orders', 'Nema evidentiranih narudžbina za odabrani period!');
        //     return redirect()->route('statistics.create');
        // }
        // $naplaceno= Order::whereBetween('updated_at', array($from, $to))->where('payment_status',2)->where('discounted_price', 0)->sum('total_price');
        // $naplaceno_p= Order::whereBetween('updated_at', array($from, $to))->where('payment_status',2)->where('discounted_price','>', 0)->sum('discounted_price');
        // $salary = $naplaceno + $naplaceno_p;
        // //$items = OrderItem::whereBetween('date', array($from, $to))->groupBy('configuration_id')->orderByRaw('COUNT(configuration_id) DESC')->limit(1)->get();
        // $conf_id = DB::table('order_items')
        //     ->select(DB::raw('count(configuration_id) as zbir, configuration_id'))
        //     ->whereBetween('date', array($from, $to))
        //     ->groupBy('configuration_id')
        //     ->orderByRaw('count(configuration_id) DESC')
        //     ->limit(1)
        //     ->get();
        // $items = OrderItem::where('configuration_id', $conf_id[0]->configuration_id)->limit(1)->get();
        $izbor_korisnika = json_decode($izbor_korisnika, true);
        //dd($izbor_korisnika[0]['id_izbor']);
        return view('backend.statistics_details')
            ->withFrom($from)
            ->withTo($to)
            ->withIzborKorisnika($izbor_korisnika)
            ->withSerija($serija)
            ->withPutanja($putanja);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.statistics');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
