<?php

namespace App\Http\Controllers\backend;

use App\Mail\OrderConfirm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use Mail;
use Session;
use PDF;

class ChartController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // grafik za narudzbine tekuce godine
        $orders_chart = \Lava::DataTable();
        $orders_chart->addStringColumn('Datum')
            ->addNumberColumn('Isporučeno')
            ->addNumberColumn('Odobreno')
            ->addNumberColumn('Na čekanju');
        $year = date('Y');
        for ( $i = 1;$i<13;$i++){
            $isporuceno= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('order_status',2)->count();
            $odobreno= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('order_status',1)->count();
            $naCekanju= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=',$i)->where('order_status',0)->count();
            $orders_chart->addRow([$i.'-'.$year,  $isporuceno, $odobreno, $naCekanju]);
        }

        \Lava::LineChart('OrdersChart', $orders_chart, [
            'title' => 'Pregled statusa narudžbina tekuće godine',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);

        //grafik za zaradu u tekucoj godini
        $finances = \Lava::DataTable();

        $finances->addStringColumn('Mesec')
            ->addNumberColumn('Naplaćeno u rsd')
            ->addNumberColumn('Naručeno u rsd')
            ->setDateTimeFormat('M');
        for ( $i = 1;$i<13;$i++){
            $isporuceno= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('payment_status',2)->where('discounted_price', 0)->sum('total_price');
            $isporuceno_p= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('payment_status',2)->where('discounted_price','>', 0)->sum('discounted_price');
            $ukupno_isporuceno = $isporuceno + $isporuceno_p;
            $odobreno= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('payment_status',0)->where('discounted_price', 0)->sum('total_price');
            $odobreno_p= Order::whereYear('updated_at', '=', date('Y'))->whereMonth('updated_at', '=', $i)->where('payment_status',0)->where('discounted_price','>', 0)->sum('discounted_price');
            $ukupno_odobreno = $odobreno + $odobreno_p;
            $finances->addRow([$i.'-'.$year,  $ukupno_isporuceno, $ukupno_odobreno]);
        }

        \Lava::ColumnChart('Finances', $finances, [
            'title' => 'Pregled zarade tekuće godine',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);


        return view('backend.chart');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
