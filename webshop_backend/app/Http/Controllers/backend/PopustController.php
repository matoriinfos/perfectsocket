<?php

namespace App\Http\Controllers\backend;

use App\Mail\OrderConfirm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;
use Mail;
use Session;
use PDF;
class PopustController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setdiscount(Request $request, $id)
    {
            $order = Order::find($id);

            $discounted_price = $order->total_price * $request->popust;
            $discounted_price = number_format($discounted_price, 2, '.', '');
            Order::where('id', $id)->update(['discount' =>$request->popust , 'discounted_price' => $discounted_price]);
            Session::flash('order_status_changed', 'Popust je uspesno dodat!');


        return redirect()->route('orders.edit',$id);
    }
}
