<?php


require_once 'controller/ProfileController.php';
$profileC = new ProfileController();


if ($_SERVER['REQUEST_METHOD']== 'POST') {


        $pc = new ProfileController();
        $pc->updateProfile();
        $errors = $_SESSION['greskee'];
        $uspesno = $_SESSION['uspesno'];

        if(empty($_SESSION['greskee']))
            $greske = array();
        else
        {
            $greske = $_SESSION['greskee'];
            unset($_SESSION['greskee']);
        }

        if(empty($_SESSION['uspesno']))
            $uspesno = "";
        else
        {
            $uspesno = $_SESSION['uspesno'];
            unset($_SESSION['uspesno']);
        }

}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>My Profile</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" type="text/css" href="css/registracija.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$prava = $_SESSION['prava'];
if(empty($prava))
{

}
else {
    ?>

    <div id="registracija">
        <?php
        //prikaz gresaka

        if (!empty($greske)) {
            echo "<h1 class='text-center' style='color: red'>Error</h1>
    <p style='color: red'>The following errors occur: <br />";
            foreach ($greske as $msg)  //stampanje svake greske
            {
                echo "*  $msg <br />\n";
            }

        }
        if (!empty($uspesno)) {
            echo "<h1 class='text-center'>Data successfully updated!</h1>";
        }
        $id_korisnika = $_SESSION['id_korisnika'];
        $korisnik = $profileC->getUser($id_korisnika);
        $discount = (1 - $korisnik['discount']) * 100;
        ?>

        <h3 class="text-center" style="color: #15998c">Profile type: <?php echo $korisnik['account_type']; ?></h3>
        <?php if ($korisnik['account_type'] == 'reseller') echo "<h5 class='text-center'>(you have a " . $discount . "% discount)</h5>" ?>
        <form action="MyProfile.php" method="post">
            <p>Name: <br>
                <input type="text" id="ime" name="ime" size="15" maxlength="20"
                       value="<?php if (isset($korisnik['name'])) echo $korisnik['name']; ?>" disabled>
            </p>
            <p>Surname:<br>
                <input type="text" id="prezime" name="prezime" size="15" maxlength="40"
                       value="<?php if (isset($korisnik['surname'])) echo $korisnik['surname']; ?>" disabled>
            </p>
            <p>Address:<br>
                <input type="text" id="adresa" name="adresa" size="20" maxlength="40"
                       value="<?php if (isset($korisnik['address'])) echo $korisnik['address']; ?>" disabled>
            </p>
            <p>City:<br>
                <input type="text" id="grad" name="grad" size="20" maxlength="40"
                       value="<?php if (isset($korisnik['city'])) echo $korisnik['city']; ?>" disabled>
            </p>
            <p>Telephone:<br>
                <input type="text" id="telefon" name="telefon" size="20" maxlength="40"
                       value="<?php if (isset($korisnik['telephone'])) echo $korisnik['telephone']; ?>" disabled>
            </p>
            <input type="hidden" name="id" value="<?php echo $id_korisnika ?>"/>
            <p>
                <input type="button" id="edit_data" value="Edit data"/></p>

            </p>
            <p>
            <p><input type="button" id="change_password" onclick="location.href='./ChangePassword.php';"
                      value="Change password "/></p>
            </p>

            <p><input class="dugme_potvrdi" id="potvrdi" style="display: none" type="submit" name="submit"
                      value="Confirm"/></p>
            <p><input type="button" id="odustani" style="display: none; background-color: #f57e63"
                      onclick="location.href='./MyProfile.php';" value="Cancel"/></p>
        </form>

    </div>


    <?php
}/*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script>
    $('#edit_data').click(function (e) {
        e.preventDefault();
        $('#potvrdi').css("display","block");
        $('#odustani').css("display","block");
        $('#edit_data').css("display", "none");
        $('#change_password').css("display", "none");
        $('#ime').removeAttr("disabled");
        $('#prezime').removeAttr("disabled");
        $('#adresa').removeAttr("disabled");
        $('#grad').removeAttr("disabled");
        $('#telefon').removeAttr("disabled");


    })

</script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>