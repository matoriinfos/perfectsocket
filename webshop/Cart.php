<?php

require_once ('controller/CartContentController.php');

$cartContentC = new CartContentController();
$stavke = array();


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>Cart</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/cart.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
if(!empty($_SESSION['discount']))
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2 col-md-offset-5 text-center">
                    <h3 style="color: red; font-size: 24px">
                        Moja korpa
                    </h3>
					<br>
                </div>
            </div>
            <div class="row">
                <form action="controller/CartContent.php" method="post">


                    <div class="portlet-body">
                        <div class="table-container table-responsive">
                            <table id="collections_table" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr class="myTable" style="text-align:center; font-size: 14px;">

                                    <th style="text-align: center">Proizvodi </th>
                                    <th style="text-align: center width: 80px;">Slika </th>
                                    <th style="text-align: center" >Cena</th>
                                    <th style="text-align: center; width: 120px;">Količina</th>
                                    <th style="text-align: center">Ukupno</th>
                                    <!-- <th style="text-align: center">Ukupno + PDV (20%)</th> -->
                                    <th style="text-align: center">Ukloni</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $id_korisnika = $_SESSION['id_korisnika'];
                                $stavke=$cartContentC->getCartContent($id_korisnika);
                                $ukupno = 0;
								/*dodao sale*/
								$ukupno_bez_pdva = 0;
                                foreach ($stavke as $stavka) {

                                    $cena_stavke = 0;
                                          if($_SESSION['prava']=='reseller')
                                              {
                                                  $cena_stavke = $stavka['cena'] * $discount * $stavka['quantity'];
                                              }
                                              else
                                                 $cena_stavke = $stavka['cena'] * $stavka['quantity'];
                                    ?>


                                    <tr class="myTable" style="text-align:center;">


                                        <td style="vertical-align:middle">
                                            <?php
                                            echo "<h4 style='padding-bottom: 10px'><i>" . $stavka['naziv'] . "</i></h4>";
                                            //ispisi proizvode  konfiguracije
                                            $proizvodi = $cartContentC->getConfigurationProducts($stavka['configuration_id']);
                                            $redniBroj = 1;
                                            foreach ($proizvodi as $proizvod) {

                                                       echo  "<h6 style='text-align: left; padding-left: 5px'>".$redniBroj . ".   <b>" . $proizvod['description_en'] ."</b>  &nbsp;&nbsp;&nbsp;    " . $proizvod['reference_number'] . "&nbsp;&nbsp;&nbsp;   " . $proizvod['price'] ."   RSD  &nbsp;&nbsp;&nbsp; </h6>";
                                                        $redniBroj++;

                                            }

                                            ?>
                                        </td>
                                        <td style="vertical-align:middle">
                                            <?php
                                            $proizvodi = $cartContentC->getConfigurationProducts($stavka['configuration_id']);
                                            foreach ($proizvodi as $proizvod) {

                                                echo "<div style='width: 30px; height: 30px; padding-left: 10px; padding-top: 5px;padding-bottom: 5px;'><img src='".$proizvod['image_src'] . "' class='slika_". $proizvod['image']. "' style='width: 100%; height: 100%' ></div>";


                                            }

                                            ?>
                                        </td>


                                        <td>
                                            <?php
                                            if($_SESSION['prava']=='reseller')
                                            {
                                                echo "<h4><s>" . number_format($stavka['cena'], 2, ',', ' ') . " RSD</s></h4>";
                                                //cena za reseller
                                                echo "<h4>" . number_format($stavka['cena'] * $discount, 2, ',', ' ') . " RSD</h4>";

                                            }
                                            else
                                            echo "<h4>" . number_format($stavka['cena'], 2, ',', ' '). " RSD</h4>";
											
                                            ?>
                                        </td>
                                        <td>
                                            <div>
                                            <span>
                                               <button type="button" class=" btn-number" style="background-color: #ff4532" data-type="minus"
                                                       data-field="quant<?php echo $stavka['id_korpe'] ?>">
                                             <span class="glyphicon glyphicon-minus"></span>
                                                 </button>
                                           </span>
                                                <input type="text" name="quant<?php echo $stavka['id_korpe'] ?>"
                                                       class="input-number text-center"
                                                       value="<?php echo $stavka['quantity'] ?>" min="1" max="10000" size="6" >
                                                <span >
                                                  <button type="button" class=" btn-number" style="background-color: #0ec51b"
                                                          data-type="plus"
                                                          data-field="quant<?php echo $stavka['id_korpe'] ?>">
                                               <span class="glyphicon glyphicon-plus"></span>
                                                 </button>
                                               </span>
                                            </div>
                                        </td>
                                        <td>
										<div class='total'><h4>
                                            <?php echo number_format($cena_stavke, 2, ',', ' '). " RSD";?>
										</h4></div>
                                        </td>
                                      <!--   <td> -->
                                            <?php
                                            $cena_stavke_tax = $cena_stavke * 1.2;
											/*uklonio sale html ispod*/
                                           // echo "<div class='total'><h4>".  number_format($cena_stavke_tax, 2, '.', ' ') . " RSD</h4></div>";
                                            ?>

                                      <!--  </td> -->
                                        <td>
                                            <a href="controller/CartContent.php?action=ukloni&id=<?php echo $stavka['id_korpe']?>" style="color: red;"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>

                                    <?php
                                    $ukupno = $ukupno + $cena_stavke_tax;
									/*dodao sale*/
									$ukupno_bez_pdva = $ukupno_bez_pdva + $cena_stavke;
                                }
                                ?>

                                </tbody>
                            </table>
                            <button type="submit" class="azuriraj_tabelu" id="enableDisable" style="display: none; margin-top: 5px; margin-bottom: 5px; float: right">
                                <b>
                                    Ažuriraj
                                </b>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div id="kupovina" class="col-md-4 col-md-offset-4 text-center" style="padding-top: 0px;<?php if(empty($stavke)) echo "display:none";?>">
                    <br>

                    <div style="font-size: 20px; padding-bottom: 10px;">
                        Ukupno: <td><strong><?php echo number_format($ukupno_bez_pdva, 2, ',', ' ').' RSD'; ?></strong></td>
                    </div>
					<div style="font-size: 20px;">
                        Ukupno + PDV: <td><strong><?php echo number_format($ukupno, 2, ',', ' ').' RSD'; ?></strong></td>
                    </div>

                    <br>
                    <button onclick="location.href='Checkout.php'" type="button" class="btn btn-primary">
                        <b>
                            Idi na kasu
                        </b>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>









<?php /*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>

<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>

<script>
    $('.btn-number').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
        $('.total').text("");
        $('.azuriraj_tabelu').css("display","block");
        $('#kupovina').css("display", "none");
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>
</body>
</html>