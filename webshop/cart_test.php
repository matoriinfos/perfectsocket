<?php

require_once 'controller/CartContentController.php';

$cartContentC = new CartContentController();
$stavke = array();


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>Cart</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" type="text/css" href="css/cart.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php'); ?>

<div id="cart">
    <?php

    $id_korisnika = $_SESSION['id_korisnika'];
    $stavke=$cartContentC->getCartContent($id_korisnika);
    if(count($stavke) > 0)
    {
        $bg='#eeeee';
        $ukupno = 0;
        echo "<h1>Cart:</h1>";


        echo '<table align="center" style="margin-left:10%" cellspacing="0" cellpadding="2" width="80%">
	  <tr style="color:#0000FF;" >';
        echo "<td><b>Product</b></td>
	  <td><b>Price</a></b></td>
	   <td><b>Quantity</b></td>
	   <td><b>Total</b></td>
	  </tr>";
        foreach ($stavke as $stavka) {
            $cena_stavke = 0;
            $cena_stavke = $stavka['cena'] * $stavka['quantity'];
            /* echo $stavka['id_korpe'] . " ,";
             echo $stavka['configuration_id'] . " ,";
             echo $stavka['naziv'] . " ,";
             echo $stavka['cena'] . " ,";
             echo $stavka['quantity'] . " ,";
             echo "<hr><br>"; */
            $bg=($bg=='#eeeee' ? '#fffff':'#eeeee');
            echo '<tr bgcolor="' . $bg . '">
		  <td style="padding-top:1%">
		  	  <p>' . $stavka['naziv'] . 	'</p>
				</td>
		   <td>' . number_format($stavka['cena'], 2, '.', ' ') .' EUR</td>
		  <td>' . $stavka['quantity'] . '</td>
		   <td>' . number_format($cena_stavke, 2, '.', ' ') .' EUR</td>
		    <td><a href="proizvod.php?pid=' . $stavka['id_korpe'] . '">Edit</a></td>
		   <td><a href="../controller/Korpa.php?action=ukloni&id=' . $stavka['id_korpe'] . '&k=' . $stavka['quantity'] . '&p=' . $stavka['id_korpe'] . '">Remove</a></td>
		  </tr>';
            $ukupno=$ukupno+ $cena_stavke;
        }
        echo '</br></br></br></br><tr><td></td>
	        <td></td>
	        <td></td>
        	<td></td>
        	<td></td>
        	<td style="font-size:18px"><p>TOTAL:  '. number_format($ukupno, 2, '.', ' ') .' EUR </p><p>'; ?>
        <input class="placanje" type="button" onclick="location.href='./placanje.php';" value="Placanje" />
        <?php
        echo '</p> </td>
	<td></tr>';
        echo '</table>';
    }
    else
        echo "prazna korpa";
    ?>
</div>









<?php /*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>