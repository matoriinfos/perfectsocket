<?php

require_once ('controller/CartContentController.php');

$cartContentC = new CartContentController();
$stavke = array();


?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>Cart</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/cart.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$id_korisnika = $_SESSION['id_korisnika'];
$stavke=$cartContentC->getCartContent($id_korisnika);
if(!empty($_SESSION['discount']))
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
if(empty($stavke))
{
   ?>
<!-- <div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

            <div class="row centered">
    <p><h3><a href="http://localhost/perfectsocket/"> <i class="fas fa-home"></i><span>Return to home page</span></a></h3></p>
            </div></div></div></div> -->
    <?php
    echo "<script>window.top.location='".server ."'</script>";
}
else {
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="row">
                    <form method="POST" id="form" action="controller/CheckoutController.php">
                        <div class="row text-center" style="padding-bottom: 10px;">
                            <h3 style="color: red; margin-left: 20px; font-size: 24px">
                                Informacije o dostavi
                            </h3>
                        </div>
                        <?php
                        //ako je ulogovani korisnik popuni polja odredjenim podacima
                        if ($_SESSION['prava'] == 'user' || $_SESSION['prava'] == 'reseller') {
                            $user = $cartContentC->getUser($id_korisnika);

                            ?>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="name" placeholder="Ime*"
                                       value="<?php echo $user['name'] ?>" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="surname" placeholder="Prezime*"
                                       value="<?php echo $user['surname'] ?>" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="email" placeholder="Kontakt e-mail*"
                                       value="<?php echo $user['email'] ?>" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="telephone" placeholder="Telephone*"
                                       value="<?php echo $user['telephone'] ?>" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="address" placeholder="Street and number*"
                                       value="<?php echo $user['address'] ?>"
                                       required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="apartment"
                                       value="<?php echo $user['apartment'] ?>" placeholder="Apartment">
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="city" placeholder="City / Village*"
                                       value="<?php echo $user['city'] ?>" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="post_number" placeholder="Post number*"
                                       value="<?php echo $user['post_number'] ?>"
                                       required>
                            </div>
                            <div class="col-md-12" style="padding-bottom: 60px;">
                            <textarea style="width: 100%; font-size: 15px; padding-left: 10px; border-radius: 5px"
                                      class="hokus" name="additional_note" cols="" rows="5"
                                      placeholder="Additional note"></textarea>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="name" placeholder="Ime*" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="surname" placeholder="Prezime*" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="email" placeholder="Kontakt e-mail*" required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="telephone" placeholder="Kontakt telefon*"
                                       required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="address" placeholder="Ulica i broj za dostavu*"
                                       required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="apartment" placeholder="Broj stana za dostavu">
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="city" placeholder="Grad*"
                                       required>
                            </div>
                            <div class="col-md-6" style="padding-bottom: 20px;">
                                <input type="text" class="form-control" name="post_number" placeholder="Poštanski broj*"
                                       required>
                            </div>
                            <div class="col-md-12">
                            <!-- <input type="text" class="form-control" name="additional_note" placeholder="Dodatna napomena*"> -->
                           <textarea rows="2" cols="50" style="width: 100%; min-height: 100px; padding-left: 10px; border:1px solid #cccccc; border-radius: 5px; opacity: 0.5;"
						   name="additional_note" placeholder="Dodatna napomena"></textarea>
                                <p style="margin-top: 5px; font-size: 15px; margin-bottom: 0px;">** rok isporuke je dva dana od dana kada potvrdimo vašu porudžbinu</p>
                            </div>
                        <?php } ?>

                </div>
                <div class="row" style="margin-top: 0px;">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <h3 style="color: red; font-size: 24px">Moja porudžbina</h3>
						<br>
                    </div>
                </div>
				
                <div class="row">
                    <div class="portlet-body">
                        <div class="table-container table-responsive">
                            <table id="collections_table" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr class="myTable" style="font-size: 14px;">
                                    <th style="text-align: center">Proizvodi</th>
                                    <th style="text-align: center">Slika </th>
                                    <th style="text-align: center">Cena</th>
                                    <th style="text-align: center">Količina</th>
                                    <th style="text-align: center">Ukupno</th>
                                    <!-- <th style="text-align: center">Ukupno + Pdv(20%)</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $ukupno = 0;
								$ukupno_bez_pdva = 0;
                                foreach ($stavke as $stavka) {

                                    $cena_stavke = 0;
                                    if($_SESSION['prava']=='reseller')
                                    {
                                        $cena_stavke = $stavka['cena'] * $discount * $stavka['quantity'];
                                    }
                                    else
                                        $cena_stavke = $stavka['cena'] * $stavka['quantity'];
                                    ?>


                                    <tr class="myTable" style="text-align:center;">

                                        <td style="vertical-align:middle">
                                            <?php
                                            echo "<h4 style='padding-bottom: 10px'><i>" . $stavka['naziv'] . "</i></h4>";
                                            //ispisi proizvode  konfiguracije
                                            $proizvodi = $cartContentC->getConfigurationProducts($stavka['configuration_id']);
                                            $redniBroj = 1;
                                            foreach ($proizvodi as $proizvod) {

                                                echo  "<h6 style='text-align: left; padding-left: 5px'>".$redniBroj . ".   <b>" . $proizvod['description_en'] ."</b>  &nbsp;&nbsp;&nbsp;    " . $proizvod['reference_number'] . "&nbsp;&nbsp;&nbsp;   " . $proizvod['price'] ."   RSD  &nbsp;&nbsp;&nbsp; </h6>";
                                                $redniBroj++;

                                            }

                                            ?>
                                        </td>
                                        <td style="vertical-align:middle">
                                            <?php
                                            $proizvodi = $cartContentC->getConfigurationProducts($stavka['configuration_id']);
                                            foreach ($proizvodi as $proizvod) {

                                                echo "<div style='width: 30px; height: 30px; padding-left: 10px; padding-top: 5px;padding-bottom: 5px;'><img src='".$proizvod['image_src'] . "' class='slika_". $proizvod['image']. "' style='width: 100%; height: 100%' ></div>";
                                            }

                                            ?>
                                        </td>


                                        <td>
                                            <?php
                                            if($_SESSION['prava']=='reseller')
                                            {
                                                echo "<h4><s>" . number_format($stavka['cena'], 2, ',', ' ') . " RSD</s></h4>";
                                                //cena za reseller
                                                echo "<h4>" . number_format($stavka['cena'] * $discount, 2, ',', ' ') . " RSD</h4>";

                                            }
                                            else
                                            echo "<h4>" . number_format($stavka['cena'], 2, ',', ' ') . " RSD</h4>";
                                            ?>
                                        </td>
                                        <td>
                                            <div>
                                                <?php echo "<h4>" . $stavka['quantity'] . "</h4>"; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php
                                            echo "<div class='total'><h4>" . number_format($cena_stavke, 2, ',', ' ') . " RSD</h4></div>";
                                            ?>

                                        </td>
                                     <!--   <td> -->
                                            <?php 
                                            $cena_stavke_tax = $cena_stavke * 1.2;
											/*
                                            echo "<div class='total'><h4>" . number_format($cena_stavke_tax, 2, '.', ' ') . " RSD</h4></div>";
											*/
                                            ?>

                                       <!-- </td> -->
                                    </tr>

                                    <?php
                                    $ukupno = $ukupno + $cena_stavke_tax;
									/*dodao sale*/
									$ukupno_bez_pdva = $ukupno_bez_pdva + $cena_stavke;
                                }
                                echo '<input type="hidden" name="total_price" value="' . $ukupno . '"/>';
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
				
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center" style="padding-top: 0px;">
                        <br>
                        <div style="font-size: 20px; padding-bottom: 0px;">
                        Ukupno: <td><strong><?php echo number_format($ukupno_bez_pdva, 2, ',', ' ') . " RSD"; ?></strong></td>
                        </div>
						<div style="font-size: 20px;">
                        Ukupno + PDV: <td><strong><?php echo number_format($ukupno, 2, ',', ' ').' RSD'; ?></strong></td>
						</div>
                        <button type="submit" class="btn btn-success rounding" name="enableDisable" id="enableDisable"
                                style="width: 85%; margin-bottom: 5px; margin-top: 20px; font-size: 16px">
                            <b>
                                ZAVRŠI KUPOVINU
                            </b>
                        </button>
                    </div>
                </div>
				
                </form>
            </div>
        </div>
    </div>


    <?php
}
/*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>

<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>


</body>
</html>