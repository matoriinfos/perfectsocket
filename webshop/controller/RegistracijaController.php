<?php
require_once('db_config.php');
class RegistracijaController {

    function registracija() {

        if(empty($_SESSION['greskee']))
            $greske = array();
        else
        {
            $greske = $_SESSION['greskee'];
            unset($_SESSION['greskee']);
        }

        if(empty($_SESSION['uspesno']))
            $uspesno = "";
        else
        {
            $uspesno = $_SESSION['uspesno'];
            unset($_SESSION['uspesno']);
        }



        include '../Registracija.php';

    }



    function registrujKorisnika() {


        $greske = array(); //inicijalizacija niza za greske
        $uspesno = "";
        //provera imena
        if(empty($_POST['ime']))
            $greske[]='Name missing.';
        else
            $ime=trim($_POST['ime']);

        //provera prezimena:
        if(empty($_POST['prezime']))
            $greske[]='Surname missing.';
        else
            $prezime=trim($_POST['prezime']);

        //provera adrese:
        if(empty($_POST['adresa']))
            $greske[]='Address missing.';
        else
            $adresa=trim($_POST['adresa']);

        //provera grada:
        if(empty($_POST['grad']))
            $greske[]='City missing.';
        else
            $grad=trim($_POST['grad']);

        //provera telefona:
        if(empty($_POST['telefon']))
            $greske[]='Phone number missing.';
        else
            $telefon=trim($_POST['telefon']);

// provera e_mail adrese
        if(empty($_POST['email']))
            $greske[]='Email address missing.';
        else
            $email=trim($_POST['email']);

// provera lozinke i slaganje sa potvrdom lozinke
        if(!empty($_POST['lozinka1']))
        {
            if($_POST['lozinka1'] != $_POST['lozinka2'])
            {
                $greske[]='Passwords mismatch.';
            }
            else {
                $lozinka = trim($_POST['lozinka1']);
                $lozinka = sha1($lozinka);
            }
        }
        else
            $greske[]='Password missing.';

        if(empty($greske)) //ako je sve u redu
        {
// registrovanje korisnika u bazu podataka

            // KOD ZA BAZU NA NETU
            $servername = servername;
            $username = username;
            $password = password;
            $baza = baza;

            // POVEZIVANJE NA BAZU
            $conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            //provera da li vec postoji korisnik sa datom email adresom
            try {
                $stmt1 = $conn->prepare("SELECT * FROM customers WHERE email='$email'");
                $stmt1->execute();
                $postojeci_email=$stmt1->fetch(PDO::FETCH_ASSOC);
            }
            catch(PDOException $e)
            {
                echo "Error: " . $e->getMessage();
            }

            if($postojeci_email)
                $greske[]='Account with the given email address already exists';
            else {

                try {
                    $sql = "INSERT INTO customers (name, surname, email, password, address, city, telephone, account_type) VALUES('$ime','$prezime','$email','$lozinka','$adresa','$grad','$telefon','user')";
                    $conn->exec($sql);
                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }


                //   $greske = [];
                $uspesno = "<h1>Registration successful. Welcome!</h1>";
            }
        }
        $_SESSION['greskee']= $greske;
        $_SESSION['uspesno']= $uspesno;

    }
}
?>