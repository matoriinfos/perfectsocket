<?php
require 'db_config.php';


class ProfileController
{

    function updateProfile()
    {


        $greske = array(); //inicijalizacija niza za greske
        $uspesno = "";
        //provera imena
        if (empty($_POST['ime']))
            $greske[] = 'Name missing.';
        else
            $ime = trim($_POST['ime']);

        //provera prezimena:
        if (empty($_POST['prezime']))
            $greske[] = 'Surname missing.';
        else
            $prezime = trim($_POST['prezime']);

        //provera adrese:
        if (empty($_POST['adresa']))
            $greske[] = 'Address missing.';
        else
            $adresa = trim($_POST['adresa']);

        //provera grada:
        if (empty($_POST['grad']))
            $greske[] = 'City missing.';
        else
            $grad = trim($_POST['grad']);

        //provera telefona:
        if (empty($_POST['telefon']))
            $greske[] = 'Phone number missing.';
        else
            $telefon = trim($_POST['telefon']);


        if (empty($greske)) //ako je sve u redu
        {
// azuriranje korisnika u bazu podataka

            // KOD ZA BAZU NA NETU
            $servername = servername;
            $username = username;
            $password = password;
            $baza = baza;

            // POVEZIVANJE NA BAZU
            $conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            //id_korisnika
            $id_korisnika = trim($_POST['id']);

            try {
                $sql = "UPDATE customers set name ='" . $ime . "', surname='" . $prezime . "',address='" . $adresa . "', city='" . $grad . "', telephone= '" . $telefon . "' where id = $id_korisnika";
                $conn->exec($sql);
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }


            //   $greske = [];
            $uspesno = "<h1>Data successfully updated!</h1>";
        }

        $_SESSION['greskee'] = $greske;
        $_SESSION['uspesno'] = $uspesno;

    }

    function updatePassword()
    {

        $errors = array(); //inicijalizacija niza za greske

        //provera postojece sifre
        if (empty($_POST['pass']))
            $errors[] = "Current password missing!";
        else
            $p = trim($_POST['pass']);

        //provera nove i ponovljenje sifre
        if (!empty($_POST['pass1'])) {
            if ($_POST['pass1'] != $_POST['pass2'])
                $errors[] = "Password mismatch!";
            else
                $np =  trim($_POST['pass1']);
        } else
            $errors[] = "New password missing!";


        if (empty($errors)) //ako je sve u redu
        {
// azuriranje korisnika u bazu podataka

            // KOD ZA BAZU NA NETU
            $servername = servername;
            $username = username;
            $password = password;
            $baza = baza;

            // POVEZIVANJE NA BAZU
            $conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

            //id_korisnika
            $id_korisnika = trim($_POST['id']);

            //provera dali je uneta ispravna stara lozinka

            try {
                $stmt1 = $conn->prepare("SELECT * FROM customers WHERE id= '$id_korisnika' and password = sha1('$p')");
                $stmt1->execute();
                $korisnik = $stmt1->fetch(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                echo "Error: " . $e->getMessage();
            }

            if (!empty($korisnik)) {

                //pravimo upit UPDATE
                try {
                    $newp = sha1($np);
                    $stmt1 = $conn->prepare("update customers set password = '$newp' where id = '$id_korisnika' ");
                    $stmt1->execute();
                    $uspesno = "<h1>Password changed successfully!!</h1>";
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }

            } else
                $errors[] = "Wrong password entered!";



        }
        return $errors;
    }

    function getUser($id_korisnika)
    {

        //stavke korpe za datog korisnika
        // KOD ZA BAZU NA NETU
        $servername = servername;
        $username = username;
        $password = password;
        $baza = baza;

        // POVEZIVANJE NA BAZU
        $conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

        try {
            $stmt1 = $conn->prepare("select * from customers  where id='$id_korisnika'");
            $stmt1->execute();
            $user = $stmt1->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return $user;


    }


}

?>