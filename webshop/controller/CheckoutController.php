<?php
require_once('../db_config.php');
require_once ('../controller/CartContentController.php');
require ('phpmailer/PHPMailerAutoload.php');
session_start();
$greske = array(); //inicijalizacija niza za greske

// KOD ZA BAZU NA NETU
$servername = servername;
$username = username;
$password = password;
$baza = baza;
// POVEZIVANJE NA BAZU
$conn = new PDO("mysql:host=$servername;dbname=$baza;charset=utf8", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);


//provera imena
if(empty($_POST['name']))
    $greske[]='Name missing.';
else
    $name=trim($_POST['name']);
//provera prezimena
if(empty($_POST['surname']))
    $greske[]='Surname missing.';
else
    $surname=trim($_POST['surname']);

//provera email
if(empty($_POST['email']))
    $greske[]='Email missing.';
else
    $email=trim($_POST['email']);

//provera telephone
if(empty($_POST['telephone']))
    $greske[]='Telephone missing.';
else
    $telephone=trim($_POST['telephone']);

//provera address
if(empty($_POST['address']))
    $greske[]='Address missing.';
else
    $address=trim($_POST['address']);

//apartment
$apartment=trim($_POST['apartment']);

//provera city
if(empty($_POST['city']))
    $greske[]='City missing.';
else
    $city=trim($_POST['city']);

//provera city
if(empty($_POST['post_number']))
    $greske[]='Post number missing.';
else
    $post_number=trim($_POST['post_number']);


$total_price = $_POST['total_price'];
$total_price = number_format($total_price, 2, '.', '');
$a_note = $_POST['additional_note'];

$customer_id = $_SESSION['id_korisnika'];
$discount = $_SESSION['discount'];

//generisanje order_id
$dan = date("d");
$mesec = date("m");
$godina = date("Y");
$godina = substr($godina,2,2);
$datum = $dan . $mesec . $godina;
$mesgod = $mesec . $godina;
//provera da li postoje narudzbenice tekuceg meseca
try {
    $stmt1 = $conn->prepare("SELECT * FROM mesgod WHERE mesgod= '$mesgod'");
    $stmt1->execute();
    $row=$stmt1->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e)
{
    echo "Error: " . $e->getMessage();
}
if(!empty($row))
{
    //postoje narudzbenice pa se uzima sledeci fdid
    $fdid = $row['fdid'];
    if($fdid == '9999') $fdid = '0000';
    $id_mesgod = $row['id'];
    $fdid ++;
    $fdid = str_pad($fdid, 4, '0', STR_PAD_LEFT);
    $order_id = $datum . $fdid;
    // izmena fdid
    try {
        $sql = "update mesgod set fdid = '$fdid' where id = $id_mesgod";
        $conn->exec($sql);
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}
else
{
    //ne postoje narudzbenice pa se kreira novi mesgod
    try {
        $sql = "INSERT INTO mesgod (mesgod, fdid) VALUES('$mesgod','0001')";
        $conn->exec($sql);
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
    $order_id = $datum . '0001';

}
// ako je registrovani korisnik u orders unesi customer_id u suprotnom name, surname, telephone, mail
if($_SESSION['prava']=='user' || $_SESSION['prava']=='reseller')
{
    $sql = "INSERT INTO orders (order_id,customer_id, total_price, discount, payment_status, shipping_address, shipping_apartment, shipping_city, post_number, telephone, order_status, additional_note) VALUES('$order_id','$customer_id','$total_price','$discount','0','$address','$apartment','$city','$post_number','$telephone','0','$a_note')";
}
else
{
    $sql = "INSERT INTO orders (order_id,name, surname, email, total_price,discount,discounted_price, payment_status, shipping_address, shipping_apartment, shipping_city, post_number, telephone, order_status, additional_note) VALUES('$order_id','$name', '$surname', '$email','$total_price','0','0.00','0','$address','$apartment','$city','$post_number','$telephone','0','$a_note')";
}

try {
    $_SESSION['order_id'] = $order_id;
    $nice_order_id = $order_id;
    $conn->exec($sql);
    $order_id = $conn->lastInsertId();

} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}




//Unosenje stavki narudzbenice u bazu
$cartContentC = new CartContentController();
$stavke = array();
$stavke = $cartContentC->getCartContent($customer_id);

foreach ($stavke as $stavka)
{
    $configuration_id = $stavka['configuration_id'];
    $quantity = $stavka['quantity'];
    $item_price = $stavka['cena'] * 1.2;
    $item_price = number_format($item_price, 2, '.', '');

    //kreiranje order_item
    try {
        $sql = "INSERT INTO order_items (configuration_id, quantity, item_price, order_id) VALUES('$configuration_id', '$quantity', '$item_price','$order_id')";
        $conn->exec($sql);
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

}
//brisanje korpe
try {
    $sql = "delete from cart where customer_id = '$customer_id'";
    $conn->exec($sql);
} catch (PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

//fleg za kraj poruke o uspesnoj kupovini
$_SESSION['zavrsena_kupovina'] = 1;

///////////////////////////// MIHAILOV EMAIL /////////////////////////////////

$order_id = $nice_order_id;
$server = 'test.perfectsocket.com/';

$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'http://admin.'. $server .'send_email/?order_id='.$order_id ,
    CURLOPT_USERAGENT => 'Codular Sample cURL Request',
    CURLOPT_FOLLOWLOCATION => true
]);
$resp = curl_exec($curl);
// Send the request & save response to $resp
echo "Rezultat: ". $resp;
// Close request to clear up some resources
curl_close($curl);

///////////////////////////// MIHAILOV EMAIL /////////////////////////////////


// //slanje mail-a
// $mail = new PHPMailer;
// $mail->SMTPDebug = 1;
// //$mail->isSMTP();
// $mail->CharSet = 'UTF-8';
// $mail->Host = 'mail.gmail.com';
// $mail->Port = 587;
// $mail->SMTPAuth = true;
// $mail->SMTPSecure = 'tls';

// $mail->Username = 'perfect.socket@gmail.com';
// $mail->Password = 'Promeni2020';

// $mail->setFrom('perfect.socket@gmail.com', 'Perfect Socket Shop');
// $mail->addAddress($email);

// $mail->isHTML(true);
// $mail->Subject = 'Order saved';
// $mail->Body    = '<h4>Poštovani ' . $name . ' ' . $surname . ' </h4>
// <h3>Narudžbina sa rednim brojem narudžbenice '. $nice_order_id . ' je uspešno sačuvana.</h3>
// <h4>Uskoro će Vas kontaktirati naš operater.</h4><br><br>
// <h4>S poštovanjem,</h4>
// <h4>Perfect Socket</h4>';

// if($mail->send())
//     header("Location: ". server ."ShoppingResult.php");
// else
//     echo "test";


// //slanje operateru
// $mailo = new PHPMailer;
// $mailo->isSMTP();
// $mailo->CharSet = 'UTF-8';
// $mailo->Host = 'mail.gmail.com';
// $mailo->Port = 587;
// $mailo->SMTPAuth = true;
// $mailo->SMTPSecure = 'tls';

// $mailo->Username = 'perfect.socket@gmail.com';
// $mailo->Password = 'Promeni2020';

// $mailo->setFrom('perfect.socket@gmail.com', 'Perfect Socket Shop');
// $mailo->addAddress("perfect.socket@gmail.com");

// $mailo->isHTML(true);
// $mailo->Subject = ' New Order';
// $mailo->Body    = '<h4>Korisnik ' . $name . ' ' . $surname . ' </h4>
// <h3>Uspešno je kreirao narudžbinu sa rednim brojem narudžbenice ' . $nice_order_id . '.</h3><br>
// <h4>S poštovanjem,</h4>
// <h4>Perfect Socket</h4>';
// $mailo->send();


//Redirekcija
header("Location: ". server ."ShoppingResult.php");

?>