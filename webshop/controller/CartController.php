<?php
require_once('../db_config.php');
session_start();
//korisnik
$customer_id = $_SESSION['id_korisnika'];
$page = $_POST['page'];
$page = server . $page;
// KOD ZA BAZU NA NETU
$servername = servername;
$username = username;
$password = password;
$baza = baza;
// POVEZIVANJE NA BAZU
$conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

$cena_svega_ukupno = $_POST["cena_svega_ukupno"];
$svega_ukupno_pdv = $_POST["svega_ukupno_pdv"];
$brojanje = $_POST["brojanje"];  // broj standardnih kompleta
$brojanje_ostalih = $_POST["brojanje_ostalih"]; // broj ostalih proizvoda
$brojanje_mehanizama = $_POST["brojanje_mehanizama"];


//prikupi podatke sa forme
$niz_nosaca = array ();
$niz_opis_nosaca = array ();
$niz_cena_nosaca = array ();
$niz_modul_nosaca = array ();
$niz_kolicina = array ();

$niz_okvira = array ();
$niz_cena_okvira = array ();
$niz_opis_okvira = array ();

$niz_koji_nosi_mehanizme = array ();
$niz_koji_nosi_opise_mehanizama = array ();
$niz_koji_nosi_cene_mehanizama = array ();
$niz_koji_nosi_module_mehanizama = array ();
$niz_br_ubacenih_meh = array();

$niz_ostalih_proizvoda = array ();
$niz_opis_ostalih_proizvoda = array ();
$niz_cena_ostalih_proizvoda = array ();
$niz_modul_ostalih_proizvoda = array ();
$niz_kolicina_ostalih_proizvoda = array ();

//nizovi za slike
$niz_nosaca_src_image = array();
$niz_okvira_src_image = array();
$niz_mehanizama_src_image = array();
$niz_dodatnih_src_image = array();

$i = 0;
while ($i < $brojanje)
{
    $niz_nosaca[$i] = $_POST["niz_nosaca$i"];
    $niz_opis_nosaca[$i] = $_POST["niz_opis_nosaca$i"];
    $niz_cena_nosaca[$i] = $_POST["niz_cena_nosaca$i"];
    $niz_modul_nosaca[$i] = $_POST["niz_modul_nosaca$i"];
    $niz_kolicina[$i] = $_POST["niz_kolicina$i"];

    $niz_okvira[$i] = $_POST["niz_okvira$i"];
    $niz_cena_okvira[$i] = $_POST["niz_cena_okvira$i"];
    $niz_opis_okvira[$i] = $_POST["niz_opis_okvira$i"];

    $niz_br_ubacenih_meh[$i] = $_POST["niz_br_ubacenih_meh$i"];

    $niz_nosaca_src_image[$i] = $_POST["niz_nosaca_src_image$i"];
    $niz_okvira_src_image[$i] = $_POST["niz_okvira_src_image$i"];

    $i++;
}
$i = 0;
while ($i < $brojanje_mehanizama)
{
    $niz_koji_nosi_mehanizme[$i] = $_POST["niz_koji_nosi_mehanizme$i"];
    $niz_koji_nosi_opise_mehanizama[$i] = $_POST["niz_koji_nosi_opise_mehanizama$i"];
    $niz_koji_nosi_cene_mehanizama[$i] = $_POST["niz_koji_nosi_cene_mehanizama$i"];
    $niz_koji_nosi_module_mehanizama[$i] = $_POST["niz_koji_nosi_module_mehanizama$i"];

    $niz_mehanizama_src_image[$i] = $_POST["niz_mehanizama_src_image$i"];

    $i++;
}
$i=0;
while ($i < $brojanje_ostalih)
{
    $niz_kolicina_ostalih_proizvoda[$i] = $_POST["niz_kolicina_ostalih_proizvoda$i"];
    $niz_ostalih_proizvoda[$i] = $_POST["niz_ostalih_proizvoda$i"];
    $niz_opis_ostalih_proizvoda[$i] = $_POST["niz_opis_ostalih_proizvoda$i"];
    $niz_cena_ostalih_proizvoda[$i] = $_POST["niz_cena_ostalih_proizvoda$i"];
    $niz_modul_ostalih_proizvoda[$i] = $_POST["niz_modul_ostalih_proizvoda$i"];

    $niz_dodatnih_src_image[$i] = $_POST["niz_dodatnih_src_image$i"];

    $i++;
}


// preuzmi podatke i upisi u tabele i cart
$i = 0;
$z = 0;

$postoji_konfiguracija = 1;
$cena_konfiguracije = 0;
while ($i < $brojanje) {
    $cena_konfiguracije = 0;
    $postoji_konfiguracija = 1;
    $configuration_id = 0;
    $velicina_konfiguracije = 0;
    $product_ids = array();
    $nosac_id = 0;
    $okvir_id = 0;

// ISPISIVANJE NOSACA
/*
    echo '<hr></hr>configuration<br>';
    echo $niz_opis_nosaca[$i]; // OPIS i naslov kompleta
    $string = $niz_nosaca[$i];
    $newstring = str_replace("/", "_", $string);
    //echo $string; // KATALOSKI BROJ
    echo $newstring;
    echo number_format($niz_cena_nosaca[$i], 2, ',', ' '); // JEDINICNA CENA
    echo $niz_kolicina[$i]; // KOLICINA
    echo  $niz_modul_nosaca[$i]; // SLIKA
    echo '<br>';
    */
    try {
        $stmt1 = $conn->prepare("SELECT id FROM products WHERE reference_number= '$niz_nosaca[$i]'");
        $stmt1->execute();
        $row=$stmt1->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }


    if(!empty($row))
    {
        $nosac_id = $row['id'];
    }
    else
    {
        try {
            $cena = number_format($niz_cena_nosaca[$i], 2, '.', '');
            $sql = "INSERT INTO products (description_en, reference_number, price, image, image_src) VALUES('$niz_opis_nosaca[$i]','$niz_nosaca[$i]','$cena','$niz_modul_nosaca[$i]','$niz_nosaca_src_image[$i]')";
            $conn->exec($sql);
            $nosac_id = $conn->lastInsertId();
            $postoji_konfiguracija = 0;
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }

    }
    //echo $nosac_id;

    $naziv_konfiguracije = $niz_opis_nosaca[$i];
    //za niloe, jer nema opisa nosaca
   // if($naziv_konfiguracije == "")
     //   $naziv_konfiguracije = "NILOE Configuration";
    $cena_konfiguracije+= $niz_cena_nosaca[$i];
    $kolicina = $niz_kolicina[$i];
    $product_ids[0] = $nosac_id;
    //ispisivanje okvira
    /*
    echo $niz_opis_okvira[$i]; // OPIS i naslov kompleta
    $string = $niz_okvira[$i];
    $newstring = str_replace("/", "_", $string);
    //echo $string; // KATALOSKI BROJ
    echo $newstring;
    echo number_format($niz_cena_okvira[$i], 2, ',', ' '); // JEDINICNA CENA
    echo $niz_kolicina[$i]; // KOLICINA
    echo  $niz_modul_nosaca[$i]; // SLIKA

    echo "<br><br>mehanizmi<br>";
    echo $niz_br_ubacenih_meh[$i];
    echo "<br>";
    */

    try {
        $stmt1 = $conn->prepare("SELECT id FROM products WHERE reference_number= '$niz_okvira[$i]'");
        $stmt1->execute();
        $row=$stmt1->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }


    if(!empty($row))
    {
        $okvir_id = $row['id'];
    }
    else
    {
        try {
            $cena = number_format($niz_cena_okvira[$i], 2, '.', '');
            $sql = "INSERT INTO products (description_en, reference_number, price, image, image_src) VALUES('$niz_opis_okvira[$i]','$niz_okvira[$i]','$cena','$niz_modul_nosaca[$i]','$niz_okvira_src_image[$i]')";
            $conn->exec($sql);
            $okvir_id = $conn->lastInsertId();
            $postoji_konfiguracija = 0;
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }

    }
   // echo $okvir_id;
    $cena_konfiguracije+= $niz_cena_okvira[$i];
    $product_ids[1] = $okvir_id;

    // ISPISIVANJE MEHANIZAMA
    $k = 0;
    $mehanizmi_id = array();
    $velicina_konfiguracije = $niz_br_ubacenih_meh[$i]+2;
    while ($k < $niz_br_ubacenih_meh[$i])
    {
        /*
        echo $niz_koji_nosi_opise_mehanizama[$z]; // opis
        $string = $niz_koji_nosi_mehanizme[$z];
        $newstring = str_replace("/", "_", $string);
        echo $newstring; // KATALOSKI BROJ
        echo number_format($niz_koji_nosi_cene_mehanizama[$z], 2, ',', ' '); // JEDINICNA CENA

        if($niz_koji_nosi_module_mehanizama[$z]>0.5)
        {
            echo $niz_koji_nosi_module_mehanizama[$z]; // SLIKA
        }
        else
        {
            echo 'slika_05'; // SLIKA
        }

        */
        if($niz_koji_nosi_module_mehanizama[$z]>0.5)
        {
            $slika= $niz_koji_nosi_module_mehanizama[$z]; // SLIKA
        }
        else
        {
            $slika= 'slika_05'; // SLIKA
        }
        try {
            $stmt1 = $conn->prepare("SELECT id FROM products WHERE reference_number= '$niz_koji_nosi_mehanizme[$z]'");
            $stmt1->execute();
            $row=$stmt1->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }


        if(!empty($row))
        {
            $mehanizmi_id[$k] = $row['id'];
        }
        else
        {
            try {
                $cena = number_format($niz_koji_nosi_cene_mehanizama[$z], 2, '.', '');
                $sql = "INSERT INTO products (description_en, reference_number, price, image, image_src) VALUES('$niz_koji_nosi_opise_mehanizama[$z]','$niz_koji_nosi_mehanizme[$z]','$cena','$slika','$niz_mehanizama_src_image[$z]')";
                $conn->exec($sql);
                $mehanizmi_id[$k] = $conn->lastInsertId();
                $postoji_konfiguracija = 0;
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }

        }
       // echo $mehanizmi_id[$k];
        $cena_konfiguracije+=$niz_koji_nosi_cene_mehanizama[$z];
        $product_ids[$k+2] = $mehanizmi_id[$k];
        //$cena_konfiguracije = number_format($cena_konfiguracije, 2, ',', ' ');


        $z++;
        $k++;
        echo "<br>";
    }

   // echo $postoji_konfiguracija;
    // echo $cena_konfiguracije;
   // echo "<hr><br>";
    //provera da li konfiguracija vec postoji
    if($postoji_konfiguracija)
    {

        $niz_potencijalnih_konfiguracija = array();
        //proveri da li postoji
        try {
            $sql1 = $conn->prepare("SELECT configuration_id, count(id) as broj FROM configuration_items where product_id in (" . implode(',', array_map('intval', $product_ids)) . ")
                  group by configuration_id");
            $sql1->execute();
            $nk=0;
            while ($row=$sql1->fetch(PDO::FETCH_ASSOC))
            {
                if($row['broj'] == $velicina_konfiguracije)
                {
                    $niz_potencijalnih_konfiguracija[$nk] = $row['configuration_id'];
                    //provera da li se configuration_id nalazi u nizu potencijalnih konfiguracija
                    $sql2 = $conn->prepare("select product_id from configuration_items where configuration_id = $niz_potencijalnih_konfiguracija[$nk]");
                    $sql2->execute();
                    $niz_proizvoda = array();
                    $np = 0;
                    while ($rowp=$sql2->fetch(PDO::FETCH_ASSOC)) {
                        $niz_proizvoda[$np] = $rowp['product_id'];
                        $np++;
                    }
                    $result=array_diff($product_ids,$niz_proizvoda);
                    if(empty($result))
                    {
                        $configuration_id= $niz_potencijalnih_konfiguracija[$nk];
                        break;
                    }

                    $nk++;
                }
            }

            if($configuration_id)
            {
                //konfiguracija postoji, proverava se da li je vec u korpi
                $sql3 = $conn->prepare("select * from cart where customer_id = $customer_id and configuration_id = $configuration_id");
                $sql3->execute();
                $row3=$sql3->fetch(PDO::FETCH_ASSOC);
                if (!empty($row3))
                {
                    //cart update, dodaj kolicinu konfiguracije
                    $cart_id = $row3['id'];
                    $stara_kolicina = $row3['quantity'];
                    $nova_kolicina = $stara_kolicina + $kolicina;
                    $sql4 = $conn->prepare("update cart set quantity = $nova_kolicina where id = $cart_id");
                    $sql4->execute();

                }
                else
                {
                    //dodaj u korpu
                    try {
                        $sql = "INSERT INTO cart (customer_id, configuration_id, quantity, configuration_price) VALUES('$customer_id','$configuration_id','$kolicina','$cena_konfiguracije')";
                        $conn->exec($sql);

                    } catch (PDOException $e) {
                        echo $sql . "<br>" . $e->getMessage();
                    }

                }

            }
            else
            {
                //konfiguracija ipak ne postoji i moraju se popuniti tabele i dodati u cart
                //upisi u tabele i dodaj u korpu
                //upis u configurations
                $cena_konfiguracije = number_format($cena_konfiguracije, 2, '.', '');
                try {
                    $sql = "INSERT INTO configurations (name, price) VALUES('$naziv_konfiguracije','$ce
                    na_konfiguracije')";
                    $conn->exec($sql);
                    $configuration_id = $conn->lastInsertId();
                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }
                //upis u configuration_items
                try {
                    $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$nosac_id')";
                    $conn->exec($sql);
                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }
                try {
                    $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$okvir_id')";
                    $conn->exec($sql);
                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }
                $m=0;
                while ($m < $niz_br_ubacenih_meh[$i]) {
                    try {
                        $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$mehanizmi_id[$m]')";
                        $conn->exec($sql);
                    } catch (PDOException $e) {
                        echo $sql . "<br>" . $e->getMessage();
                    }
                    $m++;
                }
                //unos u korpu
                try {
                    $sql = "INSERT INTO cart (customer_id, configuration_id, quantity,configuration_price) VALUES('$customer_id','$configuration_id','$kolicina','$cena_konfiguracije')";
                    $conn->exec($sql);
                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }

            }

        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }

    }
    else
    {
        //upisi u tabele i dodaj u korpu
        //upis u configurations
        $cena_konfiguracije = number_format($cena_konfiguracije, 2, '.', '');
        try {
            $sql = "INSERT INTO configurations (name, price) VALUES('$naziv_konfiguracije','$cena_konfiguracije')";
            $conn->exec($sql);
            $configuration_id = $conn->lastInsertId();
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        //upis u configuration_items
        try {
            $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$nosac_id')";
            $conn->exec($sql);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        try {
            $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$okvir_id')";
            $conn->exec($sql);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        $m=0;
        while ($m < $niz_br_ubacenih_meh[$i]) {
            try {
                $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$mehanizmi_id[$m]')";
                $conn->exec($sql);
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
            $m++;
        }
        //unos u korpu
        try {
            $sql = "INSERT INTO cart (customer_id, configuration_id, quantity,configuration_price) VALUES('$customer_id','$configuration_id','$kolicina','$cena_konfiguracije')";
            $conn->exec($sql);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
    }

    $i++;
}

// echo "<hr><hr><br>";

// ostali proizvodi
$i = 0;
while ($i < $brojanje_ostalih)
{
    $configuration_id = 0;
    $cena_konfiguracije=0;
    $kolicina = $niz_kolicina_ostalih_proizvoda[$i];
    $naziv_konfiguracije = $niz_opis_ostalih_proizvoda[$i];
    /*
    echo $niz_opis_ostalih_proizvoda[$i]; // OPIS
    $string = $niz_ostalih_proizvoda[$i];
    $newstring = str_replace("/", "_", $string);
    echo $newstring; // KATALOSKI BROJ
    $niz_cena_ostalih_proizvoda[$i] = (float)$niz_cena_ostalih_proizvoda[$i];
    echo number_format($niz_cena_ostalih_proizvoda[$i], 2, ',', ' '); // JEDINICNA CENA
    $niz_kolicina_ostalih_proizvoda[$i] = (float)$niz_kolicina_ostalih_proizvoda[$i];
    echo $niz_kolicina_ostalih_proizvoda[$i]; // KOLICINA
    echo $niz_modul_ostalih_proizvoda[$i]; // SLIKA
    echo "<br>";
    */
    try {
        $stmt1 = $conn->prepare("SELECT id, price FROM products WHERE reference_number= '$niz_ostalih_proizvoda[$i]'");
        $stmt1->execute();
        $row=$stmt1->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }


    if(!empty($row))
    {
        //postoji konfiguracija, nadji id
        $ostali_id = $row['id'];
        $cena_konfiguracije = $row['price'];
        try {
            $stmt2 = $conn->prepare("SELECT configuration_id FROM configuration_items where product_id = $ostali_id");
            $stmt2->execute();
            $row=$stmt2->fetch(PDO::FETCH_ASSOC);
            $configuration_id = $row['configuration_id'];
            //konfiguracija postoji, proverava se da li je vec u korpi
            $sql3 = $conn->prepare("select * from cart where customer_id = $customer_id and configuration_id = $configuration_id");
            $sql3->execute();
            $row3=$sql3->fetch(PDO::FETCH_ASSOC);
            if (!empty($row3))
            {
                //cart update, dodaj kolicinu konfiguracije
                $cart_id = $row3['id'];
                $stara_kolicina = $row3['quantity'];
                $nova_kolicina = $stara_kolicina + $kolicina;
                $sql4 = $conn->prepare("update cart set quantity = $nova_kolicina where id = $cart_id");
                $sql4->execute();

            }
            else
            {
                //dodaj u korpu
                try {
                    $sql = "INSERT INTO cart (customer_id, configuration_id, quantity,configuration_price) VALUES('$customer_id','$configuration_id','$kolicina','$cena_konfiguracije')";
                    $conn->exec($sql);

                } catch (PDOException $e) {
                    echo $sql . "<br>" . $e->getMessage();
                }

            }
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }
    else
    {
        //proizvod ne postoji
        try {
            $cena = number_format($niz_cena_ostalih_proizvoda[$i], 2, '.', '');
            $sql = "INSERT INTO products (description_en, reference_number, price, image, image_src) VALUES('$niz_opis_ostalih_proizvoda[$i]','$niz_ostalih_proizvoda[$i]','$cena','$niz_modul_ostalih_proizvoda[$i]','$niz_dodatnih_src_image[$i]')";
            $conn->exec($sql);
            $ostali_id = $conn->lastInsertId();
            //upisi u tabele i dodaj u korpu
            //upis u configurations

            try {
                $sql = "INSERT INTO configurations (name, price) VALUES('$naziv_konfiguracije','$cena')";
                $conn->exec($sql);
                $configuration_id = $conn->lastInsertId();
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
            //upis u configuration_items
            try {
                $sql = "INSERT INTO configuration_items (configuration_id, product_id) VALUES('$configuration_id','$ostali_id')";
                $conn->exec($sql);
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
            //unos u korpu
            try {
                $sql = "INSERT INTO cart (customer_id, configuration_id, quantity, configuration_price) VALUES('$customer_id','$configuration_id','$kolicina','$cena')";
                $conn->exec($sql);
            } catch (PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }

        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }

    }
  //  echo $ostali_id;

    $i++;

}

// loading dok se ne izvrsi upis
?>
<div class="loader"></div>
<style>
    .loader {
        position:fixed;
        top: 40%;
        left: 45%;
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>
<?php
echo "<script>window.top.location='$page'</script>";
//header("Location: $page");
?>