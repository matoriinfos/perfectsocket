<?php


require_once 'controller/ProfileController.php';
$profileC = new ProfileController();

$uspesno = '';
if ($_SERVER['REQUEST_METHOD']== 'POST') {

    $pc = new ProfileController();
    $greske=$pc->updatePassword();
    if(empty($greske))
        $uspesno = "Data successfully updated!";


}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>Change Password</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" type="text/css" href="css/registracija.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$prava = $_SESSION['prava'];
if(empty($prava))
{

}
else {
    ?>

    <div id="registracija">
        <?php
        //prikaz gresaka

        if (!empty($greske)) {
            echo "<h1 class='text-center' style='color: red'>Error</h1>
    <p style='color: red'>The following errors occur: <br />";
            foreach ($greske as $msg)  //stampanje svake greske
            {
                echo "*  $msg <br />\n";
            }

        }
        if (!empty($uspesno)) {
            echo "<h1 class='text-center'>Data successfully updated!</h1>";
        }
        $id_korisnika = $_SESSION['id_korisnika'];
        $korisnik = $profileC->getUser($id_korisnika);
        ?>

        <h3 class="text-center" style="color: #15998c">Password change:</h3>
        <form action="ChangePassword.php" method="post">
            <p>Current password:
                <input type="password" name="pass" size="15" maxlength="20"
                       value="<?php if (isset($_POST['pass']) && empty($uspesno)) echo $_POST['pass']; ?>">
            </p>
            <p>New password:
                <input type="password" name="pass1" size="15" maxlength="40"
                       value="<?php if (isset($_POST['pass1']) && empty($uspesno)) echo $_POST['pass1']; ?>">
            </p>
            <p>Confirmed password:
                <input type="password" name="pass2" size="20" maxlength="40"
                       value="<?php if (isset($_POST['pass2']) && empty($uspesno)) echo $_POST['pass2']; ?>">
            </p>
            <input type="hidden" name="id" value="<?php echo $id_korisnika ?>"/>

            <p><input class="dugme_potvrdi" type="submit" name="submit" value="Confirm"/></p>
            <p><input type="button" style="background-color: #f57e63" onclick="location.href='./MyProfile.php';"
                      value="Cancel"/></p>


        </form>

    </div>


    <?php
}/*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>