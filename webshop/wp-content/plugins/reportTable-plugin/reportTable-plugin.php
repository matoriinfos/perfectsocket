<?php
/**
*@package reporTable-plugin
*/
/*

Plugin Name: reporTable-plugin
Plugin URI: http://ultra.rs
Version: 1
Author: Sasa Todorovic
Author uri: http://ultra.rs
Description: Report of creating devices
Text Domain: Legrand report Table
License: GPLv2 or Later
Domain Path: /
*/

/*ukoliko ne postoji apsolutna putanja - neophodna za inicjalizaciju*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // don't access directly
};

