$('.proizvodi').hide();
$('.proizvodi#prekidaci').show();
$('.glavni_meni#link_prekidaci_tasteri').css({ 'color': '#125185'});
$('.link_moduli#link_svi').css({'color':'#125185'});
var naslovLinka = $('#link_prekidaci_tasteri').text()
$('.naslov_proizvoda').text(naslovLinka);
// IZBOR JEZIKA
$('.dropdown-content a').click(function(){
jezik = $(this).attr('id');
//alert ("jezik je:" +jezik);
$('.navigacija').hide();
$('.navigacija#'+jezik).show();
   
//NAVIGACIJA NOSACA
$('.podmeni_nosaci').hide();
$('.podmeni_nosaci#'+jezik).show();

//NASLOVI NA KOMPLETA NA DESNOJ STRANI STRANICE
$('.naslov_kompleta').hide();
$('.naslov_kompleta#'+jezik).show();

//LEVI PODMENI KOJI DELI PROIZVODE NA MODULE
$('.levi_podmeni').hide();
$('.levi_podmeni#'+jezik).show();

//NASLOVI MEHANIZAMA
$('.naslov_proizvoda').hide();
$('.naslov_proizvoda#'+jezik).show();

//IMENA SVIH MEHANIZAMA NA IZABRANOM JEZIKU
$('.mehanizam_velicine_1_modula').hide();
$('.mehanizam_velicine_1_modula#'+jezik).show();
$('.mehanizam_velicine_2_modula').hide();
$('.mehanizam_velicine_2_modula#'+jezik).show();
$('.mehanizam_velicine_3_modula').hide();
$('.mehanizam_velicine_3_modula#'+jezik).show();
$('.mehanizam_velicine_4_modula').hide();
$('.mehanizam_velicine_4_modula#'+jezik).show();
$('.mehanizam_velicine_6_modula').hide();
$('.mehanizam_velicine_6_modula#'+jezik).show();
$('.mehanizam_velicine_7_modula').hide();
$('.mehanizam_velicine_7_modula#'+jezik).show();
$('.mehanizam_velicine_-_modula').hide();
$('.mehanizam_velicine_-_modula#'+jezik).show();

/*KOMLETI MEHANIZAMA*/
$('.kompleti_mehanizama').hide();
$('.kompleti_mehanizama#'+jezik).show();

// NAZIV KOLIČINA U KOMPLETU KOJI SE MENJA PO IZBORU JEZIKA
$('.kolicina_naslov').hide();
$('.kolicina_naslov#'+jezik).show();
});

$('body').on('click','.glavni_meni#link_detektori_pokreta','li', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.proizvodi#detektori_pokreta').fadeIn(250, function(){ $(this).show();});;
	//$('.glavni_meni').css({ 'color': 'white'});
	//$('.proizvodi#detektori_pokreta').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.dropdown-contentNovo', function() {
	$(".dropdown-contentNovo").hide();
});

$('body').on('mouseover','.dropbtnNovo', function() {
	$(".dropdown-contentNovo").show();
});

// MENI NAVIGACIJA - VELIČINA - BOJA - PRELAZ MIŠEM I KLIK
$('body').on('click','.link_moduli', function() {
	$('.link_moduli').css({'color':'#d0d0d0'});
	$(this).css({ 'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_prekidaci_tasteri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$(".levi_podmeni").css("display", "block");
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#prekidaci').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);

});

$('body').on('click','.glavni_meni#link_aksijalni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#aksijalni').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_dimeri', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.proizvodi#dimeri').show();
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_detektori_pokreta', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_termostati_detektori', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#termostati_detektori').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_energetske_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#energetske_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_racunarske_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#racunarske_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_multimedijalne_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#multimedijalne_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_tv_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#tv_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_maske', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#maske').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_zvona_svetiljke', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#zvona_svetiljke').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_ostala_oprema', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#ostala_oprema').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_vodootporno', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#vodootporno').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});


$('body').on('click','.glavni_meni#link_nosaci_dozne', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#nosaci_dozne').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});


$('body').on('click','.glavni_meni#link_kupatilo','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // skriva podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#kupatilo').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
	});

$('body').on('click','.glavni_meni#link_okviri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#okviri').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});


// PODMENI NAVIGACIJA (LINKOVI: MODUL1 i MODUL2)
$('body').on('click','.link_moduli#link_svi', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show(300);
});

$('body').on('click','.link_moduli#link_1', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_2').hide(300);
});

$('body').on('click','.link_moduli#link_2', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_1').hide(300);
});