﻿<!DOCTYPE html>
<?php
/*
Template Name: index_eng_air
*/
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
<title>Living Light AIR - Bticino - Konfigurator</title>
<meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
<meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/style_livinglight.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
</head>
<body>
<?php /*include_once("analyticstracking.php")*/ ?>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>
<div class = "subMenu">
		<div class="left_container">
	<div class="bticino_logo"><img src="wp-content/themes/momentous-lite/logotipi/livinglightAir_logo.jpg"></div>
	<div class = "naslov_proizvoda"></div>
	</div>
	<div class = "right_container">
	<div class="flags"></div>
	<div class = "naslov_kompleta">MY SELECTION</div>
	</div>
</div>
<div class="leva">

<?php 

// KOD ZA BAZA NA NETU
$servername = servername;
$username = username;
$password = password;
$dbname = baza;

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM bt_living_prek_mod_1");
	$stmt2 = $conn->prepare("SELECT * FROM bt_living_prek_mod_2");
	$stmt3 = $conn->prepare("SELECT * FROM bt_living_dozne_air");
	$stmt4 = $conn->prepare("SELECT * FROM boje_okvira_ll_air");
	$stmt5 = $conn->prepare("SELECT * FROM bticino_okviri_livinglight_air");
	$stmt6 = $conn->prepare("SELECT * FROM bt_living_dimeri_mod_1");
	$stmt7 = $conn->prepare("SELECT * FROM bt_living_dimeri_mod_2");
	$stmt8 = $conn->prepare("SELECT * FROM bt_living_releji_mod_1");
	$stmt9 = $conn->prepare("SELECT * FROM bt_living_temperatura_gas_mod_2");
	//$stmt10 = $conn->prepare("SELECT * FROM bt_living_temperatura_gas_mod_3");
	$stmt11 = $conn->prepare("SELECT * FROM bt_living_uticnice_mod_1");
	$stmt12 = $conn->prepare("SELECT * FROM bt_living_uticnice_mod_2");
	$stmt13 = $conn->prepare("SELECT * FROM bt_living_dr_uticnice_mod_1");
	$stmt14 = $conn->prepare("SELECT * FROM bt_living_dr_uticnice_mod_2");
	$stmt15 = $conn->prepare("SELECT * FROM bticino_living_zvona_svet_1");
	$stmt16 = $conn->prepare("SELECT * FROM bticino_living_zvona_svet_2");
	$stmt17 = $conn->prepare("SELECT * FROM bticino_living_maske_mod_1");
	$stmt18 = $conn->prepare("SELECT * FROM bticino_living_maske_mod_2");
	$stmt19 = $conn->prepare("SELECT * FROM bticino_living_mehanizmi_mod_1");
	$stmt20 = $conn->prepare("SELECT * FROM bticino_living_mehanizmi_mod_2");
	$stmt21 = $conn->prepare("SELECT * FROM bt_living_rac_uticnice_mod_1");
	$stmt22 = $conn->prepare("SELECT * FROM bt_living_det_pokreta_mod_1");
	$stmt23 = $conn->prepare("SELECT * FROM bt_living_det_pokreta_mod_2");
	$stmt24 = $conn->prepare("SELECT * FROM bticino_nosaci_livinglight_air");
	$stmt25 = $conn->prepare("SELECT * FROM bt_living_ostala_oprema");
	$stmt26 = $conn->prepare("SELECT * FROM bt_living_vodootporno");
	$stmt27 = $conn->prepare("SELECT * FROM bticino_aksijalni_living_mod_1");
	$stmt28 = $conn->prepare("SELECT * FROM bticino_aksijalni_living_mod_2");
	$stmt29 = $conn->prepare("SELECT * FROM bticino_maske_klasicni_prek_ll");
	
	$stmt1->execute();
	$stmt2->execute();
	$stmt3->execute();
	$stmt4->execute();
	$stmt5->execute();
	$stmt6->execute();
	$stmt7->execute();
	$stmt8->execute();
	$stmt9->execute();
	$stmt11->execute();
	$stmt12->execute();
	$stmt13->execute();
	$stmt14->execute();
	$stmt15->execute();
	$stmt16->execute();
	$stmt17->execute();
	$stmt18->execute();
	$stmt19->execute();
	$stmt20->execute();
	$stmt21->execute();
	$stmt22->execute();
	$stmt23->execute();
	$stmt24->execute();
	$stmt25->execute();
	$stmt26->execute();
	$stmt27->execute();
	$stmt28->execute();
	$stmt29->execute();
	
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();


}
?>

<div class="navigacija">
<ul class="glavni">
  <li class="glavni_meni" id="link_okviri"><a>COVER FRAMES AIR</a></li>
  <li class="glavni_meni" id="link_prekidaci_tasteri"><a>SWITCHES & PUSHBUTTONS</a></li>
  <li class="glavni_meni" id="link_aksijalni"><a>AXIAL SWITCHES & PUSHBUTTONS</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>DIMMERS</a></li>
  <li class="glavni_meni" id="link_detektori_pokreta"><a>MOTION DETECTORS</a></li>
  <li class="glavni_meni" id="link_termostati_detektori"><a>TERMOSTATS & GAS DETECTORS</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>ENERGY SOCKETS</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>COMPUTER SOCKETS</a></li>
  <li class="glavni_meni" id="link_tv_audio_hdmi_uticnice"><a>TV/AUDIO/VGA/HDMI SOCKETS</a></li>
  <li class="glavni_meni" id="link_maske"><a>BLANK MASKS</a></li>
  <li class="glavni_meni" id="link_zvona_svetiljke"><a>BELLS & LIGHTS</a></li>
  <li class="glavni_meni" id="link_ostala_oprema"><a>MASKS & LAMPS</a></li>
  <li class="glavni_meni" id="link_nosaci_dozne"><a>BOXES</a></li>
  <li class="glavni_meni" id="link_vodootporno"><a>WATERPROOF</a></li>
  <!--
	<div class="jezik_navigacija" style="float:right;">
		<ul>
		<li class="lang"><div class="pozadina_zastave"><img class="flag" id="british" src="wp-content/themes/momentous-lite/flags/british.jpg"></div></li>
		<li class="lang"><div class="pozadina_zastave"><img class="flag" id="serbian" src="wp-content/themes/momentous-lite/flags/serbian.jpg"></div></li>
		</ul>
	</div>
	-->
</ul>
</div>

<div class="dropdownNovo">
  <button class="dropbtnNovo">Wiring devices</button>
  <div class="dropdown-contentNovo">
  <a class="glavni_meni" id="link_okviri">FRAMES</a>
  <a class="glavni_meni" id="link_prekidaci_tasteri">SWITCHES & PUSHBUTTONS</a>
  <a class="glavni_meni" id="link_dimeri">DIMMERS</a>
  <a class="glavni_meni" id="link_detektori_pokreta">MOTION DETECTORS</a>
  <a class="glavni_meni" id="link_termostati_detektori">TERMOSTATS & GAS DETECTORS</a>
  <a class="glavni_meni" id="link_energetske_uticnice">ENERGY SOCKETS</a>
  <a class="glavni_meni" id="link_racunarske_uticnice">COMPUTER SOCKETS</a>
  <a class="glavni_meni" id="link_tv_audio_hdmi_uticnice">TV/AUDIO/VGA/HDMI SOCKETS</a>
  <a class="glavni_meni" id="link_maske">BLANK MASKS</a>
  <a class="glavni_meni" id="link_zvona_svetiljke">BELLS & LIGHTS</a>
  <a class="glavni_meni" id="link_ostala_oprema">MASKS & LAMPS</a>
  <a class="glavni_meni" id="link_nosaci_dozne">BOXES</a>
  <a class="glavni_meni" id="link_vodootporno">WATERPROOF</a>
  </div>
</div>

<div class="bticino_levi">
<ul class = "levi_podmeni">
<li id="naslov_podmeni"></li>
<li style="float:right"><a class="link_moduli" id="link_svi">ALL MODULES</a></li>
<li style="float:right"><a class="link_moduli" id="link_2">2 MODULES</a></li>
<li style="float:right"><a class="link_moduli" id="link_1">1 MODULE</a></li>
</ul>
</div>

<!-- LEVI SCROLL BAR -->
<div class="ex1">
<div class = "proizvodi" id = "prekidaci">
<?php
// VREDNOST EVRA U DINARIMA
$money = 121;
$currency = "EUR";
// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_1_opis[$i] = $row['interni_opis'];
	$prekidaci_1_kat_br[$i] = $row['kat_br'];
	$prekidaci_1_kratak_opis[$i] = $row['opis_engleski'];
	//$prekidaci_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$prekidaci_1_cena[$i] = $row['vp_cena']/$money;
	//$prekidaci_1_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_1=$i;
}

// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_2_opis[$i] = $row['interni_opis'];
	$prekidaci_2_kat_br[$i] = $row['kat_br'];
	$prekidaci_2_kratak_opis[$i] = $row['opis_engleski'];
	//$prekidaci_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$prekidaci_2_cena[$i] = $row['vp_cena']/$money;
	//$prekidaci_2_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_2=$i;
}

//ISPISIVANJE PREKIDACA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';


while ($i< $brojanje_prekidaca_1)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$prekidaci_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$prekidaci_1_kratak_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_1_cena[$i].'" data-kratak-opis ="'.$prekidaci_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_1_kratak_opis[$i].'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$prekidaci_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_1_cena[$i].'" data-kratak-opis ="'.$prekidaci_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($prekidaci_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_prekidaci

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';
while ($i< $brojanje_prekidaca_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$prekidaci_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$prekidaci_2_kratak_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_2_cena[$i].'" data-kratak-opis ="'.$prekidaci_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$prekidaci_2_kratak_opis[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'" data-interni-opis = "'.$prekidaci_2_opis[$i].'" data-kat-br = "'.$prekidaci_2_kat_br[$i].'" data-cena="'.$prekidaci_2_cena[$i].'" data-kratak-opis ="'.$prekidaci_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';		
								echo '<div class="tekst_opis">'.$prekidaci_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($prekidaci_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
												
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_prekidaci
?>
		
</div>	<!-- KRAJ DIV PROIZVODI PREKIDACI -->

<div class = "proizvodi" id = "aksijalni">
<?php
// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt27->fetch(PDO::FETCH_ASSOC))
{	
	$aksijalni_1_opis[$i] = $row['opis_engleski'];
	$aksijalni_1_kat_br[$i] = $row['kat_br'];
	$aksijalni_1_kratak_opis[$i] = $row['opis_engleski'];
	//$aksijalni_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$aksijalni_1_cena[$i] = $row['vp_cena']/$money;
	//$aksijalni_1_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_1=$i;
}

// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt28->fetch(PDO::FETCH_ASSOC))
{	
	$aksijalni_2_opis[$i] = $row['interni_opis'];
	$aksijalni_2_kat_br[$i] = $row['kat_br'];
	$aksijalni_2_kratak_opis[$i] = $row['opis_engleski'];
	$aksijalni_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_2=$i;
}

//ISPISIVANJE PREKIDACA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_prekidaca_1)
{
	$red_br = $red_br + 1;
	$string = $aksijalni_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam"  data-modul ="'.$br_modula.'"  title="'.$aksijalni_1_kat_br[$i].'" data-opis-interni = "'.$aksijalni_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_1_cena[$i].'" data-kratak-opis ="'.$aksijalni_1_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$aksijalni_1_kratak_opis[$i].'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$aksijalni_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_1_cena[$i].'" data-kratak-opis ="'.$aksijalni_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$aksijalni_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$aksijalni_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($aksijalni_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($aksijalni_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($aksijalni_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)							
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_aksijalni

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';
while ($i< $brojanje_prekidaca_2)
{
	$red_br = $red_br + 1;
	$string = $aksijalni_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$aksijalni_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$aksijalni_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_2_cena[$i].'" data-kratak-opis ="'.$aksijalni_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$aksijalni_2_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'" data-interni-opis = "'.$aksijalni_2_opis[$i].'" data-kat-br = "'.$aksijalni_2_kat_br[$i].'" data-cena="'.$aksijalni_2_cena[$i].'" data-kratak-opis ="'.$aksijalni_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$aksijalni_2_kat_br[$i].'</div>';	
								echo '<div class="tekst_opis">'.$aksijalni_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($aksijalni_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($aksijalni_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($aksijalni_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
															
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_aksijalni
?>
		
</div>	<!-- KRAJ DIV PROIZVODI AKSIJALNI -->



<div class = "proizvodi" id = "dimeri">
<?php



// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt6->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_1_opis[$i] = $row['interni_opis'];
	$dimeri_1_kat_br[$i] = $row['kat_br'];
	$dimeri_1_kratak_opis[$i] = $row['opis_engleski'];
	//$dimeri_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$dimeri_1_cena[$i] = $row['vp_cena']/$money;
	//$dimeri_1_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_1=$i;
}

// PRAVI SE NIZ SVIH DIMERA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt7->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_2_opis[$i] = $row['interni_opis'];
	$dimeri_2_kat_br[$i] = $row['kat_br'];
	$dimeri_2_kratak_opis[$i] = $row['opis_engleski'];
	//$dimeri_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$dimeri_2_cena[$i] = $row['vp_cena']/$money;
	//$dimeri_2_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_2=$i;
}

//ISPISIVANJE DIMERA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_dimera_1)
{
	$red_br = $red_br + 1;
	$string = $dimeri_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$dimeri_1_kratak_opis[$i].'" data-interni-opis = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$dimeri_1_kat_br[$i].'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$dimeri_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$dimeri_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM				
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_dimeri

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';

while ($i< $brojanje_dimera_2)
{
	$red_br = $red_br + 1;
	$string = $dimeri_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// DIMER NA ENGLESKOM - 2 MODULA
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$dimeri_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$dimeri_2_kat_br[$i].'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$dimeri_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$dimeri_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_dimeri
?>
		
</div>	<!-- KRAJ DIV PROIZVODI DIMERI -->


<!-- DETEKTORI POKRETA -->

<div class = "proizvodi" id = "detektori_pokreta">
<?php

// PRAVI SE NIZ SVIH DETEKTORA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt22->fetch(PDO::FETCH_ASSOC))
{	
	$detektori_1_opis[$i] = $row['interni_opis'];
	$detektori_1_kat_br[$i] = $row['kat_br'];
	$detektori_1_kratak_opis[$i] = $row['opis_engleski'];
	//$detektori_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$detektori_1_cena[$i] = $row['vp_cena']/$money;
	//$detektori_1_cena_evro[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_detektori_pokreta_1=$i;
}

// PRAVI SE NIZ SVIH DETEKTORA POKRETA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt23->fetch(PDO::FETCH_ASSOC))
{	
	$detektori_2_opis[$i] = $row['interni_opis'];
	$detektori_2_kat_br[$i] = $row['kat_br'];
	$detektori_2_kratak_opis[$i] = $row['opis_engleski'];
	//$detektori_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$detektori_2_cena[$i] = $row['vp_cena']/$money;
	//$detektori_2_cena_evro[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_detektori_pokreta_2=$i;
}

//ISPISIVANJE DETEKTORA POKRETA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';


while ($i< $brojanje_detektori_pokreta_1)
{
	$red_br = $red_br + 1;
	$string = $detektori_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			// DETEKTORI POKRETA - SRPSKI - 1 modul
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$detektori_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$detektori_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$detektori_1_cena[$i].'" data-kratak-opis ="'.$detektori_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$detektori_1_kratak_opis[$i].'" data-interni-opis = "'.$detektori_1_opis[$i].'" data-kat-br = "'.$detektori_1_kat_br[$i].'" data-cena="'.$detektori_1_cena[$i].'" data-kratak-opis ="'.$detektori_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$detektori_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$detektori_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($detektori_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($detektori_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($detektori_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)	
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

$i=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';

while ($i< $brojanje_detektori_pokreta_2)
{
	$red_br = $red_br + 1;
	$string = $detektori_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// DETEKTORI POKRETA - SRPSKI - 2 MODULA
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$detektori_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$detektori_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$detektori_2_cena[$i].'" data-kratak-opis ="'.$detektori_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$detektori_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$detektori_2_opis[$i].'" data-kat-br = "'.$detektori_2_kat_br[$i].'" data-cena="'.$detektori_2_cena[$i].'" data-kratak-opis ="'.$detektori_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$detektori_2_kat_br[$i].'</div>';	
								echo '<div class="tekst_opis">'.$detektori_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($detektori_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE			
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>
</div>	<!-- KRAJ DIV PROIZVODI - DETEKTORI POKRETA -->
<!-- TERMOSTATI I DETEKTORI GASA -->

<div class = "proizvodi" id = "termostati_detektori">
<?php

// PRAVI SE NIZ SVIH TERMOSTATA_DETEKTORA - VELICINE 2 MODULA

$i=0;
while ($row=$stmt9->fetch(PDO::FETCH_ASSOC))
{	
	$termostati_detektori_2_opis[$i] = $row['interni_opis'];
	$termostati_detektori_2_kat_br[$i] = $row['kat_br'];
	$termostati_detektori_2_kratak_opis[$i] = $row['opis_engleski'];
	//$termostati_detektori_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$termostati_detektori_2_cena[$i] = $row['vp_cena']/$money;
	//$termostati_detektori_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_termostata_detektora_2=$i;
}

//ISPISIVANJE DETEKTORA POKRETA - 2 MODULA

$i=0;
$red_br=0;


echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';

while ($i< $brojanje_termostata_detektora_2)
{
	$red_br = $red_br + 1;
	$string = $termostati_detektori_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// TERMOSTATI I DETEKTORI NA SRPSKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$termostati_detektori_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$termostati_detektori_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$termostati_detektori_2_cena[$i].'" data-kratak-opis ="'.$termostati_detektori_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$termostati_detektori_2_kratak_opis[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'" data-interni-opis = "'.$termostati_detektori_2_opis[$i].'" data-kat-br = "'.$termostati_detektori_2_kat_br[$i].'" data-cena="'.$termostati_detektori_2_cena[$i].'" data-kratak-opis ="'.$termostati_detektori_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$termostati_detektori_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$termostati_detektori_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($termostati_detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($termostati_detektori_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($termostati_detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>
</div>	<!-- KRAJ DIV PROIZVODI - TERMOSTATI DETEKTORI -->

<!-- ENERGETSKE UTICNICE -->

<div class = "proizvodi" id = "energetske_uticnice">
<?php

// PRAVI SE NIZ SVIH ENERGETSKIH UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt11->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_1_opis[$i] = $row['interni_opis'];
	$energetske_uticnice_1_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_1_kratak_opis[$i] = $row['opis_engleski'];
	//$energetske_uticnice_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$energetske_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	//$energetske_uticnice_1_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_energetske_uticnice_1=$i;
}
// PRAVI SE NIZ SVIH DETEKTORA POKRETA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt12->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_2_opis[$i] = $row['interni_opis'];
	$energetske_uticnice_2_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_2_kratak_opis[$i] = $row['opis_engleski'];
	$energetske_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	//$energetske_uticnice_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$energetske_uticnice_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_energetske_uticnice_2=$i;
}
//ENERGETSKE UTICNICE- 1 MODUL
$i=0;
$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_energetske_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';	
					echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$energetske_uticnice_1_kratak_opis[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'" data-interni-opis = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$energetske_uticnice_1_kat_br[$i].'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$energetske_uticnice_1_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$energetske_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($energetske_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
				
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';
while ($i< $brojanje_energetske_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				//ENERGETSKE UTICNICE - 2 MODULA - srpski
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$energetske_uticnice_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$energetske_uticnice_2_kat_br[$i].'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$energetske_uticnice_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$energetske_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($energetske_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>	
</div> <!-- KRAJ DIV ENERGETSKE UTICNICE -->


<!-- RACUNARSKE UTICNICE -->

<div class = "proizvodi" id = "racunarske_uticnice">
<?php

// PRAVI SE NIZ SVIH RACUNARSKIH UTICNICA - VELICINE 1 MODULA

$i=0;
while ($row=$stmt21->fetch(PDO::FETCH_ASSOC))
{	
	$racunarske_uticnice_opis[$i] = $row['interni_opis'];
	$racunarske_uticnice_kat_br[$i] = $row['kat_br'];
	$racunarske_uticnice_kratak_opis[$i] = $row['opis_engleski'];
	$racunarske_uticnice_cena[$i] = $row['vp_cena']/$money;
	//$racunarske_uticnice_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$racunarske_uticnice_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_racunarske_uticnice=$i;
}

//ISPISIVANJE RACUNARSKIH UTICNICA - 1 MODUL

$i=0;
$red_br=0;


echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_racunarske_uticnice)
{
	$red_br = $red_br + 1;
	$string = $racunarske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$racunarske_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$racunarske_uticnice_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$racunarske_uticnice_kat_br[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$racunarske_uticnice_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$racunarske_uticnice_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($racunarske_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>
</div>	<!-- KRAJ DIV PROIZVODI - RACUNARSKE UTICNICE -->


<!-- TV/AUDIO/HDMI UTICNICE -->

<div class = "proizvodi" id = "tv_audio_hdmi_uticnice">
<?php

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt13->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_1_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_1_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_1_kratak_opis[$i] = $row['opis_engleski'];
	$tv_audio_hdmi_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	//$tv_audio_hdmi_uticnice_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$tv_audio_hdmi_uticnice_1_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_tv_audio_hdmi_uticnice_1=$i;
}

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt14->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_2_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_2_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_2_kratak_opis[$i] = $row['opis_engleski'];
	$tv_audio_hdmi_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	//$tv_audio_hdmi_uticnice_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$tv_audio_hdmi_uticnice_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_tv_audio_hdmi_uticnice_2=$i;
}

//ISPISIVANJE TV/AUDIO/HDMIA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_tv_audio_hdmi_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_1_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 	
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
					// TV/AUDIO/HDMI UTIČNICE 1 MODUL - SRPSKI
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';		
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
								
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';

while ($i< $brojanje_tv_audio_hdmi_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_2_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// TV/AUDIO/HDMI UTIČNICE 2 MODULA - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>	
</div> <!-- KRAJ DIV TV/AUDIO/HDMI UTICNICE -->


<!-- ZVONA SVETILJKE -->

<div class = "proizvodi" id = "zvona_svetiljke">
<?php

// PRAVI SE NIZ SVIH ZVONA I SVETILJKI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt15->fetch(PDO::FETCH_ASSOC))
{	
	$zvona_svetiljke_1_opis[$i] = $row['interni_opis'];
	$zvona_svetiljke_1_kat_br[$i] = $row['kat_br'];
	$zvona_svetiljke_1_kratak_opis[$i] = $row['opis_engleski'];
	$zvona_svetiljke_1_cena[$i] = $row['vp_cena']/$money;
	//$zvona_svetiljke_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$zvona_svetiljke_1_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_zvona_svetiljke_1=$i;
}

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt16->fetch(PDO::FETCH_ASSOC))
{	
	$zvona_svetiljke_2_opis[$i] = $row['interni_opis'];
	$zvona_svetiljke_2_kat_br[$i] = $row['kat_br'];
	$zvona_svetiljke_2_kratak_opis[$i] = $row['opis_engleski'];
	$zvona_svetiljke_2_cena[$i] = $row['vp_cena']/$money;
	//$zvona_svetiljke_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$zvona_svetiljke_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_zvona_svetiljke_2=$i;
}

//ISPISIVANJE TV/AUDIO/HDMIA - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_zvona_svetiljke_1)
{
	$red_br = $red_br + 1;
	$string = $zvona_svetiljke_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
					// ZVONA I SVETILJKE 1 MODUL - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$zvona_svetiljke_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$zvona_svetiljke_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$zvona_svetiljke_1_cena[$i].'" data-kratak-opis ="'.$zvona_svetiljke_1_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$zvona_svetiljke_1_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$zvona_svetiljke_1_opis[$i].'" data-kat-br = "'.$zvona_svetiljke_1_kat_br[$i].'" data-cena="'.$zvona_svetiljke_1_cena[$i].'" data-kratak-opis ="'.$zvona_svetiljke_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$zvona_svetiljke_1_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$zvona_svetiljke_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($zvona_svetiljke_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($zvona_svetiljke_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($zvona_svetiljke_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
								
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';
while ($i< $brojanje_zvona_svetiljke_2)
{
	$red_br = $red_br + 1;
	$string = $zvona_svetiljke_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
					// ZVONA I SVETILJKE 2 MODULA - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$zvona_svetiljke_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$zvona_svetiljke_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$zvona_svetiljke_2_cena[$i].'" data-kratak-opis ="'.$zvona_svetiljke_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$zvona_svetiljke_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$zvona_svetiljke_2_opis[$i].'" data-kat-br = "'.$zvona_svetiljke_2_kat_br[$i].'" data-cena="'.$zvona_svetiljke_2_cena[$i].'" data-kratak-opis ="'.$zvona_svetiljke_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$zvona_svetiljke_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$zvona_svetiljke_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($zvona_svetiljke_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($zvona_svetiljke_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($zvona_svetiljke_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>	
</div> <!-- KRAJ ZVONA I SVETILJKE -->

<!-- MASKE -->

<div class = "proizvodi" id = "maske">
<?php

// PRAVI SE NIZ SVIH MASKI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt17->fetch(PDO::FETCH_ASSOC))
{	
	$maske_1_opis[$i] = $row['interni_opis'];
	$maske_1_kat_br[$i] = $row['kat_br'];
	$maske_1_kratak_opis[$i] = $row['opis_engleski'];
	$maske_1_cena[$i] = $row['vp_cena']/$money;
	//$maske_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$maske_1_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_maske_1=$i;
}

// PRAVI SE NIZ SVIH MASKI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt18->fetch(PDO::FETCH_ASSOC))
{	
	$maske_2_opis[$i] = $row['interni_opis'];
	$maske_2_kat_br[$i] = $row['kat_br'];
	$maske_2_kratak_opis[$i] = $row['opis_engleski'];
	$maske_2_cena[$i] = $row['vp_cena']/$money;
	//$maske_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$maske_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_maske_2=$i;
}

//ISPISIVANJE MASKI - 1 MODUL

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_maske_1)
{
	$red_br = $red_br + 1;
	$string = $maske_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				// SLEPE MASKE 1 MODUL - SRPSKI
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$maske_1_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$maske_1_opis[$i].'" data-kat-br = "'.$maske_1_kat_br[$i].'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$maske_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

$i=0;
//$red_br=0;
echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">';

while ($i< $brojanje_maske_2)
{
	$red_br = $red_br + 1;
	$string = $maske_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// SLEPE MASKE 2 MODULA - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_2_cena[$i].'" data-kratak-opis ="'.$maske_2_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$maske_2_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$maske_2_opis[$i].'" data-kat-br = "'.$maske_2_kat_br[$i].'" data-cena="'.$maske_2_cena[$i].'" data-kratak-opis ="'.$maske_2_kratak_opis[$i].'">';
						//echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/dodaj.png" class="dugme_dodaj_mehanizam_1" id="'.$maske_1_kat_br[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'" data-cena="'.$maske_1_cena[$i].'" data-naziv-slike="'.$newstring.'" data-modul ="'.$br_modula.'" />';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								
								echo '<div class="tekst_katbr">'.$maske_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$maske_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($maske_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
		
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi
?>	
</div> <!-- KRAJ MASKI -->


<!-- OSTALA OPREMA -->

<div class = "proizvodi" id = "ostala_oprema">
<?php


//ISPISIVANJE OSTALE OPREME
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt25->fetch(PDO::FETCH_ASSOC))
{	
	$ostala_oprema_opis[$i] = $row['interni_opis'];
	$ostala_oprema_kat_br[$i] = $row['kat_br'];
	$ostala_oprema_kratak_opis[$i] = $row['opis_engleski'];
	$ostala_oprema_cena[$i] = $row['vp_cena']/$money;
	//$ostala_oprema_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$ostala_oprema_cena_evro[$i] = $row['vp_cena']/$evro;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_ostala_oprema=$i;
}

$i=0;
$red_br=0;
echo '<div class = "vrsta_mehanizmi">';

while ($i< $brojanje_ostala_oprema)
{
	$red_br = $red_br + 1;
	$string = $ostala_oprema_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				// DODATNA OPREMA - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$ostala_oprema_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$ostala_oprema_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$ostala_oprema_cena[$i].'" data-kratak-opis ="'.$ostala_oprema_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'"  data-modul ='.$br_modula.'  title="'.$ostala_oprema_kratak_opis[$i].'" data-interni-opis = "'.$ostala_oprema_opis[$i].'" data-kat-br = "'.$ostala_oprema_kat_br[$i].'" data-cena="'.$ostala_oprema_cena[$i].'" data-kratak-opis ="'.$ostala_oprema_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$ostala_oprema_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$ostala_oprema_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($ostala_oprema_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

// UVEZIVANJE SA BAZOM - MASKE ZA PREKIDAČE
$j=0;
while ($row=$stmt29->fetch(PDO::FETCH_ASSOC))
{	
	$maske_prekidaci_opis[$j] = $row['interni_opis'];
	$maske_prekidaci_kat_br[$j] = $row['kat_br'];
	$maske_prekidaci_kratak_opis[$j] = $row['opis_engleski'];
	$maske_prekidaci_cena[$j] = $row['vp_cena']/$money;
	$broj_modula[$j] = $row['modul'];
	$j++;
	$brojanje_maske_prekidaci=$j;
}


//ISPISIVANJE MASKI ZA PREKDIACE I TASTERE

$j=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi">';

while ($j< $brojanje_maske_prekidaci)
{
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_kat_br[$j];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$j];
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				// DODATNA OPREMA - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$j].'" id="dodatni_mehanizam_dodatni" title="'.$maske_prekidaci_kat_br[$j].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_prekidaci_opis[$j].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_prekidaci_cena[$j].'" data-kratak-opis ="'.$maske_prekidaci_kratak_opis[$j].'">';				
					echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id='.$br_modula_id.'  data-modul ="'.$br_modula.'"  title="'.$maske_prekidaci_kratak_opis[$j].'" data-interni-opis = "'.$maske_prekidaci_opis[$j].'" data-kat-br = "'.$maske_prekidaci_kat_br[$j].'" data-cena="'.$maske_prekidaci_cena[$j].'" data-kratak-opis ="'.$maske_prekidaci_kratak_opis[$j].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$maske_prekidaci_kat_br[$j].'</div>';
							echo '<div class="tekst_opis">'.$maske_prekidaci_kratak_opis[$j].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($maske_prekidaci_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($maske_prekidaci_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($maske_prekidaci_cena[$j], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$j++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

?>	
</div> <!-- KRAJ OSTALA OPREMA -->



<!-- VODOOTPORNO -->

<div class = "proizvodi" id = "vodootporno">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - VODOOTPORNO
$i=0;
while ($row=$stmt26->fetch(PDO::FETCH_ASSOC))
{	
	$vodootporno_opis[$i] = $row['interni_opis'];
	$vodootporno_kat_br[$i] = $row['kat_br'];
	$vodootporno_kratak_opis[$i] = $row['opis_engleski'];
	$vodootporno_cena[$i] = $row['vp_cena']/$money;
	//$vodootporno_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$vodootporno_cena_evro[$i] = $row['vp_cena']/$evro;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_vodootporno=$i;
}

//ISPISIVANJE OPREME VODOOTPORNO

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_vodootporno)
{
	$red_br = $red_br + 1;
	$string = $vodootporno_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
			
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$vodootporno_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$vodootporno_kratak_opis[$i].'"  data-interni-opis = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$vodootporno_kat_br[$i].'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$vodootporno_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$vodootporno_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($vodootporno_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

?>	
</div> <!-- KRAJ - VODOOTPORNO -->


<!-- NOSACI I DOZNE -->

<div class = "proizvodi" id = "nosaci_dozne">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_dozne_opis[$i] = $row['interni_opis'];
	$nosaci_dozne_kat_br[$i] = $row['kat_br'];
	$nosaci_dozne_kratak_opis[$i] = $row['opis_engleski'];
	$nosaci_dozne_cena[$i] = $row['vp_cena']/$money;
	//$nosaci_dozne_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$nosaci_dozne_cena_evro[$i] = $row['vp_cena']/$evro;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci_dozne=$i;
}

//ISPISIVANJE OPREME - 

$i=0;
$red_br=0;

echo '<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">';

while ($i< $brojanje_nosaci_dozne)
{
	$red_br = $red_br + 1;
	$string = $nosaci_dozne_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
				
						if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 33)
				{
					$br_modula_id = "tri_tri";
				}
					// DOZNE I NOSACI - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$nosaci_dozne_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" title="'.$nosaci_dozne_kratak_opis[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$nosaci_dozne_kat_br[$i].'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$nosaci_dozne_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$nosaci_dozne_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($nosaci_dozne_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($nosaci_dozne_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($nosaci_dozne_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

?>	
</div> <!-- KRAJ - nosaci_dozne -->

<!-- OKVIRI -->

<div class = "proizvodi" id = "okviri">
<?php

// PRAVI SE NIZ SVIH BOJA OKVIRA
$i=0;
while ($row=$stmt4->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_boje[$i] = $row['boje'];
	$okviri_nazivi[$i] = $row['naziv'];
	$okviri_skracenice_boja[$i] = $row['skracenica'];
	$i++;
	$brojanje_okviri=$i;
}

// PRAVI SE NIZ SVIH OKVIRA
$j=0;
while ($row=$stmt5->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_kat_br[$j] = $row['kat_br'];
	$okviri_cena[$j] = $row['vp_cena']/$money;
	$okviri_interni_opis[$j] = $row['opis_engleski'];
	$okviri_modul[$j] = $row['modul'];
	$okviri_kratak_opis[$j] = $row['opis_engleski'];
	$okviri_materijal[$j] = $row['materijal'];
	$okviri_skracenice[$j] = $row['skracenica'];
	$okviri_okviri_boje[$j] = $row['boje'];
	$okviri_opis[$j] = $row['opis_engleski'];
	$j++;
	$brojanje_svi_okviri=$j;
}

//ISPISIVANJE BOJA OKVIRA

$i=0;
$red_br=0;
$ukupan_broj_okvira = 0;

echo '<div class = "vrsta_mehanizmi" id="okviri_vrsta">';

/*echo '<div class = "okviri">';*/

while ($i< $brojanje_okviri)
{
	$red_br = $red_br + 1;
	$string = $okviri_boje[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				//$br_modula = 1;
			
					echo '<div class = "boje_okvira">';
						echo '<div class = "slika_tekst">';
						echo '<img class="okvir_slika" src = "wp-content/themes/momentous-lite/livinglight_air/slike/okviri/'.$newstring.'.jpg" title="'.$okviri_nazivi[$i].'" id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'">';
						echo '<h2 id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'"><span>'.$okviri_nazivi[$i].'</span></h2>';
						echo '</div>';

						echo '<table class="podaci_okvira" id="podaci_okvira_'.$okviri_skracenice_boja[$i].'">'; //TABELA PODACI OKVIRA								
								
								echo '<tr><td class="kolona_modul">Module</td><td class="kolona_kat_br">Ref.no.</td><td class = "kolona_cena_2">Price '.$currency.'</td></tr>';
								//PRIKAZIVANJE CENE - OPISA
								$j=0;
								while ($j < $brojanje_svi_okviri)
								{
									if($okviri_skracenice_boja[$i] == $okviri_skracenice[$j])
									{
										$ukupan_broj_okvira++;
										if ($okviri_modul[$j] == 33)
										{
											$okviri_modul[$j] = "3+3";
											}
                                        if($_SESSION['prava']=='reseller')
                                        {
                                            echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2"><s>'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</s><br>'.number_format($okviri_cena[$j] * $discount, 2, ',', ' ').' '.$currency.'</td></tr>';

                                        }
                                        else
										echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2">'.number_format($okviri_cena[$j], 2, ',', ' ').'</td></tr>';
									}
									$j++;
								}
								
								  echo '<input type="hidden" id = "skriven" data-ukupan-broj-okvira="'.$brojanje_svi_okviri.'">';
								
							echo '</table>';//TABELA PODACI OKVIRA
							
					echo '</div>'; // kraj boje okvira
				
			}	
	$i++;
}
echo '</div>'; // kraj DIV vrsta_mehanizmi

?>
</div> <!-- KRAJ class=.PROIZVODI id=KVIRI -->
</div><!-- KRAJ SKROLA U LEVOJ-->
</div>	<!-- KRAJ DIV LEVA -->
<!-- DESNI DEO STRANICE -->
<div class = "desna">
<div class = "desna_dugme_kreiranje"></div>

<?php 

// PRAVI SE NIZ NOSAČA
$i=0;
while ($row=$stmt24->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_opis[$i] = $row['interni_opis'];
	$nosaci_kat_br[$i] = $row['kat_br'];
	$nosaci_kratak_opis[$i] = $row['opis_engleski'];
	//$nosaci_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$nosaci_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci=$i;
}

echo '<ul class="podmeni_nosaci">';
echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'"><a><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$nosaci_kat_br[0].'.png" id=2 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2 MODULES</p><p class = "nosac_slova_2_broj">2</p><p class = "nosac_slova_2">MODULES</p></a></li>';
echo '<li class="slika_nav_x_modula" id=3 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'"><a><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$nosaci_kat_br[1].'.png" id=3 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">3 MODULES</p><p class = "nosac_slova_2_broj">3</p><p class = "nosac_slova_2">MODULES</p></a></li>';
echo '<li class="slika_nav_x_modula" id=4 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'"><a><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$nosaci_kat_br[2].'.png" id=4 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">4 MODULES</p><p class = "nosac_slova_2_broj">4</p><p class = "nosac_slova_2">MODULES</p></a></li>';
echo '<li class="slika_nav_x_modula" id=7 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'"><a><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$nosaci_kat_br[3].'.png" id=7 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">7 MODULES</p><p class = "nosac_slova_2_broj">7</p><p class = "nosac_slova_2">MODULES</p></a></li>';
/*echo '<li><a><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$nosaci_kat_br[4].'.jpg" class="slika_nav_x_modula" id=33 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'"><br> 3+3 MODULA</a></li>';*/
echo '</ul>';
//polja za reseller
echo '<div id="prava" data-prava = "'.$prava.'"></div>';
echo '<div id="discount" data-discount = "'.$discount.'"></div>';


?>

<!-- DESNI SCROLL BAR -->
<div class="ex2">
<div class = "desna_mehanizmi"></div>
</div><!-- KRAJ DESNOG SCROLL BARA -->

<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_air/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_air/skripta_air.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_air/upravljanje_eng_air.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_air/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>