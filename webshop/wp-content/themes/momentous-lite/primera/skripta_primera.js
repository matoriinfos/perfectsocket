$('.proizvodi').hide();
$('.proizvodi#prekidaci').show();
$('.glavni_meni#link_prekidaci_tasteri').css({ 'color': '#125185'});
$('.link_moduli#link_svi').css({'color':'#125185'});
var naslovLinka = $('#link_prekidaci').text()
$('.naslov_proizvoda').text(naslovLinka);
//$('.naslov_proizvoda').show();


//////////////////
// NAVIGACIJA JEZICIMA
/////////////////
/*
$('body').on('click','#brit2', function() {

if (confirm('Attention: All previously will be deleted. Are you sure?')) {
        window.location = "?page_id=598";
    }
});

$('body').on('click','#srb2', function() {

if (confirm('Pažnja: Sve prethodno biće obrisano. Da li ste sigurni?')) {
        window.location = "?page_id=602&lang=sr";
    }
});

// PRELAZAK PREKO ZASTAVICE SRB2
$('body').on('mouseover','#srb2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/srb.jpg';
	$(this).css('cursor','pointer');
});

$('body').on('mouseout','#srb2', function() {

	this.src = 'wp-content/themes/momentous-lite/flags/srbDark.jpg';
});

// KRAJ PRELASKA PREKO ZASTAVICE SRB2
////
// PRELAZAK PREKO ZASTAVICE BRIT2
$('body').on('mouseover','#brit2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/brit.jpg';
	$(this).css('cursor','pointer');
});

$('body').on('mouseout','#brit2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/britDark.jpg';
});

*/

// IZBOR JEZIKA
$('.dropdown-content a').click(function(){
   jezik = $(this).attr('id');
   //alert ("jezik je:" +jezik);
   $('.navigacija').hide();
   $('.navigacija#'+jezik).show();
   //NAVIGACIJA NOSACA
   $('.podmeni_nosaci').hide();
   $('.podmeni_nosaci#'+jezik).show();
   //NASLOVI KOMPLETA NA DESNOJ STRANI STRANICE
$('.naslov_kompleta').hide();
$('.naslov_kompleta#'+jezik).show();
	//LEVI PODMENI KOJI DELI PROIZVODE NA MODULE
	$('.levi_podmeni').hide();
	$('.levi_podmeni#'+jezik).show();
	//NASLOVI MEHANIZAMA
$('.naslov_proizvoda').hide();
$('.naslov_proizvoda#'+jezik).show();
//IMENA SVIH MEHANIZAMA NA IZABRANOM JEZIKU
$('.mehanizam_velicine_1_modula').hide();
$('.mehanizam_velicine_1_modula#'+jezik).show();
$('.mehanizam_velicine_2_modula').hide();
$('.mehanizam_velicine_2_modula#'+jezik).show();
$('.mehanizam_velicine_3_modula').hide();
$('.mehanizam_velicine_3_modula#'+jezik).show();
$('.mehanizam_velicine_4_modula').hide();
$('.mehanizam_velicine_4_modula#'+jezik).show();
$('.mehanizam_velicine_6_modula').hide();
$('.mehanizam_velicine_6_modula#'+jezik).show();
$('.mehanizam_velicine_7_modula').hide();
$('.mehanizam_velicine_7_modula#'+jezik).show();
$('.mehanizam_velicine_-_modula').hide();
$('.mehanizam_velicine_-_modula#'+jezik).show();
/*KOMLETI MEHANIZAMA*/
$('.kompleti_mehanizama').hide();
$('.kompleti_mehanizama#'+jezik).show();
// NAZIV KOLIČINA U KOMPLETU KOJI SE MENJA PO IZBORU JEZIKA
$('.kolicina_naslov').hide();
$('.kolicina_naslov#'+jezik).show();

});

$('body').on('click','.glavni_meni#link_detektori_pokreta','li', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.dropdown-contentNovo', function() {
	
	$(".dropdown-contentNovo").hide();
});

$('body').on('mouseover','.dropbtnNovo', function() {
	
	$(".dropdown-contentNovo").show();
});


$('body').on('click','.glavni_meni#link_prekidaci','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.proizvodi#prekidaci').fadeIn(250, function(){ $(this).show();});;; // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);

});

$('body').on('click','.glavni_meni#link_tasteri','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.proizvodi#tasteri').fadeIn(250, function(){ $(this).show();});; // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);

});

$('body').on('click','.glavni_meni#link_dimeri','li', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': 'white'}); //sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); //link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#dimeri').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_detektori_pokreta','li', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': 'white'}); //sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); //link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_termostati_detektori','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#termostati_detektori').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_energetske_uticnice','li', function() {
		$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#energetske_uticnice').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_racunarske_uticnice','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.proizvodi#racunarske_uticnice').fadeIn(250, function(){ $(this).show();}); // vraca nosac;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_tv_audio_hdmi_uticnice','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#tv_audio_hdmi_uticnice').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_maske','li', function() {
		$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#maske').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});


$('body').on('click','.glavni_meni#link_kupatilo','li', function() {
		$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#kupatilo').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_kutije','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#kutije').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	$('.bticino_levi').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_ostala_oprema','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#ostala_oprema').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_vodootporno','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#vodootporno').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_nosaci_dozne','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#nosaci_dozne').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_okviri','li', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#okviri').fadeIn(250, function(){ $(this).show();});;
	//$(this).css({'color': '#125185'});
	$('.naslov_proizvoda').hide();
	$('.bticino_container').show();
	$('.naslov_proizvoda').show();
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});



// PODMENI NAVIGACIJA (LINKOVI: MODUL1 i MODUL2)
$('body').on('click','.link_moduli#link_svi', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.link_moduli').css({'color': '#d0d0d0'});
	$(this).css({'color': '#125185'});
	
	$('.mehanizam_velicine_1_modula_dodatno#line').fadeIn(400);
	$('.mehanizam_velicine_1_modula_dodatno#classic').fadeIn(400);
	//alert ("klik");
});

$('body').on('click','.link_moduli#link_1', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.link_moduli').css({'color': '#d0d0d0'});
	$(this).css({'color': '#125185'});
	$('.mehanizam_velicine_1_modula_dodatno#line').fadeIn(400);
	$('.mehanizam_velicine_1_modula_dodatno#classic').fadeOut(400);
});

$('body').on('click','.link_moduli#link_2', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.link_moduli').css({'color': '#d0d0d0'});
	$(this).css({'color': '#125185'});
	$('.mehanizam_velicine_1_modula_dodatno#line').fadeOut(400);
	$('.mehanizam_velicine_1_modula_dodatno#classic').fadeIn(400);
	//alert ("klik");
});