<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
<title>Niloe - Legrand - Configurator</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
<meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
<meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/niloe/style_niloe.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<?php
/*
Template Name: index_niloe_eng
*/
?>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>
<div class = "subMenu">
	<div class="bticino_container">
	<div class="bticino_logo"><img src="wp-content/themes/momentous-lite/logotipi/niloe_logo.jpg" alt="logo of the range"></div>
	<div class = "naslov_proizvoda"></div>
	</div>
	<div class = "naslov_kompleta">MY SELECTION</div>
</div>
<div class="leva">
<?php 

// KOD ZA BAZA NA NETU
$servername = servername;
$username = username;
$password = password;
$dbname = baza;

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM niloebojeokvira");
	$stmt2 = $conn->prepare("SELECT * FROM niloeokvirisvi");
	$stmt3 = $conn->prepare("SELECT * FROM niloeprekidaci");
	$stmt4 = $conn->prepare("SELECT * FROM niloetasteri");
	$stmt5 = $conn->prepare("SELECT * FROM niloedimeri");
	$stmt6 = $conn->prepare("SELECT * FROM niloedetektoripokreta");
	$stmt7 = $conn->prepare("SELECT * FROM niloetermostat");
	$stmt8 = $conn->prepare("SELECT * FROM niloeuticnice");
	$stmt9 = $conn->prepare("SELECT * FROM niloetvutcinice");
	$stmt10 = $conn->prepare("SELECT * FROM niloeracunarskeuticnice");
	$stmt11 = $conn->prepare("SELECT * FROM niloeoprema");
	$stmt12 = $conn->prepare("SELECT * FROM niloekompleti");
	
	$stmt1->execute();
	$stmt2->execute();
	$stmt3->execute();
	$stmt4->execute();
	$stmt5->execute();
	$stmt6->execute();
	$stmt7->execute();
	$stmt8->execute();
	$stmt9->execute();
	$stmt10->execute();
	$stmt11->execute();
	$stmt12->execute();
	
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();


}
?>
<div class="navigacija">
<ul class="glavni">
  <li class="glavni_meni" id="link_okviri"><a>FRAMES</a></li>
  <li class="glavni_meni" id="link_prekidaci"><a>SWITCHES</a></li>
  <li class="glavni_meni" id="link_tasteri"><a>PUSH-BUTTONS</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>DIMMERS</a></li>
  <li class="glavni_meni" id="link_detektori_pokreta"><a>MOTION DETECTORS</a></li>
  <li class="glavni_meni" id="link_termostati_detektori"><a>TERMOSTATS</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>ENERGY SOCKETS</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>COMPUTER SOCKETS</a></li>
  <li class="glavni_meni" id="link_tv_audio_hdmi_uticnice"><a>TV SOCKETS</a></li>
</ul>
</div>

<div class="dropdownNovo">
  <button class="dropbtnNovo">Wiring devices</button>
  <div class="dropdown-contentNovo">
  <a class="glavni_meni" id="link_okviri">FRAMES</a>
  <a class="glavni_meni" id="link_prekidaci">SWITCHES</a>
  <a class="glavni_meni" id="link_tasteri">PUSH-BUTTONS</a>
  <a class="glavni_meni" id="link_dimeri">DIMMERS</a>
  <a class="glavni_meni" id="link_detektori_pokreta">MOTION DETECTORS</a>
  <a class="glavni_meni" id="link_termostati_detektori">TERMOSTATS</a>
  <a class="glavni_meni" id="link_energetske_uticnice">ENERGY SOCKETS</a>
  <a class="glavni_meni" id="link_racunarske_uticnice">COMPUTER SOCKETS</a>
  <a class="glavni_meni" id="link_tv_audio_hdmi_uticnice">TV SOCKETS</a>
  </div>
</div>


<div class="bticino_levi">
<ul class = "levi_podmeni">
	<li><a class="link_moduli" id="link_svi">BOTH COLORS</a></li>
	<li><a class="link_moduli" id="link_2">IVORY</a></li>
	<li><a class="link_moduli" id="link_1">WHITE</a></li>
</ul>
</div>

<!-- LEVI SCROLL BAR -->
<div class="ex1">
<div class = "proizvodi" id = "prekidaci">
<?php
// VREDNOST EVRA U DINARIMA
$money = 1;
$currency = "EUR";
// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_opis[$i] = $row['opis_srpski'];
	$prekidaci_kat_br[$i] = $row['kat_br'];
	$prekidaci_opis_jezik[$i] = $row['opis_engleski'];
	$prekidaci_cena[$i] = $row['vp_cena_evro']/$money;
	$prekidaci_boja[$i] = $row['boja'];
	$i++;
	$brojanje_prekidaca = $i;
}
?>


<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_prekidaca)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$prekidaci_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$prekidaci_kat_br[$i].'" data-opis-interni = "'.$prekidaci_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_cena[$i].'" data-opis ="'.$prekidaci_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$prekidaci_opis_jezik[$i].'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$prekidaci_kat_br[$i].'" data-opis-interni = "'.$prekidaci_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_cena[$i].'" data-opis ="'.$prekidaci_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($prekidaci_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>
</div>	<!-- KRAJ DIV PROIZVODI PREKIDACI -->

<div class = "proizvodi" id = "tasteri">
<?php
// PRAVI SE NIZ SVIH TASTERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt4->fetch(PDO::FETCH_ASSOC))
{	
	$tasteri_opis[$i] = $row['opis_srpski'];
	$tasteri_kat_br[$i] = $row['kat_br'];
	$tasteri_opis_jezik[$i] = $row['opis_engleski'];
	$tasteri_cena[$i] = $row['vp_cena_evro']/$money;
	$tasteri_boja[$i] = $row['boja'];
	$i++;
	$brojanje_tastera=$i;
}
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE TASTERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_tastera)
{
	$red_br = $red_br + 1;
	$string = $tasteri_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula", data-boja="'.$tasteri_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$tasteri_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$tasteri_kat_br[$i].'" data-opis-interni = "'.$tasteri_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tasteri_cena[$i].'" data-opis ="'.$tasteri_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$tasteri_opis_jezik[$i].'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$tasteri_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$tasteri_kat_br[$i].'" data-opis-interni = "'.$tasteri_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tasteri_cena[$i].'" data-opis ="'.$tasteri_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$tasteri_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$tasteri_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tasteri_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tasteri_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($tasteri_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>
</div><!-- KRAJ DIV PROIZVODI AKSIJALNI -->

<div class = "proizvodi" id = "dimeri">
<?php
// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt5->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_opis[$i] = $row['interni_opis'];
	$dimeri_kat_br[$i] = $row['kat_br'];
	$dimeri_opis_jezik[$i] = $row['opis_engleski'];
	$dimeri_cena[$i] = $row['vp_cena_evro']/$money;
	$dimeri_boja[$i] = $row['boja'];
	$i++;
	$brojanje_dimera=$i;
}
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE DIMERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_dimera)
{
	$red_br = $red_br + 1;
	$string = $dimeri_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$dimeri_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$dimeri_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$dimeri_kat_br[$i].'" data-opis-interni = "'.$dimeri_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_cena[$i].'" data-opis ="'.$dimeri_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$dimeri_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$dimeri_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$dimeri_kat_br[$i].'" data-opis-interni = "'.$dimeri_opis[$i].'" data-kat-br = "'.$dimeri_kat_br[$i].'" data-cena="'.$dimeri_cena[$i].'" data-opis ="'.$dimeri_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$dimeri_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$dimeri_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($dimeri_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM				
			}	
	$i++;
}
?>
</div>
</div><!-- KRAJ DIV PROIZVODI DIMERI -->

<!-- DETEKTORI POKRETA -->
<div class = "proizvodi" id = "detektori_pokreta">
<?php
// PRAVI SE NIZ SVIH DETEKTORA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt6->fetch(PDO::FETCH_ASSOC))
{	
	$detektori_opis[$i] = $row['interni_opis'];
	$detektori_kat_br[$i] = $row['kat_br'];
	$detektori_opis_jezik[$i] = $row['opis_engleski'];
	$detektori_cena[$i] = $row['vp_cena_evro']/$money;
	$detektori_boja[$i] = $row['boja'];
	$i++;
	$brojanje_detektori_pokreta=$i;
}
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE DETEKTORA POKRETA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_detektori_pokreta)
{
	$red_br = $red_br + 1;
	$string = $detektori_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			// DETEKTORI POKRETA - SRPSKI - 1 modul
			
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$detektori_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$detektori_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$detektori_kat_br[$i].'" data-opis-interni = "'.$detektori_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$detektori_cena[$i].'" data-opis ="'.$detektori_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$detektori_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$detektori_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$detektori_kat_br[$i].'" data-opis-interni = "'.$detektori_opis[$i].'" data-kat-br = "'.$detektori_kat_br[$i].'" data-cena="'.$detektori_cena[$i].'" data-opis ="'.$detektori_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$detektori_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$detektori_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($detektori_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($detektori_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($detektori_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div>	<!-- KRAJ DIV PROIZVODI - DETEKTORI POKRETA -->

<!-- TERMOSTATI I DETEKTORI GASA -->
<div class = "proizvodi" id = "termostati_detektori">

<?php
// PRAVI SE NIZ SVIH TERMOSTATA_DETEKTORA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt7->fetch(PDO::FETCH_ASSOC))
{	
	$termostati_opis[$i] = $row['interni_opis'];
	$termostati_kat_br[$i] = $row['kat_br'];
	$termostati_opis_jezik[$i] = $row['opis_engleski'];
	$termostati_cena[$i] = $row['vp_cena_evro']/$money;
	$termostati_boja[$i] = $row['boja'];
	$i++;
	$brojanje_termostata=$i;
}

//ISPISIVANJE DETEKTORA POKRETA - 2 MODULA

$i=0;
$red_br=0;
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_termostata)
{
	$red_br = $red_br + 1;
	$string = $termostati_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// TERMOSTATI I DETEKTORI NA SRPSKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$termostati_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$termostati_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$termostati_kat_br[$i].'" data-opis-interni = "'.$termostati_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$termostati_cena[$i].'" data-opis ="'.$termostati_opis_jezik[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$termostati_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$termostati_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$termostati_kat_br[$i].'" data-opis-interni = "'.$termostati_opis[$i].'" data-kat-br = "'.$termostati_kat_br[$i].'" data-cena="'.$termostati_cena[$i].'" data-opis ="'.$termostati_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$termostati_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$termostati_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($termostati_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($termostati_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($termostati_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ DIV PROIZVODI - TERMOSTATI DETEKTORI -->

<!-- ENERGETSKE UTICNICE -->
<div class = "proizvodi" id = "energetske_uticnice">

<?php
// PRAVI SE NIZ SVIH ENERGETSKIH UTICNICA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt8->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$energetske_uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	$energetske_uticnice_boja[$i] = $row['boja'];
	$i++;
	$brojanje_energetske_uticnice = $i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ENERGETSKE UTICNICE- 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_energetske_uticnice)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
								// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$energetske_uticnice_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$energetske_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_cena[$i].'" data-opis ="'.$energetske_uticnice_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$energetske_uticnice_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$energetske_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_opis[$i].'" data-kat-br = "'.$energetske_uticnice_kat_br[$i].'" data-cena="'.$energetske_uticnice_cena[$i].'" data-opis ="'.$energetske_uticnice_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$energetske_uticnice_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$energetske_uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($energetske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($energetske_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($energetske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
				
			}	
	$i++;
}
?>
</div>
</div> <!-- KRAJ DIV ENERGETSKE UTICNICE -->

<!-- RACUNARSKE UTICNICE -->
<div class = "proizvodi" id = "racunarske_uticnice">

<?php
// PRAVI SE NIZ SVIH RACUNARSKIH UTICNICA - VELICINE 1 MODULA

$i=0;
while ($row=$stmt10->fetch(PDO::FETCH_ASSOC))
{	
	$racunarske_uticnice_opis[$i] = $row['opis_srpski'];
	$racunarske_uticnice_kat_br[$i] = $row['kat_br'];
	$racunarske_uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$racunarske_uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	$racunarske_uticnice_boja[$i] = $row['boja'];
	$i++;
	$brojanje_racunarske_uticnice = $i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
$i = 0;
$red_br = 0;
while ($i< $brojanje_racunarske_uticnice)
{
	$red_br = $red_br + 1;
	$string = $racunarske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$racunarske_uticnice_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$racunarske_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$racunarske_uticnice_kat_br[$i].'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-opis ="'.$racunarske_uticnice_opis_jezik[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$racunarske_uticnice_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$racunarske_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$racunarske_uticnice_kat_br[$i].'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$racunarske_uticnice_kat_br[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-opis ="'.$racunarske_uticnice_opis_jezik[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div div class="tekst_katbr">'.$racunarske_uticnice_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$racunarske_uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($racunarske_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div>	<!-- KRAJ DIV  -->

<!-- TV/AUDIO/HDMI UTICNICE -->
<div class = "proizvodi" id = "tv_audio_hdmi_uticnice">
<?php
// PRAVI SE NIZ SVIH TV/AUDIO/HDMI UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt9->fetch(PDO::FETCH_ASSOC))
{	
	$tv_uticnice_opis[$i] = $row['opis_srpski'];
	$tv_uticnice_kat_br[$i] = $row['kat_br'];
	$tv_uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$tv_uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	$tv_uticnice_boja[$i] = $row['boja'];
	$i++;
	$brojanje_tv_audio_hdmi_uticnice=$i;
}

?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE TV/AUDIO/HDMIA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_tv_audio_hdmi_uticnice)
{
	$red_br = $red_br + 1;
	$string = $tv_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_uticnice_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 		
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}	
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$tv_uticnice_boja[$i].'" id='.$prekidaci_boja[$i].' title="'.$tv_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$tv_uticnice_kat_br[$i].'" data-opis-interni = "'.$tv_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_uticnice_cena[$i].'" data-opis ="'.$tv_uticnice_opis_jezik[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$tv_uticnice_opis_jezik[$i].'" class="slika_velicine_x_modula" title="'.$tv_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_uticnice_kat_br[$i].'" data-opis-interni = "'.$tv_uticnice_opis[$i].'" data-kat-br = "'.$tv_uticnice_kat_br[$i].'" data-cena="'.$tv_uticnice_cena[$i].'" data-opis ="'.$tv_uticnice_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$tv_uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($tv_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
?>
</div>

</div> <!-- KRAJ DIV TV/AUDIO/HDMI UTICNICE -->

<!-- OSTALA OPREMA -->
<div class = "proizvodi" id = "ostala_oprema">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt11->fetch(PDO::FETCH_ASSOC))
{	
	$ostala_oprema_opis[$i] = $row['opis_srpski'];
	$ostala_oprema_kat_br[$i] = $row['kat_br'];
	$ostala_oprema_opis_jezik[$i] = $row['opis_engleski'];
	$ostala_oprema_pozicija[$i] = $row['pozicija'];
	$ostala_oprema_cena[$i] = $row['vp_cena_evro']/$money;
	$broj_modula[$i] = $row['mehanizam'];
	$i++;
	$brojanje_ostala_oprema=$i;
}

//ISPISIVANJE OSTALE OPREME
?>

<div class = "vrsta_mehanizmi">
<?php
$i=0;
$red_br=0;
while ($i< $brojanje_ostala_oprema)
{
	$red_br = $red_br + 1;
	$string = $ostala_oprema_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				
					if ($br_modula == "11")
				{
					$br_modula_id = "jedan_jedan";
				}
				
				// DODATNA OPREMA - SRPSKI
				
				
					// DODATNE LAMPE ZA MASKE

					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
						echo '<img src = "wp-content/themes/momentous-lite/niloe/slike/'.$newstring.'.png" alt = "'.$ostala_oprema_opis_jezik[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$ostala_oprema_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$ostala_oprema_kat_br[$i].'" data-opis-interni = "'.$ostala_oprema_opis[$i].'" data-pozicija = "'.$ostala_oprema_pozicija[$i].'" data-kat-br = "'.$ostala_oprema_kat_br[$i].'" data-cena="'.$ostala_oprema_cena[$i].'" data-opis ="'.$ostala_oprema_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$ostala_oprema_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$ostala_oprema_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($ostala_oprema_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					

			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ OSTALA OPREMA -->

<!-- OKVIRI -->
<div class = "proizvodi" id = "okviri">
<?php

// PRAVI SE NIZ SVIH BOJA OKVIRA
$i=0;

$brojanje_okviri;

while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_boje[$i] = $row['boje'];
	$okviri_nazivi[$i] = $row['naziv_engleski'];
	$okviri_skracenice_boja[$i] = $row['skracenica'];
	$i++;
	$brojanje_okviri=$i;
}

// PRAVI SE NIZ SVIH OKVIRA
$j=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_kat_br[$j] = $row['kat_br'];
	$okviri_cena[$j] = $row['vp_cena_evro']/$money;
	$okviri_opis[$j] = $row['opis_srpski'];
	$okviri_modul[$j] = $row['mehanizam'];
	$okviri_kratak_opis[$j] = $row['opis_engleski'];
	$okviri_materijal[$j] = $row['materijal'];
	$okviri_skracenice[$j] = $row['skracenica'];
	$okviri_okviri_boje[$j] = $row['color'];
	$okviri_opis_jezik[$j] = $row['opis_engleski'];
	$j++;
	$brojanje_svi_okviri=$j;
}

?>

<div class = "vrsta_mehanizmi" id="okviri_vrsta">
<?php

//ISPISIVANJE BOJA OKVIRA

$i=0;
$red_br=0;
$ukupan_broj_okvira = 0;

while ($i< $brojanje_okviri)
{
	$red_br = $red_br + 1;
	$string = $okviri_boje[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				//$br_modula = 1;
			
					echo '<div class = "boje_okvira" id="skracenica_'.$okviri_skracenice_boja[$i].'">';
						echo '<div class = "slika_tekst">';
						echo '<img class="okvir_slika" src = "wp-content/themes/momentous-lite/niloe/slike/okviri/'.$newstring.'.jpg" alt="'.$okviri_kratak_opis[$i].'" title="'.$okviri_nazivi[$i].'" id="okvir_slika" data-opis="'.$okviri_kratak_opis[$i].'" data-skracenica="'.$okviri_skracenice_boja[$i].'">';
						echo '<h2 id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'"><span>'.$okviri_nazivi[$i].'</span></h2>';
						echo '</div>';

						echo '<table class="podaci_okvira" id="podaci_okvira_'.$okviri_skracenice_boja[$i].'">'; //TABELA PODACI OKVIRA								
								
								echo '<tr><td class="kolona_modul"><span class="fullText">MECHANISM</span><span class="shortText">MCHN.</span></td><td class="kolona_kat_br">REF.NO.</td><td class = "kolona_cena_2">PRICE</td></tr>';								
								
								//PRIKAZIVANJE CENE - OPISA
								$j=0;
								while ($j < $brojanje_svi_okviri)
								{
									if($okviri_skracenice_boja[$i] == $okviri_skracenice[$j])
									{
										$ukupan_broj_okvira++;
										if ($okviri_modul[$j] == 33)
										{
											$okviri_modul[$j] = "3+3";
										}
										if ($okviri_cena[$j] == 0 )
										{
											echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis_jezik[$j].'">-</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2">-</td></tr>';
										}
										else
										{
                                            if($_SESSION['prava']=='reseller')
                                            {
                                                echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2"><s>'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</s><br>'.number_format($okviri_cena[$j] * $discount, 2, ',', ' ').' '.$currency.'</td></tr>';

                                            }
                                            else
										echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis_jezik[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2">'. number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</td></tr>';
										}	
									}
									$j++;
								}
								
								  echo '<input type="hidden" id = "skriven" data-ukupan-broj-okvira="'.$brojanje_svi_okviri.'">';
								
							echo '</table>';//TABELA PODACI OKVIRA
							
					echo '</div>'; // kraj boje okvira
				
			}	
	$i++;
}
?>

</div>
</div> <!-- KRAJ class=PROIZVODI id=OKVIRI -->
</div> <!-- KRAJ LEVOG SCROLL BARA -->
</div><!-- KRAJ DIV LEVA -->
<!-- DESNI DEO STRANICE -->
<div class = "desna">
<div class = "desna_dugme_kreiranje"></div>

<?php 
// PRAVI SE NIZ NOSAČA
$i=0;
while ($row=$stmt12->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_opis[$i] = $row['opis_engleski'];
	$nosaci_kat_br[$i] = $row['kat_br'];
	$nosaci_cena[$i] = $row['vp_cena_evro']/$money;
	$broj_modula[$i] = $row['mehanizam'];
	$i++;
	$brojanje_nosaci=$i;
}
echo '<ul class="podmeni_nosaci">';
echo '<li class="slika_nav_x_modula" id=1 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_opis[0].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[0].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">1 MECHANISM</p><p class = "nosac_slova_2_broj">1</p><p class = "nosac_slova_2">MECHANISM</p></a></li>';
echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_opis[1].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[1].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2 MECHANISMS</p><p class = "nosac_slova_2_broj">2</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '<li class="slika_nav_x_modula" id=3 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_opis[2].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[2].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">3 MECHANISMS</p><p class = "nosac_slova_2_broj">3</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '<li class="slika_nav_x_modula" id=4 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_opis[3].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[3].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">4 MECHANISMS</p><p class = "nosac_slova_2_broj">4</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '</ul>';
//polja za reseller
echo '<div id="prava" data-prava = "'.$prava.'"></div>';
echo '<div id="discount" data-discount = "'.$discount.'"></div>';
?>

<!-- DESNI SCROLL BAR -->
<div class="ex2">
<div class = "desna_mehanizmi"></div>
</div><!-- KRAJ DESNOG SCROLL BARA -->

</div>

<?php 
/*require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_3/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/niloe/skripta_niloe.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/niloe/upravljanje_niloe_eng.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_3/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>