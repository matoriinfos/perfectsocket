<!DOCTYPE html><!-- HTML 5 -->
<html>
<head>
<title>Report - PerfectSocket.com</title>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<link rel="pingback" href = "<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/styleReportTabela.css">
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');?>
<?php
/*
Template Name: index_pregled_novo
*/
?>


<?php 
// KOD ZA BAZA NA NETU
$servername = "localhost";
$username = "ultrars_sale";
$password = "sale99999";
$dbname = "ultrars_wpdbul12";

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM izbor_korisnika");
	$stmt1->execute();
}
catch(PDOException $e) {
//echo "Error: " . $e->getMessage();
}

// PRAVI SE NIZ SVIH PROIZVODA
$i=0;

	while ($row = $stmt1->fetch(PDO::FETCH_ASSOC))
		{	
		$niz_izbora_kat_br[$i] = $row['kat_br'];
		$niz_izbora_opisa[$i] = $row['kratak_opis'];
		$niz_izbora_cena[$i] = $row['jed_cena'];
		$niz_izbora_kolicina[$i] = $row['kolicina_kreiranja'];
		$niz_izbora_meseca[$i] = $row['mesec_kreiranja'];
		$niz_izbora_godina[$i] = $row['godina_kreiranja'];
		$i++;
		}
		
$brojProizvoda = count($niz_izbora_kat_br);	
//uzimanje imena fajlova iz direktorijuma i uporedjivanje sa nazivom iz niza

/*ZA LIVINGLIGHT*/
$LivingLightDir = 'wp-content/themes/momentous-lite/livinglight_3/slike/';
$LivingLightFiles = scandir($LivingLightDir, 1);

/*ZA LIVINGLIGHT OKVIRE*/
$LivingLightOkviriDir = 'wp-content/themes/momentous-lite/livinglight_3/slike/okviri/';
$LivingLightOkviriFiles = scandir($LivingLightOkviriDir, 1);

/*ZA NILOE MEHANIZME*/
$NiloeDir = 'wp-content/themes/momentous-lite/niloe/slike/';
$NiloeFiles = scandir($NiloeDir, 1);

/*ZA NILOE OKVIRE*/
$NiloeOkviriDir = 'wp-content/themes/momentous-lite/niloe/slike/okviri/';
$NiloeOkviriFiles = scandir($NiloeOkviriDir, 1);

/*ZA NILOE DOZNE*/
$NiloeDozneDir = 'wp-content/themes/momentous-lite/niloe/slike/';
$NiloeDozneFiles = scandir($NiloeDozneDir, 1);

/*ZA INTERIO*/
$InterioDir = 'wp-content/themes/momentous-lite/interio/slike/';
$InterioFiles = scandir($InterioDir, 1);
//echo "<script type='text/javascript'>alert('$InterioFiles[3]');</script>";

/*ZA INTERIO OKVIRE*/
$InterioOkviriDir = 'wp-content/themes/momentous-lite/interio/slike/okviri/';
$InterioOkviriFiles = scandir($InterioOkviriDir, 1);

$sumOfElementsLivingLight = count($LivingLightFiles);
$sumOfElementsLivingLightOkviri = count($LivingLightOkviriFiles);
$sumOfElementsNiloe = count($NiloeFiles);
$sumOfElementsNiloeOkviri = count($NiloeOkviriFiles);
$sumOfElementsNiloeDozne = count($NiloeDozneFiles);
$sumOfElementsInterio = count($InterioFiles);
$sumOfElementsInterioOkviri = count($InterioOkviriFiles);
?>

<div class = "tableContainer">
<table class = "tabelaIzbora" id="tabelaLivingLight">

<?php
$i = 0;
$nazivMeseca = "";
$imeSerije = "livinglight";
echo '<tr class="velikiNaslov" id='.$imeSerije.' data-brojProizvoda = '.$brojProizvoda.'><td colspan="5">TOTAL NUMBER OF CREATED PRODUCTS - BTICINO LIVINGLIGHT</td></tr>';
while ($i < $brojProizvoda)
{
	if($nazivMeseca != $niz_izbora_meseca[$i])
	{
		$idBroj = $i;
		$nazivMeseca = $niz_izbora_meseca[$i];
		echo '<tr class = "prikazRezultataNaslovMesec" id= '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.' data-brojProizvoda = '.$brojProizvoda.'>';
		echo '<td colspan="5">'.$nazivMeseca.' - '.$niz_izbora_godina[$i].'</td>';
		echo '</tr>';
		echo '<tr class="prikazRezultataNaslov" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.'>';
		echo '<td>No.</td>';
		echo '<td>Product</td>';
		echo '<td>Description</td>';
		echo '<td>Quantity</td>';
		echo '</tr>';
		$k=0;
	}
	
	$string = $niz_izbora_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	

	echo '<tr class = "prikazRezultataVrsta" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.' >';
	echo '<td class = "prikaz-rbr">'.($k+1).'</td>';
	echo '<td class = "prikazSlika">';
	
	/*PROVERA DA LI SE POKLAPA I AKO SE POKLAPA ONDA ISPISIVANJE*/
	$j=0;
	$indikacijaPoklapanja = 0;
	
	/*za livinglight*/
	while( $j < $sumOfElementsLivingLight)
		{
			$LivingLightFiles[$j] = str_replace(".jpg", "", $LivingLightFiles[$j]);	
			if((strcasecmp($LivingLightFiles[$j], $newstring) == 0))
			{
				echo $LivingLightFiles[$j];
				echo '<div><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$LivingLightFiles[$j].'.jpg" class="slika_velicine_x_modula" title = "livinglight" dataSerija = "livinglight"></div>';
				$indikacijaPoklapanja = 1;
			}
			$j++;	
		}

		/*za okvire i nosace*/
		if ($indikacijaPoklapanja == 0)
		{
			$j=0;
			while( $j < $sumOfElementsLivingLightOkviri)
			{
				$LivingLightOkviriFiles[$j] = str_replace(".png", "", $LivingLightOkviriFiles[$j]);	
				if((strcasecmp($LivingLightOkviriFiles[$j], $newstring) == 0))
					{
					echo $LivingLightOkviriFiles[$j];
					echo '<div><img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$LivingLightOkviriFiles[$j].'.png" class="slika_velicine_x_modula" title = "livinglight" dataSerija = "livinglight"></div>';
					$indikacijaPoklapanja = 1;
					}
			$j++;	
			}
		}	
			
	echo '</td>';
	echo '<td class = "prikaz-opis">'.$niz_izbora_opisa[$i].'</td>';
	echo '<td class = "prikaz-kolicina">'.$niz_izbora_kolicina[$i].'</td>';
	echo '</tr>';
	
	$i++;
	$k++;
}


$conn = null;
?>
</table> <!-- tabela izbora LivingLight -->


<!-- TABELA NILOE -->
<table class = "tabelaIzbora" id="tabelaNiloe">
<?php



//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt2 = $conn->prepare("SELECT * FROM izbor_korisnika_niloe");
	$stmt2->execute();
}
catch(PDOException $e) {
}

// PRAVI SE NIZ SVIH PROIZVODA
$i=0;
$brojProizvoda_niloe = 0;

while ($row = $stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$niz_izbora_kat_br_niloe[$i] = $row['kat_br'];
	$niz_izbora_opisa_niloe[$i] = $row['kratak_opis'];
	$niz_izbora_cena_niloe[$i] = $row['jed_cena'];
	$niz_izbora_kolicina_niloe[$i] = $row['kolicina_kreiranja'];
	$niz_izbora_meseca_niloe[$i] = $row['mesec_kreiranja'];
	$i++;
	$brojProizvoda_niloe = $i;
	
}

//$brojProizvoda_niloe = count($niz_izbora_kat_br_niloe);	

$i = 0;
$nazivMeseca = "";
$imeSerije = "niloe";
echo '<tr class="velikiNaslov"><td colspan="5">TOTAL NUMBER OF CREATED PRODUCTS - LEGRAND NILOE</td></tr>';
while ($i < $brojProizvoda_niloe)
{
	if($nazivMeseca != $niz_izbora_meseca_niloe[$i])
	{
		$idBroj = $i;
		$nazivMeseca = $niz_izbora_meseca_niloe[$i];
		echo '<tr class = "prikazRezultataNaslovMesec" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.'>';
		echo '<td colspan="5">'.$nazivMeseca.' - '.$niz_izbora_godina[$i].'</td>';
		echo '</tr>';
		echo '<tr class="prikazRezultataNaslov" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.'>';
		echo '<td>No.</td>';
		echo '<td>Product</td>';
		echo '<td>Description</td>';
		echo '<td>Quantity</td>';
		echo '</tr>';
		$k=0;
	}
	
	$string = $niz_izbora_kat_br_niloe[$i];
    $newstring = str_replace("/", "_", $string);	

	echo '<tr class = "prikazRezultataVrsta" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.' >';
	echo '<td class = "prikaz-rbr">'.($k+1).'</td>';
	echo '<td class = "prikazSlika">';
	
	/*PROVERA DA LI SE POKLAPA I AKO SE POKLAPA ONDA ISPISIVANJE*/
	$j=0;
	$indikacijaPoklapanja = 0;
	
	/*za Niloe mehanizme*/
	while( $j < $sumOfElementsNiloe)
		{
			$NiloeFiles[$j] = str_replace(".png", "", $NiloeFiles[$j]);	
			//if((strcasecmp($NiloeFiles[$j], $newstring) == 0))
			if($NiloeFiles[$j] == $newstring)
			{
				echo $newstring;
				//echo $NiloeFiles[$j];
				echo '<div><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$NiloeFiles[$j].'.png" class="slika_velicine_x_modula" title = "niloe" dataSerija = "niloe"></div>';
				$indikacijaPoklapanja = 1;
			}
			$j++;	
		}

		/*za Niloe okvire*/
		if ($indikacijaPoklapanja == 0)
		{
			$j=0;
			while( $j < $sumOfElementsNiloeOkviri)
			{
				$NiloeOkviriFiles[$j] = str_replace(".png", "", $NiloeOkviriFiles[$j]);	
				if((strcasecmp($NiloeOkviriFiles[$j], $newstring) == 0))
					{
					echo $NiloeOkviriFiles[$j];
					echo '<div><img src = "wp-content/themes/momentous-lite/niloe/slike/okviri/'.$NiloeOkviriFiles[$j].'.png" class="slika_velicine_x_modula" title = "niloe" dataSerija = "niloe"></div>';
					$indikacijaPoklapanja = 1;
					}
			$j++;	
			}
		}	
			
	echo '</td>';
	echo '<td class = "prikaz-opis">'.$niz_izbora_opisa_niloe[$i].'</td>';
	echo '<td class = "prikaz-kolicina">'.$niz_izbora_kolicina_niloe[$i].'</td>';
	echo '</tr>';
	
	$i++;
	$k++;
}

$conn = null;
?>
</table> <!-- tabela izbora Niloe -->


<!-- TABELA INTERIO -->
<table class = "tabelaIzbora" id="tabelaInterio">
<?php



//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt2 = $conn->prepare("SELECT * FROM izbor_korisnika_interio");
	$stmt2->execute();
}
catch(PDOException $e) {
}

// PRAVI SE NIZ SVIH PROIZVODA
$i=0;
$brojProizvoda_interio = 0;

while ($row = $stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$niz_izbora_kat_br_interio[$i] = $row['kat_br'];
	$niz_izbora_opisa_interio[$i] = $row['kratak_opis'];
	$niz_izbora_cena_interio[$i] = $row['jed_cena'];
	$niz_izbora_kolicina_interio[$i] = $row['kolicina_kreiranja'];
	$niz_izbora_meseca_interio[$i] = $row['mesec_kreiranja'];
	$i++;
	$brojProizvoda_interio = $i;
	
}

//$brojProizvoda_interio = count($niz_izbora_kat_br_interio);	

$i = 0;
$nazivMeseca = "";
$imeSerije = "interio";
echo '<tr class="velikiNaslov"><td colspan="5">TOTAL NUMBER OF CREATED PRODUCTS - NOPAL INTERIO</td></tr>';
while ($i < $brojProizvoda_interio)
{
	if($nazivMeseca != $niz_izbora_meseca_interio[$i])
	{
		$idBroj = $i;
		$nazivMeseca = $niz_izbora_meseca_interio[$i];
		echo '<tr class = "prikazRezultataNaslovMesec" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.'>';
		echo '<td colspan="5">'.$nazivMeseca.' - '.$niz_izbora_godina[$i].'</td>';
		echo '</tr>';
		echo '<tr class="prikazRezultataNaslov" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.'>';
		echo '<td>No.</td>';
		echo '<td>Product</td>';
		echo '<td>Description</td>';
		echo '<td>Quantity</td>';
		echo '</tr>';
		$k=0;
	}
	$string = $niz_izbora_kat_br_interio[$i];
    $newstring = str_replace("/", "_", $string);	
	echo '<tr class = "prikazRezultataVrsta" id = '.$imeSerije.''.$idBroj.' data-serija = '.$imeSerije.' >';
	echo '<td class = "prikaz-rbr">'.($k+1).'</td>';
	echo '<td class = "prikazSlika">';
	
	/*PROVERA DA LI SE POKLAPA I AKO SE POKLAPA ONDA ISPISIVANJE*/
	$j=0;
	$indikacijaPoklapanja = 0;
	
	/*za interio mehanizme*/
	while( $j < $sumOfElementsInterio)
		{
			$InterioFiles[$j] = str_replace(".jpg", "", $InterioFiles[$j]);	
			if($InterioFiles[$j] == $newstring)
			{
				//echo "<script type='text/javascript'>alert('$InterioFiles[$j]');</script>";
				//echo $newstring;
				echo $InterioFiles[$j];
				echo '<div><img src = "wp-content/themes/momentous-lite/interio/slike/'.$InterioFiles[$j].'.jpg" class="slika_velicine_x_modula" title = "interio" dataSerija = "interio"></div>';
				$indikacijaPoklapanja = 1;
			}
			$j++;	
		}

		/*za interio okvire*/
		if ($indikacijaPoklapanja == 0)
		{
			$j=0;
			while( $j < $sumOfElementsInterioOkviri)
			{
				$InterioOkviriFiles[$j] = str_replace(".png", "", $InterioOkviriFiles[$j]);	
				if((strcasecmp($InterioOkviriFiles[$j], $newstring) == 0))
					{
					echo $InterioOkviriFiles[$j];
					echo '<div><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$InterioOkviriFiles[$j].'.png" class="slika_velicine_x_modula" title = "interio" dataSerija = "interio"></div>';
					$indikacijaPoklapanja = 1;
					}
			$j++;	
			}
		}	
			
	echo '</td>';
	echo '<td class = "prikaz-opis">'.$niz_izbora_opisa_interio[$i].'</td>';
	echo '<td class = "prikaz-kolicina">'.$niz_izbora_kolicina_interio[$i].'</td>';
	echo '</tr>';
	
	$i++;
	$k++;
}

$conn = null;
?>
</table> <!-- tabela izbora interio -->



<table class = "tabelaNavigacija">
<tr><th class = "navigacMeni" >MENI(under construction)</th></tr>
<tr><td id = "zatvoriSve" >SVE SERIJE ON/OFF</td></tr>
<tr><td class = "dugmeMeni" id = "LivingLightBticino">LivingLight - BTICINO<td></tr>
<tr><td class = "dugmeMeni" id = "NiloeLegrand">Niloe - LEGRAND<td></tr>
<tr><td class = "dugmeMeni" id = "InterioNopal">Interio - NopalLUX<td></tr>
</table>
</div> <!--tableContainer --> 
<!--</div>--><!-- header-wrap -->
<!-- </div> --> <!-- hfeed -->

<div class = "razmak123"></div>
<?php
/*require_once('wp-content/themes/momentous-lite/lteme/parts/footer.php');*/
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');
?>
<script type="text/javascript" src="wp-content/themes/momentous-lite/upravljanje_tabelom.js"></script>
</body>
</html>