<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Showroom</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

    <div class="banner">
        <div class="container">
            <ul class="breadcrumb">
                 <li><a href="#">Home</a></li>
                 <li class="active">Showroom & flagship stores</li>
            </ul>
            <h1 class="banner__title">Showroom & flagship stores</h1>
        </div>
    </div>

    <section class="section section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-showroom"></i>
                    <h2 class="title mb--40">Title level 2</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--no-padding video-container--small-height">
        <div class="video-container">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="mb--40 clearfix">
                    <div class="col-lg-4 col-lg-offset-2 col-md-6">
                        <div class="title title--light title--transform-none mb--30">Title level 3</div>
                        <p>
                            Whatever the type of establishment, every detail counts
                            when it comes to building customer loyalty. High quality
                            service and comfort are there to ensure a maximum fill
                            rate. Whatever the type of establishment, every detail
                            counts when it comes to building customer loyalty. High
                            quality service and comfort are there to ensure a
                            maximum fill rate.
                        </p>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <img src="../../images/placeholders/showroom-1.jpg" class="img-responsive mb--20">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="../../images/placeholders/showroom-2.jpg" class="img-responsive">
                            </div>
                            <div class="col-xs-6">
                                <img src="../../images/placeholders/showroom-3.jpg" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 flex-v">
                    <a href="#" class="social social-modal-trigger social--inline social--neg mr--35 mr-xxs--8">
                       <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </a>

                    <a href="#" class="btn">Contact us</a>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--no-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 flex-v flex-v--left" data-eq="map-column">
                    <div class="section">
                        <div class="title title--xsmall text-color-1 mb--10">Showroom</div>
                        <h2 class="title mb--40">
                            Showroom name
                        </h2>
                        <div class="para--border-small para--space-top">
                            <div>Address 1</div>
                            <div>Address 2</div>
                            <div>Address 3</div>
                            <div>T 0000 0000</div>
                            <div>F 0000 0000</div>
                            <div>E-mail : legrand@legrand.com</div>
                            <div class="mb--10">Website : legrand.com</div>

                            <div class="mb--25">
                                <a href="#" class="social-link">
                                    <i class="fa fa-facebook text-color-1 mr--20" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="social-link">
                                    <i class="fa fa-pinterest-p text-color-1" aria-hidden="true"></i>
                                </a>
                            </div>

                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="social social-modal-trigger social--inline social--neg mr--35 mr-xxs--8">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="place-map" class="map map--big" data-eq="map-column"></div>
                </div>
            </div>
        </div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsNXLDa5MRSNp6HScDK1dFjt72a_wvt6k"></script>
        <script>
            var markers = [];

            function initMap() {
                google.maps.visualRefresh = true;
                var centerCoords = {lat: 48.860611, lng: 2.337644};
                var map = new google.maps.Map(document.getElementById('place-map'), {
                    zoom: 13,
                    center: centerCoords
                });


                // Markers
                var redMarker = defaultIcon = {
                    url: '../../images/btn-pin-fruity.png',
                    size: new google.maps.Size(32, 44),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                };

                // Marker
                var marker_demo_1 = new google.maps.Marker({
                    position: {lat: 48.854169, lng: 2.332599},
                    map: map,
                    icon: redMarker
                });
                markers.push(marker_demo_1);

                var marker_demo_2 = new google.maps.Marker({
                    position: {lat: 48.860611, lng: 2.337644},
                    map: map,
                    icon: redMarker
                });
                markers.push(marker_demo_2);
            }

            google.maps.event.addDomListener(window, 'load', initMap);
        </script>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>