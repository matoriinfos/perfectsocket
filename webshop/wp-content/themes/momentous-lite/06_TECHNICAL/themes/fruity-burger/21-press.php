<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Press</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="banner__title">Press</h1>

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Press</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<section class="section section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-arrows-ext"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna.
                    Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur.
                    Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui
                    officia deserunt mollit anim id est laborum.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
            <div class="row mb--100">
                <div class="col-sm-6">
                    <h3 class="title title--light mb--25">Title level 3</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat
                        cupidatat non proident, sunt in
                    </p>
                </div>
                <div class="col-sm-6">
                    <img class="img-responsive hidden-xs" src="../../images/content/img1.jpg">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="col-sm-6">
                    <h3 class="title title--light mb--25">Title level 3</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat
                        cupidatat non proident, sunt in
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-foursquare"></i>
        <h2 class="title">
            Latest news
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img4.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        News title legrand group's reference project...
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img5.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        News title legrand group's reference project...
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img6.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        News title legrand group's reference project...
                    </div>
                </a>
            </div>
        </div>
        <div class="text-center">
            <a href="#" class="btn-abs-more btn-abs-more--static">View all news</a>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-biglist"></i>
        <h2 class="title">
            Press kits
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img4.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <div class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                        </div>
                        DOSSIER DE PRESSE DÉVELOPPEMENT DURABLE
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img1.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <div class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                        </div>
                        Press kit
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img3.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <div class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                        </div>
                        Press kit
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img6.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <div class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                        </div>
                        Press kit
                    </div>
                </a>
            </div>
        </div>
        <div class="text-center">
            <a href="#" class="btn-abs-more btn-abs-more--static">View all press kits</a>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="section__header">
            <i class="lgpicto--title lgicon-btn-plug"></i>
            <h2 class="title mb--100">
                Press contacts
            </h2>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Headquarters</div>
                    <p class="text text--small mb--10">
                        Lorem ipsum dolor si amet
                        <br>00 000 - CITY
                        <br>+33 (0)5 55 06 87 87
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Entreprise</div>
                    <p class="text text--small mb--10">
                        Thomas Mayer
                        <br>thomas.mayer@legrand.fr
                        <br>+33 (0)7 57 43 05 83
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Marketing & product</div>
                    <p class="text text--small mb--10">
                        Kelly Manson
                        <br>kelly.manson@bticino.it
                        <br>+39 0884 497 342
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Finance</div>
                    <p class="text text--small mb--10">
                        George Walter
                        <br>georges.walter@legrand.fr
                        <br>+33 (0)1 74 24 16 98
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Headquarters</div>
                    <p class="text text--small mb--10">
                        Lorem ipsum dolor si amet
                        <br>00 000 - CITY
                        <br>+33 (0)5 55 06 87 87
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Entreprise</div>
                    <p class="text text--small mb--10">
                        Thomas Mayer
                        <br>thomas.mayer@legrand.fr
                        <br>+33 (0)7 57 43 05 83
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Marketing & product</div>
                    <p class="text text--small mb--10">
                        Kelly Manson
                        <br>kelly.manson@bticino.it
                        <br>+39 0884 497 342
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 mb--40">
                <div class="border-bottom" data-eq="press-contact">
                    <div class="title title--medium title--light mb--15">Finance</div>
                    <p class="text text--small mb--10">
                        George Walter
                        <br>georges.walter@legrand.fr
                        <br>+33 (0)1 74 24 16 98
                    </p>
                    <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                        <span class="mr--15">Send an email</span> <i class="lgpicto lgicon-arrow-right"></i>
                    </a>
                    <div class="clearfix mb--30"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-list"></i>
        <h2 class="title">Discover more</h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-1">
                    <div class="lgpicto lgpicto--big lgicon-btn-speaking text-white"></div>
                    <div class="card__text card__text--white" data-eq="card-discover-title">
                        Social<br>
                        Media wall
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Access</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-2r">
                    <div class="lgpicto lgpicto--big lgicon-btn-trombonne text-white"></div>
                    <div class="card__text card__text--white" data-eq="card-discover-title">
                        Download &
                        <br>Resources
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Access</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-3">
                    <div class="lgpicto lgpicto--big lgicon-btn-writing text-white"></div>
                    <div class="card__text card__text--white" data-eq="card-discover-title">
                        News &
                        <br>Press releases
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Access</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>