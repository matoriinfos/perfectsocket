<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - News and press release</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <h1 class="banner__title">News & Press releases</h1>

            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">News & Press releases</li>
            </ul>
        </div>
    </div>

    <section class="section section--grey section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Donec pharetra vestibulum ligula, nec ultrices neque
                        sollicitudin vitae. Praesent faucibus euismod magna, in
                        ornare lacus pretium pulvinar. Nunc ac lorem ac massa
                        rhoncus iaculis. Donec efficitur tincidunt euismod.
                        Donec non odio sit amet neque laoreet tincidunt aliquam
                        nec nulla. Phasellus vestibulum, nunc in accumsan
                        tristique, diam nisl iaculis sapien, feugiat malesuada
                        neque felis vel arcu. Suspendisse potenti.
                    </p>
                    <div class="row">
                        <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                            <form action="#">
                                <input type="text" class="lg-input lg-input--no-round lg-input--primary" placeholder="Building">
                                <button type="submit" class="btn">
                                    <span class="mr--50">ok</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <form action="#">
                <div class="row mb--40">
                    <div class="col-md-12 mb--10">Filter by:</div>

                    <div class="col-md-8 col-xs-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="all" value="all" checked />
                                <label for="all">All articles</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="residential" value="residential" />
                                <label for="residential">Residential</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="hotels" value="hotels" />
                                <label for="hotels">Hotels</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="education" value="education" />
                                <label for="education">Education</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="hospitals" value="hospitals" />
                                <label for="hospitals">Hospitals</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="industry" value="industry" />
                                <label for="industry">Industry</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="trade" value="trade" />
                                <label for="trade">Trade</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="datacenter" value="datacenter" />
                                <label for="datacenter">Data Center</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-xs-12">
                        <div class="row">
                            <div class="col-xs-8">
                                <label class="select">
                                     <select>
                                         <option value="0">Year of publication</option>
                                         <option value="1">2017</option>
                                         <option value="2">2016</option>
                                     </select>
                                </label>
                            </div>
                            <div class="col-xs-4">
                                <button class="btn-second btn-second--primary-color btn-second--square">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-5">
                        <picture>
                            <source srcset="../../images/content/img7.jpg" media="(max-width: 991px)">
                            <source srcset="../../images/content/img1.jpg">
                            <img class="img-responsive" srcset="../../images/content/img1.jpg" alt="My default image">
                        </picture>
                    </div>
                    <div class="col-md-7">
                        <div class="title">Title 2</div>
                        <div class="mb--30 text--faded">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--15">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad <strong><span class="text-color-1">building</span></strong>, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>

                            <div class="mb--50">
                                <a href="#" class="mb--10 btn-second btn-second--medium-2 btn-second--primary-color">Residential</a>
                            </div>

                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                   <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled social--inline" download>
                                    <span class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </span>
                                    <span class="social-labeled__label text-color-11">
                                        Download<br>
                                        press release
                                    </span>
                                </a>
                                <a href="#" class="social-labeled social--inline mr--25" download>
                                    <span class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </span>
                                    <span class="social-labeled__label text-color-11">
                                        Download<br>
                                        Presentation
                                    </span>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-5">
                        <picture>
                            <source srcset="../../images/content/img7.jpg" media="(max-width: 991px)">
                            <source srcset="../../images/content/img1.jpg">
                            <img class="img-responsive" srcset="../../images/content/img1.jpg" alt="My default image">
                        </picture>
                    </div>
                    <div class="col-md-7">
                        <div class="title">Title 2</div>
                        <div class="mb--30 text--faded">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--15">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad <strong><span class="text-color-1">building</span></strong>, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>

                            <div class="mb--50">
                                <a href="#" class="mb--10 btn-second btn-second--inline btn-second--medium-2 btn-second--primary-color">Industry</a>
                                <a href="#" class="mb--10 btn-second btn-second--inline btn-second--medium-2 btn-second--primary-color">Trade</a>
                            </div>


                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                   <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled social--inline mr--25" download>
                                    <span class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </span>
                                    <span class="social-labeled__label text-color-11">
                                        Download<br>
                                        hd image
                                    </span>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-5">
                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-7">
                        <div class="title">Title 2</div>
                        <div class="mb--30 text--faded">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--15">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad <strong><span class="text-color-1">building</span></strong>, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>

                            <div class="mb--50">
                                <a href="#" class="mb--10 btn-second btn-second--inline btn-second--medium-2 btn-second--primary-color">Education</a>
                            </div>


                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled social--inline mr--25" download>
                                    <span class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </span>
                                    <span class="social-labeled__label text-color-11">
                                        Download<br>
                                        video
                                    </span>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title">Title 2</div>
                        <div class="mb--30">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--15">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Nulla quam ante, egestas sit
                                amet viverra at, hendrerit luctus tellus. Donec
                                consequat dignissim magna, a ornare urna laoreet
                                quis. Maecenas vitae mi eu leo fermentum
                                tincidunt <strong><span class="text-color-1">building</span></strong> ac libero. Phasellus
                                dignissim eu purus vitae fringilla. Nullam massa
                                augue, vulputate sed dolor ac, imperdiet egestas
                                risus. Proin luctus cursus tellus, et viverra
                                leo placerat sit amet. Vivamus vel ex in dui
                                pulvinar tempor sed quis orci. Nunc nec quam
                                ullamcorper, semper neque quis, venenatis nisl.
                                Quisque rutrum laoreet maximus. Nulla rhoncus
                                aliquet dui, quis lobortis lacus dapibus
                                at. </p>

                            <div class="mb--50">
                                <a href="#" class="mb--10 btn-second btn-second--inline btn-second--medium-2 btn-second--primary-color">Industry</a>
                            </div>
                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                   <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled social--inline" download>
                                    <span class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </span>
                                    <span class="social-labeled__label text-color-11">
                                        Download<br>
                                        press release
                                    </span>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <div class="pagered text-center">
                <p class="pagered__label">Page 1 of 5</p>
                <ul class="pagered__items js-pagered__items pagered ">
                    <li class="pagered__item pagered__item--first pagered-item">
                        <a href="#" title="First page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--previous pagered-prev">
                        <a href="#" title="Previous page" rel="prev">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered-item active">
                        <a href="#">1</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">2</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">3</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">4</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">5</a>
                    </li>
                    <li class="pagered__item pagered__item--next pagered-next ">
                        <a href="#" title="Next page" rel="next">
                             <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--last pagered-item">
                        <a href="#" title="Last page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>