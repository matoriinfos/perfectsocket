<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Reference projects</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <h1 class="banner__title">Reference projects</h1>

            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Reference projects</li>
            </ul>
        </div>
    </div>

    <section class="section section--grey section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-openbook"></i>
                    <h2 class="title mb--40">Title level 2</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Donec pharetra vestibulum ligula, nec ultrices neque
                        sollicitudin vitae. Praesent faucibus euismod magna, in
                        ornare lacus pretium pulvinar. Nunc ac lorem ac massa
                        rhoncus iaculis. Donec efficitur tincidunt euismod.
                        Donec non odio sit amet neque laoreet tincidunt aliquam
                        nec nulla. Phasellus vestibulum, nunc in accumsan
                        tristique, diam nisl iaculis sapien, feugiat malesuada
                        neque felis vel arcu. Suspendisse potenti.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <form action="#">
                <div class="row mb--40">
                    <div class="col-md-12 mb--10">Filter by:</div>

                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="all" value="all" checked />
                                <label for="all">All</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="hotels" value="hotels" />
                                <label for="hotels">Hotels</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="hospitals" value="hospitals" />
                                <label for="hospitals">Hospitals</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="education" value="education" />
                                <label for="education">Education</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="residential" value="residential" />
                                <label for="residential">Residential</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="industry" value="industry" />
                                <label for="industry">Industry</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="trade" value="trade" />
                                <label for="trade">Trade</label>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 mb--30">
                                <input type="checkbox" id="datacenter" value="datacenter" />
                                <label for="datacenter">Data Center</label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-4">
                        <picture>
                            <source srcset="../../images/content/img7.jpg" media="(max-width: 991px)">
                            <source srcset="../../images/content/img1.jpg">
                            <img class="img-responsive" srcset="../../images/content/img1.jpg" alt="My default image">
                        </picture>
                    </div>
                    <div class="col-md-8">
                        <div class="title title--xsmall text-color-1">Residential - multimedia</div>
                        <div class="title title--medium">Project 1</div>
                        <div class="text--faded mb--30">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--30">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>


                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg mr--35">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-4">
                        <picture>
                            <source srcset="../../images/content/img7.jpg" media="(max-width: 991px)">
                            <source srcset="../../images/content/img1.jpg">
                            <img class="img-responsive" srcset="../../images/content/img1.jpg" alt="My default image">
                        </picture>
                    </div>
                    <div class="col-md-8">
                        <div class="title title--xsmall text-color-1">Office - Security</div>
                        <div class="title title--medium">Project 2</div>
                        <div class="text--faded mb--30">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--30">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>

                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg mr--35">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="hidden-xs mb--30 col-md-4">
                        <picture>
                            <source srcset="../../images/content/img7.jpg" media="(max-width: 991px)">
                            <source srcset="../../images/content/img1.jpg">
                            <img class="img-responsive" srcset="../../images/content/img1.jpg" alt="My default image">
                        </picture>
                    </div>
                    <div class="col-md-8">
                        <div class="title title--xsmall text-color-1">Education - low consumption</div>
                        <div class="title title--medium">Project 3</div>
                        <div class="text--faded mb--30">12/05/2017</div>

                        <div class="para--border-small">
                            <p class="mb--30">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur.
                            </p>

                            <div class="flex-v flex-v--left flex-v--wrap">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                   <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg mr--35">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>

                                <a href="#" class="btn">Read more</a>
                            </div>

                        </div>
                    </div>
                </div>
            </article>

            <div class="pagered text-center">
                <p class="pagered__label">Page 1 of 5</p>
                <ul class="pagered__items js-pagered__items pagered ">
                    <li class="pagered__item pagered__item--first pagered-item">
                        <a href="#" title="First page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--previous pagered-prev">
                        <a href="#" title="Previous page" rel="prev">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered-item active">
                        <a href="#">1</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">2</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">3</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">4</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">5</a>
                    </li>
                    <li class="pagered__item pagered__item--next pagered-next ">
                        <a href="#" title="Next page" rel="next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--last pagered-item">
                        <a href="#" title="Last page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>