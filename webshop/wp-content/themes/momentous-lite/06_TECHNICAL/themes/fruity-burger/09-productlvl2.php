<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Product level 2</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--no-gradient banner--burger" style="background-image:url('../../images/placeholders/product-lvl-2.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1 class="banner__title title--light">
                    Product<br>
                    level 2
                </h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit
                </p>

                <a href="#" class="btn btn--arrow-left btn--no-color">Go to Céliane</a>
            </div>

            <div class="col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Collections</a></li>
                    <li><a href="#">Product level 1</a></li>
                    <li class="active">Product level 2</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="section section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-housebold"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="row mb--60">
                    <div class="col-md-6">
                        <h3 class="title title--light title--transform-none mb--40">Switch 1</h3>
                        <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>

                        <a href="#" class="link link--static link--download" download>
                            <span class="mr--10">Download the Céliane Switches Brochure</span> <i class="lgpicto lgicon-btn-dwld"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive" src="../../images/placeholders/product-lvl-2-1.jpg">
                    </div>
                </div>
                <div class="row mb--60">
                    <div class="col-md-6 col-md-push-6">
                        <h3 class="title title--light title--transform-none mb--40">Switch 2</h3>
                        <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>

                        <a href="#" class="link link--static link--download" download>
                            <span class="mr--10">Download the Céliane Switches Brochure</span> <i class="lgpicto lgicon-btn-dwld"></i>
                        </a>
                    </div>

                    <div class="col-md-6 col-md-pull-6">
                        <img class="img-responsive" src="../../images/placeholders/product-lvl-2-2.jpg">
                    </div>
                </div>
                <div class="row mb--80">
                    <div class="col-md-6">
                        <h3 class="title title--light title--transform-none mb--40">Switch 3</h3>
                        <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>

                        <a href="#" class="link link--static link--download" download>
                            <span class="mr--10">Download the Céliane Switches Brochure</span> <i class="lgpicto lgicon-btn-dwld"></i>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive" src="../../images/placeholders/product-lvl-2-3.jpg">
                    </div>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <div class="text-center">
                    <div class="flex-v flex-v--wrap">
                        <a href="#" class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social--svg mr--15">
                            <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="btn mr--15 bg--gradient-3">
                            <i class="fa fa-flag mr--5" aria-hidden="true"></i>
                            Find a distributor
                        </a>
                        <a href="#" class="btn mr--15 bg--gradient-1">E-catalogue</a>
                        <a href="#" class="btn btn--alt-color">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--bg section--quote bg--hover-gradient-2" style="background-image: url('../../images/bg-ecatalogue.jpg');">
    <div class="section__content--centered">
        <div class="container text-center text-white">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <p class="para--quote">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eum accusamus dolorem quis distinctio cum excepturi.
                    </p>

                    <div class="title title--medium title--light text-white mb--10">Laurence bernard</div>
                    <div class="card__text card__text--xsmall text-white">Business manager</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding section--no-overflow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 no-eq-sm no-eq-xs" data-eq="home-inspiration">
                <div class="row">
                    <div class="col-md-11">
                        <div class="section">
                            <h2 class="title mb--15">Lack of inspiration ?</h2>
                            <p class="para para--border-large mb--25">
                                Lorem ipsum dolor sit amet,<br>
                                consectetur adipiscing elit integer quis.
                            </p>
                            <div class="card card--bg card--large-padding card--dark-layer no-shadow mb--100" style="background-image:url('../../images/placeholders/inspiration-1.jpg')">
                                <div class="card__text card__text--xsmall text-white mb--25">See, touch, project, be advised.</div>
                                <h2 class="title title--card-category text-white mb--45">Showroom</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow btn--large-width">Visit</a>
                            </div>

                            <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
                            <div class="card__text card__text--xsmall text-color-1">Youtube legrand</div>
                            <div class="title title--medium title--light">Video title</div>

                            <a href="#" class="btn-abs-more">View all inspirations</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section section--darkgrey section--bg-continious-right section--bg-continious-left-bs no-eq-sm no-eq-xs" data-eq="home-inspiration">
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <h2 class="title mb--15">Reference projects</h2>
                                    <p class="para para--border-large mb--25">
                                        Discover all our reference projects<br>
                                        and the products used
                                    </p>
                                    <div class="card card--inline-block no-shadow mb--15">
                                        <img class="card__image" src="../../images/placeholders/reference-3.jpg">

                                        <a href="#" class="btn card__link">View</a>
                                    </div>

                                    <div class="card__text card__text--xsmall text-color-1">Brand image & design</div>
                                    <div class="title title--medium title--light mb--0">Bristol Hotel</div>
                                    <div class="text-grey"><i class="fa fa-map-marker text-color-1 mb--10"></i> Ile-de-France</div>
                                    <div class="text-right">
                                        <a href="#" class="btn-abs-more btn-abs-more--static">View all reference projects</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            Céliane<br>
            <span class="title title--light">Switches</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-7.jpg" alt="Product" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-8.jpg" alt="Product" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-9.jpg" alt="Product" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card card--bg bg--gradient-2 no-shadow" data-eq="featured-products">
                        <div class="fullCenter">
                            <div class="card__text text-white card__text--big-2 mb--20">
                                View all<br>
                                Céliane<br>
                                switches
                            </div>
                            <div class="social social--neg social--white">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>