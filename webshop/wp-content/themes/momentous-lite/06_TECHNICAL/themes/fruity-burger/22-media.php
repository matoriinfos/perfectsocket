<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Media</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--burger">
    <div class="container">
        <h1 class="banner__title">Medias library</h1>

        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Resource and tools</a></li>
            <li class="active">Medias library</li>
        </ul>
    </div>
</div>

<section class="section section--grey section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-centerarrow"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum vitae odio ac enim rutrum mattis. Donec placerat
                    commodo ultrices. Praesent ullamcorper malesuada tristique.
                    Mauris imperdiet efficitur risus nec iaculis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <form action="#">
            <div class="row mb--40">
                <div class="col-md-12 mb--20">Filter by:</div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="all" value="all" checked/>
                    <label for="all">All</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="residential"
                           value="residential"/>
                    <label for="residential">Residential</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="collections"
                           value="collections"/>
                    <label for="collections">Collections</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="piecture" value="piecture"/>
                    <label for="piecture">Picture</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="video" value="video"/>
                    <label for="video">Video</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <input type="checkbox" id="ambiance" value="ambiance"/>
                    <label for="ambiance">Ambiance & solutions</label>
                </div>

            </div>
        </form>
</section>

<section class="section section--grey section--no-padding section--media">
    <div class="container-fluid">
        <div class="row grid-no-padding">
            <div class="col-md-7 grid-small-padding">
                <div class="col-md-6">
                    <div class="media__content">
                        <img src="../../images/content/img1.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="media__content">
                        <img src="../../images/content/img1.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="media__content">
                        <img src="../../images/content/img7.jpg"
                             class="img-fit img-responsive" data-eq="media-1">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 grid-small-padding">
                <div class="col-md-12">
                    <div class="responsive-iframe">
                        <iframe width="100%" height="315"
                                src="https://www.youtube.com/embed/xBHMaxgA440"
                                class="no-eq-sm no-eq-xs" frameborder="0"
                                allowfullscreen data-eq="media-1"></iframe>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="media__content">
                        <img src="../../images/content/img2.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 grid-small-padding">
                <div class="col-xs-12">
                    <div class="responsive-iframe">
                        <iframe width="100%" height="315"
                                src="https://www.youtube.com/embed/xBHMaxgA440"
                                class="no-eq-sm no-eq-xs" frameborder="0"
                                allowfullscreen data-eq="media-1"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-7 grid-small-padding">
                <div class="col-md-6">
                    <div class="media__content">
                        <img src="../../images/content/img1.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media__content">
                        <img src="../../images/content/img1.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="media__content">
                        <img src="../../images/content/img7.jpg"
                             class="img-fit img-responsive" data-eq="media-1">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 grid-small-padding">
                <div class="col-md-12">
                    <div class="media__content">
                        <img src="../../images/content/img2.jpg"
                             class="img-fit img-responsive" data-eq="media-1">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="media__content">
                        <img src="../../images/content/img2.jpg"
                             class="img-fit img-responsive" data-eq="media-2">
                        <div class="media__overlay">
                            <div class="fullCenter">
                                <div class="title title--transform-none text-white">FILENAME.jpg</div>
                                <div class="title title--medium-2 text-color-1 mb--15 text-white">1 Mo</div>
                                <a href="#" class="social social--white">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>