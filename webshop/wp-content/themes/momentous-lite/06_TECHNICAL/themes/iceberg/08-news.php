<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - News</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

<div class="banner">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">About us</a></li>
            <li><a href="#">News</a></li>
            <li class="active">News title</li>
        </ul>
        <h1 class="banner__title">News title</h1>
    </div>
</div>

<section class="section section--small-top-padding section--no-padding-bottom">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
    </div>
</section>
<section class="section section--small-top-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <h2 class="title title--xsmall text-color-1">Innovations</h2>
                <h2 class="title">Article title</h2>
                <div class="text--faded mb--15">15/03/2017</div>

                <div class="para--border-small para--space-top">

                    <h3 class="title title--light title--transform-none mb--30">Title level 3</h3>

                    <p class="mb--30">
                        Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Nulla quam ante, egestas sit amet viverra at,
                        hendrerit luctus tellus. Donec consequat dignissim
                        magna, a ornare urna laoreet quis. Maecenas vitae mi
                        eu leo fermentum tincidunt condimentum ac libero.
                        Phasellus dignissim eu purus vitae fringilla. Nullam
                        massa augue, vulputate sed dolor ac, imperdiet
                        egestas risus. Proin luctus cursus tellus, et
                        viverra leo placerat sit amet. Vivamus vel ex in dui
                        pulvinar tempor sed quis orci. Nunc nec quam
                        ullamcorper, semper neque quis, venenatis nisl.
                        Quisque rutrum laoreet maximus. Nulla rhoncus
                        aliquet dui, quis lobortis lacus dapibus at.
                        Suspendisse ultrices sapien urna, sit amet lacinia
                        magna cursus vitae. Nam commodo sit amet mi in
                        feugiat.
                    </p>

                    <img src="../../images/placeholders/news-01.jpg" class="img-responsive mb--15">

                    <a href="#" class="social-labeled mb--30">
                            <span class="social social--inline social--neg">
                                <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                            </span>
                            <span class="social-labeled__label text-color-11">
                                Download<br>
                                hd image
                            </span>
                    </a>


                    <p class="mb--30">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat
                        cupidatat non proident, sunt in culpa qui officia
                        deserunt mollit anim id est laborum. Sed ut
                        perspiciatis unde omnis iste natus error sit
                        voluptatem accusantium doloremque.
                    </p>

                    <h4 class="title title--light title--h4 title--transform-none mb--15">Title level 4</h4>

                    <p class="mb--30">
                        Laudantium, totam rem aperiam, eaque ipsa quae ab
                        illo inventore veritatis et quasi architecto beatae
                        vitae dicta sunt explicabo. Nemo enim ipsam
                        voluptatem quia voluptas sit aspernatur aut odit aut
                        fugit, sed quia consequuntur magni dolores eos qui
                        ratione voluptatem sequi nesciunt. Neque porro
                        quisquam est, qui dolorem ipsum quia dolor sit amet,
                        consectetur, adipisci velit, sed quia non numquam
                        eius modi tempora incidunt ut labore et dolore
                        magnam aliquam quaerat voluptatem.
                    </p>

                    <div class="flex-v flex-v--left no-flex-xxs">
                        <a href="#" class="social social--inline social--neg mb--10">
                            <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social-modal-trigger social--inline social--neg mb--10">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>

                        <a href="#" class="social-labeled mb--10">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    press release
                                </span>
                        </a>

                        <a href="#" class="btn mb--10">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            Featured<br>
            <span class="title title--light">Products</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-7.jpg" alt="Multimedia" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-8.jpg" alt="Multimedia" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-9.jpg" alt="Multimedia" data-eq="featured-products" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card card--bg bg--gradient-2 no-shadow" data-eq="featured-products">
                        <div class="fullCenter">
                            <div class="card__text text-white card__text--big-2 mb--20">
                                View all<br>
                                featured<br>
                                products
                            </div>
                            <div class="social social--neg social--white">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>