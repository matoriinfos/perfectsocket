<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Contact</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

    <div class="banner">
        <div class="container">
            <ul class="breadcrumb">
                 <li><a href="#">Home</a></li>
                 <li class="active">Contact us</li>
            </ul>
            <h1 class="banner__title">Contact us</h1>
        </div>
    </div>

    <section class="section section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <h2 class="title mb--50">Title level 2</h2>

                    <form class="text-left" action="#">
                        <label class="select select--primary mb--20">
                             <select>
                                 <option value="0">I am a professional</option>
                                 <option value="1">I am a professional</option>
                                 <option value="2">I am a professional</option>
                             </select>
                        </label>

                        <label class="select select--primary mb--20">
                             <select>
                                 <option value="0">I am looking for information about training</option>
                                 <option value="1">I am looking for information about training</option>
                                 <option value="2">Other</option>
                             </select>
                        </label>

                        <div class="radio">
                            <input id="Mr" type="radio" name="gender" value="Mr">
                            <label for="Mr">Mr</label>
                            <input id="Mrs" type="radio" name="gender" value="Mrs">
                            <label for="Mrs">Mrs</label>
                        </div>

                        <input class="lg-input lg-input--no-round lg-input--primary lg-input--dark-text" type="text" placeholder="Name*" />

                        <input class="lg-input lg-input--no-round lg-input--primary lg-input--dark-text" type="text" placeholder="Surname*" />

                        <label class="select mb--20">
                             <select>
                                 <option value="">Country*</option>
                                 <option value="1">France</option>
                                 <option value="2">Us</option>
                             </select>
                        </label>

                        <input class="lg-input lg-input--no-round lg-input--primary lg-input--dark-text" type="text" placeholder="Tel*" />

                        <textarea placeholder="Your message here..."></textarea>

                        <p class="para--indic para--tiny">Required fields*</p>

                        <div class="text-right">
                            <a href="#" class="btn">Send</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>