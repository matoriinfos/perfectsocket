<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Building solutions</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

<div class="banner banner--no-gradient">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Solutions</a></li>
            <li class="active">Building solutions</li>
        </ul>

        <div class="row">
            <div class="col-md-4">
                <h1 class="banner__title title--light">Building solutions</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                    non proident, sunt in culpa qui officia deserunt mollit
                </p>
            </div>
        </div>
    </div>
</div>

<section class="section section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--medium lgicon-btn-arrows-ext"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                    non proident, sunt in culpa qui officia deserunt mollit
                    anim id est laborum. Sed
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="row mb--80">
                    <div class="col-md-6">
                        <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 mb--20">
                                <img class="img-responsive img-center" src="../../images/placeholders/product-lvl-1-1.jpg">
                            </div>
                            <div class="col-sm-6">
                                <img class="img-responsive img-center mb-xs--20" src="../../images/placeholders/product-lvl-1-2.jpg">
                            </div>
                            <div class="col-sm-6">
                                <img class="img-responsive img-center mb-xs--20" src="../../images/placeholders/product-lvl-1-3.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb--80">
                    <div class="col-md-6 col-md-push-6">
                        <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>

                        <h4 class="title title--light title--transform-none title--h4">Title level 4</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>

                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <img class="img-responsive img-center" src="../../images/placeholders/product-lvl-1-4.jpg">
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="text-center">
                            <div class="flex-v">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--svg mr--15">
                                    <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding section--no-overflow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12" data-eq="home-inspiration">
                <div class="row">
                    <div class="col-md-11">
                        <div class="section no-eq-sm no-eq-xs">
                            <h2 class="title mb--15">Lack of inspiration ?</h2>
                            <p class="para para--border-large mb--25">
                                Lorem ipsum dolor sit amet,<br>
                                consectetur adipiscing elit integer quis.
                            </p>
                            <div class="card card--bg card--large-padding card--dark-layer no-shadow mb--100" style="background-image:url('../../images/content/img1.jpg')">
                                <div class="card__text card__text--xsmall text-white mb--25">See, touch, project, be advised.</div>
                                <h2 class="title title--card-category text-white mb--45">Showroom</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow btn--large-width">Visit</a>
                            </div>

                            <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
                            <div class="card__text card__text--xsmall text-color-1">Youtube legrand</div>
                            <div class="title title--medium title--light">Video title</div>

                            <a href="#" class="btn-abs-more">View all inspirations</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section section--darkgrey section--bg-continious-right section--bg-continious-left-bs no-eq-sm no-eq-xs" data-eq="home-inspiration">
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <h2 class="title mb--15">Reference projects</h2>
                                    <p class="para para--border-large mb--25">
                                        Discover all our reference projects<br>
                                        and the products used
                                    </p>
                                    <div class="card card--inline-block no-shadow mb--15">
                                        <img class="card__image" src="../../images/content/img9.jpg">

                                        <a href="#" class="btn card__link">View</a>
                                    </div>

                                    <div class="card__text card__text--xsmall text-color-1">Brand image & design</div>
                                    <div class="title title--medium title--light mb--0">Bristol Hotel</div>
                                    <div class="text-grey"><i class="fa fa-map-marker text-color-1 mb--10"></i> Ile-de-France</div>
                                    <div class="text-right">
                                        <a href="#" class="btn-abs-more btn-abs-more--static">View all reference projects</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section bg--gradient-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <i class="lgpicto--title lgicon-btn-chain text-white"></i>
                <h2 class="title title--light title--transform-none text-white mb--10">You will also be interested by</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-xs">
                        <div class="col-sm-8 col-xs-12">
                            <h3 class="title title--medium-2 text-white mb--15">Improve guest confort</h3>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" class="social-labeled social-labeled--left">
                                    <span class="social-labeled__label social-labeled__label--small text-white">
                                        See more
                                    </span>
                                    <span class="social social--inline social--neg social--white">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-sm no-flex-xs">
                        <div class="col-sm-8 col-xs-12">
                            <h3 class="title title--medium-2 text-white mb--15">Flexible multimedia and power access</h3>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" class="social-labeled social-labeled--left">
                                    <span class="social-labeled__label social-labeled__label--small text-white">
                                        See more
                                    </span>
                                    <span class="social social--inline social--neg social--white">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-sm no-flex-xs">
                        <div class="col-sm-8 col-xs-12">
                            <h3 class="title title--medium-2 text-white mb--15">Increase energy efficiency</h3>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" class="social-labeled social-labeled--left">
                                    <span class="social-labeled__label social-labeled__label--small text-white">
                                        See more
                                    </span>
                                    <span class="social social--inline social--neg social--white">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-sm no-flex-xs">
                        <div class="col-sm-8 col-xs-12">
                            <h3 class="title title--medium-2 text-white mb--15">Safety for people and property</h3>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" class="social-labeled social-labeled--left">
                                    <span class="social-labeled__label social-labeled__label--small text-white">
                                        See more
                                    </span>
                                    <span class="social social--inline social--neg social--white">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<section class="section bg--gradient-2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <i class="lgpicto--title lgicon-btn-dwld text-white"></i>
                <h2 class="title title--light title--transform-none text-white mb--10">Downloads</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-xs">
                        <div class="col-sm-8">
                            <h3 class="title title--medium-3 text--light text-white mb--10">Catalogue1.pdf</h3>
                            <div class="title title--medium-2 text-white mb--20">6 Mo</div>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit. Mauris a lacinia sapien.</p>
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" title="Download file" class="social social--inline social--neg social--white mr--20" download>
                                    <i class="lgpicto lgicon-btn-dwld text-white" aria-hidden="true"></i>
                                </a>
                                <a href="#" title="File preview">
                                    <img src="../../images/content/img8.jpg" alt="" title="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-xs">
                        <div class="col-sm-8">
                            <h3 class="title title--medium-3 text--light text-white mb--10">Catalogue1.pdf</h3>
                            <div class="title title--medium-2 text-white mb--20">6 Mo</div>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit. Mauris a lacinia sapien.</p>
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" title="Download file" class="social social--inline social--neg social--white mr--20" download>
                                    <i class="lgpicto lgicon-btn-dwld text-white" aria-hidden="true"></i>
                                </a>
                                <a href="#" title="File preview">
                                    <img src="../../images/content/img8.jpg" alt="" title="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <article class="article-container article-container--small-padding border-bottom border-white mb--0">
                    <div class="row flex-v no-flex-xs">
                        <div class="col-sm-8">
                            <h3 class="title title--medium-3 text--light text-white mb--10">Catalogue1.pdf</h3>
                            <div class="title title--medium-2 text-white mb--20">6 Mo</div>

                            <p class="title title--medium-2 title--light text-white">Lorem ipsum consectetur adipiscing elit. Mauris a lacinia sapien.</p>
                        </div>
                        <div class="col-sm-4 text-right">
                            <div class="flex-v flex-v--right">
                                <a href="#" title="Download file" class="social social--inline social--neg social--white mr--20" download>
                                    <i class="lgpicto lgicon-btn-dwld text-white" aria-hidden="true"></i>
                                </a>
                                <a href="#" title="File preview">
                                    <img src="../../images/content/img8.jpg" alt="" title="" />
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <div class="text-center flex-v mt--35">
                    <a href="#" class="social-labeled">
                        <span class="social social--inline social--neg social--white mr--20">
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </span>
                        <span class="social-labeled__label social-labeled__label--small text-white">
                            View all downloads
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            Hotel<br>
            <span class="title title--light">Solution Products</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img4.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img5.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img6.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card card--bg bg--gradient-2 no-shadow" data-square="1">
                        <div class="fullCenter fullwidth">
                            <div class="card__text text-white card__text--big-2 mb--20">
                                View all<br>
                                hotels solution<br>
                                products
                            </div>
                            <div class="social social--neg social--white">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 no-eq-sm no-eq-xs" data-eq="block-latest-news">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="section">
                            <div class="title title--xsmall text-color-1">Latest news</div>
                            <div class="title mb--30">
                                Discover legrand's<br>
                                latest connected<br>
                                lorem ipsums
                            </div>

                            <div class="para--border-small para--space-top mb--100">
                                <p class="mb--30">
                                    Cane with Netatmo AT CES 2017, Las Vegas
                                    elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua. Ut enim ad
                                    minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea
                                    commodo consequat. Duis aute irure dolor in
                                </p>

                                <a href="#" class="btn">View more</a>
                            </div>

                            <a href="#" class="btn-abs-more">View all News</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 no-eq-sm no-eq-xs card card--image no-shadow mb--0"
                 style="background-image: url('../../images/content/img7.jpg')"
                 data-eq="block-latest-news">
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div id="refSlider" class="carousel no-padding slide carousel-fade" data-ride="carousel" data-interval="false">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#refSlider" data-slide="prev">
                    <span class="icon-prev"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#refSlider" data-slide="next">
                    <span class="icon-next"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>