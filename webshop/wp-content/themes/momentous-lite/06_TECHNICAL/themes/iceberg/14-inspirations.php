<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Inspirations</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

<div class="banner banner--no-gradient">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Inspiration</li>
        </ul>

        <div class="row">
            <div class="col-md-4">
                <h1 class="banner__title title--light">Inspiration</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit
                </p>
            </div>
        </div>
    </div>
</div>

<section class="section section--grey section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <h2 class="title mb--40">Select a type of building</h2>
                <div class="row">
                    <div class="col-lg-offset-2 col-md-8 col-md-offset-1">
                        <label class="select select--primary">
                            <select>
                                <option value="">Select</option>
                                <option value="0">Building</option>
                                <option value="1">Building</option>
                                <option value="2">Building</option>
                            </select>
                        </label>

                        <button type="submit" class="btn">
                            <span class="mr--50">Ok</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding section--no-overflow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 no-eq-sm no-eq-xs" data-eq="home-inspiration">
                <div class="row">
                    <div class="col-md-11">
                        <div class="section">
                            <h2 class="title mb--15">Showroom</h2>
                            <p class="para para--border-large mb--25">
                                Lorem ipsum dolor sit amet,<br>
                                consectetur adipiscing elit integer quis.
                            </p>
                            <div class="card card--image card--large-padding card--dark-layer mb--100 card--inline-block no-shadow " style="background-image:url('../../images/placeholders/showroom.jpg')">
                                <img class="card__image" src="../../images/placeholders/showroom.jpg">

                                <div class="fullCenter text-center">
                                    <div class="card__text card__text--xsmall text-white mb--25">See, touch, project, be advised.</div>
                                    <h2 class="title title--card-category text-white mb--45">Showroom</h2>
                                </div>

                                <a href="#" class="btn card__link">Visit</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section section--darkgrey section--bg-continious-right section--bg-continious-left-bs no-eq-sm no-eq-xs" data-eq="home-inspiration">
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <h2 class="title mb--15">Most popular video</h2>
                                    <p class="para para--border-large mb--25">
                                        Discover all our video<br>
                                        and the products used
                                    </p>
                                    <div class="mb--20">
                                        <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440"    frameborder="0" allowfullscreen></iframe>
                                        <div class="card__text card__text--xsmall text-color-1">Youtube legrand</div>
                                        <div class="title title--medium title--light">Legrand guest room management system for hotels</div>
                                    </div>

                                    <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440"    frameborder="0" allowfullscreen></iframe>
                                    <div class="card__text card__text--xsmall text-color-1">Youtube legrand</div>
                                    <div class="title title--medium title--light">Legrand guest room management system for hotels</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-openbook"></i>
        <h2 class="title">
            Reference<br>
            <span class="title title--light">projects</span>
        </h2>
    </div>
    <div class="container">
        <div class="row mr--30">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img1.jpg">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow">
                        <div class="card__text card__text--xsmall text-color-1">Brand image & design</div>
                        <div class="card__text title title--medium title--light mb--0">Bristol Hotel</div>
                        <div class="text-grey title--transform-none"><i class="fa fa-map-marker text-color-1 mb--10"></i> Ile-de-France</div>
                        <span class="link link-center link--vertical-center">
                            <i class="lgicon-arrow-right" aria-hidden="true"></i>
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img2.jpg">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow">
                        <div class="card__text card__text--xsmall text-color-1">Brand image & design</div>
                        <div class="card__text title title--medium title--light mb--0">Bristol Hotel</div>
                        <div class="text-grey title--transform-none"><i class="fa fa-map-marker text-color-1 mb--10"></i> Ile-de-France</div>
                        <span class="link link-center link--vertical-center">
                            <i class="lgicon-arrow-right" aria-hidden="true"></i>
                        </span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img3.jpg">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow">
                        <div class="card__text card__text--xsmall text-color-1">Brand image & design</div>
                        <div class="card__text title title--medium title--light mb--0">Bristol Hotel</div>
                        <div class="text-grey title--transform-none"><i class="fa fa-map-marker text-color-1 mb--10"></i> Ile-de-France</div>
                        <span class="link link-center link--vertical-center">
                            <i class="lgicon-arrow-right" aria-hidden="true"></i>
                        </span>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="#" class="btn-abs-more btn-abs-more--static">View all reference projects</a>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            All<br>
            <span class="title title--light">Products</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img4.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img5.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img6.jpg" alt="Product category" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Product category
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card card--bg bg--gradient-2 no-shadow" data-square="1">
                        <div class="fullCenter">
                            <div class="card__text text-white card__text--big-2 mb--20">
                                View all<br>
                                products
                            </div>
                            <div class="social social--neg social--white">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div id="refSlider" class="carousel no-padding slide carousel-fade" data-ride="carousel" data-interval="false">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#refSlider" data-slide="prev">
                    <span class="icon-prev"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#refSlider" data-slide="next">
                    <span class="icon-next"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-list"></i>
        <h2 class="title">Resources and tools</h2>
        <h3 class="title title--small">Tools and services that make life easier</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-1">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-disc text-white"></div>
                        Applications,<br>
                        Software<br>
                        & Configurators
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-2">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-sheet text-white"></div>
                        Documentation<br>
                        &<br>
                        Guides
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-3">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-sheetmedal text-white"></div>
                        Standards<br>
                        &<br>
                        Regulation
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once('../../parts/footer.php'); ?>
</body>
</html>