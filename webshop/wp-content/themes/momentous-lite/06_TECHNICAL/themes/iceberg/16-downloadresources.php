<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Download and resources</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

    <div class="banner">
        <div class="container">
            <ul class="breadcrumb">
                 <li><a href="#">Home</a></li>
                 <li><a href="#">Resources and Tools</a></li>
                 <li class="active">Download and resources</li>
            </ul>
            <h1 class="banner__title">Download and resources</h1>
        </div>
    </div>

    <section class="section section--grey section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-cinema"></i>
                    <h2 class="title mb--40">Title level 2</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit corrupti, nostrum ex repellendus repellat consequatur enim corporis perspiciatis, provident veniam odio. Minima adipisci quasi cumque vero, facere animi sunt neque.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <form action="#" class="border-bottom">
                <div class="row mb--40">
                    <div class="col-md-12 mb--20">Filter by:</div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="protection">
                       <label for="protection">Protection</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="occupancy">
                       <label for="occupancy">Occupancy sensors</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="range5">
                       <label for="range5">range 5</label>
                    </div>
                    <div class="col-md-3 col-lg-6 col-sm-4 col-xs-6">
                       <input type="checkbox" id="range7">
                       <label for="range7">range 7</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="wiring">
                       <label for="wiring">Wiring devices</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="residential" checked>
                       <label for="residential">Residential</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="range6">
                       <label for="range6">range 6</label>
                    </div>
                    <div class="col-md-3 col-lg-2 col-sm-4 col-xs-6">
                       <input type="checkbox" id="range8">
                       <label for="range8">range 8</label>
                    </div>
                </div>
            </form>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="col-md-7">
                        <div class="title title--xsmall text-color-1">Residential</div>
                        <div class="title title--transform-none">TITLE 2.pdf</div>
                        <div class="title title--medium-2 text-color-1 mb--15">6 Mo</div>
                        <p class="mb--30 para--border-small para--space-top">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum. Sed ut
                            perspiciatis unde omni.
                        </p>
                    </div>
                    <div class="hidden-xs col-md-4 col-md-offset-1 mb--10">
                        <img class="img-responsive" src="../../images/placeholders/download-2.jpg" alt="My default image" />
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="flex-v flex-v--left ml-md--45">
                                    <a href="#" class="social social--neg social--inline">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="btn mr--120 mr-xxs--8">
                                        Preview
                                    </a>
                                    <a href="#" class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social-modal-trigger social--inline social--neg">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="col-md-7">
                        <div class="title title--xsmall text-color-1">Residential</div>
                        <div class="title title--transform-none">TITLE 2.zip</div>
                        <div class="title title--medium-2 text-color-1 mb--15">6 Mo</div>
                        <p class="mb--30 para--border-small para--space-top">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum. Sed ut
                            perspiciatis unde omni.
                        </p>
                    </div>
                    <div class="hidden-xs col-md-4 col-md-offset-1 mb--10">
                        <img class="img-responsive" src="../../images/placeholders/download-2.jpg" alt="My default image" />
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="flex-v flex-v--left ml-md--45">
                                    <a href="#" class="social social--neg social--inline">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="btn mr--120 mr-xxs--8">
                                        Preview
                                    </a>
                                    <a href="#" class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social-modal-trigger social--inline social--neg">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="col-md-7">
                        <div class="title title--xsmall text-color-1">Residential</div>
                        <div class="title title--transform-none">TITLE 2.exe</div>
                        <div class="title title--medium-2 text-color-1 mb--15">6 Mo</div>
                        <p class="mb--30 para--border-small para--space-top">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum. Sed ut
                            perspiciatis unde omni.
                        </p>
                    </div>
                    <div class="hidden-xs col-md-4 col-md-offset-1 mb--10">
                        <img class="img-responsive" src="../../images/placeholders/download-2.jpg" alt="My default image" />
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="flex-v flex-v--left ml-md--45">
                                    <a href="#" class="social social--neg social--inline">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="btn mr--120 mr-xxs--8">
                                        Preview
                                    </a>
                                    <a href="#" class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social-modal-trigger social--inline social--neg">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container border-bottom">
                <div class="row">
                    <div class="col-md-7">
                        <div class="title title--xsmall text-color-1">Residential</div>
                        <div class="title title--transform-none">TITLE 2.pdf</div>
                        <div class="title title--medium-2 text-color-1 mb--15">6 Mo</div>
                        <p class="mb--30 para--border-small para--space-top">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip
                            ex ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum dolore
                            eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum. Sed ut
                            perspiciatis unde omni.
                        </p>
                    </div>
                    <div class="hidden-xs col-md-4 col-md-offset-1 mb--10">
                        <img class="img-responsive" src="../../images/placeholders/download-2.jpg" alt="My default image" />
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="flex-v flex-v--left ml-md--45">
                                    <a href="#" class="social social--neg social--inline">
                                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="btn mr--120 mr-xxs--8">
                                        Preview
                                    </a>
                                    <a href="#" class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social-modal-trigger social--inline social--neg">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <div class="pagered text-center">
                <p class="pagered__label">Page 1 of 5</p>
                <ul class="pagered__items js-pagered__items pagered ">
                    <li class="pagered__item pagered__item--first pagered-item">
                        <a href="#" title="First page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--previous pagered-prev">
                        <a href="#" title="Previous page" rel="prev">
                            <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered-item active">
                        <a href="#">1</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">2</a>
                    </li>
                    <li class="pagered__item pagered-item">
                        <a href="#">3</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">4</a>
                    </li>
                    <li class="pagered__item pagered-item hidden-xs">
                        <a href="#">5</a>
                    </li>
                    <li class="pagered__item pagered__item--next pagered-next ">
                        <a href="#" title="Next page" rel="next">
                            <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                        </a>
                    </li>
                    <li class="pagered__item pagered__item--last pagered-item">
                        <a href="#" title="Last page">
                            <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/content/img1.jpg')">
                        <div class="section__content--centered">
                            <div class="card__text card__text--big card__text--semilight mb--40 text-white">Living room</div>
                            <a href="#" class="btn btn--no-color">Download</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/content/img2.jpg')">
                        <div class="section__content--centered">
                            <div class="card__text card__text--big card__text--semilight mb--40 text-white">Bedroom</div>
                            <a href="#" class="btn btn--no-color">Download</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/content/img3.jpg')">
                        <div class="section__content--centered">
                            <div class="card__text card__text--big card__text--semilight mb--40 text-white">Bathroom</div>
                            <a href="#" class="btn btn--no-color">Download</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>