<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Home Page</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

<div id="homeSlider" class="carousel--simple carousel slide carousel-fade" data-ride="carousel" data-interval="false">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="bg-slider" style="background-image:url('../../images/head-simple.jpg');"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="slider-text">
                            <div class="title title--big text-color-1">
                                Transforming spaces
                                <div class="title--light">
                                    Where people live & work
                                </div>
                            </div>
                            <div class="para--border-large">
                                We deliver access to power, light and data to millions of spaces around the world
                            </div>
                            <a href="#" class="btn card__link">See more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="bg-slider" style="background-image:url('../../images/head-simple.jpg');"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <div class="slider-text">
                            <div class="title title--big text-color-1">
                                Transforming spaces
                                <div class="title--light">
                                    Where people live & work
                                </div>
                            </div>
                            <div class="para--border-large">
                                We deliver access to power, light and data to millions of spaces around the world
                            </div>
                            <a href="#" class="btn card__link">See more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#homeSlider" data-slide="prev">
        <span class="icon-prev"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#homeSlider" data-slide="next">
        <span class="icon-next"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">Our products</h2>
        <h3 class="title title--light">Transform spaces</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card ">
                    <img class="card__image" src="../../images/placeholders/product-1.jpg">
                    <div class="card__text card__text--spaced card__text--big no-eq-sm no-eq-xs" data-eq="cards-product">Living spaces</div>
                    <a href="#" class="btn card__link">View living spaces</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card ">
                    <img class="card__image" src="../../images/placeholders/product-2.jpg">
                    <div class="card__text card__text--spaced card__text--big no-eq-sm no-eq-xs" data-eq="cards-product">Work spaces</div>
                    <a href="#" class="btn card__link">View work spaces</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card ">
                    <img class="card__image" src="../../images/placeholders/product-3.jpg">
                    <div class="card__text card__text--spaced card__text--big no-eq-sm no-eq-xs" data-eq="cards-product">Product of the month</div>
                    <a href="#" class="btn card__link">View "Celiane"</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-list"></i>
        <h2 class="title">Ressources and tools</h2>
        <h3 class="title title--small">Tools and services that make life easier</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-1">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-disc text-white mb--20"></div>
                        Applications,<br>
                        Software<br>
                        & Configurators
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-2">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-sheet text-white mb--20"></div>
                        Documentation<br>
                        &<br>
                        Guides
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card card--bg bg--gradient-3">
                    <div class="card__text card__text--white" data-eq="card-resources-title">
                        <div class="lgpicto lgpicto--big lgicon-btn-sheetmedal text-white mb--20"></div>
                        Standards<br>
                        &<br>
                        Regulation
                    </div>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            All<br>
            <span class="title title--light">Products</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-4.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Multimedia
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-5.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Outlet and switch
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-6.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Collections
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/placeholders/product-4.jpg">
                    <div class="card__text card__text--spaced-v card__text--big">
                        <span class="link link--vertical-center">
                            <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                        </span>
                        Multimedia
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--bg" style="background-image: url('../../images/bg-ecatalogue.jpg');">
    <div class="section__content--centered">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3 text-center">
                    <i class="lgpicto--title lgicon-btn-list text-white"></i>
                    <h2 class="title text-white">E-Catalogue</h2>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">
                        Discover</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-list"></i>
        <h2 class="title">Our solutions</h2>
        <h3 class="title title--small">Lorem ipsum dolor si amet</h3>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/placeholders/solution-1.jpg')">
                    <div class="section__content--centered">
                        <div class="card__text card__text--big mb--20 text-white">Residential</div>
                        <a href="#" class="btn no-shadow btn--short-width">View</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/placeholders/solution-2.jpg')">
                    <div class="section__content--centered">
                        <div class="card__text card__text--big mb--20 text-white">Offices</div>
                        <a href="#" class="btn no-shadow btn--short-width">View</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card--bg card--dark-layer card--large" style="background-image:url('../../images/placeholders/solution-3.jpg')">
                    <div class="section__content--centered">
                        <div class="card__text card__text--big mb--20 text-white">Education</div>
                        <a href="#" class="btn no-shadow btn--short-width">View</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 bg--blue-1">
                <a href="#" class="card-link--large">
                    <div class="section__content--centered">
                        <h2 class="card__text card__text--xsmall text-color-1">Special offer</h2>
                        <h3 class="title title--medium title--light text-white">Promotion of the moment</h3>
                        <span class="link"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 bg--blue-2">
                <a href="#" class="card-link--large">
                    <div class="section__content--centered">
                        <h2 class="card__text card__text--xsmall text-color-1">Special offer</h2>
                        <h3 class="title title--medium title--light text-white">Download now</h3>
                        <span class="link"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-sm-4 bg--blue-3">
                <a href="#" class="card-link--large">
                    <div class="section__content--centered">
                        <h2 class="card__text card__text--xsmall text-color-1">Special offer</h2>
                        <h3 class="title title--medium title--light text-white">Consult online</h3>
                        <span class="link"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding section--no-overflow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-11">
                        <div class="section no-eq-sm no-eq-xs" data-eq="home-inspiration">
                            <h2 class="title mb--15">Lack of inspiration ?</h2>
                            <p class="para para--border-large mb--25">
                                Lorem ipsum dolor sit amet,<br>
                                consectetur adipiscing elit integer quis.
                            </p>
                            <div class="card card--bg card--large-padding card--dark-layer no-shadow mb--100" style="background-image:url('../../images/placeholders/inspiration-1.jpg')">
                                <div class="card__text card__text--xsmall text-white mb--25">See, touch, project, be advised.</div>
                                <h2 class="title title--card-category text-white mb--45">Showroom</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow btn--large-width">Visit</a>
                            </div>

                            <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
                            <div class="card__text card__text--xsmall text-color-1">Youtube legrand</div>
                            <div class="title title--medium title--light">Video title</div>

                            <a href="#" class="btn-abs-more">View all inspirations</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 section--grey no-eq-sm no-eq-xs" data-eq="home-inspiration">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section section--darkgrey section--bg-continious-right section--bg-continious-left-bs">
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <h2 class="title mb--15">Reference projects</h2>
                                    <p class="para para--border-large mb--25">
                                        Discover all our reference projects<br>
                                        and the products used
                                    </p>
                                    <div class="card card--inline-block no-shadow mb--0">
                                        <img class="card__image" src="../../images/placeholders/reference-1.jpg">

                                        <a href="#" class="btn card__link">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section section--grey section--bg-continious-right section--bg-continious-left-bs">
                            <div class="row">
                                <div class="col-md-11 col-md-offset-1">
                                    <h2 class="title mb--15">Corporate social responsability</h2>
                                    <p class="para para--border-large mb--25">
                                        Discover all our reference projects<br>
                                        and the products used
                                    </p>
                                    <div class="card card--inline-block no-shadow mb--0">
                                        <img class="card__image" src="../../images/placeholders/reference-2.jpg">

                                        <a href="#" class="btn card__link">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 no-eq-sm no-eq-xs" data-eq="home-feature">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="section">
                            <div class="title title--xsmall text-color-1">Feature</div>
                            <div class="title mb--30">Norms & electical regulations : everything that subsist and will evolve in 2017</div>

                            <div class="para--border-small mb--100">
                                <p class="mb--100">
                                    Electrical installation diagnosis, MF C
                                    15-100 norm, smoke detectors, products
                                    marking, electrical charging stations
                                    installation : what are the
                                    current regulations and obligations on your
                                    electrical installations ?<br>
                                    <strong>Everything that subsist and will evolve in 2017</strong>
                                </p>

                                <a href="#" class="btn">View more</a>
                            </div>

                            <a href="#" class="btn-abs-more">View all News</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hidden-xs col-md-6 col-sm-12 col-xs-12 card card--image no-shadow mb--0"
                 style="background-image: url('../../images/placeholders/actu-1.jpg')"
                 data-eq="home-feature">
            </div>
        </div>
    </div>
</section>

<section class="section section--bg-gradient-2" style="background-image: url('../../images/bg-seminar.jpg');">
    <div class="container">
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <i class="lgpicto--title lgicon-btn-note text-white"></i>
                <h2 class="title text-white mb--55">Seminar & training solutions</h2>
                <p class="text-white mb--30">
                    TeLorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id
                    est laborum. Sed ut
                </p>

                <a href="#" class="btn-abs-more text-white btn-abs-more--static">View all training solutions</a>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-cursor"></i>
        <h2 class="title">Customize your experience</h2>
        <h3 class="title title--small">
            Tell us who you are so we can provide you with the information<br>
            that is most important to you, and help you find what you are looking for faster.
        </h3>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Contractor<br>
                            or integrator
                        </h5>
                        <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Distributor
                        </h5>
                        <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Facility or technology manager
                        </h5>
                        <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Home builder
                        </h5>
                        <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Homeowner
                        </h5>
                        <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 card-bordered">
                <a href="#" class="card-link--medium">
                    <div class="section__content--centered">
                        <h4 class="title title--xsmall text-color-1 mb--10">I am a</h4>
                        <h5 class="title title--medium title--light mb--10">
                            Specifier, designer<br>
                            or consultant
                        </h5>
                         <span class="link link--static ml--0"><i class="lgicon-arrow-right"></i></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title fa fa-facebook mr--20 mb--20" aria-hidden="true"></i>
        <i class="lgpicto--title fa fa-twitter" aria-hidden="true"></i>

        <h2 class="title">Our last posts</h2>
        <h3 class="title title--small mb--20">
            Lorem ipsum dolor si amet lorem ipsum dolor<br>
            si amet lorem ipsum
        </h3>
        <div class="text-center">
            <a href="#" class="btn-abs-more text-color-1 btn-abs-more--static">View the social media wall</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 section__separator--right">
                <div class="row">
                    <div class="col-md-10">
                        <h4 class="title mb--15">
                            <i class="lgpicto--title fa fa-facebook" aria-hidden="true"></i>
                            Facebook feed
                        </h4>
                        <div class="para para--border-large mb--20">
                            <p class="mb--0">
                                Lorem ipsum dolor si<br>
                                amet lorem ipsum
                            </p>
                        </div>
                        <div class="section__iframe mb--15">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLegrandFrance%2F&tabs=timeline&width=400&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=716368235229738"
                                    width="400" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Facebook</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-md-10 col-md-offset-2">
                        <h4 class="title mb--15">
                            <i class="lgpicto--title fa fa-twitter" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large mb--20">
                            <p class="mb--0">
                                by <span class="text-blue-link">@Legrand</span>
                                <br><br>
                            </p>
                        </div>
                        <div class="section__iframe mb--15">
                            <a class="twitter-timeline" href="https://twitter.com/Legrand"
                               data-width="400"
                               data-height="500"
                               data-chrome="nofooter"></a>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>