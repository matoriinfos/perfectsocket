<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Reference project</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--no-gradient banner--burger">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb--40">
                <h1 class="banner__title title--light">Residential project multimedia</h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit
                </p>

                <a href="#" class="btn btn--arrow-left btn--no-color">Go to reference projects</a>
            </div>
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Reference projects</a></li>
                    <li class="active">Residential project multimedia</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="section section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-housebold"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat
                </p>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="imgSlider" class="carousel no-padding slide" data-ride="carousel" data-interval="false">
        <ol class="carousel-indicators">
            <li data-target="#imgSlider" data-slide-to="0" class="active"></li>
            <li data-target="#imgSlider" data-slide-to="1"></li>
            <li data-target="#imgSlider" data-slide-to="2"></li>
            <li data-target="#imgSlider" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item bg--darker active">
                <img src="../../images/content/img12.jpg">
                <div class="carousel-caption">
                    <div class="title title--light text-white">Photo title</div>
                </div>
            </div>
            <div class="item bg--darker">
                <img src="../../images/content/img12.jpg">
                <div class="carousel-caption">
                    <div class="title title--light text-white">Photo title</div>
                </div>
            </div>
            <div class="item bg--darker">
                <img src="../../images/content/img12.jpg">
                <div class="carousel-caption">
                    <div class="title title--light text-white">Photo title</div>
                </div>
            </div>
            <div class="item bg--darker">
                <img src="../../images/content/img12.jpg">
                <div class="carousel-caption">
                    <div class="title title--light text-white">Photo title</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="row mb--80">
                    <div class="col-md-6 col-md-push-6">
                        <img class="mb--15 img-responsive" src="../../images/content/img2.jpg">

                        <div class="title title--xsmall text-color-1 mb--0">Lorem ipsum</div>
                        <div class="text--faded mb--15">12/05/2017</div>
                    </div>
                    <div class="col-md-6 col-md-pull-6">
                        <h3 class="title title--light title--transform-none mb--20">Title level 3</h3>

                        <div class="text-color-1 mb--40">
                            <i class="fa fa-map-marker text-color-1 mb--0 mr--15"></i> <span class="title title--xsmall text-color-1">Paris, 75 000, France</span>
                            <div class="text--faded mb--15 ml--26">Lorem ipsum dolor</div>
                        </div>

                        <h4 class="title title--light title--transform-none title--h4">Title level 4</h4>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua. Ut
                            enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat
                        </p>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-md-12">
                        <div class="text-center">
                            <div class="flex-v flex-v--wrap">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--svg mr--15">
                                    <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="video-container">
        <iframe width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
    </div>
</section>

<section class="section section--bg section--quote bg--hover-gradient-2" style="background-image: url('../../images/bg-ecatalogue.jpg');">
    <div class="section__content--centered">
        <div class="container text-center text-white">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <p class="para--quote">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad mi
                    </p>

                    <div class="title title--medium title--light text-white mb--10">Laurence bernard</div>
                    <div class="card__text card__text--xsmall text-white">Business manager</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="section__header">
        <i class="lgpicto--title lgicon-btn-pushlight"></i>
        <h2 class="title">
            Featured<br>
            <span class="title title--light">Products</span>
        </h2>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img4.jpg" alt="Multimedia" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Multimedia
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img5.jpg" alt="Lorem ipsum and switch" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Lorem ipsum and switch
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <img class="card__image" src="../../images/content/img6.jpg" alt="Collections" data-square="1" />
                    <div class="card__text card__text--spaced-v card__text--big">
                            <span class="link link--vertical-center">
                                <i class="lgicon-btn-arrow-circle" aria-hidden="true"></i>
                            </span>
                        Collections
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card card--bg bg--gradient-2 no-shadow" data-square="1">
                        <div class="fullCenter">
                            <div class="card__text text-white card__text--big-2 mb--20">
                                View all<br>
                                products
                            </div>
                            <div class="social social--neg social--white">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section section--bg" style="background-image: url('../../images/bg-ecatalogue.jpg');">
    <div class="section__content--centered">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3 text-center">
                    <i class="lgpicto--title lgicon-btn-list text-white"></i>
                    <h2 class="title text-white">
                        <span class="title--normal">Featured collection</span> céliane
                    </h2>
                    <a href="#" class="btn btn--no-color btn--icon-right no-shadow">
                        Discover</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding">
    <div class="container-fluid">
        <div class="row">
            <div id="refSlider" class="carousel no-padding slide carousel-fade" data-ride="carousel" data-interval="false">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-sm-4 bg--gradient-1">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-housebold"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Residential</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-2">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-bigdesk"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Office</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                        <div class="col-sm-4 bg--gradient-3">
                            <div class="card-link--large">
                                <i class="lgpicto--big text-white lgicon-btn-sheetmedal"></i>
                                <h2 class="title title--transform-none title--light text-white mb--40">Education</h2>
                                <a href="#" class="btn btn--no-color btn--icon-right no-shadow">Discover</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#refSlider" data-slide="prev">
                    <span class="icon-prev"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#refSlider" data-slide="next">
                    <span class="icon-next"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>


<?php require_once('../../parts/footer.php'); ?>
</body>
</html>