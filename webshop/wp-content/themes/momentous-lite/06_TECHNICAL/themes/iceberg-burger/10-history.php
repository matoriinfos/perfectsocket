<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - History</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="banner__title">History</h1>

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About us</a></li>
                        <li class="active">History</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section class="section section--grey section--small-top-padding">
        <div class="container">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-cinema"></i>
                    <h2 class="title mb--40">Title level 2</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis
                    </p>
                </div>
            </div>
        </div>
    </section>


    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="timeline">
                        <li>
                            <div class="title title--xsmall text-color-1">1850 - 1900</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>

                        <li class="timeline-inverted">
                            <div class="title title--xsmall text-color-1">1901 - 1918</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="title title--xsmall text-color-1">1919 - 1955</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>

                        <li class="timeline-inverted">
                            <div class="title title--xsmall text-color-1">1956 - 1980</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="title title--xsmall text-color-1">1981 - 2000</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>

                        <li class="timeline-inverted">
                            <div class="title title--xsmall text-color-1">2001 - 2010</div>

                            <h3 class="title title--medium mb--20">
                                Title level 3
                            </h3>
                            <p class="para--border-large mb--30">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            </p>
                            <img class="img-responsive mb--15" src="../../images/placeholders/history-1.jpg" alt="" />

                            <div class="flex-v flex-v--left">
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social-labeled">
                                <span class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </span>
                                <span class="social-labeled__label text-color-11">
                                    Download<br>
                                    hd image
                                </span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="text-center">
            <div class="flex-v">
                <a href="#" class="social social--inline social--neg">
                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                </a>
                <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                </a>
                <a href="#" class="btn">Contact us</a>
            </div>

        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-world"></i>
                    <h2 class="title mb--40">Discover more about Legrand history</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis
                    </p>

                    <a href="#" class="btn">Read more on legrand.com</a>
                </div>
            </div>
        </div>
    </section>


<?php require_once('../../parts/footer.php'); ?>
</body>
</html>