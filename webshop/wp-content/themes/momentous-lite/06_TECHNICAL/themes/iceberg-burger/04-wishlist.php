<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Wishlist</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <h1 class="banner__title">Wishlist</h1>

            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Wishlist</li>
            </ul>
        </div>
    </div>

    <section class="section section--small-top-padding">
        <div class="container mb--10">
            <div class="text-right mb--45">
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
                <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
            </div>
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-cinema"></i>
                    <h2 class="title mb--40">Title level 2</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="title title--small title--hr text-color-1">YOUR WISHLIST</div>

            <article class="article-container article-container--small-padding border-bottom mb--0">
                <div class="row flex-v no-flex-xs">
                    <div class="col-md-8 col-sm-6 mb--10">
                        <div class="title mb--0">Product</div>
                        <ul class="trail">
                            <li>Céliane</li>
                            <li class="active">Switches by Céliane</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <div class="flex-v flex-v--right">
                            <a href="#" class="social social--inline social--neg">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            
                            <a href="#" class="btn no-shadow">Read more</a>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container article-container--small-padding border-bottom mb--0">
                <div class="row flex-v no-flex-xs">
                    <div class="col-md-8 col-sm-6 mb--10">
                        <div class="title mb--0">Spaces</div>

                        <ul class="trail">
                            <li>Living spaces</li>
                            <li class="active">Offices</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <div class="flex-v flex-v--right">
                            <a href="#" class="social social--inline social--neg">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            
                            <a href="#" class="btn no-shadow">Read more</a>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container article-container--small-padding border-bottom mb--0">
                <div class="row flex-v no-flex-xs">
                    <div class="col-md-8 col-sm-6 mb--10">
                        <div class="title mb--0">Product</div>
                        <ul class="trail">
                            <li>Céliane</li>
                            <li class="active">Switches by Céliane</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <div class="flex-v flex-v--right">
                            <a href="#" class="social social--inline social--neg">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            
                            <a href="#" class="btn no-shadow">Read more</a>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container article-container--small-padding border-bottom mb--0">
                <div class="row flex-v no-flex-xs">
                    <div class="col-md-8 col-sm-6 mb--10">
                        <div class="title mb--0">Spaces</div>

                        <ul class="trail">
                            <li>Living spaces</li>
                            <li class="active">Offices</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <div class="flex-v flex-v--right">
                            <a href="#" class="social social--inline social--neg">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            
                            <a href="#" class="btn no-shadow">Read more</a>
                        </div>
                    </div>
                </div>
            </article>

            <article class="article-container article-container--small-padding border-bottom mb--60">
                <div class="row flex-v no-flex-xs">
                    <div class="col-md-8 col-sm-6 mb--10">
                        <div class="title mb--0">Product</div>
                        <ul class="trail">
                            <li>Céliane</li>
                            <li class="active">Switches by Céliane</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <div class="flex-v flex-v--right">
                            <a href="#" class="social social--inline social--neg">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>
                            
                            <a href="#" class="btn no-shadow">Read more</a>
                        </div>
                    </div>
                </div>
            </article>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="flex-v">
                        <a href="#" class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social-modal-trigger social--inline mr--25 social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="btn">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>