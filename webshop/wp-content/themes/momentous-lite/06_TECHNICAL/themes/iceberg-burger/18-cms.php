<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - CMS Page</title>

    <?php require_once('../../parts/head--iceberg.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--no-gradient banner--burger">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1 class="banner__title title--light mb--60">Title level 1</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit
                    </p>

                    <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Solutions</a></li>
                        <li><a href="#">Hotels</a></li>
                        <li class="active">Lorem ipsum</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section class="section">
        <div class="container">

            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-pushlight"></i>
                    <h2 class="title mb--40">Title level 2</h2>

                    <div class="cms-content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit, sed do eiusmod tempor incididunt ut labore et
                            dolore magna aliqua. Ut enim ad minim veniam, quis
                            nostrud exercitation ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in
                            reprehenderit in voluptate velit esse cillum.
                        </p>
                        <p>
                            <strong>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam, quis nostrud exercitation ullamco laboris
                                nisi ut aliquip ex ea commodo consequat. Duis aute
                                irure dolor in reprehenderit in voluptate velit esse
                                cillum.
                            </strong>
                        </p>
                        <p>
                            <em>
                                And comfort are there to ensure a maximum fill rate. Whatever the type of establishment, every detail counts when it.
                            </em>
                        </p>

                        <a href="#">The type of establishment</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-2 col-sm-6">
                    <h2 class="title mb--30">Title level 2</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserun.
                    </p>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="img-legend">
                        <img src="../../images/content/img1.jpg" class="img-responsive">
                        <div class="img-legend__text">
                            <h3 class="title text-white title--h4">Title level 3</h3>
                            <p class="para--border-large text--small mb--0">
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">

            <div class="row mb--80">
                <div class="col-md-6">
                    <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Sed ut perspiciatis unde omnis iste
                        natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam.
                    </p>
                    <p>
                        Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.
                        Phasellus leo nunc, bibendum vel tellus at, mattis
                        cursus ligula. Aenean varius diam nec ex fermentum, non
                        pretium lectus venenatis. Pellentesque mauris nulla,
                        porta nec faucibus tincidunt, ornare non mi. Nunc vel
                        diam dictum, viverra lorem et, tristique ligula. Donec
                        eget vulputate lectus, scelerisque consequat est.
                        Pellentesque finibus sem in scelerisque blandit.
                        Suspendisse mollis tempor nunc ac efficitur. Lorem ipsum
                        dolor sit amet, consectetur adipisicing elit, sed do
                        eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Sed ut perspiciatis unde omnis iste
                        natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam.
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 mb--20">
                            <img class="img-responsive" src="../../images/content/img11.jpg">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img3.jpg">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img3.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb--80">
                <div class="col-md-6 col-md-push-6">
                    <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>

                    <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        prehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Lorem ipsum dolor sit amet,
                        consectetur adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua.
                    </p>

                </div>
                <div class="col-md-6 col-md-pull-6">
                    <img class="img-responsive" src="../../images/content/img11.jpg">
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="flex-v flex-v--wrap">
                            <a href="#" class="social social--inline social--neg">
                                <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="social social--svg mr--15">
                                <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="btn">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="video-container">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="title title--transform-none title--light mb--40">Title level 3</h3>
                    <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <p>
                        In pulvinar pretium mi at varius. Cras ac neque in
                        libero ornare commodo vel vitae leo. Proin convallis
                        mauris diam, a malesuada libero euismod porta. Mauris in
                        volutpat massa. Aliquam vulputate facilisis dui, ac
                        molestie lorem sodales fringilla. Donec ullamcorper,
                        magna vitae ornare vestibulum, sem metus interdum neque,
                        eget ultrices lectus augue eget lectus. Ut et risus
                        dictum arcu porta efficitur. Nunc a felis lacus. Nam vel
                        efficitur eros, ut tempor libero. Nam felis lacus,
                        maximus a orci ac, condimentum faucibus tellus. Aenean
                        sit amet ex semper.
                    </p>
                </div>
                <div class="col-sm-6">
                    <p>
                        Quisque cursus enim nec dui pellentesque tincidunt.
                        Interdum et malesuada fames ac ante ipsum primis in
                        faucibus. Nulla facilisi. Proin nec porta metus. Sed a
                        libero in nibh aliquam tincidunt. Etiam non lectus
                        augue. Donec ultrices nisi sed tortor volutpat, at
                        vestibulum nulla condimentum. Suspendisse tincidunt
                        lacus et massa tincidunt efficitur. Curabitur ut lorem
                        sit amet mi cursus faucibus sit amet vitae augue.
                        Maecenas mi erat, accumsan non mattis congue, gravida ut
                        est. Quisque sed lorem volutpat turpis vestibulum
                        fringilla in a velit.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--bg banner banner--no-gradient" style="background-image: url('../../images/bg-ecatalogue.jpg');">
        <div class="section__content--centered">
            <div class="container text-white">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="para--border-small para--no-color">
                            <h1 class="banner__title title--light">
                                Title<br>
                                level<br>
                                1
                            </h1>
                            <p>
                                Quisque cursus enim nec dui pellentesque
                                tincidunt. Interdum et malesuada fames ac ante
                                ipsum primis in faucibus. Nulla facilisi. Proin
                                nec porta metus.
                            </p>

                            <a href="#" class="btn btn--no-color">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--no-padding video-container--small-height">
        <div class="video-container">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" allowfullscreen></iframe>
        </div>
    </section>


    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <iframe class="mb--15" width="100%" height="315" src="https://www.youtube.com/embed/xBHMaxgA440"    frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col-sm-6">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Donec non arcu sit amet quam
                                luctus feugiat eu in orci. Duis a pretium velit,
                                sit amet egestas nulla. Donec ornare fringilla
                                mauris eu convallis. Etiam congue dapibus velit,
                                ut sagittis risus dignissim ut. Integer in massa
                                sit amet metus finibus finibus. Quisque bibendum
                                magna ut eleifend cursus.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="title mb--30">Title level 2</h2>
                    <p>
                        Pellentesque egestas aliquet nisi, vitae porttitor dui
                        semper in. Aliquam vitae est efficitur, finibus lacus
                        id, suscipit metus. Cras convallis nisi quis quam
                        maximus, sit amet molestie neque vestibulum. Quisque
                        tempor porta luctus. Nam lorem augue, lacinia eget
                        semper ut, congue eget lorem. Ut eu posuere magna, at
                        semper mauris. Curabitur faucibus ex cursus ex imperdiet
                        efficitur. Orci varius natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Donec ornare
                        placerat nunc, non semper felis laoreet sit amet. Nam
                        elementum auctor purus. Nunc erat orci, pellentesque et
                        nibh eget, volutpat accumsan velit. Integer non odio non
                        purus auctor elementum ut a augue. Mauris lobortis
                        consequat odio sit amet sollicitudin.
                    </p>
                </div>
                <div class="col-sm-6">
                    <img class="img-responsive" src="../../images/content/img1.jpg">
                </div>
            </div>
        </div>
    </section>

    <section class="section section--bg section--quote bg--hover-gradient-2" style="background-image: url('../../images/bg-ecatalogue.jpg');">
        <div class="section__content--centered">
            <div class="container text-center text-white">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <p class="para--quote">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet vestibulum magna. Morbi quis quis
                            tortor auctor ultricies.
                        </p>

                        <div class="title title--medium title--light text-white mb--10">Bernard Laurent</div>
                        <div class="card__text card__text--xsmall text-white">Chargé d'affaires</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <img class="img-responsive mb-xs--30" src="../../images/content/img3.jpg">
                </div>
                <div class="col-sm-6">
                    <h2 class="title mb--30">Title level 2</h2>
                    <p>
                        Pellentesque egestas aliquet nisi, vitae porttitor dui
                        semper in. Aliquam vitae est efficitur, finibus lacus
                        id, suscipit metus. Cras convallis nisi quis quam
                        maximus, sit amet molestie neque vestibulum. Quisque
                        tempor porta luctus. Nam lorem augue, lacinia eget
                        semper ut, congue eget lorem. Ut eu posuere magna, at
                        semper mauris. Curabitur faucibus ex cursus ex imperdiet
                        efficitur. Orci varius natoque penatibus et magnis dis
                        parturient montes, nascetur ridiculus mus. Donec ornare
                        placerat nunc, non semper felis laoreet sit amet. Nam
                        elementum auctor purus. Nunc erat orci, pellentesque et
                        nibh eget, volutpat accumsan velit. Integer non odio non
                        purus auctor elementum ut a augue. Mauris lobortis
                        consequat odio sit amet sollicitudin.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud.
                                Exercitation ullamco laboris nisi ut aliquip ex
                                ea commodo consequat. Duis aute irure dolor in
                                prehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat non proident, sunt in culpa
                                qui officia deserunt mollit anim id est laborum.
                                Sed ut perspiciatis unde omnis iste natus error
                                sit voluptatem accusantium.
                            </p>
                            <p>
                                Eaque ipsa quae ab illo inventore veritatis et
                                quasi architecto beatae vitae dicta sunt
                                explicabo. Nemo enim ipsam voluptatem quia
                                voluptas sit aspernatur aut odit aut fugit, sed
                                quia consequuntur magni dolores eos qui ratione
                                voluptatem sequi nesciunt. Neque porro quisquam
                                est, qui dolorem ipsum quia dolor sit amet.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <img class="img-responsive" src="../../images/content/img9.jpg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <img class="img-responsive mb-xs--30" src="../../images/content/img4.jpg">
                        </div>
                        <div class="col-sm-6">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud. Exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in prehenderit
                                in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Excepteur sint occaecat
                                cupidatat non proident, sunt in culpa qui
                                officia deserunt mollit anim id est laborum. Sed
                                ut perspiciatis unde omnis iste natus error sit
                                voluptatem accusantium.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="mb--40 clearfix">
                            <div class="col-sm-7">
                                <img class="img-responsive img-fit mb-xs--20" src="../../images/content/img5.jpg" data-eq="cms-images-1">
                            </div>
                            <div class="col-sm-5">
                                <img class="img-responsive img-fit mb-xs--20" src="../../images/content/img6.jpg" data-eq="cms-images-1">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud. Exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in prehenderit
                                in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Excepteur sint occaecat
                                cupidatat non proident, sunt in culpa qui
                                officia deserunt mollit anim id est laborum. Sed
                                ut perspiciatis unde omnis iste natus error sit
                                voluptatem accusantium.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row mb--80">
                        <div class="col-md-6">
                            <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat
                            </p>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12 mb--20">
                                    <img class="img-responsive" src="../../images/content/img11.jpg">
                                </div>
                                <div class="col-sm-6 mb-xs--20">
                                    <img class="img-responsive" src="../../images/content/img1.jpg">
                                </div>
                                <div class="col-sm-6 mb-xs--20">
                                    <img class="img-responsive" src="../../images/content/img1.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb--80">
                        <div class="col-md-6 col-md-push-6">
                            <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>

                            <h4 class="title title--light title--transform-none title--h4">Title level 4</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in
                                reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat
                            </p>

                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <img class="img-responsive" src="../../images/content/img5.jpg">
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <div class="text-center">
                                <div class="flex-v flex-v--wrap">
                                    <a href="#" class="social social--inline social--neg">
                                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="social social--svg mr--15">
                                        <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" class="btn">Contact us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 mb-xs--30">
                            <img class="img-responsive" src="../../images/content/img9.jpg">
                        </div>
                        <div class="col-sm-6">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud.
                                Exercitation ullamco laboris nisi ut aliquip ex
                                ea commodo consequat. Duis aute irure dolor in
                                prehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat non proident, sunt in culpa
                                qui officia deserunt mollit anim id est laborum.
                                Sed ut perspiciatis unde omnis iste natus error
                                sit voluptatem accusantium.
                            </p>
                            <p>
                                Eaque ipsa quae ab illo inventore veritatis et
                                quasi architecto beatae vitae dicta sunt
                                explicabo. Nemo enim ipsam voluptatem quia
                                voluptas sit aspernatur aut odit aut fugit, sed
                                quia consequuntur magni dolores eos qui ratione
                                voluptatem sequi nesciunt. Neque porro quisquam
                                est, qui dolorem ipsum quia dolor sit amet.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit. Donec non arcu sit amet quam
                                luctus feugiat eu in orci. Duis a pretium velit,
                                sit amet egestas nulla. Donec ornare fringilla
                                mauris eu convallis. Etiam congue dapibus velit,
                                ut sagittis risus dignissim ut. Integer in massa
                                sit amet metus finibus finibus. Quisque bibendum
                                magna ut eleifend cursus.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="grid-no-padding">
                                    <div class="col-sm-6">
                                        <img class="img-responsive mb-xs--20" src="../../images/content/img1.jpg">
                                    </div>
                                    <div class="col-sm-6">
                                        <img class="img-responsive mb-xs--20" src="../../images/content/img6.jpg">
                                    </div>
                                    <div class="col-sm-12">
                                        <iframe class="mb--15" width="100%" height="215" src="https://www.youtube.com/embed/xBHMaxgA440"    frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section clearfix">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6" style="background-image: url('../../images/bg-ecatalogue.jpg');" data-eq="zone-quote-1"></div>
                <div class="section col-md-6 bg--gradient-2 text-center" data-eq="zone-quote-1">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">

                            <p class="para--quote text-white">
                                Lorem ipsum sit amet, consectetur adipiscing
                                elit Aliquam sit amet dolor vestibulum magna...
                            </p>

                            <div class="title title--medium title--light text-white mb--10">Bernard Laurent</div>
                            <div class="card__text card__text--xsmall text-white">Chargé d'affaires</div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row grid-no-padding">
                <div class="col-md-6 col-md-push-6">
                    <iframe width="100%" height="360" src="https://www.youtube.com/embed/xBHMaxgA440" frameborder="0" data-eq="zone-quote-2" allowfullscreen></iframe>
                </div>
                <div class="section col-md-6 col-md-pull-6 bg--gradient-2 text-center" data-eq="zone-quote-2">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <p class="para--quote text-white">
                                Lorem ipsum sit amet, consectetur adipiscing
                                elit Aliquam sit amet dolor vestibulum magna...
                            </p>

                            <div class="title title--medium title--light text-white mb--10">Bernard Laurent</div>
                            <div class="card__text card__text--xsmall text-white">Chargé d'affaires</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">
                        <div class="col-sm-8">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud. Exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in prehenderit
                                in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Excepteur sint occaecat
                                cupidatat non proident, sunt in culpa qui
                                officia deserunt mollit anim id est laborum. Sed
                                ut perspiciatis unde omnis iste natus error sit
                                voluptatem accusantium.
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img5.jpg">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img5.jpg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <div class="row">

                        <div class="col-sm-12">
                            <h2 class="title mb--30">Title level 2</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud. Exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in prehenderit
                                in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Excepteur sint occaecat
                                cupidatat non proident, sunt in culpa qui
                                officia deserunt mollit anim id est laborum. Sed
                                ut perspiciatis unde omnis iste natus error sit
                                voluptatem accusantium.
                            </p>
                        </div>

                        <div class="mb--40 clearfix">
                            <div class="col-sm-7">
                                <img alt="" class="img-responsive img-fit mb-xs--20" src="../../images/content/img1.jpg" data-eq="cms-images-2" />
                            </div>
                            <div class="col-sm-5">
                                <img alt="" class="img-responsive img-fit mb-xs--20" src="../../images/content/img5.jpg" data-eq="cms-images-2" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row mb--80">
                <div class="col-md-6">
                    <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Sed ut perspiciatis unde omnis iste
                        natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo
                        inventore veritatis et quasi architecto beatae vitae
                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                        voluptas sit aspernatur aut odit aut fugit, sed quia
                        consequuntur magni dolores eos qui ratione voluptatem
                        sequi nesciunt neque porro quisquam est, qui dolorem
                        ipsum quia dolor sit amet, consectetur, adipisci velit,
                        sed quia non numquam.
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 mb--20">
                            <img class="img-responsive" src="../../images/content/img11.jpg">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img5.jpg">
                        </div>
                        <div class="col-sm-6">
                            <img class="img-responsive mb-xs--20" src="../../images/content/img5.jpg">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb--80">
                <div class="col-md-6 col-md-push-6">
                    <h3 class="title title--light title--transform-none mb--40">Title level 3</h3>

                    <h4 class="title title--light title--transform-none title--h4 mb--15">Title level 4</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip ex
                        ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Sed ut perspiciatis unde omnis iste
                        natus error sit voluptatem accusantium doloremque
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo
                        inventore veritatis et quasi architecto beatae vitae
                        dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                        voluptas sit aspernatur aut odit aut fugit, sed quia
                        consequuntur magni dolores eos qui ratione voluptatem
                        sequi nesciunt. Neque
                    </p>

                </div>
                <div class="col-md-6 col-md-pull-6">
                    <img class="img-responsive" src="../../images/content/img11.jpg">
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="text-center">
                        <div class="flex-v flex-v--wrap">
                            <a href="#" class="social social--inline social--neg">
                                <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="social social--svg mr--15">
                                <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                            </a>
                            <a href="#" class="btn">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--grey">
        <div class="container">
            <div class="row">
                <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                    <i class="lgpicto--title lgicon-btn-housebold"></i>
                    <h2 class="title mb--30">Title level 2</h2>
                    <p>
                        Whatever the type of establishment, every detail counts
                        when it comes to building customer loyalty. High quality
                        service and comfort are there to ensure a maximum fill
                        rate. Whatever the type of establishment, every detail
                        counts when it comes to building customer loyalty. High
                        quality service and comfort are there to ensure a
                        maximum fill rate.
                    </p>
                    <p>
                        The type of establishment, every detail counts when it
                        comes to building customer loyalty. High quality service
                        and comfort are there to ensure a maximum fill rate.
                        Whatever the type of establishment, every detail counts
                        when it comes to building customer loyalty. High quality
                        service and comfort are there to ensure a maximum fill
                        rate.Whatever the type of establishment, every detail
                        counts when it comes to building customer loyalty. High
                        quality service and comfort are theomer loyalty. High
                        quality service and comfort are there to ensure a
                        maximum fill rate. Whatever the type of establishment,
                        every detail counts.
                    </p>
                    <p>
                        <strong>
                            Whatever the type of
                            establishment, every detail counts when it comes to
                            building customer loyalty. High quality service and
                            comfort are there to ensure a maximum fill rate.
                        </strong>
                    </p>

                    <a href="#" class="social social--inline social--neg">
                        <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="social social-modal-trigger social--inline social--neg">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="social social--inline social--neg">
                        <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>