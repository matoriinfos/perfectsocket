require('es6-promise').polyfill();

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    jsmin = require('gulp-jsmin'),
    sourcemaps = require('gulp-sourcemaps'),
    run = require('gulp-run'),
    spritesmith = require('gulp.spritesmith'),
    browserify = require('gulp-browserify'),
    autoprefixer = require('gulp-autoprefixer'),
    php2html = require("gulp-php2html"),
    fs = require('fs');

var basePath = './';

var configuration =
{
    'spriteDirectory': basePath + 'images/spritable/**/*.png',
    'jsDirectory': basePath + 'js/app.js',
    'sassDirectory': basePath + 'scss/**/*.scss',
    'phpDirectory': basePath + 'themes/'
};

// Compile the .scss files into .css
gulp.task('css', function () {
    gulp.src(configuration.sassDirectory)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(basePath + 'css'));
});

// Minify the js files
gulp.task('js', function () {
    gulp.src(configuration.jsDirectory)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(browserify())
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(basePath + 'js'));
});

// Generate a sprite image with multiple images on images/spritable folder
gulp.task('sprite', function () {
    var datas = gulp.src(configuration.spriteDirectory)
        .pipe(spritesmith({
            imgName: 'images/sprite.png',
            cssName: 'scss/_sprite.scss'
        }));

    datas.img.pipe(gulp.dest(basePath));
    datas.css.pipe(gulp.dest(basePath));
});

// use gulp-run to start a pipeline
gulp.task('kss', function () {
    return run('npm run kss').exec()    // prints "Hello World\n".
        ;
});

// Compile the php files into html
gulp.task('html', function() {
    var themes = fs.readdirSync(configuration.phpDirectory);
    themes.forEach(function(theme) {
        gulp.src(configuration.phpDirectory + '/' + theme + '/*.php')
            .pipe(php2html())
            .pipe(gulp.dest("./compiled/" + theme));
    });
});

// Production ready build
gulp.task('build', ['sprite', 'css', 'js', 'html', 'kss']);

gulp.task('default', ['sprite', 'css', 'js', 'kss'], function () {
    gulp.watch([basePath + 'scss/**/*.scss'], ['css']);
    gulp.watch([basePath + 'js/app.js'], ['js']);
    gulp.watch([basePath + 'images/spritable/**/*.png'], ['sprite']);
    gulp.watch([basePath + 'scss/**/*.scss'], ['kss']);
});