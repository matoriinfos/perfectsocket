# Install the solution to work in development mode

* Install node (version < 5.0.0) - <a href="https://nodejs.org/en/">setup</a>
* Install gulp - <a href="https://bower.io/">setup</a>

To prepare this solution, you need to :

    > npm install

This will create or regenerate the `node_modules` folder in root directory

    > gulp

This will launch a watcher. Each time you will modify scss or js, the watcher starts some tasks like compiling, regenerating documentation, etc. You have to let this command in 'watch' mode. For your interest, watch the `gulpfile.js`.

In order to generate the CSS documentation, if you want to launch that manualy you should execute the following command:

    > npm run kss

As you understood, kss is used to generate the documentation. To know more about it, <a href="https://github.com/kss-node/kss-node">click here</a>.

# Urls

<a href="/">/</a> (theme choice)

<a href="readme.md">/readme.md</a> (here)

<a href="styleguide">/styleguide</a> (documentation frontend)

<a href="recommendations.md">/recommendations.md</a> (tool recommandation)

<a href="compiled">/compiled</a> (Version of integration without advanced tools, full html/css/js only)


# Folders explain

##  /bower_components

Contains all bower external library.

##  /compiled

This is the "compiled" version of HTML. All pages are full HTML without php tags (require_once)

##  /css

This folder is for compress .css, final files which can be included in DOM html

##  /font

Font for font awesome AND custom font for pictogram

##  /images

All background images, content images, etc.

##  /js

App.js is the application javascript for sliders, filters, etc. It is compiled with gulp in app.min.js in the same folder.
Some jquery plugin was developed and put in this folder too.

## /node_modules

Contains libraries for preproduction (development) jobs like compressing css files, generating sprites, livereload, etc.

## /parts

Contains common parts of all php file, header and footer, for all headers (burger menu or not).

## /scss

This is uncompressed / unminified scss files. Use to develop.

## /styleguide

Folder created from scss comment to generate a full documentation about design

## /themes

Php file of this entire integration, separated by folder and per theme

# Required development level

## Use and do not modify

To use compiled or php version without modifying sources, the development team should only know the basics of HTML / PHP.

They only have to import CSS / JS and copy past DOM HTML into their source code.

## Use and modify

In order to fully understand how to use this package the development team should master some technologies.

* `SCSS` is the principal,
* `node` and `npm` are needed
* `Gulp` to compile
* `Javascript`

# Tools recommandation

See <a href="recommendations.md">recommendations.md</a>