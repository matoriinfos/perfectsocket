<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Home Page</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1, width=device-width">

    <!-- Stylesheets -->
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/lg-icons.css" type="text/css">
    <link rel="stylesheet" href="css/fruity.min.css" type="text/css">
</head>
<body>

<section class="section">
    <div class="section__header">
        <h2 class="title">Legrand</h2>
        <h3 class="title title--light">Theme choice</h3>
    </div>
    <div class="container">
        <div class="row mb--30">
            <div class="col-md-6">
                <div class="card ">
                    <img class="card__image" src="images/fruity.jpg">
                    <div class="card__text card__text--spaced card__text--big">Fruity (Classic header)</div>
                    <a href="themes/fruity/" class="btn card__link">View fruity theme</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card ">
                    <img class="card__image" src="images/iceberg.jpg">
                    <div class="card__text card__text--spaced card__text--big">Iceberg (Classic header)</div>
                    <a href="themes/iceberg/" class="btn card__link">View iceberg theme</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card ">
                    <img class="card__image" src="images/fruity-burger.jpg">
                    <div class="card__text card__text--spaced card__text--big">Fruity (Burger menu)</div>
                    <a href="themes/fruity-burger/" class="btn card__link">View fruity theme</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card ">
                    <img class="card__image" src="images/iceberg-burger.jpg">
                    <div class="card__text card__text--spaced card__text--big">Iceberg (Burger menu)</div>
                    <a href="themes/iceberg-burger/" class="btn card__link">View iceberg theme</a>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>