# Plugins

## Frontend recommendations

All frontend plugins needed are used in integration.

* `jQuery`
* `isotope` is here for all listing grids (social media wall for example)
* `bootstrap` javascript for sliders and other elements

### GMAP

For the Google map iframe you have to change the Google API KEY by creating a new one and replacing the one on the script tag.

`<script src="https://maps.googleapis.com/maps/api/js?key=[THE NEW API KEY]"></script>`

In order to add marker you have to add another marker:

    var marker_demo_2 = new google.maps.Marker({
        position: {lat: 48.860611, lng: 2.337644},
        map: map,
        icon: blueMarker
    });
    markers.push(marker_demo_2);

### Fonts

fonts svg generated by https://glyphter.com/ .

## Backend recommendations

The following best practices are recommended to use. They are general and not really relative to the development team choices.

* All coding convention MUST be followed: https://www.drupal.org/docs/develop/standards/coding-standards
* Follow oriented object convention: https://www.drupal.org/docs/develop/coding-standards/object-oriented-code
* Most of the code MUST be coded on `/modules/custom` their is no need to full hard code in `/themes/XXX/XXX.theme`
* Any application configuration MUST be hard coded (type of node, extra fields, etc) and not created in admin and DB stocked. It makes no sense to stock architecture data of your application on DB.
* Security logic MUST be followed, for every line of code.
* Most generally, all of these points (https://www.drupal.org/docs/develop/standards) MUST be the references to create treatment in Drupal.
* The current structure has to be used and respected. For example if you want to modify some style, you MUST modify the `.scss` and not the `.css`, then recompile and generate the `.css` from the modified `.scss`.