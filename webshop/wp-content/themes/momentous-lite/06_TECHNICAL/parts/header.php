<header class="header-simple">
    <div class="header-simple__sticky">
        <div class="topheader hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="flex-v flex-v--left">
                            <a href="#">
                                <i class="lgpicto lgicon-btn-wishlist"></i>
                                Wishlist
                            </a>
                            <a href="#" class="btn-second btn-second--medium">
                                <i class="lgpicto lgicon-btn-pin-wtb" aria-hidden="true"></i>
                                Where to buy
                            </a>
                            <a href="#" class="btn-second btn-second--medium">
                                <i class="lgpicto lgicon-btn-projector" aria-hidden="true"></i>
                                Showroom
                            </a>

                            <div class="flex-item flex-v flex-v--right">
                                <a href="#" class="btn-second btn-second--medium">
                                    <i class="lgpicto lgicon-btn-world" aria-hidden="true"></i>
                                    Legrand.com
                                </a>
                                <a href="#" class="btn-second btn-second--medium">
                                    <i class="lgpicto lgicon-btn-contact" aria-hidden="true"></i>
                                    Contact
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn-second btn-second--medium-3">E-catalogue</a>

                                <ul class="nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">FR <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">FR</a></li>
                                            <li><a href="#">EN</a></li>
                                            <li><a href="#">PU</a></li>
                                            <li><a href="#">RU</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <nav class="topmenu text-right">
            <div class="container">
                <div class="row valignRow">
                    <div class="col-md-2 col-sm-12 col-xs-8">
                        <div class="header-burger__head-left flex-v flex-v--left">
                            <a href="#" title="Legrand">
                                <img src="../../images/logo.png" srcset="../../images/logo@2x.png 2x" class="header-simple__logo img-responsive">
                            </a>

                            <a href="#" class="header-simple__trigger visible-xs visible-sm">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>

                    </div>
                    <div class="col-md-10 col-sm-12 col-xs-12 header-simple__navigation">
                        <nav class="header-burger__dropdown header-burger">
                            <ul class="header-burger__content">
                                <li class="burger-has-children">
                                    <a href="#">Solutions</a>

                                    <ul class="burger-dropdown burger-dropdown--hidden">
                                        <li class="burger-back visible-sm visible-xs"><a href="#">Solutions</a></li>

                                        <li><a href="#">Residential</a></li>

                                        <li class="burger-has-children">
                                            <a href="#">Office</a>

                                            <ul class="burger-dropdown--hidden">
                                                <li class="burger-back"><a href="#">Office</a></li>
                                                <li><a href="#">Switch</a></li>
                                                <li><a href="#">Plates</a></li>
                                            </ul>
                                        </li>

                                        <li class="burger-has-children">
                                            <a href="#">Education</a>
                                            <ul class="burger-dropdown--hidden">
                                                <li class="burger-back"><a href="#">Education</a></li>
                                                <li><a href="#">Switch</a></li>
                                                <li><a href="#">Plates</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="#">Menu item 4</a></li>

                                        <li><a href="#">Menu item 5</a></li>

                                        <li><a href="#">Menu item 6</a></li>

                                        <li><a href="#">Menu item 7</a></li>

                                        <li><a href="#">Menu item 8</a></li>

                                        <li><a href="#">Menu item 9</a></li>

                                        <li><a href="#">Menu item 10</a></li>

                                        <li><a href="#">Menu item 11</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Products</a>
                                </li>
                                <li>
                                    <a href="#">Spaces</a>
                                </li>
                                <li>
                                    <a href="#">Brands</a>
                                </li>
                                <li>
                                    <a href="#">Reference projects</a>
                                </li>
                                <li>
                                    <a href="#">Resources and tools</a>
                                </li>
                                <li>
                                    <a href="#">About us</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <div class="hover-header">
        <div class="container hover-header__bg">
            <div class="col-sm-7 header-search">
                <div class="row">
                    <div class="col-xs-10">
                        <input type="text" placeholder="What are you looking for ?" class="header-search__field">
                    </div>
                    <div class="col-xs-2">
                        <button type="submit" class="header-search__submit">Ok</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 header-customiz">
                <a href="#" class="title title--light text-white">
                    <i class="lgpicto lgicon-btn-cursor text-white"></i>
                    Customize your experience
                </a>
                <div class="customize-menu">
                    <div class="customize-menu__label">Tell us who you are</div>
                    <ul class="customize-menu__nav">
                        <li>
                            <a href="#">Contractor or Integrator</a>
                        </li>
                        <li>
                            <a href="#">Distributor</a>
                        </li>
                        <li>
                            <a href="#">Facility or Technology Manager</a>
                        </li>
                        <li>
                            <a href="#">Architect</a>
                        </li>
                        <li>
                            <a href="#">Home Builder</a>
                        </li>
                        <li>
                            <a href="#">Homeowner</a>
                        </li>
                        <li>
                            <a href="#">Specifier, Designer or Consultant</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>