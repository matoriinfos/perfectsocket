<aside class="social__sticky">
    <ul>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-map-marker"></i>
                    <span class="social__text">Where to buy</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-commenting-o"></i>
                    <span class="social__text">Contact us</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-facebook"></i>
                    <span class="social__text">Facebook</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-twitter"></i>
                    <span class="social__text">Twitter</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-linkedin"></i>
                    <span class="social__text">Linkedin</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-pinterest-p"></i>
                    <span class="social__text">Pinterest</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-youtube-play"></i>
                    <span class="social__text">Youtube</span>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                    <span class="social__text">Print</span>
                </div>
            </a>
        </li>
    </ul>
</aside>

<section class="section section--newsletter text-center">
    <i class="lgpicto--title lgicon-btn-letter text-white"></i>
    <h2 class="title text-white">Newsletter subscription</h2>
    <div class="title title--small mb--60 text-white">Receive the latest informations</div>

    <form action="#">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-3">
                    <input class="lg-input" type="text" placeholder="Enter Email Address" />
                </div>
                <div class="col-lg-2">
                    <button type="submit" class="btn">Sign up</button>
                </div>
            </div>
        </div>
    </form>
</section>

<footer class="footer">
    <div class="f-links">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div class="f-links__image no-eq-sm no-eq-xs" data-eq="f-links__nav">
                        <img src="../../images/logo-small.png" srcset="../../images/logo-small@2x.png 2x" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <nav>
                                <ul class="no-eq-xs" data-eq="f-links__nav">
                                    <li><a class="f-link__link" href="#">E-Catalogue</a></li>
                                    <li><a class="f-link__link" href="#">Solutions</a></li>
                                    <li><a class="f-link__link" href="#">Products</a></li>
                                    <li><a class="f-link__link" href="#">Spaces</a></li>
                                    <li><a class="f-link__link" href="#">Brands</a></li>
                                    <li><a class="f-link__link" href="#">About us</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <nav>
                                <ul class="no-eq-xs" data-eq="f-links__nav">
                                    <li><a class="f-link__link" href="#">Reference projects</a></li>
                                    <li><a class="f-link__link" href="#">Inspirations</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <nav>
                                <ul class="no-eq-xs" data-eq="f-links__nav">
                                    <li><a class="f-link__link" href="#">Press contact</a></li>
                                    <li><a class="f-link__link" href="#">Press release</a></li>
                                    <li><a class="f-link__link" href="#">Press kit</a></li>
                                    <li><a class="f-link__link" href="#">Social media wall</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <nav>
                                <ul class="no-eq-xs" data-eq="f-links__nav">
                                    <li><a class="f-link__link f-link__link--big" href="#">Contact</a></li>
                                    <li><a class="f-link__link f-link__link--big" href="#">Training</a></li>
                                    <li><a class="f-link__link" href="#">Ressources and tools</a></li>
                                    <li><a class="f-link__link" href="#">News</a></li>
                                    <li><a class="f-link__link" href="#">Blog</a></li>
                                    <li><a class="f-link__link" href="#">Social media wall</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="f-social">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 f-social__container flex-v no-flex-sm no-flex-xs">
                    <div class="f-social__social-medias flex-v">
                        <span class="f-social__label">Follow us</span>
                        <a href="#" class="social social--inline social--big"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="social social--inline social--big"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="social social--inline social--big"><i class="fa fa-linkedin"></i></a>
                        <a href="#" class="social social--inline social--big"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#" class="social social--inline social--big"><i class="fa fa-youtube-play"></i></a>
                    </div>
                    <div class="f-social__social-links">
                        <a href="#" class="f-social__link f-social__link--first">General Conditions</a>
                        <a href="#" class="f-social__link">Site map</a>
                        <a href="#" class="f-social__link f-social__link--last">FAQ</a>
                        <a href="#" class="f-social__link f-social__link--big">Legrand.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" id="share-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body text-center">
                <div class="modal-dialog__title mb--20">Share on</div>

                <div class="social-modal">
                    <ul>
                        <li class="social-modal social-modal--fb">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=" class="social social--xbig social--fb">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="social-modal social-modal--tw">
                            <a href="https://twitter.com/home?status=" class="social social--xbig social--tw">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="social-modal social-modal--pin">
                            <a href="https://pinterest.com/pin/create/button/?url=&media=&description=" class="social social--xbig social--pin">
                                <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../../bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../../js/app.min.js"></script>
<script type="text/javascript" src="../../js/header.js"></script>
<script type="text/javascript" src="../../js/burger.js"></script>