<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Stylesheets -->
<link rel="icon" href="../../favicon.ico" type="image/x-icon" />
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,500,700" rel="stylesheet">
<link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="../../css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="../../css/lg-icons.css" type="text/css">
<link rel="stylesheet" href="../../css/lg-burger.css" type="text/css">
<link rel="stylesheet" href="../../css/fruity.min.css?v=dev2017092702" type="text/css">
