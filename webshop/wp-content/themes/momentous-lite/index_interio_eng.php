﻿<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
<title>Interio - Nopal Lux - Configurator</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
<meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
<meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
<link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
<link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
<link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
<link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
ini_set("default_charset", "UTF-8");
//header('Content-type: text/html; charset=UTF-8');
/*
Template Name: index_interio_eng
*/
?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>


<div class = "subMenu">
	<div class="left_container">
	<div class="bticino_logo"><img src="wp-content/themes/momentous-lite/logotipi/interio_logo.jpg"></div>
	<div class = "naslov_proizvoda"></div>
	</div>
	<div class = "right_container">
<!--	<div class="flags"><div class="flag"><img src="wp-content/themes/momentous-lite/flags/brit.jpg" title="english" id="brit1"></div><div class="flag"><img src="wp-content/themes/momentous-lite/flags/srbDark.jpg" title="srpski" id="srb2"></div></div> -->
	<div class = "naslov_kompleta">MOJ IZBOR</div>
	</div>
</div>
<div class = "leva">
<?php 

// KOD ZA BAZA NA NETU
$servername = servername;
$username = username;
$password =password;
$dbname = baza;

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM interio_prek_mod_1");
	$stmt2 = $conn->prepare("SELECT * FROM interio_prek_mod_2");
	$stmt3 = $conn->prepare("SELECT * FROM interio_kutije");
	$stmt4 = $conn->prepare("SELECT * FROM interio_boje_okvira");
	$stmt5 = $conn->prepare("SELECT * FROM interio_okviri_svi");
	$stmt6 = $conn->prepare("SELECT * FROM interio_dimeri_mod_1");
	$stmt7 = $conn->prepare("SELECT * FROM interio_dimeri_mod_2");
	$stmt8 = $conn->prepare("SELECT * FROM bt_living_releji_mod_1");
	$stmt9 = $conn->prepare("SELECT * FROM primera_kompleti_kupatila");
	$stmt11 = $conn->prepare("SELECT * FROM interio_prikljucnice_mod_1");
	$stmt12 = $conn->prepare("SELECT * FROM interio_prikljucnice_mod_2");
	$stmt13 = $conn->prepare("SELECT * FROM interio_dr_prikljucnice_mod_1");
	$stmt14 = $conn->prepare("SELECT * FROM interio_dr_prikljucnice_mod_2");
	$stmt15 = $conn->prepare("SELECT * FROM interio_tv_prikljucnice_mod_1");
	$stmt16 = $conn->prepare("SELECT * FROM interio_tv_prikljucnice_mod_2");
	$stmt17 = $conn->prepare("SELECT * FROM interio_maske_mod_1");
	$stmt18 = $conn->prepare("SELECT * FROM interio_maske_mod_1_2");
	$stmt19 = $conn->prepare("SELECT * FROM bticino_living_mehanizmi_mod_1");
	$stmt20 = $conn->prepare("SELECT * FROM bticino_living_mehanizmi_mod_2");
	$stmt21 = $conn->prepare("SELECT * FROM interio_rac_uticnice_mod_1");
	$stmt22 = $conn->prepare("SELECT * FROM bt_living_det_pokreta_mod_1");
	$stmt23 = $conn->prepare("SELECT * FROM bt_living_det_pokreta_mod_2");
	$stmt24 = $conn->prepare("SELECT * FROM interio_nosaci");
	$stmt25 = $conn->prepare("SELECT * FROM bt_living_ostala_oprema");
	$stmt26 = $conn->prepare("SELECT * FROM bt_living_vodootporno");
	$stmt27 = $conn->prepare("SELECT * FROM interio_tasteri_mod_1");
	//$stmt28 = $conn->prepare("SELECT * FROM interio_tasteri__mod_2");
	$stmt29 = $conn->prepare("SELECT * FROM bticino_maske_klasicni_prek_ll");
	
	$stmt1->execute();
	$stmt2->execute();
	$stmt3->execute();
	$stmt4->execute();
	$stmt5->execute();
	$stmt6->execute();
	$stmt7->execute();
	$stmt8->execute();
	$stmt9->execute();
	$stmt11->execute();
	$stmt12->execute();
	$stmt13->execute();
	$stmt14->execute();
	$stmt15->execute();
	$stmt16->execute();
	$stmt17->execute();
	$stmt18->execute();
	$stmt19->execute();
	$stmt20->execute();
	$stmt21->execute();
	$stmt22->execute();
	$stmt23->execute();
	$stmt24->execute();
	$stmt25->execute();
	$stmt26->execute();
	$stmt27->execute();
	//$stmt28->execute();
	$stmt29->execute();
	
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();
}
?>

<div class="navigacija">
<ul class="glavni">
    <li class="glavni_meni" id="link_okviri"><a>RAMOVI</a></li>
    <li class="glavni_meni" id="link_prekidaci_tasteri"><a>SKLOPKE</a></li>
    <li class="glavni_meni" id="link_aksijalni"><a>TASTERI</a></li>
    <li class="glavni_meni" id="link_dimeri"><a>DIMERI</a></li>
    <li class="glavni_meni" id="link_energetske_uticnice"><a>PRIKLJUČNICE</a></li>
    <li class="glavni_meni" id="link_tv_uticnice"><a>TV PRIKLJUČNICE</a></li>
    <li class="glavni_meni" id="link_racunarske_uticnice"><a>RAČUNARSKE PRIKLJUČNICE</a></li>
    <li class="glavni_meni" id="link_multimedijalne_uticnice"><a>MULTIMEDIJALNE PRIKLJUČNICE</a></li>
    <li class="glavni_meni" id="link_maske"><a>SLEPE MASKE</a></li>
    <li class="glavni_meni" id="link_kupatilo"><a>KUPATILO</a></li>
    <li class="glavni_meni" id="link_nosaci_dozne"><a>KUTIJE</a></li>
</ul>
</div>

<div class="dropdownNovo">
  <button class="dropbtnNovo">Interio Equipment</button>
  <div class="dropdown-contentNovo">
      <li class="glavni_meni" id="link_okviri"><a>RAMOVI</a></li>
      <li class="glavni_meni" id="link_prekidaci_tasteri"><a>SKLOPKE</a></li>
      <li class="glavni_meni" id="link_aksijalni"><a>TASTERI</a></li>
      <li class="glavni_meni" id="link_dimeri"><a>DIMERI</a></li>
      <li class="glavni_meni" id="link_energetske_uticnice"><a>PRIKLJUČNICE</a></li>
      <li class="glavni_meni" id="link_tv_uticnice"><a>TV PRIKLJUČNICE</a></li>
      <li class="glavni_meni" id="link_racunarske_uticnice"><a>RAČUNARSKE PRIKLJUČNICE</a></li>
      <li class="glavni_meni" id="link_multimedijalne_uticnice"><a>MULTIMEDIJALNE PRIKLJUČNICE</a></li>
      <li class="glavni_meni" id="link_maske"><a>SLEPE MASKE</a></li>
      <li class="glavni_meni" id="link_kupatilo"><a>KUPATILO</a></li>
      <li class="glavni_meni" id="link_nosaci_dozne"><a>KUTIJE</a></li>

  </div>
</div>

<div class="bticino_levi">
<ul class = "levi_podmeni">
<li id="naslov_podmeni"></li>
    <li style="float:right"><a class="link_moduli" id="link_svi">SVI MODULI</a></li>
    <li style="float:right"><a class="link_moduli" id="link_2">2 MODULA</a></li>
    <li style="float:right"><a class="link_moduli" id="link_1">1 MODUL</a></li>
</ul>
</div>
<!-- LEVI SCROLL BAR -->
<div class="ex1">

<div class = "proizvodi" id = "prekidaci">
<?php
// VREDNOST EVRA U DINARIMA
$money = 1;
$currency = "RSD";
// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_1_opis[$i] = $row['opis_srpski'];
	$prekidaci_1_kat_br[$i] = $row['kat_br'];
	$prekidaci_1_kratak_opis[$i] = $row['opis_srpski'];
	$prekidaci_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_1=$i;
}

// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_2_opis[$i] = $row['opis_engleski'];
	$prekidaci_2_kat_br[$i] = $row['kat_br'];
	$prekidaci_2_kratak_opis[$i] = $row['opis_engleski'];
	$prekidaci_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_2=$i;
}
?>


<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_prekidaca_1)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "kontejner">';
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$prekidaci_1_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$prekidaci_1_kat_br[$i].'" data-opis-interni = "'.$prekidaci_1_kratak_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_1_cena[$i].'" data-kratak-opis ="'.$prekidaci_1_kratak_opis[$i].'">';
				//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt="'.$prekidaci_1_kratak_opis[$i].'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_1_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$prekidaci_1_kat_br[$i].'" data-interni-opis = "'.$prekidaci_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_1_cena[$i].'" data-kratak-opis ="'.$prekidaci_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_kratak_opis[$i].'</div>';
							if($_SESSION['prava']=='reseller')
                            {
                                echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                                //cena za reseller
                                echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                            }
                            else
							echo '<div class="tekst_cena">Price: '.number_format($prekidaci_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE
				//echo '<div class = "uputstvo">Preuzmi uputstvo</div>';
				echo '</div>';
			}	
	$i++;
}
?>

</div>


<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_prekidaca_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$prekidaci_2_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$prekidaci_2_kat_br[$i].'" data-opis-interni = "'.$prekidaci_2_kratak_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$prekidaci_2_cena[$i].'" data-kratak-opis ="'.$prekidaci_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt="'.$prekidaci_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$prekidaci_2_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$prekidaci_2_kat_br[$i].'" data-interni-opis = "'.$prekidaci_2_opis[$i].'" data-kat-br = "'.$prekidaci_2_kat_br[$i].'" data-cena="'.$prekidaci_2_cena[$i].'" data-kratak-opis ="'.$prekidaci_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';	
								echo '<div class="tekst_opis">'.$prekidaci_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($prekidaci_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
					
			}	
	$i++;
}
?>
</div>	
</div>	<!-- KRAJ DIV PROIZVODI PREKIDACI -->

<!-- TASTERI -->
<div class = "proizvodi" id = "aksijalni">
<?php
// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt27->fetch(PDO::FETCH_ASSOC))
{	
	$aksijalni_1_opis[$i] = $row['opis_srpski'];
	$aksijalni_1_kat_br[$i] = $row['kat_br'];
	$aksijalni_1_kratak_opis[$i] = $row['opis_srpski'];
	$aksijalni_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_1=$i;
}


// PRAVI SE NIZ SVIH PREKIDACA - VELICINE 2 MODULA
/*
$i=0;
while ($row=$stmt28->fetch(PDO::FETCH_ASSOC))
{	
	$aksijalni_2_opis[$i] = $row['interni_opis'];
	$aksijalni_2_kat_br[$i] = $row['kat_br'];
	$aksijalni_2_kratak_opis[$i] = $row['opis_srpski'];
	//$aksijalni_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$aksijalni_2_cena[$i] = $row['vp_cena']/$money;
	//$aksijalni_2_cena_money[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_prekidaca_2=$i;
}*/
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_prekidaca_1)
{
	$red_br = $red_br + 1;
	$string = $aksijalni_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "kontejner">';
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$aksijalni_1_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$aksijalni_1_kat_br[$i].'" data-opis-interni = "'.$aksijalni_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_1_cena[$i].'" data-kratak-opis ="'.$aksijalni_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt="'.$aksijalni_1_kratak_opis[$i].'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$aksijalni_1_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$aksijalni_1_kat_br[$i].'" data-interni-opis = "'.$aksijalni_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_1_cena[$i].'" data-kratak-opis ="'.$aksijalni_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$aksijalni_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$aksijalni_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($aksijalni_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($aksijalni_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($aksijalni_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE
				echo '</div>'; // KRAJ KONTEJNERA						
			}	
	$i++;
}
?>
</div>
<!-- KRAJ TASTERA -->


<!-- ISPISIVANJE PREKIDACA MOD 2 -->

<!-- <div class = "vrsta_mehanizmi" id="mehanizmi_modul_2"> -->
<?php
/*
$i=0;
//$red_br=0;
while ($i< $brojanje_prekidaca_2)
{
	$red_br = $red_br + 1;
	$string = $aksijalni_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// MEHANIZAM NA SRPSKOM
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$prekidaci_2_kat_br[$i].'"  data-modul ="'.$br_modula.'"  title="'.$aksijalni_1_kat_br[$i].'" data-opis-interni = "'.$aksijalni_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$aksijalni_1_cena[$i].'" data-kratak-opis ="'.$aksijalni_1_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt="'.$aksijalni_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$aksijalni_2_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$aksijalni_2_kat_br[$i].'" data-interni-opis = "'.$aksijalni_2_opis[$i].'" data-kat-br = "'.$aksijalni_2_kat_br[$i].'" data-cena="'.$aksijalni_2_cena[$i].'" data-kratak-opis ="'.$aksijalni_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$aksijalni_2_kat_br[$i].'</div>';	
								echo '<div class="tekst_opis">'.$aksijalni_2_kratak_opis[$i].'</div>';
								echo '<div class="tekst_cena">Price: '.number_format($aksijalni_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';						
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}
*/?>
<!-- </div>	-->

</div><!-- KRAJ DIV PROIZVODI AKSIJALNI -->

<div class = "proizvodi" id = "dimeri">
<?php
// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt6->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_1_opis[$i] = $row['interni_opis'];
	$dimeri_1_kat_br[$i] = $row['kat_br'];
	$dimeri_1_kratak_opis[$i] = $row['opis_srpski'];
	$dimeri_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_1=$i;
}

// PRAVI SE NIZ SVIH DIMERA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt7->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_2_opis[$i] = $row['interni_opis'];
	$dimeri_2_kat_br[$i] = $row['kat_br'];
	$dimeri_2_kratak_opis[$i] = $row['opis_srpski'];
	$dimeri_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_2=$i;
}
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE DIMERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_dimera_1)
{
	$red_br = $red_br + 1;
	$string = $dimeri_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL
				echo '<div class = "kontejner">';
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" alt ="'.$dimeri_1_kratak_opis[$i].'" title="'.$dimeri_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$dimeri_1_kat_br[$i].'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$dimeri_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$dimeri_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM
				echo '</div>'; // KRAJ KONTEJNERA						
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
//$red_br=0;

while ($i< $brojanje_dimera_2)
{
	$red_br = $red_br + 1;
	$string = $dimeri_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// DIMER NA ENGLESKOM - 2 MODULA
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$dimeri_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$dimeri_2_kat_br[$i].'" id="'.$br_modula_id.'"  alt ="'.$dimeri_2_kratak_opis[$i].'" data-modul ="'.$br_modula.'"  title="'.$dimeri_2_kat_br[$i].'" data-interni-opis = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$dimeri_2_kat_br[$i].'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$dimeri_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$dimeri_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';	
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ DIV PROIZVODI DIMERI -->



<!-- ENERGETSKE UTICNICE -->
<div class = "proizvodi" id = "energetske_uticnice">

<?php
// PRAVI SE NIZ SVIH ENERGETSKIH UTICNICA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt11->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_1_opis[$i] = $row['interni_opis'];
	$energetske_uticnice_1_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_1_kratak_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_energetske_uticnice_1=$i;
}
// PRAVI SE NIZ SVIH DETEKTORA POKRETA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt12->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_2_opis[$i] = $row['interni_opis'];
	$energetske_uticnice_2_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_2_kratak_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_energetske_uticnice_2=$i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ENERGETSKE UTICNICE- 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_energetske_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';	
					echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$energetske_uticnice_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$energetske_uticnice_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$energetske_uticnice_1_kat_br[$i].'" data-interni-opis = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$energetske_uticnice_1_kat_br[$i].'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$energetske_uticnice_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$energetske_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($energetske_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // kraj KONTEJNERA
				
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">

<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_energetske_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				//ENERGETSKE UTICNICE - 2 MODULA - srpski
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$energetske_uticnice_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$energetske_uticnice_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$energetske_uticnice_2_kat_br[$i].'" data-interni-opis = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$energetske_uticnice_2_kat_br[$i].'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div div class="tekst_katbr">'.$energetske_uticnice_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$energetske_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($energetske_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // kraj KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ DIV ENERGETSKE UTICNICE -->


<!-- TV UTICNICE -->
<div class = "proizvodi" id = "tv_uticnice">
<?php
// PRAVI SE NIZ SVIH TV/AUDIO/HDMI UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt15->fetch(PDO::FETCH_ASSOC))
{	
	$tv_1_opis[$i] = $row['opis_srpski'];
	$tv_1_kat_br[$i] = $row['kat_br'];
	$tv_1_kratak_opis[$i] = $row['opis_srpski'];
	$tv_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_tv_1=$i;
}

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt16->fetch(PDO::FETCH_ASSOC))
{	
	$tv_2_opis[$i] = $row['opis_srpski'];
	$tv_2_kat_br[$i] = $row['kat_br'];
	$tv_2_kratak_opis[$i] = $row['opis_srpski'];
	$tv_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_tv_2=$i;
}

?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE TV/AUDIO/HDMIA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_tv_1)
{
	$red_br = $red_br + 1;
	$string = $tv_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_1_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 		
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}	
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_1_cena[$i].'" data-kratak-opis ="'.$tv_1_kratak_opis[$i].'">';		
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$tv_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_1_kat_br[$i].'" data-interni-opis = "'.$tv_1_opis[$i].'" data-kat-br = "'.$tv_1_kat_br[$i].'" data-cena="'.$tv_1_cena[$i].'" data-kratak-opis ="'.$tv_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$tv_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($tv_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // kraj KONTEJNERA
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_tv_2)
{
	$red_br = $red_br + 1;
	$string = $tv_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_2_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// TV/AUDIO/HDMI UTIČNICE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_2_cena[$i].'" data-kratak-opis ="'.$tv_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$tv_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_2_kat_br[$i].'" data-interni-opis = "'.$tv_2_opis[$i].'" data-kat-br = "'.$tv_2_kat_br[$i].'" data-cena="'.$tv_2_cena[$i].'" data-kratak-opis ="'.$tv_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$tv_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($tv_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ TV UTICNICE -->

<!-- RACUNARSKE UTICNICE -->
<div class = "proizvodi" id = "racunarske_uticnice">

<?php
// PRAVI SE NIZ SVIH RACUNARSKIH UTICNICA - VELICINE 1 MODULA

$i=0;
while ($row=$stmt21->fetch(PDO::FETCH_ASSOC))
{	
	$racunarske_uticnice_opis[$i] = $row['interni_opis'];
	$racunarske_uticnice_kat_br[$i] = $row['kat_br'];
	$racunarske_uticnice_kratak_opis[$i] = $row['opis_srpski'];
	$racunarske_uticnice_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_racunarske_uticnice=$i;
}

//ISPISIVANJE RACUNARSKIH UTICNICA - 1 MODUL
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
$i=0;
$red_br=0;
while ($i< $brojanje_racunarske_uticnice)
{
	$red_br = $red_br + 1;
	$string = $racunarske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$racunarske_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg"  alt ="'.$racunarske_uticnice_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$racunarske_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$racunarske_uticnice_kat_br[$i].'" data-interni-opis = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$racunarske_uticnice_kat_br[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div div class="tekst_katbr">'.$racunarske_uticnice_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$racunarske_uticnice_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($racunarske_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ DIV PROIZVODI - RACUNARSKE UTICNICE -->

<!-- TV/AUDIO/HDMI UTICNICE -->
<div class = "proizvodi" id = "multimedijalne_uticnice">
<?php
// PRAVI SE NIZ SVIH TV/AUDIO/HDMI UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt13->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_1_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_1_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_1_kratak_opis[$i] = $row['opis_srpski'];
	$tv_audio_hdmi_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	//$tv_audio_hdmi_uticnice_1_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$tv_audio_hdmi_uticnice_1_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_tv_audio_hdmi_uticnice_1=$i;
}

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt14->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_2_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_2_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_2_kratak_opis[$i] = $row['opis_srpski'];
	$tv_audio_hdmi_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	//$tv_audio_hdmi_uticnice_2_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$tv_audio_hdmi_uticnice_2_cena_evro[$i] = $row['vp_cena']/$evro;
	$i++;
	$brojanje_tv_audio_hdmi_uticnice_2=$i;
}

?>



<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE TV/AUDIO/HDMIA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_tv_audio_hdmi_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_1_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 		
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}	
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';		
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_tv_audio_hdmi_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_2_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// TV/AUDIO/HDMI UTIČNICE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ DIV TV/AUDIO/HDMI UTICNICE -->

<!-- MASKE -->
<div class = "proizvodi" id = "maske">
<?php

// PRAVI SE NIZ SVIH MASKI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt17->fetch(PDO::FETCH_ASSOC))
{	
	$maske_1_opis[$i] = $row['interni_opis'];
	$maske_1_kat_br[$i] = $row['kat_br'];
	$maske_1_kratak_opis[$i] = $row['opis_srpski'];
	$maske_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_maske_1=$i;
}

// PRAVI SE NIZ SVIH MASKI - VELICINE 2 MODULA

$i=0;
while ($row=$stmt18->fetch(PDO::FETCH_ASSOC))
{	
	$maske_12_opis[$i] = $row['interni_opis'];
	$maske_12_kat_br[$i] = $row['kat_br'];
	$maske_12_kratak_opis[$i] = $row['opis_srpski'];
	$maske_12_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_maske_12=$i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE MASKI - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_maske_1)
{
	$red_br = $red_br + 1;
	$string = $maske_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
					$br_modula_class = 1;
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
					$br_modula_class = 2;
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				
					if ($br_modula == 0.5 )
				{
					$br_modula_id = "pola";
					$br_modula_class = 05;
				}
				// SLEPE MASKE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$maske_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$maske_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula_brojanje.'"  title="'.$maske_1_kat_br[$i].'" data-interni-opis = "'.$maske_1_opis[$i].'" data-kat-br = "'.$maske_1_kat_br[$i].'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$maske_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA

			}	
	$i++;
}

?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
while ($i< $brojanje_maske_12)
{
	$red_br = $red_br + 1;
	$string = $maske_12_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 0.5 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
					$br_modula_class = 1;
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
					$br_modula_class = 2;
				}
					if ($br_modula == 0.5)
				{
					$br_modula_id = "pola";
					$br_modula_class = "05";
					$br_modula_brojanje = 1;
				}
				// SLEPE MASKE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula_class.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_12_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_12_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_12_cena[$i].'" data-kratak-opis ="'.$maske_12_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/'.$newstring.'.jpg" alt ="'.$maske_12_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$maske_12_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$maske_12_kat_br[$i].'" data-interni-opis = "'.$maske_12_opis[$i].'" data-kat-br = "'.$maske_12_kat_br[$i].'" data-cena="'.$maske_12_cena[$i].'" data-kratak-opis ="'.$maske_12_kratak_opis[$i].'">';
						//echo '<img src = "wp-content/themes/momentous-lite/interio/slike/dodaj.png" class="dugme_dodaj_mehanizam_1" id="'.$maske_1_kat_br[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'" data-cena="'.$maske_1_cena[$i].'" data-naziv-slike="'.$newstring.'" data-modul ="'.$br_modula.'" />';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$maske_12_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$maske_12_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($maske_12_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($maske_12_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($maske_12_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								//echo '<div>Broj modula: '.$br_modula.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
		
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ MASKI -->

<!-- VODOOTPORNO -->
<div class = "proizvodi" id = "vodootporno">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - VODOOTPORNO
$i=0;
while ($row=$stmt26->fetch(PDO::FETCH_ASSOC))
{	
	$vodootporno_opis[$i] = $row['interni_opis'];
	$vodootporno_kat_br[$i] = $row['kat_br'];
	$vodootporno_kratak_opis[$i] = $row['opis_srpski'];
	$vodootporno_cena[$i] = $row['vp_cena']/$money;
	//$vodootporno_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$vodootporno_cena_evro[$i] = $row['vp_cena']/$evro;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_vodootporno=$i;
}

//ISPISIVANJE OPREME VODOOTPORNO

$i=0;
$red_br=0;

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_vodootporno)
{
	$red_br = $red_br + 1;
	$string = $vodootporno_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
			
						//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$vodootporno_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$newstring.'" data-interni-opis = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$vodootporno_kat_br[$i].'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$vodootporno_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$vodootporno_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($vodootporno_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div>
<!-- KRAJ - VODOOTPORNO -->

<!-- UGRADNE KUTIJE -->
<div class = "proizvodi" id = "nosaci_dozne">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - VODOOTPORNO
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$vodootporno_opis[$i] = $row['interni_opis'];
	$vodootporno_kat_br[$i] = $row['kat_br'];
	$vodootporno_kratak_opis[$i] = $row['opis_srpski'];
	$vodootporno_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_vodootporno=$i;
}

//ISPISIVANJE OPREME VODOOTPORNO

$i=0;
$red_br=0;

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_vodootporno)
{
	$red_br = $red_br + 1;
	$string = $vodootporno_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
			
						//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
						echo '<div class = "kontejner">';
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$vodootporno_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$newstring.'" data-interni-opis = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$vodootporno_kat_br[$i].'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$vodootporno_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$vodootporno_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($vodootporno_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div>
<!-- KRAJ - UGRADNE KUTIJE -->



<!-- POČETAK KUPATILO -->
<div class = "proizvodi" id = "kupatilo">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - kupatilo
$i=0;
while ($row=$stmt9->fetch(PDO::FETCH_ASSOC))
{	
	$kupatilo_opis[$i] = $row['interni_opis'];
	$kupatilo_kat_br[$i] = $row['kat_br'];
	$kupatilo_kratak_opis[$i] = $row['kratak_opis_srpski'];
	$kupatilo_cena[$i] = $row['vp_cena_evro']/$money;
	$broj_modula[$i] = $row['mehanizam'];
	$i++;
	$brojanje_kupatilo=$i;
}

//ISPISIVANJE OPREME kupatilo

$i=0;
$red_br=0;

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_kupatilo)
{
	$red_br = $red_br + 1;
	$string = $kupatilo_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
			
						//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$kupatilo_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$kupatilo_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$kupatilo_cena[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$newstring.'" data-interni-opis = "'.$kupatilo_opis[$i].'" data-kat-br = "'.$kupatilo_kat_br[$i].'" data-cena="'.$kupatilo_cena[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$kupatilo_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$kupatilo_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($kupatilo_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($kupatilo_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Price: '.number_format($kupatilo_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div>
<!-- KRAJ - KUPATILO -->



<!-- DOZNE I MALTER ZA GIPS -->
<!-- <div class = "proizvodi" id = "nosaci_dozne"> -->
<?php
/*
// PRAVI SE NIZ SVE OSTALE OPREME
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_dozne_opis[$i] = $row['interni_opis'];
	$nosaci_dozne_kat_br[$i] = $row['kat_br'];
	$nosaci_dozne_kratak_opis[$i] = $row['opis_engleski'];
	$nosaci_dozne_cena[$i] = $row['vp_cena']/$money;
	//$nosaci_dozne_kratak_opis_engleski[$i] = $row['opis_engleski'];
	//$nosaci_dozne_cena_evro[$i] = $row['vp_cena']/$evro;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci_dozne=$i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE OPREME - 
$i=0;
$red_br=0;
while ($i< $brojanje_nosaci_dozne)
{
	$red_br = $red_br + 1;
	$string = $nosaci_dozne_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
				
						if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 33)
				{
					$br_modula_id = "tri_tri";
				}
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$nosaci_dozne_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" title="'.$nosaci_dozne_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$nosaci_dozne_kat_br[$i].'" data-interni-opis = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$nosaci_dozne_kat_br[$i].'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$nosaci_dozne_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$nosaci_dozne_kratak_opis[$i].'</div>';
								echo '<div class="tekst_cena">Price: '.number_format($nosaci_dozne_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}*/
?>	
<!-- </div> --> <!-- kraj DIV vrsta_mehanizmi-->
<!-- </div> --> <!-- KRAJ - nosaci_dozne -->

<!-- OKVIRI -->
<div class = "proizvodi" id = "okviri">
<?php

// PRAVI SE NIZ SVIH BOJA OKVIRA
$i=0;
while ($row=$stmt4->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_boje[$i] = $row['boje'];
	$okviri_nazivi[$i] = $row['naziv'];
	$okviri_skracenice_boja[$i] = $row['skracenica'];
	$i++;
	$brojanje_okviri=$i;
}

// PRAVI SE NIZ SVIH OKVIRA
$j=0;
while ($row=$stmt5->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_kat_br[$j] = $row['kat_br'];
	$okviri_cena[$j] = $row['vp_cena']/$money;
	$okviri_interni_opis[$j] = $row['opis_srpski'];
	$okviri_modul[$j] = $row['modul'];
	$okviri_kratak_opis[$j] = $row['opis_srpski'];
	$okviri_materijal[$j] = $row['materijal'];
	$okviri_skracenice[$j] = $row['skracenica'];
	$okviri_okviri_boje[$j] = $row['boje'];
	$okviri_opis[$j] = $row['opis_srpski'];
	$j++;
	$brojanje_svi_okviri=$j;
}

?>

<div class = "vrsta_mehanizmi" id="okviri_vrsta">
<?php

//ISPISIVANJE BOJA OKVIRA

$i=0;
$red_br=0;
$ukupan_broj_okvira = 0;

while ($i< $brojanje_okviri)
{
	$red_br = $red_br + 1;
	$string = $okviri_boje[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				//$br_modula = 1;
			
					echo '<div class = "boje_okvira">';
						echo '<div class = "slika_tekst">';
						echo '<img class="okvir_slika" src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$newstring.'.jpg" title="'.$okviri_nazivi[$i].'" id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'">';
						echo '<h2 id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'"><span>'.$okviri_nazivi[$i].'</span></h2>';
						echo '</div>';

						echo '<table class="podaci_okvira" id="podaci_okvira_'.$okviri_skracenice_boja[$i].'">'; //TABELA PODACI OKVIRA								
								
								echo '<tr><td class="kolona_modul">MODUL</td><td class="kolona_kat_br">KAT.BR.</td><td class = "kolona_cena_2">CENA</td></tr>';
								//PRIKAZIVANJE CENE - OPISA
								$j=0;
								while ($j < $brojanje_svi_okviri)
								{
									if($okviri_skracenice_boja[$i] == $okviri_skracenice[$j])
									{
										$ukupan_broj_okvira++;
										if ($okviri_modul[$j] == 33)
										{
											$okviri_modul[$j] = "3+3";
											}
                                        if($_SESSION['prava']=='reseller')
                                        {
                                            echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2"><s>'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</s><br>'.number_format($okviri_cena[$j] * $discount, 2, ',', ' ').' '.$currency.'</td></tr>';

                                        }
                                        else
										echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$okviri_kat_br[$j].'</td><td class = "kolona_cena_2">'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</td></tr>';
									}
									$j++;
								}
								
								  echo '<input type="hidden" id = "skriven" data-ukupan-broj-okvira="'.$brojanje_svi_okviri.'">';
								
							echo '</table>';//TABELA PODACI OKVIRA
							
					echo '</div>'; // kraj boje okvira
				
			}	
	$i++;
}
?>

</div>
</div> <!-- KRAJ class=PROIZVODI id=OKVIRI -->
</div><!-- KRAJ SKROLA U LEVOJ-->
</div>	<!-- KRAJ DIV LEVA -->
<!-- DESNI DEO STRANICE -->
<div class = "desna">
<div class = "desna_dugme_kreiranje"></div>
<?php 
// PRAVI SE NIZ NOSAČA
$i=0;
while ($row=$stmt24->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_opis[$i] = $row['opis_srpski'];
	$nosaci_kat_br[$i] = $row['kat_br'];
	$nosaci_kratak_opis[$i] = $row['opis_srpski'];
	//$nosaci_kratak_opis_engleski[$i] = $row['opis_engleski'];
	$nosaci_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci=$i;
}
 
echo '<ul class="podmeni_nosaci">';
echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'"><a><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$nosaci_kat_br[0].'.png" id=2 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2 MODULA</p><p class = "nosac_slova_2_broj">2</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=3 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'"><a><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$nosaci_kat_br[1].'.png" id=3 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">3 MODULA</p><p class = "nosac_slova_2_broj">3</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=4 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'"><a><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$nosaci_kat_br[2].'.png" id=4 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">4 MODULA</p><p class = "nosac_slova_2_broj">4</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=5 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'"><a><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$nosaci_kat_br[3].'.png" id=5 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">5 MODULA</p><p class = "nosac_slova_2_broj">5</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=7 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'"><a><img src = "wp-content/themes/momentous-lite/interio/slike/okviri/'.$nosaci_kat_br[4].'.png" id=7 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">7 MODULA</p><p class = "nosac_slova_2_broj">7</p><p class = "nosac_slova_2">MODULA</p></a></li>';
/*echo '<li><a><img src = "wp-content/themes/momentous-lite/interio/slike/'.$nosaci_kat_br[4].'.jpg" class="slika_nav_x_modula" id=33 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'"><br> 3+3 MODULA</a></li>';*/
echo '</ul>';
//polja za reseller
echo '<div id="prava" data-prava = "'.$prava.'"></div>';
echo '<div id="discount" data-discount = "'.$discount.'"></div>';

?>

<!-- DESNI SCROLL BAR -->
<div class="ex2">
<div class = "desna_mehanizmi"></div>
</div><!-- KRAJ DESNOG SCROLL BARA -->
</div>
<?php /*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>