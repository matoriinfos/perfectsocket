# Legrand design graphic

## Tools used

* node & npm (https://www.npmjs.com/get-npm)
* bower (https://bower.io/)

Node & npm are used to compress, pre-process, and making tasks (with gulp) for all the project needs.

Bower is here to manage external dependencies (like bootstrap / isotope)

## Techniques used

This integration was developed using `BEM` method (http://getbem.com/introduction/) and `SMACSS` categorization (https://smacss.com/book/categorizing)

## Install developpers environment

You should install a complete developer environment in order to add changes or modify integration. To do this, you have to follow the installation documentation on <a href="../readme.md">readme.md</a> in root folder then you could modify `scss` / `js` files and get the compiled `.css` / `.js` output.