(function ($) {
    $(document).ready(function () {
        $('.header-simple__trigger').on('click', function (event) {
            event.preventDefault();
            toggleNav();
        });

        function toggleNav() {
            var navigation = $('.header-simple__navigation');
            var navIsVisible = !navigation.hasClass('header-simple__navigation--active');
            navigation.toggleClass('header-simple__navigation--active', navIsVisible);
            $('.header-simple__trigger').toggleClass('header-simple__trigger--active', navIsVisible);

            var navBurgerIsVisible = ( !$('.header-burger__dropdown').hasClass('header-burger__dropdown--active') ) ? true : false;
            $('.header-burger__dropdown').toggleClass('header-burger__dropdown--active', navBurgerIsVisible);
            $('.header-burger__trigger').toggleClass('header-burger__trigger--active', navBurgerIsVisible);
            $('body').toggleClass('no-overflow', navBurgerIsVisible);

            if (!navBurgerIsVisible) {
                $('.header-burger__dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $('.burger-has-children > a').next().addClass('burger-dropdown--hidden');
                    $('.burger-dropdown--move-out').removeClass('burger-dropdown--move-out');
                });
            }
        }
    });
})(jQuery);