(function ($) {
    $(document).ready(function () {
        //open/close mega-navigation
        $('.header-burger__trigger').on('click', function (event) {
            event.preventDefault();
            toggleNav();
        });

        //close meganavigation
        $('.header-burger__dropdown .header-burger__close').on('click', function (event) {
            event.preventDefault();
            toggleNav();
        });

        //on mobile - open submenu
        $('.burger-has-children').children('a').on('click', function (event) {
            //prevent default clicking on direct children of .has-children
            event.preventDefault();
            var selected = $(this);
            selected.next('ul').removeClass('burger-dropdown--hidden').end().parent('.burger-has-children').parent('ul').addClass('burger-dropdown--move-out');
        });

        //submenu items - go back link
        $('.burger-back').on('click', function () {
            var selected = $(this),
                visibleNav = $(this).parent('ul').parent('.burger-has-children').parent('ul');
            selected.parent('ul').addClass('burger-dropdown--hidden').parent('.burger-has-children').parent('ul').removeClass('burger-dropdown--move-out');
        });

        $('.burger-back--home').on('click', function() {
            $('.header-burger__content ul').addClass('burger-dropdown--hidden').removeClass('burger-dropdown--move-out');
            $('.header-burger__content').removeClass('burger-dropdown--move-out');
        })
        function toggleNav() {
            var navIsVisible = ( !$('.header-burger__dropdown').hasClass('header-burger__dropdown--active') ) ? true : false;
            $('.header-burger__dropdown').toggleClass('header-burger__dropdown--active', navIsVisible);
            $('.header-burger__trigger').toggleClass('header-burger__trigger--active', navIsVisible);
            $('body').toggleClass('no-overflow', navIsVisible);

            if (!navIsVisible) {
                $('.header-burger__dropdown').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                    $('.burger-has-children ul').addClass('burger-dropdown--hidden');
                    $('.burger-dropdown--move-out').removeClass('burger-dropdown--move-out');
                });
            }
        }
    });
})(jQuery);