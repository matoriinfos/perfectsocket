<div class="topheader topheader--transparent hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flex-v flex-v--left">
                    <a href="#">
                        <i class="lgpicto lgicon-btn-wishlist" aria-hidden="true"></i>
                        Wishlist
                    </a>
                    <a href="#" class="btn-second btn-second--medium">
                        <i class="lgpicto lgicon-btn-pin-wtb" aria-hidden="true"></i>
                        Where to buy
                    </a>
                    <a href="#" class="btn-second btn-second--medium">
                        <i class="lgpicto lgicon-btn-projector" aria-hidden="true"></i>
                        Showroom
                    </a>
                    <div class="flex-item flex-v flex-v--right">
                        <ul class="topheader__list">
                            <li class="topheader__search-container hidden-sm hidden-xs">
                                <a href="#" class="topheader__search-btn">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                                <ul>
                                    <li>
                                        <div class="hover-header hover-header--small">
                                            <div class="hover-header__bg">
                                                <div class="header-search">
                                                    <input type="text" placeholder="What are you looking for ?" class="header-search__field">
                                                    <button type="submit" class="header-search__submit">Ok</button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="btn-cta bg--color-12">
                                    <i class="lgpicto lgicon-btn-world" aria-hidden="true"></i>
                                    Legrand.com
                                </a>
                            </li>
                            <li>
                                <a href="#" class="btn-cta bg--color-13">
                                    <i class="lgpicto lgicon-btn-contact" aria-hidden="true"></i>
                                    Contact
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <ul class="topheader__dropdown">
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Support & Service</a></li>
                                    <li><a href="#">Help</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="btn-cta btn-cta--big bg--color-14">
                                    <i class="lgpicto lgicon-btn-ecatalogue" aria-hidden="true"></i>
                                    E-catalogue
                                </a>
                            </li>
                        </ul>

                        <ul class="nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">FR <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">FR</a></li>
                                    <li><a href="#">EN</a></li>
                                    <li><a href="#">PU</a></li>
                                    <li><a href="#">RU</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="header-burger">
    <div class="header-burger__wrapper">
        <div class="header-burger__head">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flex-v flex-v--spaced header-burger__wrapper">
                            <div class="header-burger__head-left flex-v flex-v--left header-burger__top-wrapper">
                                <a class="header-burger__logo" href="#" title="Legrand">
                                    <img src="../../images/logo-white.png" srcset="../../images/logo-white@2x.png 2x" alt="Legrand" />
                                </a>

                                <a href="#" class="header-burger__trigger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                            </div>

                            <a href="#" class="header-burger__customize-btn">
                                <i class="lgpicto lgicon-btn-cursor"></i>
                                Customize your experience
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="customize-menu customize-menu--burger">
            <div class="customize-menu__label">Tell us who you are</div>
            <ul class="customize-menu__nav">
                <li>
                    <a href="#">Contractor or Integrator</a>
                </li>
                <li>
                    <a href="#">Distributor</a>
                </li>
                <li>
                    <a href="#">Facility or Technology Manager</a>
                </li>
                <li>
                    <a href="#">Architect</a>
                </li>
                <li>
                    <a href="#">Home Builder</a>
                </li>
                <li>
                    <a href="#">Homeowner</a>
                </li>
                <li>
                    <a href="#">Specifier, Designer or Consultant</a>
                </li>
            </ul>
        </div>

        <nav class="header-burger__dropdown" data-eq="burger-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <ul class="header-burger__content" data-eq="burger-wrapper">
                            <li class="burger-has-children">

                                <a href="#">Solutions</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Solutions</a></li>
                                    <li><a href="#">Solutions lvl 1 - 1</a></li>

                                    <li class="burger-has-children">
                                        <a href="#">Solutions lvl 1 - 2</a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Solutions</a></li>
                                            <li class="burger-see-all"><a href="#">Solutions lvl 1 - 2</a></li>

                                            <li><a href="#">Solutions lvl 2 - 1</a></li>
                                            <li><a href="#">Solutions lvl 2 - 2</a></li>
                                            <li><a href="#">Solutions lvl 2 - 3</a></li>
                                            <li><a href="#">Solutions lvl 2 - 4</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Solutions lvl 1 - 3</a></li>
                                    <li><a href="#">Solutions lvl 1 - 4</a></li>
                                    <li><a href="#">Solutions lvl 1 - 5</a></li>
                                    <li><a href="#">Solutions lvl 1 - 6</a></li>
                                </ul>
                            </li>
                            <li class="burger-has-children">

                                <a href="#">Products</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Products</a></li>
                                    <li>
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-outlet"></i>
                                            Collection
                                        </a>
                                    </li>
                                    <li class="burger-has-children">
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-outlet"></i>
                                            Switch and Sockets
                                        </a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Products</a></li>
                                            <li class="burger-see-all"><a href="#">Sockets and switch</a></li>

                                            <li><a href="#">Sockets</a></li>
                                            <li><a href="#">Switch</a></li>
                                            <li><a href="#">Wall plates</a></li>
                                            <li><a href="#">Case flush</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-electronic"></i>
                                            Electric switchboard
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-multimedia"></i>
                                            Multimedia
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-multisocket"></i>
                                            Multi socket
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="header-burger__picto lgburger-cordcable"></i>
                                            Cord & cable management
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="burger-has-children">

                                <a href="#">Spaces</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Spaces</a></li>
                                    <li><a href="#">Spaces lvl 1 - 1</a></li>

                                    <li class="burger-has-children">
                                        <a href="#">Spaces lvl 1 - 2</a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Spaces</a></li>
                                            <li class="burger-see-all"><a href="#">Spaces lvl 1 - 2</a></li>

                                            <li><a href="#">Spaces lvl 2 - 1</a></li>
                                            <li><a href="#">Spaces lvl 2 - 2</a></li>
                                            <li><a href="#">Spaces lvl 2 - 3</a></li>
                                            <li><a href="#">Spaces lvl 2 - 4</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Spaces lvl 1 - 3</a></li>
                                    <li><a href="#">Spaces lvl 1 - 4</a></li>
                                    <li><a href="#">Spaces lvl 1 - 5</a></li>
                                    <li><a href="#">Spaces lvl 1 - 6</a></li>
                                </ul>
                            </li>
                            <li class="burger-has-children">

                                <a href="#">Brands</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Brands</a></li>
                                    <li><a href="#">Brands lvl 1 - 1</a></li>

                                    <li class="burger-has-children">
                                        <a href="#">Brands lvl 1 - 2</a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Brands</a></li>
                                            <li class="burger-see-all"><a href="#">Brands lvl 1 - 2</a></li>

                                            <li><a href="#">Brands lvl 2 - 1</a></li>
                                            <li><a href="#">Brands lvl 2 - 2</a></li>
                                            <li><a href="#">Brands lvl 2 - 3</a></li>
                                            <li><a href="#">Brands lvl 2 - 4</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Brands lvl 1 - 3</a></li>
                                    <li><a href="#">Brands lvl 1 - 4</a></li>
                                    <li><a href="#">Brands lvl 1 - 5</a></li>
                                    <li><a href="#">Brands lvl 1 - 6</a></li>
                                </ul>
                            </li>
                            <li class="burger-has-children">

                                <a href="#">Reference projects</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Reference projects</a></li>
                                    <li><a href="#">Reference projects lvl 1 - 1</a></li>

                                    <li class="burger-has-children">
                                        <a href="#">Reference projects lvl 1 - 2</a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Reference projects</a></li>
                                            <li class="burger-see-all"><a href="#">Reference projects lvl 1 - 2</a></li>

                                            <li><a href="#">Reference projects lvl 2 - 1</a></li>
                                            <li><a href="#">Reference projects lvl 2 - 2</a></li>
                                            <li><a href="#">Reference projects lvl 2 - 3</a></li>
                                            <li><a href="#">Reference projects lvl 2 - 4</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Reference projects lvl 1 - 3</a></li>
                                    <li><a href="#">Reference projects lvl 1 - 4</a></li>
                                    <li><a href="#">Reference projects lvl 1 - 5</a></li>
                                    <li><a href="#">Reference projects lvl 1 - 6</a></li>
                                </ul>
                            </li>
                            <li class="burger-has-children">

                                <a href="#">Resources and tools</a>

                                <ul class="burger-dropdown burger-dropdown--hidden">
                                    <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                    <li class="burger-see-all"><a href="#">Resources and tools</a></li>
                                    <li><a href="#">Resources and tools lvl 1 - 1</a></li>

                                    <li class="burger-has-children">
                                        <a href="#">Resources and tools lvl 1 - 2</a>

                                        <ul class="burger-dropdown--hidden">
                                            <li class="burger-back burger-back--home"><a href="#">HOME</a></li>
                                            <li class="burger-back"><a href="#">Resources and tools</a></li>
                                            <li class="burger-see-all"><a href="#">Resources and tools lvl 1 - 2</a></li>

                                            <li><a href="#">Resources and tools lvl 2 - 1</a></li>
                                            <li><a href="#">Resources and tools lvl 2 - 2</a></li>
                                            <li><a href="#">Resources and tools lvl 2 - 3</a></li>
                                            <li><a href="#">Resources and tools lvl 2 - 4</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Resources and tools lvl 1 - 3</a></li>
                                    <li><a href="#">Resources and tools lvl 1 - 4</a></li>
                                    <li><a href="#">Resources and tools lvl 1 - 5</a></li>
                                    <li><a href="#">Resources and tools lvl 1 - 6</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">About us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>