<?php
require_once('db_config.php');
?>
<header class="header-simple">
    <div class="header-simple__sticky">
        <nav class="topmenu text-right">
            <div class="container">
                <div class="row valignRow">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                    <?php
                    session_start();
                    if(empty($_SESSION['prava'])) $_SESSION['prava'] = 'guest';

                    // KOD ZA BAZU NA NETU
                    $servername = servername;
                    $username = username;
                    $password = password;
                    $baza = baza;

                    // POVEZIVANJE NA BAZU
                    $conn = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
                    //ako korisnik nije logovan generisi mu id
                    if(empty($_SESSION['id_korisnika']))
                    {
                        //proveri da li je uniq u bazi
                        $id_postoji = 1;
                        while($id_postoji) {
                            $uniq_id = hexdec(uniqid()) % 10000000;
                            if($uniq_id < 0) $uniq_id = $uniq_id * -1;
                            //udji u bazu i proveri id korisnika
                            try {
                                $sql = $conn->prepare("select distinct customer_id from cart where customer_id = '$uniq_id'");
                                $sql->execute();
                                $row=$sql->fetch(PDO::FETCH_ASSOC);
                                if(empty($row))
                                    $id_postoji = 0;
                            }
                            catch(PDOException $e)
                            {
                                echo "Error: " . $e->getMessage();
                            }
                        }
                        //$uniq_id = hexdec(uniqid()) % 10000000;
                        $_SESSION['id_korisnika'] = $uniq_id;
                    }

                    //izracunaj broj proizvoda u korpi
                    $id_korisnika = $_SESSION['id_korisnika'];


                    try {
                        $stmt1 = $conn->prepare("select count(id) as ukupno from cart where customer_id=$id_korisnika");
                        $stmt1->execute();
                        $row=$stmt1->fetch(PDO::FETCH_ASSOC);
                        $stavke= $row['ukupno'];
                    }
                    catch(PDOException $e)
                    {
                        echo "Error: " . $e->getMessage();
                    }

                    if($_SESSION['prava']=='user' || $_SESSION['prava']=='reseller')
                    {
                      //  echo 'Hi '. $_SESSION['ime'] . '';
                    /*    echo '<a href="Cart.php" style="padding-left: 20px">
                        <i class="fas fa-shopping-cart"></i><span style="color: red;">' . $stavke . '</span>

                        </a>';*/

                      //  echo '<a href="./Home.php?action=logout" style="padding-left: 20px">Log out</a>';
                        //padajuci meni
                        ?>
                     <!--   <nav class="navbar">
                            <div class="container-fluid">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Hi '. $_SESSION['ime']; ?>
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><span class="icon"><i class="fa fa-fw  fa-user-circle-o"> </i> </span>My profile</a></li>

                                            <li><a href="./Home.php?action=logout"><span class="icon">
                                             <i class="fa fa-fw fa-sign-out m-r-10"></i>
                                             </span>Logout</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>
                        </nav> -->

                        <div class="reg_user ">
                        <ul style="display: inline; list-style-type: none;">
                        <li style="float: left; margin-left: 1px"> <a href="Cart.php" style="padding-left: 20px; padding-top: 0px">
                        <i class="fas fa-shopping-cart"></i><span style="color: red;">(<?php echo $stavke ?>)</span>
                            </a> </li>
                            <li style="float: left; margin-left: 10px">
                                <div class="dropdown">
                                    <button class="dropbtn"><?php echo  $_SESSION['ime']. " &nbsp;" ?>
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <div class="dropdown-content">
                                         <a href="./MyProfile.php"><span class="icon"><i class="fa fa-fw  fa-user-circle-o"> </i> </span>My profile</a>
                                        <hr style="padding: 0px 0px; margin: 0">
                                        <a href="./Home.php?action=logout" style="padding-left: 20px"><span class="icon">
                                             <i class="fa fa-fw fa-sign-out m-r-10"></i>
                                             </span>Log out</a>
                                    </div>
                                </div> </li>
                        </ul>
                        </div>
                        <?php
                    }
                    else
                    {
                        echo '<div class="reg_user ">';
                     /*   echo '<a href="Cart.php" style="padding-left: 20px;">
                        <i class="fas fa-shopping-cart"></i><span style="color: red;">' . $stavke . '</span>

                        </a>';*/
                        echo '</div>';
                    }

                    ?>

                    </div>
                </div>
               <div class="row valignRow">
                    <div class="col-md-2 col-sm-12 col-xs-8">
                        <div class="header-burger__head-left flex-v flex-v--left">
                            <a href="<?php echo server;?>" title="PerfectSocket.com">
                                <img src="wp-content/themes/momentous-lite/lteme/images/logo.png" srcset="wp-content/themes/momentous-lite/lteme/images/logo@2x.png 2x" class="header-simple__logo img-responsive">
                            </a>

                            <a href="#" class="header-simple__trigger visible-xs visible-sm">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>

                        </div>

                    </div>
                    <div class="col-md-10 col-sm-12 col-xs-12 header-simple__navigation">
                        <nav class="header-burger__dropdown header-burger">

                            <ul class="header-burger__content">
                                <li>
                                    <a href="<?php echo server;?>" id="homeEng">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo server;?>?page_id=801">ALING CONEL</a>
                                </li>
                                <li>
                                    <a id="nopallux">NOPAL LUX</a>
                                </li>
                                <li>
                                    <a id="bticino">BTICINO</a>
                                </li>
                                <li>
                                    <a id="legrand">LEGRAND</a>
                                </li>
                                <li>
                                    <a href="<?php echo server;?>?page_id=644">SCHRACK</a>
                                </li>
                                <!-- <li>
                                    <a href="<?php echo server;?>?page_id=271">Blog</a>
                                </li>
                                <li>
                                    <a href="<?php echo server;?>?page_id=683">Contact</a>
                                </li> -->
                                <!-- <li>
                                    <a id="termsOfUse">Terms Of Use</a>
                                </li> -->
                                <li>
                                    <?php
                        echo '<a href="Cart.php" style="padding-left: 20px">
                        <i class="fas fa-shopping-cart"></i><span style="color: red;">' . $stavke . '</span>

                        </a>';
                        echo '</div>';

                    ?>
                                </li>


                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
        </nav>
    </div>
    <div class = "glNavPodmeni" id="podmeniAling">
        <a href="<?php echo server;?>?page_id=648"><p><b>Experience - modular standard</b></p></a>
    </div>
    <div class = "glNavPodmeni" id="podmeniNopalLux">
        <a href="<?php echo server;?>?page_id=648"><p><b>INTERIO - modular standard</b></p></a>
        <a href="<?php echo server;?>?page_id=630"><p>PRIMERA - european standard</p></a>
        <a href="<?php echo server;?>?page_id=598"><p>PRIMERA - european standard - completed solutions</p></a>
    </div>
    <div class = "glNavPodmeni" id="podmeniLegrand">
        <a href="<?php echo server;?>?page_id=106"><p>NILOE - european standard</p></a>
    </div>
    <div class = "glNavPodmeni" id="podmeniBticino">
        <a href="<?php echo server;?>?page_id=2"><p>LIVING LIGHT - modular standard</p></a>
        <a href="<?php echo server;?>?page_id=74"><p>LIVING LIGHT AIR - modular standard</p></a>
    </div>
    <div class = "glNavPodmeni" id="podmeniTermsOfUse">
        <a href="<?php echo server;?>?page_id=3"><p>General Terms Of Use</p></a>
        <a href="<?php echo server;?>?page_id=338"><p>GDPR</p></a>
        <a href="<?php echo server;?>?page_id=344"><p>Policy Cookies</p></a>
    </div>



</header>

<!--login forma -->
<div id="id01" class="modal">

    <form class="modal-content animate" method="post" action="Home.php" >
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

        </div>

        <div class="container_login">
            <?php

            //napraviti stranicu
           // $errors_log = $_SESSION['greske_log'];
            $errors_log = $_SESSION['greske_log'];
            $_SESSION['greske_log']= '';
            if(isset($errors_log) && !empty($errors_log))
            {

                echo "<h1 style='text-align: center; color: #15998c'>Try again!</h1><br />";
                ?>
                <script>
                    var modal = document.getElementById('id01');
                    modal.style.display= "block";
                </script>
                <?php
            }

            ?>
            <label><b>E mail</b></label>
            <input type="text" placeholder="Email address" name="mail" required>

            <label><b>Password</b></label>
            <input type="password" placeholder="Password" name="loz" required>
            <div style="text-align: center">
            <button type="submit">Confirm</button>
            </div>
        </div>


    </form>
</div>

<script type="text/javascript" src="wp-content/themes/momentous-lite/livinglight_3/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/upravljanje_podNavigacija.js"></script>
<script>
    // Get the modal
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

</script>
