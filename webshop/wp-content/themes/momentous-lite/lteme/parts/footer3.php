
<aside class="social__sticky">
    <ul>
        <li>
            <a href="#" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-commenting-o"></i>
                    <span class="social__text">Contact us</span>
                </div>
            </a>
        </li>
        <li>
            <a href="https://www.facebook.com/PerfectSocketcom-384832618949652/" target="_blank" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-facebook"></i>
                    <span class="social__text">Facebook</span>
                </div>
            </a>
        </li>
        <li>
            <a href="https://twitter.com/PerfectSocket" target="_blank" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-twitter"></i>
                    <span class="social__text">Twitter</span>
                </div>
            </a>
        </li>
		     <li>
            <a href="https://www.pinterest.com/ultra4all/" target="_blank" class="social social--sticky-link">
                <div class="social__content">
                    <i class="fa fa-pinterest"></i>
                    <span class="social__text">Pinterest</span>
                </div>
            </a>
        </li>
    </ul>
</aside>

<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="'wp-content/themes/momentous-lite/lteme/js/burger.js"></script>