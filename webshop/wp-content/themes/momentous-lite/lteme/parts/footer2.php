<footer class="footer">
    <div class="f-social"> 
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12 f-social__container flex-v no-flex-sm no-flex-xs">
                    <div class="f-social__social-medias flex-v">
                        <span class="f-social__label">Follow us</span>
                        <a href="https://www.facebook.com/PerfectSocketcom-384832618949652/" target="_blank" class="social social--inline social--big"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/perfectsocket/" target="_blank" class="social social--inline social--big"><i class="fa fa-instagram"></i></a>
						<a href="https://twitter.com/PerfectSocket" target="_blank" class="social social--inline social--big"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.pinterest.com/perfectsocket/" target="_blank" class="social social--inline social--big"><i class="fa fa-pinterest-p"></i></a>
						<a href="https://www.youtube.com/channel/UCbPGZISDH6_zwlNimrLg7FA" target="_blank" class="social social--inline social--big"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
	</div>
</footer>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
