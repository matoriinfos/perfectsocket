<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Stylesheets -->
<link rel="icon" href="../../favicon.ico" type="image/x-icon" />
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,500,700" rel="stylesheet">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/css/font-awesome.min.css" type="text/css">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/css/lg-icons.css" type="text/css">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/css/lg-burger.css" type="text/css">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/css/fruity.min.css?v=dev2017092702" type="text/css">
<link rel="stylesheet" href="wp-content/themes/momentous-lite/lteme/upravljanje_podNavigacija.css" type="text/css">
<link rel="stylesheet" href="css/login.css" type="text/css">
<script src="https://kit.fontawesome.com/cca9cc10d8.js" crossorigin="anonymous"></script>
