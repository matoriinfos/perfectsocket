<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Where to buy</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--burger">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="banner__title">Where to buy</h1>

                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Where to buy</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="section section--grey section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-centerarrow"></i>
                <h2 class="title mb--40">Title level 2</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Vestibulum vitae odio ac enim rutrum mattis. Donec placerat
                    commodo ultrices. Praesent ullamcorper malesuada tristique.
                    Mauris imperdiet efficitur risus nec iaculis.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section section--no-padding">
    <div class="container-fluid">
        <div class="row grid-no-padding">
            <div class="col-md-4 hidden-xs hidden-sm" data-eq="map-column">
                <div class="section">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="map__section border-bottom border-black mb--60">
                                <h2 class="title mb--20 flex-v flex-v--left">
                                    <img class="mr--15" src="../../images/btn-pin-iceberg.png" srcset="../../images/btn-pin-iceberg@2x.png 2x" alt="" />
                                    <span>Our distributors</span>
                                </h2>

                                <h3 class="title title--medium title--light mb--15">Search</h3>

                                <div class="input-wrapper mb--20">
                                    <input type="text" placeholder="Zip code" />
                                    <button type="submit">OK</button>
                                </div>

                                <div class="row mb--50">
                                    <div class="col-md-12 mb--10">Or filter by</div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area1">
                                        <label for="area1">Area 1</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area2">
                                        <label for="area2">Area 2</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area3">
                                        <label for="area3">Area 3</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area4">
                                        <label for="area4">Area 4</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area5">
                                        <label for="area5">Area 5</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area6">
                                        <label for="area6">Area 6</label>
                                    </div>
                                </div>

                                <h3 class="title title--medium title--light mb--15">Around me</h3>

                                <a href="#" class="btn-second btn-second--primary-color btn-second--input mb--50">
                                    <i class="lgicon-btn-target mr--10 pull-left"></i> <span>Locate me</span>
                                </a>
                            </div>
                            <div class="map__section">
                                <h2 class="title mb--20 flex-v flex-v--left">
                                    <img class="mr--15" src="../../images/btn-pin-fruity.png" srcset="../../images/btn-pin-fruity@2x.png 2x" alt="" />
                                    <span>Our locations</span>
                                </h2>

                                <h3 class="title title--medium title--light mb--15">Search</h3>

                                <div class="input-wrapper mb--20">
                                    <input type="text" placeholder="Zip code" />
                                    <button type="submit">OK</button>
                                </div>

                                <div class="row mb--50">
                                    <div class="col-md-12 mb--10">Or filter by</div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area1-b">
                                        <label for="area1-b">Area 1</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area2-b">
                                        <label for="area2-b">Area 2</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area3-b">
                                        <label for="area3-b">Area 3</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area4-b">
                                        <label for="area4-b">Area 4</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area5-b">
                                        <label for="area5-b">Area 5</label>
                                    </div>
                                    <div class="col-xs-6 col-xs-6">
                                        <input type="checkbox" id="area6-b">
                                        <label for="area6-b">Area 6</label>
                                    </div>
                                </div>

                                <h3 class="title title--medium title--light mb--15">Around me</h3>

                                <a href="#" class="btn-second btn-second--primary-color btn-second--input mb--50">
                                    <i class="lgicon-btn-target mr--10 pull-left"></i> <span>Locate me</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div id="place-map" class="map" data-eq="map-column"></div>
            </div>
        </div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsNXLDa5MRSNp6HScDK1dFjt72a_wvt6k"></script>
    <script>
        var markers = [];

        function initMap() {
            google.maps.visualRefresh = true;
            var centerCoords = {lat: 48.860611, lng: 2.337644};
            var map = new google.maps.Map(document.getElementById('place-map'), {
                zoom: 15,
                center: centerCoords
            });


            // Markers
            var redMarker = defaultIcon = {
                url: '../../images/btn-pin-fruity.png',
                size: new google.maps.Size(32, 44),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };

            var blueMarker = defaultIcon = {
                url: '../../images/btn-pin-iceberg.png',
                size: new google.maps.Size(32, 44),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            };

            // Marker
            var marker_demo_1 = new google.maps.Marker({
                position: {lat: 48.854169, lng: 2.332599},
                map: map,
                icon: redMarker
            });
            markers.push(marker_demo_1);

            var marker_demo_2 = new google.maps.Marker({
                position: {lat: 48.860611, lng: 2.337644},
                map: map,
                icon: blueMarker
            });
            markers.push(marker_demo_2);
        }

        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
</section>

<div class="section section--lightgrey">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-11">
                        <h2 class="title mb--20">Our distributors</h2>

                        <h3 class="title title--medium title--light mb--15">Search</h3>

                        <div class="input-wrapper mb--20">
                            <input type="text" placeholder="Zip code" />
                            <button type="submit">OK</button>
                        </div>

                        <div class="row mb--50">
                            <div class="col-md-12 mb--10">Or filter by</div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area1-c">
                                <label for="area1-c">Area 1</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area2-c">
                                <label for="area2-c">Area 2</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area3-c">
                                <label for="area3-c">Area 3</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area4-c">
                                <label for="area4-c">Area 4</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area5-c">
                                <label for="area5-c">Area 5</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area6-c">
                                <label for="area6-c">Area 6</label>
                            </div>
                        </div>

                        <h3 class="title title--medium title--light mb--15">Around me</h3>

                        <a href="#" class="btn-second btn-second--primary-color btn-second--input mb--50">
                            <i class="lgicon-btn-target mr--10 pull-left"></i> <span>Locate me</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Distributor name</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Distributor name</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Distributor name</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Distributor name</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-11">
                        <h2 class="title mb--20">Our locations</h2>

                        <h3 class="title title--medium title--light mb--15">Search</h3>

                        <div class="input-wrapper mb--20">
                            <input type="text" placeholder="Zip code" />
                            <button type="submit">OK</button>
                        </div>

                        <div class="row mb--50">
                            <div class="col-md-12 mb--10">Or filter by</div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area1-d">
                                <label for="area1-d">Area 1</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area2-d">
                                <label for="area2-d">Area 2</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area3-d">
                                <label for="area3-d">Area 3</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area4-d">
                                <label for="area4-d">Area 4</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area5-d">
                                <label for="area5-d">Area 5</label>
                            </div>
                            <div class="col-xs-6 col-xs-6">
                                <input type="checkbox" id="area6-d">
                                <label for="area6-d">Area 6</label>
                            </div>
                        </div>

                        <h3 class="title title--medium title--light mb--15">Around me</h3>

                        <a href="#" class="btn-second btn-second--primary-color btn-second--input mb--50">
                            <i class="lgicon-btn-target mr--10 pull-left"></i> <span>Locate me</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Legrand</h3>
                            <div class="text text--small mb--10">
                                France headquarters<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Brand 1</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Brand 2</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                        <div class="border-bottom mb--30">
                            <h3 class="title title--medium title--light mb--15">Brand 3</h3>
                            <div class="text text--small mb--10">
                                Address 1<br>
                                Lorem ipsum dolor si amet<br>
                                00 000 - CITY
                            </div>
                            <a href="#" class="card__text card__text--link card__text--xsmall text-color-1 flex-v flex-v--left mb--20">
                                <span class="mr--15">Google map</span> <i class="lgpicto lgicon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>