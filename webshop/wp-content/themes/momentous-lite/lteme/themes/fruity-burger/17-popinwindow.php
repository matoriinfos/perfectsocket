<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Popup</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

    <div class="banner banner--burger">
        <div class="container">
            <h1 class="banner__title">Contact us</h1>

            <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Contact us</li>
            </ul>
        </div>
    </div>

    <section class="section">

        <div class="container">
            <button class="btn" data-toggle="modal" data-target="#errorModal">
                Error modal
            </button>
            <button class="btn" data-toggle="modal" data-target="#successModal">
                Success modal
            </button>
            <button class="btn" data-toggle="modal" data-target="#imageModal">
                Image modal
            </button>
        </div>

        <!-- Error Modal -->
        <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center">
                        <div class="modal-dialog__title mb--20">We apologize but...</div>

                        <div class="modal-dialog__icon modal-dialog__icon--error">
                            <i class="lgicon-btn-sad" aria-hidden="true"></i>
                        </div>

                        <p>
                            Your message has not been sent successfully.
                            <br>
                            <span class="text-color-modal">You must complete the required field.</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Success Modal -->
        <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center">
                        <div class="modal-dialog__title mb--20">Success</div>

                        <div class="modal-dialog__icon modal-dialog__icon--success">
                            <i class="lgicon-btn-happy" aria-hidden="true"></i>
                        </div>

                        <p>
                            Your message has been sent successfully.
                            <br>
                            Thanks for your trust.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Image Modal -->
        <div class="modal modal-image fade" id="imageModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-close" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img src="/images/bg-ecatalogue.jpg" class="img-responsive mb--30">

                        <div class="modal-image__content">
                            <div class="title title--xsmall text-color-1">1850 - 1900</div>
                            <div class="title mb--20">Title level 3</div>
                            <p class="para--border-large">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod dolores, similique adipisci, expedita maiores aperiam, at sint reprehenderit, deserunt necessitatibus rerum sed! Tenetur magnam inventore cupiditate, animi corporis laudantium obcaecati.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>