<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Search results</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--burger">
    <div class="container">
        <h1 class="banner__title">Search results</h1>

        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Search results</li>
        </ul>
    </div>
</div>

<section class="section section--grey">
    <div class="container">
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <form action="#">
                    <input type="text" class="lg-input lg-input--no-round lg-input--primary" placeholder="Building">
                    <button type="submit" class="btn">
                        <span class="mr--50">Ok</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="border-bottom mb--40">
                    <h2 class="title text-left mb--15">
                        <span class="text-color-1">10</span> <span class="title--normal title--transform-none">Results in web pages category</span>
                    </h2>
                </div>

                <article class="text-left mb--50 border-bottom">
                    <div class="mb--50">
                        <h2 class="title title--medium">Title level 2 du groupe legrand : neo center</h2>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="text-left mb--50 border-bottom">
                    <div class="mb--50">
                        <h2 class="title title--medium">TITLE LEVEL 2 DATA CENTER - SALLE SERVEUR</h2>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="text-left mb--50">
                    <div class="mb--50">
                        <h2 class="title title--medium">TITLE LEVEL 2 CHANTIER REFERENT DU GROUPE LEGRAND : NEO CENTER OUEST DATA CENTER</h2>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Read more</a>
                            </div>
                        </div>
                    </div>
                </article>

                <div class="text-right mb--40 no-flex-xxs">
                    <a href="#" class="btn btn--no-bg-color btn--xsmall btn--no-icon no-shadow ">Consult all results <br class="visible-xs" />in web pages category</a>
                </div>

                <div class="pagered text-center">
                    <p class="pagered__label">Page 1 of 5</p>
                    <ul class="pagered__items js-pagered__items pagered ">
                        <li class="pagered__item pagered__item--first pagered-item">
                            <a href="#" title="First page">
                                <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered__item--previous pagered-prev">
                            <a href="#" title="Previous page" rel="prev">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered-item active">
                            <a href="#">1</a>
                        </li>
                        <li class="pagered__item pagered-item">
                            <a href="#">2</a>
                        </li>
                        <li class="pagered__item pagered-item">
                            <a href="#">3</a>
                        </li>
                        <li class="pagered__item pagered-item hidden-xs">
                            <a href="#">4</a>
                        </li>
                        <li class="pagered__item pagered-item hidden-xs">
                            <a href="#">5</a>
                        </li>
                        <li class="pagered__item pagered__item--next pagered-next ">
                            <a href="#" title="Next page" rel="next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered__item--last pagered-item">
                            <a href="#" title="Last page">
                                <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="border-bottom mb--40">
                    <h2 class="title text-left mb--15">
                        <span class="text-color-1">10</span> <span class="title--normal title--transform-none">Results in documents category</span>
                    </h2>
                </div>

                <article class="text-left mb--50 border-bottom">
                    <div class="mb--50">
                        <h2 class="title title--medium">Title level 2 communique raritan</h2>
                        <div class="text--faded mb--20">12/05/2017</div>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Access the document</a>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="text-left mb--50 border-bottom">
                    <div class="mb--50">
                        <h2 class="title title--medium">Title level 2 document reference 2016</h2>
                        <div class="text--faded mb--20">12/05/2017</div>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Access the document</a>
                            </div>
                        </div>
                    </div>
                </article>

                <article class="text-left mb--50">
                    <div class="mb--50">
                        <h2 class="title title--medium">TITLE LEVEL 2 DU GROUPE LEGRAND : NEO CENTER</h2>
                        <div class="text--faded mb--20">12/05/2017</div>
                        <div class="para--border-small">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                                Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip
                                ex ea commodo consequat. Duis aute irure
                                dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat atur
                                <strong><span class="text-color-1">building</span></strong> ounts when it comes to building
                                customer loyalty. High quality service and
                                comfort are there to ensure a maximum fill
                                rate.
                            </p>

                            <div class="flex-v flex-v--left no-flex-xxs">
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-download" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social--inline social--neg">
                                    <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="social social-modal-trigger social--inline social--neg">
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                </a>
                                <a href="#" class="btn">Access the document</a>
                            </div>
                        </div>
                    </div>
                </article>

                <div class="text-right mb--40 no-flex-xxs">
                    <a href="#" class="btn btn--no-bg-color btn--xsmall btn--no-icon no-shadow ">Consult all results <br class="visible-xs" />in documents category</a>
                </div>

                <div class="pagered text-center">
                    <p class="pagered__label">Page 1 of 5</p>
                    <ul class="pagered__items js-pagered__items pagered ">
                        <li class="pagered__item pagered__item--first pagered-item">
                            <a href="#" title="First page">
                                <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered__item--previous pagered-prev">
                            <a href="#" title="Previous page" rel="prev">
                                <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered-item active">
                            <a href="#">1</a>
                        </li>
                        <li class="pagered__item pagered-item">
                            <a href="#">2</a>
                        </li>
                        <li class="pagered__item pagered-item">
                            <a href="#">3</a>
                        </li>
                        <li class="pagered__item pagered-item hidden-xs">
                            <a href="#">4</a>
                        </li>
                        <li class="pagered__item pagered-item hidden-xs">
                            <a href="#">5</a>
                        </li>
                        <li class="pagered__item pagered__item--next pagered-next ">
                            <a href="#" title="Next page" rel="next">
                                <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                            </a>
                        </li>
                        <li class="pagered__item pagered__item--last pagered-item">
                            <a href="#" title="Last page">
                                <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>