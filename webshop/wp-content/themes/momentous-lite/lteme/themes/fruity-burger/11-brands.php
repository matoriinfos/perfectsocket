<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Our brands</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header--burger.php'); ?>

<div class="banner banner--burger">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="banner__title">Our brands</h1>

                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Our brands</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="section section--grey section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <i class="lgpicto--title lgicon-btn-label"></i>
                <h2 class="title mb--40">Legrand group's brands</h2>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing
                    elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis
                    nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in
                    reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="card card--bg no-shadow" data-eq="brand-intro">
                        <img class="img-responsive fullCenter" src="../../images/logo.png">
                    </div>
                </div>
                <div class="col-md-6 mb--25" data-eq="brand-intro">
                    <h2 class="title mb--25">Title level 2</h2>
                    <p class="mb--0">
                        Lorem ipsum dolor sit amet, consectetur adipisicing
                        elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis
                        nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore
                        eu fugiat nulla pariatur. Excepteur sint occaecat
                        cupidatat non proident, sunt in culpa qui officia
                        deserun. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur.
                    </p>
                </div>
            </div>

            <h3 class="title title--light title--transform-none mb--25">Title level 3</h3>
            <p>
                Pellentesque egestas aliquet nisi, vitae porttitor dui
                semper in. Aliquam vitae est efficitur, finibus lacus id,
                suscipit metus. Cras convallis nisi quis quam maximus, sit
                amet molestie neque vestibulum. Quisque tempor porta luctus.
                Nam lorem augue, lacinia eget semper ut, congue eget lorem.
                Ut eu posuere magna, at semper mauris. Curabitur faucibus ex
                cursus ex imperdiet efficitur. Orci varius natoque penatibus
                et magnis dis parturient montes, nascetur ridiculus mus.
                Donec ornare placerat nunc, non semper felis laoreet sit
                amet. Pellentesque egestas aliquet nisi, vitae porttitor dui
                semper in. Aliquam vitae est efficitur, finibus lacus id,
                suscipit metus. Cras convallis nisi quis quam maximus, sit
                amet molestie neque vestibulum. Quisque tempor porta luctus.
                Nam lorem augue, lacinia eget semper ut, congue eget lorem.
                Ut eu posuere magna, at semper mauris. Curabitur faucibus ex
                cursus ex imperdiet efficitur.
            </p>
        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="text-center">
            <i class="lgpicto--title lgicon-btn-pictopix"></i>
            <h2 class="title">Our</h2>
            <div class="title title--light mb--40">Brands</div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="card card--bg no-shadow bw" data-square="1">
                    <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 1
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 2
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 3
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 4
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 5
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="card card--link no-shadow">
                    <div class="card__text card__text--spaced-v card__text--spaced-v--arrow card__text--big">
                        Brand 6
                            <span class="link link--static">
                                <i class="lgicon-arrow-right" aria-hidden="true"></i>
                            </span>
                    </div>
                </a>
            </div>

        </div>
    </div>
</section>

<section class="section section--grey">
    <div class="container">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
            <div class="row">
                <div class="row">
                    <div class="col-md-6 hidden-xs">
                        <div class="card card--bg no-shadow" data-eq="brand-text-1">
                            <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                        </div>
                    </div>
                    <div class="col-md-6 mb--20" data-eq="brand-text-1">
                        <h2 class="title mb--40">Title level 2</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Maxime eaque odio natus, vel, ex in, odit placeat consequuntur accusantium nostrum temporibus nisi vero
                            assumenda minima quis voluptatibus ut tempora adipisci.
                        </p>
                        <a href="#" class="btn">Discover</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <div class="col-md-6 mb--20" data-eq="brand-text-2">
                        <h2 class="title mb--40 text-right text-left-xs">Title level 2</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Maxime eaque odio natus, vel, ex in, odit placeat consequuntur accusantium nostrum temporibus nisi vero
                            assumenda minima quis voluptatibus ut tempora adipisci.
                        </p>
                        <a href="#" class="btn">Discover</a>
                    </div>
                    <div class="col-md-6 mb--20 hidden-xs">
                        <div class="card card--bg no-shadow" data-eq="brand-text-2">
                            <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <div class="row mb--100">
                    <div class="col-sm-6 mb--20">
                        <div class="card card--bg no-shadow mb--20" data-eq="brand-text-1">
                            <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                        </div>
                        <a href="#" class="btn">Discover</a>
                    </div>
                    <div class="col-sm-6">
                        <div class="card card--bg no-shadow mb--20" data-eq="brand-text-1">
                            <img class="img-responsive fullCenter" src="../../images/placeholders/brand-logo.png">
                        </div>
                        <a href="#" class="btn">Discover</a>
                    </div>
                </div>

                <div class="text-center">
                    <div class="flex-v flex-v--wrap">
                        <a href="#" class="social social--inline social--neg">
                            <i class="lgpicto lgicon-btn-printer" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social-modal-trigger social--inline social--neg mr--35">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="social social--svg mr--15">
                            <i class="lgicon-btn-wishlist" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="btn mr--15 bg--gradient-3">
                            <i class="fa fa-flag mr--5" aria-hidden="true"></i>
                            Find a distributor
                        </a>
                        <a href="#" class="btn btn--alt-color">Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>