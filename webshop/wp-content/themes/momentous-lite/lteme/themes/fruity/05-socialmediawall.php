<!DOCTYPE html>
<html lang="en">
<head>
    <title>Legrand - Social media wall</title>

    <?php require_once('../../parts/head--fruity.php'); ?>
</head>
<body>
<?php require_once('../../parts/header.php'); ?>

<div class="banner">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Social media wall</li>
        </ul>
        <h1 class="banner__title">Social media wall</h1>
    </div>
</div>

<section class="section section--grey section--small-top-padding">
    <div class="container">
        <div class="text-right mb--45">
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Have a project ? Tell us</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">FAQ</a>
            <a href="#" class="btn-second btn-second--inline btn-second--primary-color">Training</a>
        </div>
        <div class="row">
            <div class="text-center col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Donec pharetra vestibulum ligula, nec ultrices neque
                    sollicitudin vitae. Praesent faucibus euismod magna, in
                    ornare lacus pretium pulvinar. Nunc ac lorem ac massa
                    rhoncus iaculis.
                    Donec efficitur tincidunt euismod. Donec non odio sit amet neque laoreet tincidunt aliquam nec nulla.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <form action="#" class="mb--100">
            <div class="row">
                <div class="col-md-12 mb--20">Filter by:</div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="facebook" value=".grid-facebook">
                    <label for="facebook">Facebook</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="twitter" value=".grid-twitter">
                    <label for="twitter">Twitter</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="linkedin" value=".grid-linkedin">
                    <label for="linkedin">Linked in</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="pinterest" value=".grid-pinterest">
                    <label for="pinterest">Pinterest</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="youtube" value=".grid-youtube">
                    <label for="youtube">Youtube</label>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <input class="grid-filter" type="checkbox" id="all" value="*" checked>
                    <label for="all">All</label>
                </div>
            </div>
        </form>

        <div class="row">
            <div class="col-xs-12">
                <div class="grid" style="opacity: 0;">
                    <div class="col-md-4 col-sm-6 grid-sizer"></div>
                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-facebook" data-eq="socialmediablock">
                        <div class="title mb--20">
                            <i class="fa fa-facebook text-fb" aria-hidden="true"></i>
                            Facebook feed
                        </div>
                        <p class="para--border-large border-fb mb--15">Lorem ipsum dolor si amet lorem ipsum</p>

                        <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FLegrandFrance%2Fposts%2F1879017752124733&width=500" height="300" width="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Facebook</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-twitter" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-twitter text-tw" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large border-tw mb--20">
                            by <span class="text-blue-link">@Legrand</span>
                        </div>
                        <div class="section__iframe mb--15">
                            <blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Legrand?src=hash">#Legrand</a> wireless chargers bring you maximum possible autonomy every day, so you can live totally mobile. <a href="https://t.co/z8phg3fox4">https://t.co/z8phg3fox4</a></p>&mdash; Legrand (@Legrand) <a href="https://twitter.com/Legrand/status/890920952900575232">28 juillet 2017</a></blockquote>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-twitter" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-twitter text-tw" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large border-tw mb--20">
                            by <span class="text-blue-link">@Legrand</span>
                        </div>
                        <div class="section__iframe mb--15">
                            <blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Legrand?src=hash">#Legrand</a> wireless chargers bring you maximum possible autonomy every day, so you can live totally mobile. <a href="https://t.co/z8phg3fox4">https://t.co/z8phg3fox4</a></p>&mdash; Legrand (@Legrand) <a href="https://twitter.com/Legrand/status/890920952900575232">28 juillet 2017</a></blockquote>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-youtube" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-youtube text-yt" aria-hidden="true"></i>
                            Youtube feed
                        </h4>
                        <p class="para--border-large border-yt mb--15">Lorem ipsum dolor si amet lorem ipsum</p>
                        <div class="section__iframe mb--15">
                            <iframe width="100%" height="300" src="https://www.youtube.com/embed/PWTAwN0grko" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-twitter" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-twitter text-tw" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large border-tw mb--20">
                            by <span class="text-blue-link">@Legrand</span>
                        </div>
                        <div class="section__iframe mb--15">
                            <blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Legrand?src=hash">#Legrand</a> wireless chargers bring you maximum possible autonomy every day, so you can live totally mobile. <a href="https://t.co/z8phg3fox4">https://t.co/z8phg3fox4</a></p>&mdash; Legrand (@Legrand) <a href="https://twitter.com/Legrand/status/890920952900575232">28 juillet 2017</a></blockquote>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-twitter" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-twitter text-tw" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large border-tw mb--20">
                            by <span class="text-blue-link">@Legrand</span>
                        </div>
                        <div class="section__iframe mb--15">
                            <blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Legrand?src=hash">#Legrand</a> wireless chargers bring you maximum possible autonomy every day, so you can live totally mobile. <a href="https://t.co/z8phg3fox4">https://t.co/z8phg3fox4</a></p>&mdash; Legrand (@Legrand) <a href="https://twitter.com/Legrand/status/890920952900575232">28 juillet 2017</a></blockquote>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-facebook" data-eq="socialmediablock">
                        <div class="title mb--20">
                            <i class="fa fa-facebook text-fb" aria-hidden="true"></i>
                            Facebook feed
                        </div>
                        <p class="para--border-large border-fb mb--15">Lorem ipsum dolor si amet lorem ipsum</p>

                        <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FLegrandFrance%2Fposts%2F1879017752124733&width=500" height="300" width="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Facebook</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-twitter" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-twitter text-tw" aria-hidden="true"></i>
                            Twitter feed
                        </h4>
                        <div class="para para--border-large border-tw mb--20">
                            by <span class="text-blue-link">@Legrand</span>
                        </div>
                        <div class="section__iframe mb--15">
                            <blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Legrand?src=hash">#Legrand</a> wireless chargers bring you maximum possible autonomy every day, so you can live totally mobile. <a href="https://t.co/z8phg3fox4">https://t.co/z8phg3fox4</a></p>&mdash; Legrand (@Legrand) <a href="https://twitter.com/Legrand/status/890920952900575232">28 juillet 2017</a></blockquote>
                            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="mb--100 mb-xs--20 col-md-4 col-sm-6 grid-item grid-pinterest" data-eq="socialmediablock">
                        <h4 class="title mb--15">
                            <i class="fa fa-pinterest-p text-pin" aria-hidden="true"></i>
                            Pinterest feed
                        </h4>
                        <p class="para--border-large border-pin mb--15">Lorem ipsum dolor si amet lorem ipsum</p>
                        <div class="section__iframe mb--15">
                            <img src="../../images/content/img1.jpg" class="img-responsive">
                        </div>
                        <div class="text-right">
                            <a href="#" class="card__text card__text--xsmall text-color-11">View on Twitter</a>
                        </div>

                        <a href="#" class="social social-modal-trigger social--neg">
                            <i class="fa fa-share-alt" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="pagered text-center">
            <p class="pagered__label">Page 1 of 5</p>
            <ul class="pagered__items js-pagered__items pagered ">
                <li class="pagered__item pagered__item--first pagered-item">
                    <a href="#" title="First page">
                        <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                    </a>
                </li>
                <li class="pagered__item pagered__item--previous pagered-prev">
                    <a href="#" title="Previous page" rel="prev">
                        <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                    </a>
                </li>
                <li class="pagered__item pagered-item active">
                    <a href="#">1</a>
                </li>
                <li class="pagered__item pagered-item">
                    <a href="#">2</a>
                </li>
                <li class="pagered__item pagered-item">
                    <a href="#">3</a>
                </li>
                <li class="pagered__item pagered-item hidden-xs">
                    <a href="#">4</a>
                </li>
                <li class="pagered__item pagered-item hidden-xs">
                    <a href="#">5</a>
                </li>
                <li class="pagered__item pagered__item--next pagered-next ">
                    <a href="#" title="Next page" rel="next">
                        <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                    </a>
                </li>
                <li class="pagered__item pagered__item--last pagered-item">
                    <a href="#" title="Last page">
                        <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>

<?php require_once('../../parts/footer.php'); ?>
</body>
</html>