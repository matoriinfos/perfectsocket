<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/experience/tabela_stil_experience.css">
</head>
<body>
<form method="post" action="wp-content/themes/momentous-lite/experience/create_pdf_experience_eng.php" target="_blank">
<table class = "tabela_proizvoda_potvrda" id="tabela_proizvoda_potvrda" width="100%">
		<tr height="40px" >
		<th id="redni_broj" width="3%">R.BR.</th>
		<th id="interni_opis" width="35%">OPIS</th>
		<th id="kataloski_broj" width="10%">KATALOŠKI BROJ</th>
		<th id="jedinicna_cena" width="15%">CENA</th>
		<th id="kolicina" width="5%"><span class="fullText_2">KOLIČINA</span><span class="shortText_2">KOL.</span></th>
		<th id="cena" width="17%"><span class="fullText_2">KOLIČINA x </span><span class="fullText_2">CENA</span><span class="shortText_2">UKUPNO</span></th>
		<th id="slika" width="15%">SLIKA</th>
		</tr>
<?php
$currency = "RSD";
$brojanje_maski = 0;

//za reseller
if(isset($_REQUEST['prava'])){
    $prava = $_REQUEST['prava'];
}
if(isset($_REQUEST['discount'])){
    $discount = $_REQUEST['discount'];
}
else
    $discount = "ne radi";

// NIZOVI MEHANIZAMA KOJI SE KOPIRAJU IZ JQUERY-ja

if(isset($_REQUEST['niz_koji_nosi_mehanizme'])){
  $niz_koji_nosi_mehanizme = $_REQUEST['niz_koji_nosi_mehanizme'];
}

if(isset($_REQUEST['niz_koji_nosi_opise_mehanizama'])){
  $niz_koji_nosi_opise_mehanizama = $_REQUEST['niz_koji_nosi_opise_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_cene_mehanizama'])){
  $niz_koji_nosi_cene_mehanizama = $_REQUEST['niz_koji_nosi_cene_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_kolicine_mehanizama'])){
  $niz_koji_nosi_kolicine_mehanizama = $_REQUEST['niz_koji_nosi_kolicine_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_module_mehanizama'])){
  $niz_koji_nosi_module_mehanizama = $_REQUEST['niz_koji_nosi_module_mehanizama'];
}

// NIZOVI OSTALIH PROIZVODA
// AKO NIJE PRAZAN NIZ ONDA DODAJ

if(isset($_REQUEST['niz_ostalih_proizvoda'])){
  $niz_ostalih_proizvoda = $_REQUEST['niz_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_opis_ostalih_proizvoda'])){
  $niz_opis_ostalih_proizvoda = $_REQUEST['niz_opis_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_opis_ostalih_proizvoda_engleski'])){
  $niz_opis_ostalih_proizvoda_engleski = $_REQUEST['niz_opis_ostalih_proizvoda_engleski'];
}

if(isset($_REQUEST['niz_cena_ostalih_proizvoda']) ){
  $niz_cena_ostalih_proizvoda = $_REQUEST['niz_cena_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_modul_ostalih_proizvoda']) ){
  $niz_modul_ostalih_proizvoda = $_REQUEST['niz_modul_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_kolicina_ostalih_proizvoda']) ){
  $niz_kolicina_ostalih_proizvoda = $_REQUEST['niz_kolicina_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_nosaca']) ){
  $niz_nosaca = $_REQUEST['niz_nosaca'];
}

if(isset($_REQUEST['niz_opis_nosaca']) ){
  $niz_opis_nosaca = $_REQUEST['niz_opis_nosaca'];
}

if(isset($_REQUEST['niz_opis_nosaca_engleski']) ){
  $niz_opis_nosaca_engleski = $_REQUEST['niz_opis_nosaca_engleski'];
}

if(isset($_REQUEST['niz_cena_nosaca']) ){
  $niz_cena_nosaca = $_REQUEST['niz_cena_nosaca'];
}

if(isset($_REQUEST['niz_modul_nosaca']) ){
  $niz_modul_nosaca = $_REQUEST['niz_modul_nosaca'];
}

if(isset($_REQUEST['niz_br_ubacenih_meh']) ){
  $niz_br_ubacenih_meh = $_REQUEST['niz_br_ubacenih_meh'];
}

$niz_cena_nosaca_ukupno = array();

// NIZOVI MEHANIZAMA
$niz_cena_mehanizama_ukupno = array();
$niz_cena_mehanizama_kompleta_ukupno = array();

//nizovi za slike
$niz_nosaca_src_image = array();
$niz_okvira_src_image = array();
$niz_mehanizama_src_image = array();
$niz_dodatnih_src_image = array();


//NIZOVI OKVIRA
if(isset($_REQUEST['niz_okvira'])){
  $niz_okvira = $_REQUEST['niz_okvira'];
}

if(isset($_REQUEST['niz_opis_okvira'])){
  $niz_opis_okvira = $_REQUEST['niz_opis_okvira'];
}

if(isset($_REQUEST['niz_opis_okvira_engleski'])){
  $niz_opis_okvira_engleski = $_REQUEST['niz_opis_okvira_engleski'];
}

if(isset($_REQUEST['niz_cena_okvira'])){
  $niz_cena_okvira = $_REQUEST['niz_cena_okvira'];
}

if(isset($_REQUEST['niz_kolicina'])){
  $niz_kolicina = $_REQUEST['niz_kolicina'];
}

if(isset($_REQUEST['niz_nosaca']) ){
  $brojanje = count($niz_nosaca);
}
else
{
	$brojanje = 0;
}

$niz_cena_ukupno = array();

if(isset($_REQUEST['niz_ostalih_proizvoda']) ){
  $brojanje_ostalih = count($niz_ostalih_proizvoda);
}
else
{
	$brojanje_ostalih = 0;
}

if(isset($_REQUEST['niz_koji_nosi_mehanizme']) ){
  $brojanje_mehanizama = count($niz_koji_nosi_mehanizme);
}
else
{
	$brojanje_mehanizama = 0;
}


$i = 0;
//$k = 0;
$redni_broj_tabela = 0;
$svega_ukupno = 0;
$vec_bio_k = 0;
$z = 0;

// ISPISIVANJE NIZOVA
while ($i < $brojanje)
{
$redni_broj_tabela ++;

// ISPISIVANJE NASLOVA
echo '<tr>';
echo'<td colspan="7" align="left">'.$niz_opis_nosaca[$i].'</td>'; // NASLOV
echo '</tr>';

// ISPISIVANJE NOSACA
echo '<tr>';
echo'<td align="center">'.$redni_broj_tabela.'</td>'; // redni broj
echo'<td align="left">'.$niz_opis_nosaca[$i].'</td>'; // OPIS
$string = $niz_nosaca[$i];
$newstring = str_replace("/", "_", $string);

$newKatbr = $niz_nosaca[$i];
$newKatbr = str_replace("_", ".", $newKatbr);
echo'<td align="center">'.$newKatbr.'</td>'; // KATALOSKI BROJ
echo'<td align="center">'.number_format($niz_cena_nosaca[$i], 2, ',', ' ').' '.$currency.'</td>'; // JEDINICNA CENA
echo'<td align="center">'.$niz_kolicina[$i].'</td>'; // KOLICINA
$niz_cena_nosaca_ukupno[$i] = $niz_cena_nosaca[$i] * $niz_kolicina[$i];
echo'<td align="center">'.number_format($niz_cena_nosaca_ukupno[$i], 2, ',', ' ').' '.$currency.'</td>'; // UKUPNO
echo'<td align="center"><img src="wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring .'.png" class="slika_'.$niz_modul_nosaca[$i].'"></td>'; // SLIKA
echo '</tr>';

//$string = $niz_nosaca[$i];
//$newstring = str_replace("/", "_", $string);
$niz_cena_nosaca_ukupno[$i] = $niz_cena_nosaca[$i] * $niz_kolicina[$i];
$niz_nosaca_src_image[$i] = "wp-content/themes/momentous-lite/experience/slike/okviri/".$newstring .".png";


// ISPISIVANJE MEHANIZAMA I MASKI
$k = 0;
$niz_cena_mehanizama_kompleta_ukupno[$i] = 0;
while ($k < $niz_br_ubacenih_meh[$i])
{
	/*OVDE SE ODVAJA KATALOSKI BROJ MASKE OD KATALOSKOG BROJA PREKIDACA DA BI SE KASNIJE PRIKAZALA SLIKA*/
	$kat_br = $niz_koji_nosi_mehanizme[$z];

	if(strpos($kat_br, '+'))
	{
		$kat_br = substr($kat_br, strpos($kat_br, "+")); // izdvajanje reci posle plusa
		$kat_br = substr($kat_br, 1);  // brisanje znaka plus
	}

	//echo "<script type='text/javascript'>alert('$kat_br');</script>";

	$redni_broj_tabela ++;
	echo '<tr>';
	echo'<td align="center">'.$redni_broj_tabela.'</td>'; // redni broj
	echo'<td align="left">'.$niz_koji_nosi_opise_mehanizama[$z].'</td>'; // opis
	$string = $kat_br;
	$newstring = str_replace("/", "_", $string);

	$newKatbr = $niz_koji_nosi_mehanizme[$z];
	$newKatbr = str_replace("_", ".", $newKatbr);

	echo'<td align="center">'.$newKatbr.'</td>'; // KATALOSKI BROJ
	echo'<td align="center">'.number_format($niz_koji_nosi_cene_mehanizama[$z], 2, ',', ' ').' '.$currency.'</td>'; // JEDINICNA CENA
	echo'<td align="center">'.$niz_kolicina[$i].'</td>'; // KOLICINA
	$niz_cena_mehanizama_ukupno[$z] = $niz_koji_nosi_cene_mehanizama[$z] * $niz_kolicina[$i];
	$niz_koji_nosi_kolicine_mehanizama[$z] = $niz_kolicina[$i];
	echo'<td align="center">'.number_format($niz_cena_mehanizama_ukupno[$z], 2, ',', ' ').' '.$currency.'</td>'; // UKUPNO
	echo'<td align="center"><img src="wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_'.$niz_koji_nosi_module_mehanizama[$z].'"></td>'; // SLIKA
	echo '</tr>';
	$niz_cena_mehanizama_kompleta_ukupno[$i] = $niz_cena_mehanizama_kompleta_ukupno[$i] + $niz_cena_mehanizama_ukupno[$z];
    $niz_mehanizama_src_image[$z] = "wp-content/themes/momentous-lite/experience/slike/".$newstring .".jpg";

	$z++;
	$k++;
}
// ISPISIVANJE NIZA KATALOSKIH BROJEVA - OKVIRA
$redni_broj_tabela ++;
echo '<tr>';
echo'<td align="center">'.$redni_broj_tabela.'</td>'; // redni broj
echo'<td align="left">'.$niz_opis_okvira[$i].'</td>'; // OPIS
$string = $niz_okvira[$i];
$newstring = str_replace("/", "_", $string);
$newKatbr = $niz_okvira[$i];
$newKatbr = str_replace("_", ".", $newKatbr);
echo'<td align="center">'.$newKatbr.'</td>'; // KATALOSKI BROJ
echo'<td align="center">'.number_format($niz_cena_okvira[$i], 2, ',', ' ').' '.$currency.'</td>'; // JEDINICNA CENA
echo'<td align="center">'.$niz_kolicina[$i].'</td>'; // KOLICINA
$niz_cena_okvira_ukupno[$i] = $niz_cena_okvira[$i] * $niz_kolicina[$i];
echo'<td align="center">'.number_format($niz_cena_okvira_ukupno[$i], 2, ',', ' ').' '.$currency.'</td>'; // UKUPNO
echo'<td align="center"><img src="wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring .'.png" class="slika_'.$niz_modul_nosaca[$i].'"></td>'; // SLIKA
echo '</tr>';
$niz_cena_ukupno[$i] = $niz_cena_nosaca_ukupno[$i] + $niz_cena_okvira_ukupno[$i] + $niz_cena_mehanizama_kompleta_ukupno[$i];
$niz_okvira_src_image[$i] = "wp-content/themes/momentous-lite/experience/slike/okviri/".$newstring .".png";
$i++;
}
$ukupna_cena_ostalih_proizvoda = 0;
// ISPISIVANJE NIZA DODATNIH PROIZVODA

$i = 0;
//$niz_cena_ostalih_proizvoda_ukupno[$i] = 0;
while ($i < $brojanje_ostalih)
{
//$k = $i+1;
$redni_broj_tabela ++;
echo '<tr>';
echo'<td align="center">'.$redni_broj_tabela.'</td>'; // redni broj
echo'<td align="left">'.$niz_opis_ostalih_proizvoda[$i].'</td>'; // OPIS
$string = $niz_ostalih_proizvoda[$i];
$newstring = str_replace("/", "_", $string);
$newKatbr = $niz_ostalih_proizvoda[$i];
$newKatbr = str_replace("_", ".", $newKatbr);
echo'<td align="center">'.$newKatbr.'</td>'; // KATALOSKI BROJ
$niz_cena_ostalih_proizvoda[$i] = (float)$niz_cena_ostalih_proizvoda[$i];
echo'<td align="center">'.number_format($niz_cena_ostalih_proizvoda[$i], 2, ',', ' ').' '.$currency.'</td>'; // JEDINICNA CENA
$niz_kolicina_ostalih_proizvoda[$i] = (float)$niz_kolicina_ostalih_proizvoda[$i];
echo'<td align="center">'.$niz_kolicina_ostalih_proizvoda[$i].'</td>'; // KOLICINA
$niz_cena_ostalih_proizvoda_ukupno[$i] = $niz_cena_ostalih_proizvoda[$i] * $niz_kolicina_ostalih_proizvoda[$i];
echo'<td align="center">'.number_format($niz_cena_ostalih_proizvoda_ukupno[$i], 2, ',', ' ').' '.$currency.'</td>'; // UKUPNO
echo'<td align="center"><img src="wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring .'.png" class="slika_'.$niz_modul_ostalih_proizvoda[$i].'"></td>'; // SLIKA
echo '</tr>';
$ukupna_cena_ostalih_proizvoda = $ukupna_cena_ostalih_proizvoda + $niz_cena_ostalih_proizvoda_ukupno[$i];
$niz_dodatnih_src_image[$i] = "wp-content/themes/momentous-lite/experience/slike/okviri/".$newstring .".png";
$i++;
//$k++;
}


$brojanje_kompleta = count($niz_cena_ukupno);

$j = 0;
$cena_ukupno = 0;

while ($j < $brojanje_kompleta)
{
	$cena_ukupno = $cena_ukupno +  $niz_cena_ukupno[$j];
	$j++;
}

$cena_ukupno = $cena_ukupno +  $ukupna_cena_ostalih_proizvoda;
$ukupno_pdv = $cena_ukupno * 1.2;
$svega_ukupno_pdv = $ukupno_pdv;
$svega_ukupno = $cena_ukupno;

?>

</table>
<div class = "cene_ukupno">
<div>UKUPNO: <strong><?php echo number_format($cena_ukupno, 2, ',', ' ')." ".$currency;?></strong></div>
<div>UKUPNO + PDV(20%): <strong><?php echo number_format($ukupno_pdv, 2, ',', ' ')." ".$currency;?></strong></div>
    <?php



    if($prava =='reseller')
    {

        echo "<div><strong>Discounted price: ". number_format($ukupno_pdv * $discount, 2, ',', ' ')." ".$currency . "</strong></div>";

    }
    ?>
</div>

<div class="wrapper-dugmad">

<input type="submit" name="submit" class="neko_dugme" id="neko_dugme1" value="PREUZMI PDF">

</form>
 <form method="post" action="wp-content/themes/momentous-lite/kreiraj_csv_interio_eng.php" target="_blank">

    <?php

    // NIZOVI MEHANIZAMA KOJI SE KOPIRAJU IZ JQUERY-ja
    if(isset($_REQUEST['niz_koji_nosi_mehanizme'])){
        $niz_koji_nosi_mehanizme = $_REQUEST['niz_koji_nosi_mehanizme'];
    }

    if(isset($_REQUEST['niz_koji_nosi_opise_mehanizama'])){
        $niz_koji_nosi_opise_mehanizama = $_REQUEST['niz_koji_nosi_opise_mehanizama'];
    }

    if(isset($_REQUEST['niz_koji_nosi_cene_mehanizama'])){
        $niz_koji_nosi_cene_mehanizama = $_REQUEST['niz_koji_nosi_cene_mehanizama'];
    }

    if(isset($_REQUEST['niz_koji_nosi_kolicine_mehanizama'])){
        $niz_koji_nosi_kolicine_mehanizama = $_REQUEST['niz_koji_nosi_kolicine_mehanizama'];
    }

    if(isset($_REQUEST['niz_koji_nosi_module_mehanizama'])){
        $niz_koji_nosi_module_mehanizama = $_REQUEST['niz_koji_nosi_module_mehanizama'];
    }


    // NIZOVI OSTALIH PROIZVODA
    $niz_cena_ostalih_proizvoda = array();
    $niz_kolicina_ostalih_proizvoda = array();
    $niz_cena_ostalih_proizvoda_ukupno = array();

    // AKO NIJE PRAZAN NIZ ONDA DODAJ

    if(isset($_REQUEST['niz_ostalih_proizvoda'])){
        $niz_ostalih_proizvoda = $_REQUEST['niz_ostalih_proizvoda'];
    }

    if(isset($_REQUEST['niz_opis_ostalih_proizvoda'])){
        $niz_opis_ostalih_proizvoda = $_REQUEST['niz_opis_ostalih_proizvoda'];
    }

    if(isset($_REQUEST['niz_cena_ostalih_proizvoda']) ){
        $niz_cena_ostalih_proizvoda = $_REQUEST['niz_cena_ostalih_proizvoda'];
    }

    if(isset($_REQUEST['niz_modul_ostalih_proizvoda']) ){
        $niz_modul_ostalih_proizvoda = $_REQUEST['niz_modul_ostalih_proizvoda'];
    }

    if(isset($_REQUEST['niz_kolicina_ostalih_proizvoda']) ){
        $niz_kolicina_ostalih_proizvoda = $_REQUEST['niz_kolicina_ostalih_proizvoda'];
    }

    if(isset($_REQUEST['niz_nosaca']) ){
        $niz_nosaca = $_REQUEST['niz_nosaca'];
    }

    if(isset($_REQUEST['niz_opis_nosaca']) ){
        $niz_opis_nosaca = $_REQUEST['niz_opis_nosaca'];
    }

    if(isset($_REQUEST['niz_cena_nosaca']) ){
        $niz_cena_nosaca = $_REQUEST['niz_cena_nosaca'];
    }

    if(isset($_REQUEST['niz_modul_nosaca']) ){
        $niz_modul_nosaca = $_REQUEST['niz_modul_nosaca'];
    }

    if(isset($_REQUEST['niz_br_ubacenih_meh']) ){
        $niz_br_ubacenih_meh = $_REQUEST['niz_br_ubacenih_meh'];
    }

    // NIZOVI MEHANIZAMA
    $niz_cena_mehanizama_ukupno = array();
    $niz_cena_mehanizama_kompleta_ukupno = array();

    //NIZOVI OKVIRA

    if(isset($_REQUEST['niz_okvira'])){
        $niz_okvira = $_REQUEST['niz_okvira'];
    }

    if(isset($_REQUEST['niz_opis_okvira'])){
        $niz_opis_okvira = $_REQUEST['niz_opis_okvira'];
    }

    /*
    if(isset($_REQUEST['niz_opis_okvira_engleski'])){
      $niz_opis_okvira_engleski = $_REQUEST['niz_opis_okvira_engleski'];
    }
    */

    if(isset($_REQUEST['niz_cena_okvira'])){
        $niz_cena_okvira = $_REQUEST['niz_cena_okvira'];
    }

    if(isset($_REQUEST['niz_kolicina'])){
        $niz_kolicina = $_REQUEST['niz_kolicina'];
    }

    if(isset($_REQUEST['niz_nosaca']) ){
        $brojanje = count($niz_nosaca);
    }
    else
    {
        $brojanje = 0;
    }
    $niz_cena_ukupno = array();

    if(isset($_REQUEST['niz_ostalih_proizvoda']) ){
        $brojanje_ostalih = count($niz_ostalih_proizvoda);
    }
    else
    {
        $brojanje_ostalih = 0;
    }

    if(isset($_REQUEST['niz_koji_nosi_mehanizme']) ){
        $brojanje_mehanizama = count($niz_koji_nosi_mehanizme);
    }
    else
    {
        $brojanje_mehanizama = 0;
    }


    // PEBACIVANJE IZABRANIH PROIZVODA NA DRUGU STRANICU DA BI SE UBACILE U BAZU - PA DA BI SE PROČITALE

    $i=0;
    while ($i < $brojanje)
    {
        {
            //niz ubacenih okvira
            echo '<input type="hidden" name="niz_br_ubacenih_meh'.$i.'" value="'.$niz_br_ubacenih_meh[$i].'"/>';
            //image
            echo '<input type="hidden" name="niz_nosaca_src_image'.$i.'" value="'.$niz_nosaca_src_image[$i].'"/>';
            echo '<input type="hidden" name="niz_okvira_src_image'.$i.'" value="'.$niz_okvira_src_image[$i].'"/>';
            // NIZOVI NOSACA
            echo '<input type="hidden" name="niz_nosaca'.$i.'" value="'.$niz_nosaca[$i].'"/>';
            echo '<input type="hidden" name="niz_opis_nosaca'.$i.'" value="'.$niz_opis_nosaca[$i].'"/>';
            echo '<input type="hidden" name="niz_opis_nosaca_engleski'.$i.'" value="'.$niz_opis_nosaca_engleski[$i].'"/>';
            echo '<input type="hidden" name="niz_cena_nosaca'.$i.'" value="'.$niz_cena_nosaca[$i].'"/>';
            echo '<input type="hidden" name="niz_kolicina'.$i.'" value="'.$niz_kolicina[$i].'"/>';
            echo '<input type="hidden" name="niz_modul_nosaca'.$i.'" value="'.$niz_modul_nosaca[$i].'"/>';

            //NIZOVI OKVIRA
            echo '<input type="hidden" name="niz_okvira'.$i.'" value="'.$niz_okvira[$i].'"/>';
            echo '<input type="hidden" name="niz_opis_okvira'.$i.'" value="'.$niz_opis_okvira[$i].'"/>';
            //echo '<input type="hidden" name="niz_opis_okvira_engleski'.$i.'" value="'.$niz_opis_okvira_engleski[$i].'"/>';
            echo '<input type="hidden" name="niz_cena_okvira'.$i.'" value="'.$niz_cena_okvira[$i].'"/>';
            $i++;
        }
    }


    $i=0;
    while ($i < $brojanje_ostalih )
    {
        {
            //image
            echo '<input type="hidden" name="niz_dodatnih_src_image'.$i.'" value="'.$niz_dodatnih_src_image[$i].'"/>';
            //NIZOVI OSTALIH PROIZVODA
            echo '<input type="hidden" name="niz_ostalih_proizvoda'.$i.'" value="'.$niz_ostalih_proizvoda[$i].'"/>';
            echo '<input type="hidden" name="niz_opis_ostalih_proizvoda'.$i.'" value="'.$niz_opis_ostalih_proizvoda[$i].'"/>';
            echo '<input type="hidden" name="niz_opis_ostalih_proizvoda_engleski'.$i.'" value="'.$niz_opis_ostalih_proizvoda_engleski[$i].'"/>';
            echo '<input type="hidden" name="niz_cena_ostalih_proizvoda'.$i.'" value="'.$niz_cena_ostalih_proizvoda[$i].'"/>';
            echo '<input type="hidden" name="niz_kolicina_ostalih_proizvoda'.$i.'" value="'.$niz_kolicina_ostalih_proizvoda[$i].'"/>';
            echo '<input type="hidden" name="niz_modul_ostalih_proizvoda'.$i.'" value="'.$niz_modul_ostalih_proizvoda[$i].'"/>';
            $i++;
        }
    }

    $i=0;
    while ($i < $brojanje_mehanizama )
    {
        {
            //image
            echo '<input type="hidden" name="niz_mehanizama_src_image'.$i.'" value="'.$niz_mehanizama_src_image[$i].'"/>';
            //NIZOVI OSTALIH PROIZVODA
            echo '<input type="hidden" name="niz_koji_nosi_mehanizme'.$i.'" value="'.$niz_koji_nosi_mehanizme[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_opise_mehanizama'.$i.'" value="'.$niz_koji_nosi_opise_mehanizama[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_cene_mehanizama'.$i.'" value="'.$niz_koji_nosi_cene_mehanizama[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_kolicine_mehanizama'.$i.'" value="'.$niz_koji_nosi_kolicine_mehanizama[$i].'"/>';
            //echo "<script type='text/javascript'>alert('$niz_koji_nosi_kolicine_mehanizama[$i]');</script>";
            echo '<input type="hidden" name="niz_koji_nosi_module_mehanizama'.$i.'" value="'.$niz_koji_nosi_module_mehanizama[$i].'"/>';
            $i++;
        }
    }

    //NIZOVI maski
    $i=0;
    while ($i < $brojanje_maski )
    {
        {
            echo '<input type="hidden" name="niz_koji_nosi_maske'.$i.'" value="'.$niz_koji_nosi_maske[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_opise_maski'.$i.'" value="'.$niz_koji_nosi_opise_maski[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_cene_maski'.$i.'" value="'.$niz_koji_nosi_cene_maski[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_kolicine_maski'.$i.'" value="'.$niz_koji_nosi_kolicine_maski[$i].'"/>';
            echo '<input type="hidden" name="niz_koji_nosi_module_maski'.$i.'" value="'.$niz_koji_nosi_module_maski[$i].'"/>';
            $i++;
        }
    }


    echo '<input type="hidden" name="cena_svega_ukupno" value="'.$svega_ukupno.'"/>';
    echo '<input type="hidden" name="svega_ukupno_pdv" value="'.$svega_ukupno_pdv.'"/>';
    echo '<input type="hidden" name="brojanje" value="'.$brojanje.'"/>';
    echo '<input type="hidden" name="brojanje_ostalih" value="'.$brojanje_ostalih.'"/>';
    echo '<input type="hidden" name="brojanje_mehanizama" value="'.$brojanje_mehanizama.'"/>';
    echo '<input type="hidden" name="brojanje_maski" value="'.$brojanje_maski.'"/>';

    //stranica gde treba da se vrati
    echo '<input type="hidden" name="page" value="?page_id=801"/>';
    ?>



<input type="submit" name="submit" class="neko_dugme" id="neko_dugme1" value="PREUZMI XLS">
</form>
<?php
// PEBACIVANJE IZABRANIH PROIZVODA NA DRUGU STRANICU DA BI SE UBACILE U BAZU - PA DA BI SE PROČITALE

$i=0;
while ($i < $brojanje)
{
	{
        // broj ubacenih mehanizama
        echo '<input type="hidden" name="niz_br_ubacenih_meh'.$i.'" value="'.$niz_br_ubacenih_meh[$i].'"/>';
		// NIZOVI NOSACA
		echo '<input type="hidden" name="niz_nosaca'.$i.'" value="'.$niz_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_nosaca'.$i.'" value="'.$niz_opis_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_nosaca'.$i.'" value="'.$niz_cena_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_kolicina'.$i.'" value="'.$niz_kolicina[$i].'"/>';
		echo '<input type="hidden" name="niz_modul_nosaca'.$i.'" value="'.$niz_modul_nosaca[$i].'"/>';
		$i++;
	}
}


$i=0;
while ($i < $brojanje)
{
	{

		//NIZOVI OKVIRA
		echo '<input type="hidden" name="niz_okvira'.$i.'" value="'.$niz_okvira[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_okvira'.$i.'" value="'.$niz_opis_okvira[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_okvira'.$i.'" value="'.$niz_cena_okvira[$i].'"/>';
		$i++;
	}
}


$i=0;
while ($i < $brojanje_ostalih )
{
	{
		//NIZOVI OSTALIH PROIZVODA
		echo '<input type="hidden" name="niz_ostalih_proizvoda'.$i.'" value="'.$niz_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_ostalih_proizvoda'.$i.'" value="'.$niz_opis_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_ostalih_proizvoda'.$i.'" value="'.$niz_cena_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_kolicina_ostalih_proizvoda'.$i.'" value="'.$niz_kolicina_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_modul_ostalih_proizvoda'.$i.'" value="'.$niz_modul_ostalih_proizvoda[$i].'"/>';
		$i++;
	}
}

//NIZOVI MEHANIZAMA
$i=0;
while ($i < $brojanje_mehanizama )
{
	{

		echo '<input type="hidden" name="niz_koji_nosi_mehanizme'.$i.'" value="'.$niz_koji_nosi_mehanizme[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_opise_mehanizama'.$i.'" value="'.$niz_koji_nosi_opise_mehanizama[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_cene_mehanizama'.$i.'" value="'.$niz_koji_nosi_cene_mehanizama[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_kolicine_mehanizama'.$i.'" value="'.$niz_koji_nosi_kolicine_mehanizama[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_module_mehanizama'.$i.'" value="'.$niz_koji_nosi_module_mehanizama[$i].'"/>';
		$i++;
	}
}

//NIZOVI MASKI
$i=0;
while ($i < $brojanje_maski )
{
	{
		echo '<input type="hidden" name="niz_koji_nosi_maske'.$i.'" value="'.$niz_koji_nosi_maske[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_opise_maski'.$i.'" value="'.$niz_koji_nosi_opise_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_cene_maski'.$i.'" value="'.$niz_koji_nosi_cene_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_kolicine_maski'.$i.'" value="'.$niz_koji_nosi_kolicine_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_module_maski'.$i.'" value="'.$niz_koji_nosi_module_maski[$i].'"/>';
		$i++;
	}
}

echo '<input type="hidden" name="cena_svega_ukupno" value="'.$svega_ukupno.'"/>';
echo '<input type="hidden" name="svega_ukupno_pdv" value="'.$svega_ukupno_pdv.'"/>';
echo '<input type="hidden" name="brojanje" value="'.$brojanje.'"/>';
echo '<input type="hidden" name="brojanje_ostalih" value="'.$brojanje_ostalih.'"/>';
echo '<input type="hidden" name="brojanje_mehanizama" value="'.$brojanje_mehanizama.'"/>';
echo '<input type="hidden" name="brojanje_maski" value="'.$brojanje_maski.'"/>';

?>
<div class = "vrsta_sumiranje" colspan="5" align="center">
<!-- <form method="post" action="wp-content/themes/momentous-lite/kreiraj_csv_experience_eng.php" target="_blank"> -->
    <form method="post" action="controller/CartController.php">
<?php

// NIZOVI MEHANIZAMA KOJI SE KOPIRAJU IZ JQUERY-ja
if(isset($_REQUEST['niz_koji_nosi_mehanizme'])){
  $niz_koji_nosi_mehanizme = $_REQUEST['niz_koji_nosi_mehanizme'];
}

if(isset($_REQUEST['niz_koji_nosi_opise_mehanizama'])){
  $niz_koji_nosi_opise_mehanizama = $_REQUEST['niz_koji_nosi_opise_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_cene_mehanizama'])){
  $niz_koji_nosi_cene_mehanizama = $_REQUEST['niz_koji_nosi_cene_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_kolicine_mehanizama'])){
  $niz_koji_nosi_kolicine_mehanizama = $_REQUEST['niz_koji_nosi_kolicine_mehanizama'];
}

if(isset($_REQUEST['niz_koji_nosi_module_mehanizama'])){
  $niz_koji_nosi_module_mehanizama = $_REQUEST['niz_koji_nosi_module_mehanizama'];
}


// NIZOVI OSTALIH PROIZVODA
$niz_cena_ostalih_proizvoda = array();
$niz_kolicina_ostalih_proizvoda = array();
$niz_cena_ostalih_proizvoda_ukupno = array();

// AKO NIJE PRAZAN NIZ ONDA DODAJ

if(isset($_REQUEST['niz_ostalih_proizvoda'])){
  $niz_ostalih_proizvoda = $_REQUEST['niz_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_opis_ostalih_proizvoda'])){
  $niz_opis_ostalih_proizvoda = $_REQUEST['niz_opis_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_cena_ostalih_proizvoda']) ){
  $niz_cena_ostalih_proizvoda = $_REQUEST['niz_cena_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_modul_ostalih_proizvoda']) ){
  $niz_modul_ostalih_proizvoda = $_REQUEST['niz_modul_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_kolicina_ostalih_proizvoda']) ){
  $niz_kolicina_ostalih_proizvoda = $_REQUEST['niz_kolicina_ostalih_proizvoda'];
}

if(isset($_REQUEST['niz_nosaca']) ){
  $niz_nosaca = $_REQUEST['niz_nosaca'];
}

if(isset($_REQUEST['niz_opis_nosaca']) ){
  $niz_opis_nosaca = $_REQUEST['niz_opis_nosaca'];
}

if(isset($_REQUEST['niz_cena_nosaca']) ){
  $niz_cena_nosaca = $_REQUEST['niz_cena_nosaca'];
}

if(isset($_REQUEST['niz_modul_nosaca']) ){
  $niz_modul_nosaca = $_REQUEST['niz_modul_nosaca'];
}

if(isset($_REQUEST['niz_br_ubacenih_meh']) ){
  $niz_br_ubacenih_meh = $_REQUEST['niz_br_ubacenih_meh'];
}

// NIZOVI MEHANIZAMA
$niz_cena_mehanizama_ukupno = array();
$niz_cena_mehanizama_kompleta_ukupno = array();

//NIZOVI OKVIRA

if(isset($_REQUEST['niz_okvira'])){
  $niz_okvira = $_REQUEST['niz_okvira'];
}

if(isset($_REQUEST['niz_opis_okvira'])){
  $niz_opis_okvira = $_REQUEST['niz_opis_okvira'];
}

/*
if(isset($_REQUEST['niz_opis_okvira_engleski'])){
  $niz_opis_okvira_engleski = $_REQUEST['niz_opis_okvira_engleski'];
}
*/

if(isset($_REQUEST['niz_cena_okvira'])){
  $niz_cena_okvira = $_REQUEST['niz_cena_okvira'];
}

if(isset($_REQUEST['niz_kolicina'])){
  $niz_kolicina = $_REQUEST['niz_kolicina'];
}

if(isset($_REQUEST['niz_nosaca']) ){
  $brojanje = count($niz_nosaca);
}
else
{
	$brojanje = 0;
}
$niz_cena_ukupno = array();

if(isset($_REQUEST['niz_ostalih_proizvoda']) ){
  $brojanje_ostalih = count($niz_ostalih_proizvoda);
}
else
{
	$brojanje_ostalih = 0;
}

if(isset($_REQUEST['niz_koji_nosi_mehanizme']) ){
  $brojanje_mehanizama = count($niz_koji_nosi_mehanizme);
}
else
{
	$brojanje_mehanizama = 0;
}


// PEBACIVANJE IZABRANIH PROIZVODA NA DRUGU STRANICU DA BI SE UBACILE U BAZU - PA DA BI SE PROČITALE

$i=0;
while ($i < $brojanje)
{
	{
        //niz ubacenih okvira
        echo '<input type="hidden" name="niz_br_ubacenih_meh'.$i.'" value="'.$niz_br_ubacenih_meh[$i].'"/>';
        //image
        echo '<input type="hidden" name="niz_nosaca_src_image'.$i.'" value="'.$niz_nosaca_src_image[$i].'"/>';
        echo '<input type="hidden" name="niz_okvira_src_image'.$i.'" value="'.$niz_okvira_src_image[$i].'"/>';
		// NIZOVI NOSACA
		echo '<input type="hidden" name="niz_nosaca'.$i.'" value="'.$niz_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_nosaca'.$i.'" value="'.$niz_opis_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_nosaca_engleski'.$i.'" value="'.$niz_opis_nosaca_engleski[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_nosaca'.$i.'" value="'.$niz_cena_nosaca[$i].'"/>';
		echo '<input type="hidden" name="niz_kolicina'.$i.'" value="'.$niz_kolicina[$i].'"/>';
		echo '<input type="hidden" name="niz_modul_nosaca'.$i.'" value="'.$niz_modul_nosaca[$i].'"/>';

		//NIZOVI OKVIRA
		echo '<input type="hidden" name="niz_okvira'.$i.'" value="'.$niz_okvira[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_okvira'.$i.'" value="'.$niz_opis_okvira[$i].'"/>';
		//echo '<input type="hidden" name="niz_opis_okvira_engleski'.$i.'" value="'.$niz_opis_okvira_engleski[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_okvira'.$i.'" value="'.$niz_cena_okvira[$i].'"/>';
		$i++;
	}
}


$i=0;
while ($i < $brojanje_ostalih )
{
	{
        //image
        echo '<input type="hidden" name="niz_dodatnih_src_image'.$i.'" value="'.$niz_dodatnih_src_image[$i].'"/>';
		//NIZOVI OSTALIH PROIZVODA
		echo '<input type="hidden" name="niz_ostalih_proizvoda'.$i.'" value="'.$niz_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_ostalih_proizvoda'.$i.'" value="'.$niz_opis_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_opis_ostalih_proizvoda_engleski'.$i.'" value="'.$niz_opis_ostalih_proizvoda_engleski[$i].'"/>';
		echo '<input type="hidden" name="niz_cena_ostalih_proizvoda'.$i.'" value="'.$niz_cena_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_kolicina_ostalih_proizvoda'.$i.'" value="'.$niz_kolicina_ostalih_proizvoda[$i].'"/>';
		echo '<input type="hidden" name="niz_modul_ostalih_proizvoda'.$i.'" value="'.$niz_modul_ostalih_proizvoda[$i].'"/>';
		$i++;
	}
}

$i=0;
while ($i < $brojanje_mehanizama )
{
	{
        //image
        echo '<input type="hidden" name="niz_mehanizama_src_image'.$i.'" value="'.$niz_mehanizama_src_image[$i].'"/>';
		//NIZOVI OSTALIH PROIZVODA
		echo '<input type="hidden" name="niz_koji_nosi_mehanizme'.$i.'" value="'.$niz_koji_nosi_mehanizme[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_opise_mehanizama'.$i.'" value="'.$niz_koji_nosi_opise_mehanizama[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_cene_mehanizama'.$i.'" value="'.$niz_koji_nosi_cene_mehanizama[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_kolicine_mehanizama'.$i.'" value="'.$niz_koji_nosi_kolicine_mehanizama[$i].'"/>';
		//echo "<script type='text/javascript'>alert('$niz_koji_nosi_kolicine_mehanizama[$i]');</script>";
		echo '<input type="hidden" name="niz_koji_nosi_module_mehanizama'.$i.'" value="'.$niz_koji_nosi_module_mehanizama[$i].'"/>';
		$i++;
	}
}

		//NIZOVI maski
$i=0;
while ($i < $brojanje_maski )
{
	{
		echo '<input type="hidden" name="niz_koji_nosi_maske'.$i.'" value="'.$niz_koji_nosi_maske[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_opise_maski'.$i.'" value="'.$niz_koji_nosi_opise_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_cene_maski'.$i.'" value="'.$niz_koji_nosi_cene_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_kolicine_maski'.$i.'" value="'.$niz_koji_nosi_kolicine_maski[$i].'"/>';
		echo '<input type="hidden" name="niz_koji_nosi_module_maski'.$i.'" value="'.$niz_koji_nosi_module_maski[$i].'"/>';
		$i++;
	}
}


echo '<input type="hidden" name="cena_svega_ukupno" value="'.$svega_ukupno.'"/>';
echo '<input type="hidden" name="svega_ukupno_pdv" value="'.$svega_ukupno_pdv.'"/>';
echo '<input type="hidden" name="brojanje" value="'.$brojanje.'"/>';
echo '<input type="hidden" name="brojanje_ostalih" value="'.$brojanje_ostalih.'"/>';
echo '<input type="hidden" name="brojanje_mehanizama" value="'.$brojanje_mehanizama.'"/>';
echo '<input type="hidden" name="brojanje_maski" value="'.$brojanje_maski.'"/>';

//stranica gde treba da se vrati
echo '<input type="hidden" name="page" value="?page_id=801"/>';
?>
<div class="wrapper-excel">
<!-- <input type="submit" name="submit_2" class="neko_dugme" id="neko_dugme2" value="PREUZMI CSV (EXCEL)"> -->
    <input type="submit" name="submit_2" class="neko_dugme" id="neko_dugme2" value="DODAJ U KORPU">
</div>
</form>
</div>

</body>
</html>
<?php
require_once('../db_config.php');
// BAZA U KOJU SE UNOSE ILI AŽURIRAJU PODACI KREIRANJA
// KOD ZA BAZU NA NETU
$servername = servername;
$username = username;
$password = password;
$baza = baza;

$i=0;
$update = 0;
$updateOkvira = 0;

$povezivanje = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
$povezivanje->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
$j = 0;

$mesec = date("F");
$godina = date("Y");

while ($j < $brojanje)
{
		//ČITANJE IZ BAZE I PREBACIVANJE U NIZ
		//POVEZIVANJE NA BAZU
			try {
				$conn = new PDO("mysql:host=$servername;dbname=$baza", $username, $password);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$stmt1 = $conn->prepare("SELECT * FROM ws_izbor_korisnika_experience");
				$stmt1->execute();
				}
				catch(PDOException $e) {
				echo "Error: " . $e->getMessage();
				}

					// ČITANJE IZ TABELE "ws_izbor_korisnika_experience"I PREBACIVANJE PROIZVODA U NIZ "niz_izbora"
					$i=0;
					$brojanje_izabranih_proizvoda = 0;
					while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
						{
						$niz_izbora_kat_br[$i] = $row['kat_br'];
						$niz_izbora_kolicina[$i] = $row['kolicina_kreiranja'];
						$niz_izbora_meseca[$i] = $row['mesec_kreiranja'];
						$niz_izbora_godine[$i] = $row['godina_kreiranja'];
						$i++;
						$brojanje_izabranih_proizvoda = $i;
						}
		$i=0;
		while ($i < $brojanje_izabranih_proizvoda)
		{
		// AKO SU ISTI KATALOSKI BROJEVI I AKO JE ISTI MESEC
		// NIZOVI NOSAČA KOJE PROVERAVAMO SA NIZOM TABELE
			if($niz_nosaca[$j]==$niz_izbora_kat_br[$i] && $niz_izbora_meseca[$i] == $mesec  && $niz_izbora_godine[$i] == $godina)
				{
					$update++;
					$niz_izbora_kolicina[$i] = $niz_izbora_kolicina[$i] + $niz_kolicina[$j];
					try
						{
							// AZURIRANJE BAZE - NOSAČIMA
							$sql = "UPDATE ws_izbor_korisnika_experience SET kolicina_kreiranja='$niz_izbora_kolicina[$i]' WHERE kat_br='$niz_nosaca[$j]' and mesec_kreiranja ='$mesec'  and godina_kreiranja ='$godina'";

							$povezivanje->exec($sql);
						}
					catch(PDOException $e)
						{
							echo $sql . "<br>" . $e->getMessage();
						}
				}
				$i++;
		}

		$i=0;
		while ($i < $brojanje_izabranih_proizvoda)
		{
			// PROVERAVANJE OKVIRA
					if($niz_okvira[$j]==$niz_izbora_kat_br[$i] && $niz_izbora_meseca[$i] == $mesec && $niz_izbora_godine[$i] == $godina)
				{
					$updateOkvira++;
					$niz_izbora_kolicina[$i] = $niz_izbora_kolicina[$i] + $niz_kolicina[$j];
					try
						{
							// AZURIRANJE BAZE - OKVIRIMA
							$sql = "UPDATE ws_izbor_korisnika_experience SET kolicina_kreiranja='$niz_izbora_kolicina[$i]' WHERE kat_br='$niz_okvira[$j]' and mesec_kreiranja ='$mesec'  and godina_kreiranja ='$godina' ";


							$povezivanje->exec($sql);
						}
					catch(PDOException $e)
						{
							echo $sql . "<br>" . $e->getMessage();
						}
				}
			$i++;
		}

		//ZA NOSAC - UBACIVANJE U BAZU
		if ($update == 0)
				{
				try
					{
					$kolicina = 1;
					//$mesec = date("F");
					$sql = "INSERT INTO ws_izbor_korisnika_experience (kat_br, kratak_opis, jed_cena, kolicina_kreiranja, mesec_kreiranja, godina_kreiranja) VALUES('$niz_nosaca[$j]','$niz_opis_nosaca[$j]','$niz_cena_nosaca[$j]','$niz_kolicina[$j]','$mesec','$godina')";
					$povezivanje->exec($sql);
					}
				catch(PDOException $e)
					{
					echo $sql . "<br>" . $e->getMessage();
					}
				}
		//ZA OKVIR - UBACIVANJE U BAZU
		if ($updateOkvira == 0)
		{
				try
					{
					$kolicina = 1;
					//$mesec = date("F");
					$sql = "INSERT INTO ws_izbor_korisnika_experience (kat_br, kratak_opis, jed_cena, kolicina_kreiranja, mesec_kreiranja, godina_kreiranja) VALUES('$niz_okvira[$j]','$niz_opis_okvira[$j]','$niz_cena_okvira[$j]','$niz_kolicina[$j]','$mesec','$godina')";
					$povezivanje->exec($sql);
					}
				catch(PDOException $e)
					{
					echo $sql . "<br>" . $e->getMessage();
					}
		}
	$update = 0;
	$updateOkvira = 0;
	$j++;
}

////
// AZURIRANJE I UBACIVANJE MEHANIZAMA

$i=0;
$j=0;
$update = 0;
while ($j < $brojanje_mehanizama)
{
		//ČITANJE IZ BAZE I PREBACIVANJE U NIZ
		//POVEZIVANJE NA BAZU
			try {
				$conn = new PDO("mysql:host=$servername;dbname=$baza", $username, $password);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$stmt1 = $conn->prepare("SELECT * FROM ws_izbor_korisnika_experience");
				$stmt1->execute();
				}
				catch(PDOException $e) {
				echo "Error: " . $e->getMessage();
				}

					// ČITANJE IZ TABELE "ws_izbor_korisnika_experience"I PREBACIVANJE PROIZVODA U NIZ "niz_izbora"
					$i=0;
					$brojanje_izabranih_proizvoda = 0;
					while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
						{
						$niz_izbora_kat_br[$i] = $row['kat_br'];
						$niz_izbora_kolicina[$i] = $row['kolicina_kreiranja'];
						$niz_izbora_meseca[$i] = $row['mesec_kreiranja'];
						$niz_izbora_godine[$i] = $row['godina_kreiranja'];
						$i++;
						$brojanje_izabranih_proizvoda = $i;
						}
		$i=0;
		while ($i < $brojanje_izabranih_proizvoda)
		{
		// AKO SU ISTI KATALOSKI BROJEVI I AKO JE ISTI MESEC
		// NIZOVI MEHANIZAMA KOJE PROVERAVAMO SA NIZOM TABELE IZBORA KORISNIKA
			if($niz_koji_nosi_mehanizme[$j] == $niz_izbora_kat_br[$i] && $niz_izbora_meseca[$i] == $mesec && $niz_izbora_godine[$i] == $godina)
				{
					$update++;
					$niz_izbora_kolicina[$i] = $niz_izbora_kolicina[$i] + $niz_koji_nosi_kolicine_mehanizama[$j];
					try
						{
							// AZURIRANJE BAZE - MEHANIZMIMA
							$sql = "UPDATE ws_izbor_korisnika_experience SET kolicina_kreiranja='$niz_izbora_kolicina[$i]' WHERE kat_br='$niz_koji_nosi_mehanizme[$j]' and mesec_kreiranja ='$mesec' and godina_kreiranja ='$godina'";

							$povezivanje->exec($sql);
						}
					catch(PDOException $e)
						{
							echo $sql . "<br>" . $e->getMessage();
						}
				}
			$i++;
		}
		//ZA MEHANIZAME - UBACIVANJE U BAZU
		if ($update == 0)
				{
				try
					{
					$kolicina = 1;
					$sql = "INSERT INTO ws_izbor_korisnika_experience (kat_br, kratak_opis, jed_cena, kolicina_kreiranja, mesec_kreiranja, godina_kreiranja) VALUES('$niz_koji_nosi_mehanizme[$j]','$niz_koji_nosi_opise_mehanizama[$j]','$niz_koji_nosi_cene_mehanizama[$j]','$niz_koji_nosi_kolicine_mehanizama[$j]','$mesec','$godina')";
					$povezivanje->exec($sql);
					}
				catch(PDOException $e)
					{
					echo $sql . "<br>" . $e->getMessage();
					}
				}
	$update = 0;
	$j++;
}

////
// AZURIRANJE I UBACIVANJE OSTALIH MEHANIZAMA - DOZNE, MASKE, ITD

$i=0;
$j=0;
$update = 0;
while ($j < $brojanje_ostalih)
{
		//ČITANJE IZ BAZE I PREBACIVANJE U NIZ
		//POVEZIVANJE NA BAZU
			try {
				$conn = new PDO("mysql:host=$servername;dbname=$baza", $username, $password);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$stmt1 = $conn->prepare("SELECT * FROM ws_izbor_korisnika_experience");
				$stmt1->execute();
				}
				catch(PDOException $e) {
				echo "Error: " . $e->getMessage();
				}

					// ČITANJE IZ TABELE "ws_izbor_korisnika_experience"I PREBACIVANJE PROIZVODA U NIZ "niz_izbora"
					$i=0;
					$brojanje_izabranih_proizvoda = 0;
					while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
						{
						$niz_izbora_kat_br[$i] = $row['kat_br'];
						$niz_izbora_kolicina[$i] = $row['kolicina_kreiranja'];
						$niz_izbora_meseca[$i] = $row['mesec_kreiranja'];
						$niz_izbora_godine[$i] = $row['godina_kreiranja'];
						$i++;
						$brojanje_izabranih_proizvoda = $i;
						}
		$i=0;
		while ($i < $brojanje_izabranih_proizvoda)
		{
		// AKO SU ISTI KATALOSKI BROJEVI I AKO JE ISTI MESEC
		// NIZOVI MEHANIZAMA KOJE PROVERAVAMO SA NIZOM TABELE IZBORA KORISNIKA
			if($niz_ostalih_proizvoda[$j] == $niz_izbora_kat_br[$i] && $niz_izbora_meseca[$i] == $mesec)
				{
					$update++;
					//echo "<script type='text/javascript'>alert('AZURIRANJE:' + 'BROJ IZABRANIH PROIZVODA JE: ' + '$brojanje_izabranih_proizvoda' + ', A i je: ' + '$i');</script>";
					$niz_izbora_kolicina[$i] = $niz_izbora_kolicina[$i] + $niz_kolicina_ostalih_proizvoda[$j];
					try
						{
							// AZURIRANJE BAZE - MEHANIZMIMA
							$sql = "UPDATE ws_izbor_korisnika_experience SET kolicina_kreiranja='$niz_izbora_kolicina[$i]' WHERE kat_br='$niz_ostalih_proizvoda[$j]' and mesec_kreiranja ='$mesec' and godina_kreiranja ='$godina'";

							$povezivanje->exec($sql);
						}
					catch(PDOException $e)
						{
							echo $sql . "<br>" . $e->getMessage();
						}
				}
			$i++;
		}
		//ZA MEHANIZAME - UBACIVANJE U BAZU
		if ($update == 0)
				{
				try
					{
					$kolicina = 1;
					//$mesec = date("F");
					$sql = "INSERT INTO ws_izbor_korisnika_experience (kat_br, kratak_opis, jed_cena, kolicina_kreiranja, mesec_kreiranja, godina_kreiranja) VALUES('$niz_ostalih_proizvoda[$j]','$niz_opis_ostalih_proizvoda[$j]','$niz_cena_ostalih_proizvoda[$j]','$niz_kolicina_ostalih_proizvoda[$j]','$mesec','$godina')";
					$povezivanje->exec($sql);
					}
				catch(PDOException $e)
					{
					echo $sql . "<br>" . $e->getMessage();
					}
				}
	$update = 0;
	$j++;
}
$povezivanje = null;
$conn = null;
?>
