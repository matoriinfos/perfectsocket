$('.proizvodi').hide();
$('.proizvodi#prekidac_jednopolni').show();
$('.glavni_meni#link_prekidaci_tasteri').css({ 'color': '#125185'});
$('.link_moduli#link_svi').css({'color':'#125185'});
var naslovLinka = $('#link_prekidaci_tasteri').text()
$('.naslov_proizvoda').text(naslovLinka);

//////////////////
// NAVIGACIJA JEZICIMA
/////////////////

/*
$('body').on('click','#brit2', function() {

if (confirm('Attention: All previously will be deleted. Are you sure?')) {
        window.location = "?page_id=2";
    }
});

$('body').on('click','#srb2', function() {

if (confirm('Pažnja: Sve prethodno biće obrisano. Da li ste sigurni?')) {
        window.location = "?page_id=5";
    }
});

// PRELAZAK PREKO ZASTAVICE SRB2
$('body').on('mouseover','#srb2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/srb.jpg';
	$(this).css('cursor','pointer');
});

$('body').on('mouseout','#srb2', function() {

	this.src = 'wp-content/themes/momentous-lite/flags/srbDark.jpg';
});

// KRAJ PRELASKA PREKO ZASTAVICE SRB2
////
// PRELAZAK PREKO ZASTAVICE BRIT2
$('body').on('mouseover','#brit2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/brit.jpg';
	$(this).css('cursor','pointer');
});

$('body').on('mouseout','#brit2', function() {
	this.src = 'wp-content/themes/momentous-lite/flags/britDark.jpg';
});
*/

// NAVIGACIJA

$('body').on('click','.glavni_meni#link_detektori_pokreta','li', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.proizvodi#detektori_pokreta').fadeIn(250, function(){ $(this).show();});;
	//$('.glavni_meni').css({ 'color': 'white'});
	//$('.proizvodi#detektori_pokreta').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.dropdown-contentNovo', function() {
	$(".dropdown-contentNovo").hide();
});

$('body').on('mouseover','.dropbtnNovo', function() {
	$(".dropdown-contentNovo").show();
});

// MENI NAVIGACIJA - VELIČINA - BOJA - PRELAZ MIŠEM I KLIK
$('body').on('click','.link_moduli', function() {
	$('.link_moduli').css({'color':'#d0d0d0'});
	$(this).css({ 'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_prekidaci_tasteri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$(".levi_podmeni").css("display", "block");
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#prekidaci').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);

});

$('body').on('click','.glavni_meni#link_roletne', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#roletne').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_dimeri', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.proizvodi#dimeri').show();
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_detektori_pokreta', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_termostati_detektori', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#termostati_detektori').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_energetske_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#energetske_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_racunarske_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#racunarske_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_tv_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#tv_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_maske', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#maske').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_multimedijalne_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#multimedijalne_uticnice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_kupatilo', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#kupatilo').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_sijalice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#sijalice').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_vodootporno', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#vodootporno').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_nosaci_dozne', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#nosaci_dozne').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_druga_oprema', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#druga_oprema').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_okviri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#okviri').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_jednopolni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_jednopolni').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_naizmenicni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_naizmenicni').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_ukrsni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_ukrsni').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_jednopolni_nula', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_jednopolni_nula').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_nula', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_nula').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_dvopolni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_dvopolni').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_taster', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	//$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_taster').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});

$('body').on('click','.glavni_meni#link_prekidac_potezni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	//$('.link_moduli').css({ 'color': '#d0d0d0'});
	//$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	//$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#prekidac_potezni').show();
	//$(this).css({'color': '#125185'});
	var tekst = $(this).text();
	$('.naslov_proizvoda').text(tekst);
});


// PODMENI NAVIGACIJA (LINKOVI: MODUL1 i MODUL2)
$('body').on('click','.link_moduli#link_svi', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show(300);
});

$('body').on('click','.link_moduli#link_1', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_2').hide(300);
});

$('body').on('click','.link_moduli#link_2', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show(300);
	$('.vrsta_mehanizmi#mehanizmi_modul_1').hide(300);
});