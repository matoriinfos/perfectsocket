<?php

$unicID = uniqid();

$cena_svega_ukupno = $_POST["cena_svega_ukupno"];
$svega_ukupno_pdv = $_POST["svega_ukupno_pdv"];
$brojanje = $_POST["brojanje"];
$brojanje_ostalih = $_POST["brojanje_ostalih"];
$brojanje_mehanizama = $_POST["brojanje_mehanizama"];

$niz_nosaca = array ();
$niz_opis_nosaca = array ();
$niz_cena_nosaca = array ();
$niz_modul_nosaca = array ();
$niz_kolicina = array ();
$niz_cena_puta_kolicina_nosaca = array();

$niz_okvira = array ();
$niz_cena_okvira = array ();
$niz_opis_okvira = array ();
$niz_modul_okvira = array ();

$niz_koji_nosi_mehanizme = array ();
$niz_koji_nosi_opise_mehanizama = array ();
$niz_koji_nosi_cene_mehanizama = array ();
$niz_koji_nosi_kolicine_mehanizama = array ();
$niz_koji_nosi_module_mehanizama = array ();
$niz_koji_nosi_cene_mehanizama_ukupno = array ();
$niz_naziv_slike = array ();

$niz_ostalih_proizvoda = array ();
$niz_opis_ostalih_proizvoda = array ();
$niz_cena_ostalih_proizvoda = array ();
$niz_modul_ostalih_proizvoda = array ();
$niz_kolicina_ostalih_proizvoda = array ();
$redni_broj_proizvoda = array();
$currency = 'RSD';

//$brojanje = count($niz_nosaca);


$i = 0;
while ($i < $brojanje)
{		
	$niz_nosaca[$i] = $_POST["niz_nosaca$i"];
	$niz_opis_nosaca[$i] = $_POST["niz_opis_nosaca$i"];
	$niz_cena_nosaca[$i] = $_POST["niz_cena_nosaca$i"];
	$niz_modul_nosaca[$i] = $_POST["niz_modul_nosaca$i"];
	$niz_kolicina[$i] = $_POST["niz_kolicina$i"];
	$niz_cena_puta_kolicina_nosaca[$i] = $niz_cena_nosaca[$i] * $niz_kolicina[$i];
	
	$niz_okvira[$i] = $_POST["niz_okvira$i"];
	$niz_cena_okvira[$i] = $_POST["niz_cena_okvira$i"];
	$niz_opis_okvira[$i] = $_POST["niz_opis_okvira$i"];
	$niz_modul_okvira[$i] = $_POST["niz_modul_nosaca$i"];
	$niz_cena_okvira_ukupno[$i] = $niz_kolicina[$i] * $niz_cena_okvira[$i]; 
	
	$redni_broj_proizvoda[$i] = $i;
	$i++;
}


$i = 0;
while ($i < $brojanje_mehanizama)
{		
	$niz_koji_nosi_mehanizme[$i] = $_POST["niz_koji_nosi_mehanizme$i"];
	$niz_koji_nosi_opise_mehanizama[$i] = $_POST["niz_koji_nosi_opise_mehanizama$i"];
	$niz_koji_nosi_cene_mehanizama[$i] = $_POST["niz_koji_nosi_cene_mehanizama$i"];
	$niz_koji_nosi_kolicine_mehanizama[$i] = $_POST["niz_koji_nosi_kolicine_mehanizama$i"];
	$niz_koji_nosi_module_mehanizama[$i] = $_POST["niz_koji_nosi_module_mehanizama$i"];
	$niz_koji_nosi_cene_mehanizama_ukupno[$i] = $niz_koji_nosi_cene_mehanizama[$i] * $niz_koji_nosi_kolicine_mehanizama[$i];

	$i++;
}

$i = 0;
while ($i < $brojanje_ostalih)
{		
	$niz_kolicina_ostalih_proizvoda[$i] = $_POST["niz_kolicina_ostalih_proizvoda$i"];
	$niz_ostalih_proizvoda[$i] = $_POST["niz_ostalih_proizvoda$i"];
	$niz_opis_ostalih_proizvoda[$i] = $_POST["niz_opis_ostalih_proizvoda$i"];
	$niz_cena_ostalih_proizvoda[$i] = $_POST["niz_cena_ostalih_proizvoda$i"];
	$niz_modul_ostalih_proizvoda[$i] = $_POST["niz_modul_ostalih_proizvoda$i"];
	$niz_cena_ostalih_proizvoda_ukupno[$i] = $niz_kolicina_ostalih_proizvoda[$i] * $niz_cena_ostalih_proizvoda[$i];
	$i++;
}

// KOD ZA BAZU NA NETU
$servername = "localhost";
$username = "ultrars_sale";
$password = "sale99999";
$baza = "ultrars_wpdbul12";

// POVEZIVANJE NA BAZU
$povezivanje = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
$povezivanje->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

// UBACIVANJE U BAZU NOSACA
$i=0;
$redni_broj = 1;
while ($i < $brojanje)
{
$redni_broj++;
try {
$sql = "INSERT INTO spisak_visio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES('$redni_broj','$niz_opis_nosaca[$i]','$niz_nosaca[$i]','$niz_cena_nosaca[$i]','$niz_kolicina[$i]','$niz_cena_puta_kolicina_nosaca[$i]','$niz_nosaca[$i]','.png','$niz_modul_nosaca[$i]','$unicID')";
$povezivanje->exec($sql);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}

// UBACIVANJE U BAZU OKVIRA
$i=0;
while ($i < $brojanje)
{
	$redni_broj++;
try {
$sql_2 = "INSERT INTO spisak_visio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_opis_okvira[$i]','$niz_okvira[$i]','$niz_cena_okvira[$i]','$niz_kolicina[$i]','$niz_cena_okvira_ukupno[$i]','$niz_okvira[$i]', 'png', '$niz_modul_nosaca[$i]','$unicID')";
$povezivanje->exec($sql_2);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}

// UBACIVANJE U BAZU MEHANIZAMA
$i=0;
while ($i < $brojanje_mehanizama)
{
$redni_broj++;
try {
$sql_ostalih = "INSERT INTO spisak_visio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_koji_nosi_opise_mehanizama[$i]','$niz_koji_nosi_mehanizme[$i]','$niz_koji_nosi_cene_mehanizama[$i]','$niz_koji_nosi_kolicine_mehanizama[$i]','$niz_koji_nosi_cene_mehanizama_ukupno[$i]','$niz_koji_nosi_mehanizme[$i]', 'jpg', '$niz_koji_nosi_module_mehanizama[$i]','$unicID')";
$povezivanje->exec($sql_ostalih);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}


$i=0;
while ($i < $brojanje_ostalih)
{
$redni_broj++;
try {
$sql_ostalih = "INSERT INTO spisak_visio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_opis_ostalih_proizvoda[$i]','$niz_ostalih_proizvoda[$i]','$niz_cena_ostalih_proizvoda[$i]','$niz_kolicina_ostalih_proizvoda[$i]','$niz_cena_ostalih_proizvoda_ukupno[$i]','$niz_ostalih_proizvoda[$i]', 'png', '$niz_modul_ostalih_proizvoda[$i]','$unicID')";
$povezivanje->exec($sql_ostalih);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}


//UBACIVANJE CENE U BAZU
try {
$sql = "INSERT INTO cena_ukupno (ukupno, ukupno_pdv, unicid) VALUES ('$cena_svega_ukupno','$svega_ukupno_pdv','$unicID')";
$povezivanje->exec($sql);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}


// VELIKO KREIRANJE PDF-a

define('FPDF_FONTPATH', 'font/');
require('../fpdf181/fpdf.php');


//Connect to your database

$servername = "localhost";
$username = "ultrars_sale";
$password = "sale99999";
$dbname = "ultrars_wpdbul12";


//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$result_1= $conn->prepare("SELECT * FROM spisak_visio WHERE unicid = '$unicID' ORDER BY redni_broj");
	$result_2= $conn->prepare("SELECT * FROM cena_ukupno WHERE unicid = '$unicID'");
	$result_1->execute();
	$result_2->execute();
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();
}

//Create new pdf file
$pdf=new FPDF();

//Open file
//$pdf->Open();

//Disable automatic page break
$pdf->SetAutoPageBreak(false);

//Add first page
$pdf->AddPage();

//set initial y axis position per page
$y_axis_initial = 15;
$row_height = 6;

//DODAVANJE LOGO-a U MEMORANDUM

//print column titles for the actual page
$pdf->SetFillColor(232, 232, 232);
$pdf->SetFont('Arial', 'B', 11);
$pdf->SetY($y_axis_initial);
		$pdf->SetX(10);
		$pdf->Cell(10, 6, 'Br.', 1, 0, 'C', 1);
        $pdf->Cell(80, 6, 'Opis', 1, 0, 'L', 1);
		$pdf->Cell(25, 6, 'Kat.br.', 1, 0, 'C', 1);
        $pdf->Cell(20, 6, 'Cena', 1, 0, 'C', 1);
		$pdf->Cell(10, 6, 'Kol', 1, 0, 'C', 1);
		$pdf->Cell(23, 6, 'Ukupno', 1, 0, 'C', 1);
		$pdf->Cell(23, 6, 'Slika', 1, 0, 'C', 1);
		//$pdf->Image('logo.png',10,6,30);

$y_axis = $y_axis_initial + $row_height;

//Select the Products you want to show in your PDF file

//initialize counter
$i = 0;
$redni_broj = 0;

//Set maximum rows per page
$max = 12;

//Set Row Height
while ($row=$result_1->fetch(PDO::FETCH_ASSOC))
{
    //If the current row is the last one, create new page and print column title
    if ($i == $max)
    {
		$pdf->SetFillColor(232, 232, 232);
		$pdf->SetFont('Arial', 'B', 11);
        $pdf->AddPage();
        //print column titles for the current page
        $pdf->SetY($y_axis_initial); // SETUJE ODAKLE POČINJE TABELA (y osa)
        $pdf->SetX(10);
		$pdf->Cell(10, 6, 'Br.', 1, 0, 'C', 1);
        $pdf->Cell(80, 6, 'Opis', 1, 0, 'L', 1);
		$pdf->Cell(25, 6, 'Kat.br.', 1, 0, 'C', 1);
        $pdf->Cell(20, 6, 'Cena', 1, 0, 'C', 1);
		$pdf->Cell(10, 6, 'Kol', 1, 0, 'C', 1);
		$pdf->Cell(23, 6, 'Ukupno', 1, 0, 'C', 1);
		$pdf->Cell(23, 6, 'Slika', 1, 0, 'C', 1);
        
		$row_height = 6;
        //Go to next row
        $y_axis = $y_axis_initial + $row_height;
        
        //Set $i variable to 0 (first row)
        $i = 0;
    }

	$row_height = 20;
    //$redni_broj = $i+1;
	$redni_broj = $redni_broj+1;
	
    $opis = $row['kratak_opis'];
	$kat_br = $row['kat_br'];
	
	/*OVDE SE ODVAJA KATALOSKI BROJ MASKE OD KATALOSKOG BROJA PREKIDACA DA BI SE KASNIJE PRIKAZALA SLIKA*/
	if(strpos($kat_br, '+'))
	{
		$kat_br = substr($kat_br, strpos($kat_br, "+")); // izdvajanje reci posle plusa
		$kat_br = substr($kat_br, 1);  // brisanje znaka plus
	}
	
	$kolicina = $row['kolicina'];
    $cena = $row['jedinicna_cena'];
	$cena = floatval($cena);
	
	$ukupno_svega = $row['ukupno'];
	$ukupno_svega = floatval($ukupno_svega);
	
	$tip_slike = $row['tip_slike'];
	$niz_modula = $row['broj_modula'];

	$pdf->SetFillColor(255, 255, 255);
	$pdf->SetFont('Arial', '', 10);
    $pdf->SetY($y_axis);
    $pdf->SetX(10);
    $pdf->Cell(10, 20, $redni_broj, 1, 0, 'C', 1);
    $pdf->Cell(80, 20, $opis, 1, 0, 'L', 1); 
	$pdf->Cell(25, 20, $kat_br, 1, 0, 'C', 1);
    $pdf->Cell(20, 20, number_format($cena, 2, ',', ' '), 1, 0, 'C', 1);
	$pdf->Cell(10, 20, $kolicina, 1, 0, 'C', 1);
	$pdf->Cell(23, 20, number_format($ukupno_svega, 2, ',', ' '), 1, 0, 'C', 1);
    $nov_kat_br = str_replace("/", "_", $kat_br);
	
	if ($tip_slike == "png")
	{
		$image1 = "slike/okviri/$nov_kat_br.png";
		if($niz_modula == 1)
		{
		$pdf->Cell( 23, 20, $pdf->Image($image1, $pdf->GetX()+7, $pdf->GetY()+2, 7), 1, 0, 'C', false );
		}
		else
		{
		$pdf->Cell( 23, 20, $pdf->Image($image1, $pdf->GetX()+4, $pdf->GetY()+2, 15), 1, 0, 'C', false );
		}
	}
	else
	{
		$image1 = "slike/$nov_kat_br.jpg";
		if($niz_modula == 1)
		{
		$pdf->Cell( 23, 20, $pdf->Image($image1, $pdf->GetX()+7, $pdf->GetY()+2, 8), 1, 0, 'C', false );
		}
		else
		{
		$pdf->Cell( 23, 20, $pdf->Image($image1, $pdf->GetX()+4, $pdf->GetY()+2, 15), 1, 0, 'C', false );
		}	
	}
	
    //Go to next row
    $y_axis = $y_axis + $row_height;
    $i = $i + 1;
}
//CITANJE UKUPNE CENE IZ TABELE UKUPNO

while ($row=$result_2->fetch(PDO::FETCH_ASSOC))
{
	$ukupno = $row['ukupno'];
	$ukupno = floatval($ukupno);
	
	$ukupno_porez = $row['ukupno_pdv'];
	$ukupno_porez = floatval($ukupno_porez);
}

// UKUPNO
$pdf->SetFont('Arial', '', 11);
$pdf->SetY($y_axis);
$pdf->SetX(10);
$pdf->Cell(145, 6, 'UKUPNO', 1, 0, 'R', 1);
$pdf->Cell(46, 6, number_format($ukupno, 2, ',', ' ').' RSD', 1, 0, 'C', 1);
// UKUPNO sa porezom
$row_height = 6;
$y_axis = $y_axis + $row_height;
$pdf->SetFont('Arial', '', 12);
$pdf->SetY($y_axis);
$pdf->SetX(10);
$pdf->Cell(145, 6, 'UKUPNO + TAX(20%)', 1, 0, 'R', 1);
$pdf->Cell(46, 6, number_format($ukupno_porez, 2, ',', ' ').' RSD', 1, 0, 'C', 1);

//mysql_close($link);

//Create file
$pdf->Output();
//BRISANJE ZAPISA U TABELI SPECIFIKACIJA

try {

	$sql = "DELETE FROM spisak_visio where unicid = '$unicID'";

    // use exec() because no results are returned
    $povezivanje->exec($sql);
    }
	catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }
//BRISANJE ZAPISA U TABELI CENA_UKUPNO

try {

	$sql = "DELETE FROM cena_ukupno where unicid = '$unicID'";

    // use exec() because no results are returned
    $povezivanje->exec($sql);
    }
	catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }	
	
	$povezivanje = null;
?>
