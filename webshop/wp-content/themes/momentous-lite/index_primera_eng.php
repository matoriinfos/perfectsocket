<!DOCTYPE html>
<?php
ini_set("default_charset", "UTF-8");
//header("Content-Type: text/html;charset=UTF-8");
/*
Template Name: index_primera_eng
*/
?>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
<title>Primera - Nopal Lux - Configurator</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
<meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
<meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/primera/style_primera.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>
<div class = "subMenu">
	<div class="left_container">
	<div class="bticino_logo"><img src="wp-content/themes/momentous-lite/logotipi/primera_logo.jpg"></div>
	<div class = "naslov_proizvoda"></div>
	</div>
	<div class = "right_container">
<!--	<div class="flags"><div class="flag"><img src="wp-content/themes/momentous-lite/flags/brit.jpg" title="english" id="brit1"></div><div class="flag"><img src="wp-content/themes/momentous-lite/flags/srbDark.jpg" title="srpski" id="srb2"></div></div> -->
	<div class = "naslov_kompleta">MY SELECTION</div>
	</div>
</div>
<div class="leva">
<?php 
// VREDNOST EVRA U DINARIMA
$money = 118;
$currency = "EUR";


// KOD ZA BAZA NA NETU
$servername = servername;
$username = username;
$password = password;
$dbname = baza;

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt3 = $conn->prepare("SELECT * FROM primera_kompleti_prekidaci");
	$stmt4 = $conn->prepare("SELECT * FROM primera_kompleti_tasteri");
	$stmt5 = $conn->prepare("SELECT * FROM primera_kompleti_dimeri");
	$stmt6 = $conn->prepare("SELECT * FROM primera_kompleti_prikljucnice");
	$stmt7 = $conn->prepare("SELECT * FROM primera_kompleti_rj45_rj11");
	$stmt8 = $conn->prepare("SELECT * FROM primera_kompleti_tv");
	$stmt9 = $conn->prepare("SELECT * FROM primera_kompleti_kupatila");
	$stmt10 = $conn->prepare("SELECT * FROM primera_kutije");

	
	$stmt3->execute();
	$stmt4->execute();
	$stmt5->execute();
	$stmt6->execute();
	$stmt7->execute();
	$stmt8->execute();
	$stmt9->execute();
	$stmt10->execute();
	
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();


}
?>
<div class="navigacija">
<ul class="glavni">
  <!--<li class="glavni_meni" id="link_okviri"><a>FRAMES</a></li>-->
  <li class="glavni_meni" id="link_prekidaci"><a>SWITCHES</a></li>
  <li class="glavni_meni" id="link_tasteri"><a>PUSH-BUTTONS</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>DIMMERS</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>ENERGY SOCKETS</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>COMPUTER SOCKETS</a></li>
  <li class="glavni_meni" id="link_tv_audio_hdmi_uticnice"><a>TV SOCKETS</a></li>
  <li class="glavni_meni" id="link_kupatilo"><a>BATHROOM</a></li>
  <li class="glavni_meni" id="link_kutije"><a>BOXES</a></li>
</ul>
</div>

<div class="dropdownNovo">
  <button class="dropbtnNovo">Wiring devices</button>
  <div class="dropdown-contentNovo">
  <!--<li class="glavni_meni" id="link_okviri"><a>FRAMES</a></li>-->
  <li class="glavni_meni" id="link_prekidaci"><a>SWITCHES</a></li>
  <li class="glavni_meni" id="link_tasteri"><a>PUSH-BUTTONS</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>DIMMERS</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>ENERGY SOCKETS</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>COMPUTER SOCKETS</a></li>
  <li class="glavni_meni" id="link_tv_audio_hdmi_uticnice"><a>TV SOCKETS</a></li>
  <li class="glavni_meni" id="link_kupatilo"><a>BATHROOM</a></li>
  <li class="glavni_meni" id="link_kutije"><a>BOXES</a></li>
  </div>
</div>


<div class="bticino_levi">
<ul class = "levi_podmeni">
<li id="naslov_podmeni"></li>
<li style="float:right"><a class="link_moduli" id="link_svi">BOTH</a></li>
<li style="float:right"><a class="link_moduli" id="link_2">CLASSIC</a></li>
<li style="float:right"><a class="link_moduli" id="link_1">LINE</a></li>
</ul>
</div>

<!-- LEVI SCROLL BAR -->
<div class="ex1">

<!-- PREKIDACI -->
<div class = "proizvodi" id = "prekidaci">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_opis[$i] = $row['opis_srpski'];
	$prekidaci_kat_br[$i] = $row['kat_br'];
	$prekidaci_opis_jezik[$i] = $row['opis_engleski'];
	$prekidaci_cena[$i] = $row['vp_cena_evro']/$money;
	//$prekidaci_boja[$i] = $row['boja'];
	$prekidaci_tip[$i] = $row['tip'];
	$i++;
	$brojanje_prekidaca = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_prekidaca)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$prekidaci_tip[$i].'" alt="'.$prekidaci_opis_jezik[$i].'" title = "'.$prekidaci_kat_br[$i].'" data-boja="'.$prekidaci_boja[$i].'" data-tip="'.$prekidaci_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$prekidaci_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$prekidaci_cena[$i].'" data-opis ="'.$prekidaci_opis_jezik[$i].'" data-kratak-opis ="'.$prekidaci_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$prekidaci_opis_jezik[$i].'" title = "'.$prekidaci_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$prekidaci_oprema_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$prekidaci_boja[$i].'" data-tip="'.$prekidaci_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$prekidaci_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$prekidaci_cena[$i].'" data-opis ="'.$prekidaci_opis_jezik[$i].'" data-kratak-opis ="'.$prekidaci_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($prekidaci_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($prekidaci_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($prekidaci_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - PREKIDACI -->




<!-- TASTERI -->
<div class = "proizvodi" id = "tasteri">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt4->fetch(PDO::FETCH_ASSOC))
{	
	$tasteri_opis[$i] = $row['opis_srpski'];
	$tasteri_kat_br[$i] = $row['kat_br'];
	$tasteri_opis_jezik[$i] = $row['opis_engleski'];
	$tasteri_cena[$i] = $row['vp_cena_evro']/$money;
	//$tasteri_boja[$i] = $row['boja'];
	$tasteri_tip[$i] = $row['tip'];
	$i++;
	$brojanje_tastera = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_tastera)
{
	$red_br = $red_br + 1;
	$string = $tasteri_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$tasteri_tip[$i].'" alt="'.$tasteri_opis_jezik[$i].'" title = "'.$tasteri_kat_br[$i].'" data-boja="'.$tasteri_boja[$i].'" data-tip="'.$tasteri_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$tasteri_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$tasteri_cena[$i].'" data-opis ="'.$tasteri_opis_jezik[$i].'" data-kratak-opis ="'.$tasteri_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$tasteri_opis_jezik[$i].'" title = "'.$tasteri_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$tasteri_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$tasteri_boja[$i].'" data-tip="'.$tasteri_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$tasteri_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$tasteri_cena[$i].'" data-opis ="'.$tasteri_opis_jezik[$i].'" data-kratak-opis ="'.$tasteri_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$tasteri_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$tasteri_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($tasteri_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($tasteri_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($tasteri_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - TASTERI -->



<!-- DIMERI -->
<div class = "proizvodi" id = "dimeri">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt5->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_opis[$i] = $row['opis_srpski'];
	$dimeri_kat_br[$i] = $row['kat_br'];
	$dimeri_opis_jezik[$i] = $row['opis_engleski'];
	$dimeri_cena[$i] = $row['vp_cena_evro']/$money;
	//$dimeri_boja[$i] = $row['boja'];
	$dimeri_tip[$i] = $row['tip'];
	$i++;
	$brojanje_dimera = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_dimera)
{
	$red_br = $red_br + 1;
	$string = $dimeri_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$dimeri_tip[$i].'" alt="'.$dimeri_opis_jezik[$i].'" title = "'.$dimeri_kat_br[$i].'" data-boja="'.$dimeri_boja[$i].'" data-tip="'.$dimeri_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$dimeri_cena[$i].'" data-opis ="'.$dimeri_opis_jezik[$i].'" data-kratak-opis ="'.$dimeri_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$dimeri_opis_jezik[$i].'" title = "'.$dimeri_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$dimeri_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$dimeri_boja[$i].'" data-tip="'.$dimeri_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$dimeri_cena[$i].'" data-opis ="'.$dimeri_opis_jezik[$i].'" data-kratak-opis ="'.$dimeri_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$dimeri_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$dimeri_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($dimeri_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($dimeri_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($dimeri_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - DIMERI -->


<!-- UTICNICE -->
<div class = "proizvodi" id = "energetske_uticnice">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt6->fetch(PDO::FETCH_ASSOC))
{	
	$uticnice_opis[$i] = $row['opis_srpski'];
	$uticnice_kat_br[$i] = $row['kat_br'];
	$uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	//$uticnice_boja[$i] = $row['boja'];
	$uticnice_tip[$i] = $row['tip'];
	$i++;
	$brojanje_uticnica = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_uticnica)
{
	$red_br = $red_br + 1;
	$string = $uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$uticnice_tip[$i].'" alt="'.$uticnice_opis_jezik[$i].'" title = "'.$uticnice_kat_br[$i].'" data-boja="'.$uticnice_boja[$i].'" data-tip="'.$uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$uticnice_cena[$i].'" data-opis ="'.$uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$uticnice_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$uticnice_opis_jezik[$i].'" title = "'.$uticnice_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$uticnice_boja[$i].'" data-tip="'.$uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$uticnice_cena[$i].'" data-opis ="'.$uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$uticnice_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$uticnice_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - UTICNICE -->



<!-- RACUNARSKE UTICNICE -->
<div class = "proizvodi" id = "racunarske_uticnice">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt7->fetch(PDO::FETCH_ASSOC))
{	
	$racunarske_uticnice_opis[$i] = $row['opis_srpski'];
	$racunarske_uticnice_kat_br[$i] = $row['kat_br'];
	$racunarske_uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$racunarske_uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	//$racunarske_uticnice_boja[$i] = $row['boja'];
	$racunarske_uticnice_tip[$i] = $row['tip'];
	$i++;
	$brojanje_racunarskih_uticnica = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_racunarskih_uticnica)
{
	$red_br = $red_br + 1;
	$string = $racunarske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$racunarske_uticnice_tip[$i].'" alt="'.$racunarske_uticnice_opis_jezik[$i].'" title = "'.$racunarske_uticnice_kat_br[$i].'" data-boja="'.$racunarske_uticnice_boja[$i].'" data-tip="'.$racunarske_uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-opis ="'.$racunarske_uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$racunarske_uticnice_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$racunarske_uticnice_opis_jezik[$i].'" title = "'.$racunarske_uticnice_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$racunarske_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$racunarske_uticnice_boja[$i].'" data-tip="'.$racunarske_uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-opis ="'.$racunarske_uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$racunarske_uticnice_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$racunarske_uticnice_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$racunarske_uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - RACUNARSKE UTICNICE -->


<!-- TV UTICNICE -->
<div class = "proizvodi" id = "tv_audio_hdmi_uticnice">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt8->fetch(PDO::FETCH_ASSOC))
{	
	$tv_uticnice_opis[$i] = $row['opis_srpski'];
	$tv_uticnice_kat_br[$i] = $row['kat_br'];
	$tv_uticnice_opis_jezik[$i] = $row['opis_engleski'];
	$tv_uticnice_cena[$i] = $row['vp_cena_evro']/$money;
	//$tv_uticnice_boja[$i] = $row['boja'];
	$tv_uticnice_tip[$i] = $row['tip'];
	$i++;
	$brojanje_tv_uticnica = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_tv_uticnica)
{
	$red_br = $red_br + 1;
	$string = $tv_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$tv_uticnice_tip[$i].'" alt="'.$tv_uticnice_opis_jezik[$i].'" title = "'.$tv_uticnice_kat_br[$i].'" data-boja="'.$tv_uticnice_boja[$i].'" data-tip="'.$tv_uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$tv_uticnice_cena[$i].'" data-opis ="'.$tv_uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$tv_uticnice_opis_jezik[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/'.$newstring.'.png" alt = "'.$tv_uticnice_opis_jezik[$i].'" title = "'.$tv_uticnice_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$tv_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$tv_uticnice_boja[$i].'" data-tip="'.$tv_uticnice_tip[$i].'" data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_uticnice_opis[$i].'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$tv_uticnice_cena[$i].'" data-opis ="'.$tv_uticnice_opis_jezik[$i].'" data-kratak-opis ="'.$tv_uticnice_opis_jezik[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$tv_uticnice_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$tv_uticnice_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - TV UTICNICE -->


<!-- KUPATILO -->
<div class = "proizvodi" id = "kupatilo">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt9->fetch(PDO::FETCH_ASSOC))
{	
	$kupatilo_opis[$i] = $row['opis_srpski'];
	$kupatilo_kat_br[$i] = $row['kat_br'];
	$kupatilo_opis_jezik[$i] = $row['opis_engleski'];
	$kupatilo_kratak_opis[$i] = $row['kratak_opis_engleski'];
	$kupatilo_cena[$i] = $row['vp_cena_evro']/$money;
	//$kupatilo_boja[$i] = $row['boja'];
	$kupatilo_tip[$i] = $row['tip'];
	$i++;
	$brojanje_kupatilo = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_kupatilo)
{
	$red_br = $red_br + 1;
	$string = $kupatilo_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$kupatilo_tip[$i].'" alt="'.$kupatilo_opis_jezik[$i].'" title = "'.$kupatilo_kat_br[$i].'" data-boja="'.$kupatilo_boja[$i].'" data-tip="'.$kupatilo_tip[$i].'" data-modul ="'.$br_modula.'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$kupatilo_cena[$i].'" data-opis ="'.$kupatilo_opis[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/okviri/'.$newstring.'.png" alt = "'.$kupatilo_opis_jezik[$i].'" title = "'.$kupatilo_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$kupatilo_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$kupatilo_boja[$i].'" data-tip="'.$kupatilo_tip[$i].'" data-modul ="'.$br_modula.'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$kupatilo_cena[$i].'" data-opis ="'.$kupatilo_opis[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$kupatilo_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$kupatilo_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - KUPATILO -->


<!-- KUTIJE -->
<div class = "proizvodi" id = "kutije">
<?php
// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt10->fetch(PDO::FETCH_ASSOC))
{	
	$kutije_opis[$i] = $row['opis_srpski'];
	$kutije_kat_br[$i] = $row['kat_br'];
	$kutije_opis_jezik[$i] = $row['opis_engleski'];
	$kutije_kratak_opis[$i] = $row['kratak_opis_engleski'];
	$kutije_cena[$i] = $row['vp_cena_evro']/$money;
	//$kutije_boja[$i] = $row['boja'];
	$kutije_tip[$i] = $row['tip'];
	$i++;
	$brojanje_kutije = $i;
}


?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE OSTALE OPREME
$i=0;
$red_br=0;
while ($i< $brojanje_kutije)
{
	$red_br = $red_br + 1;
	$string = $kutije_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
					
					// MEHANIZAM NA ENGLESKOM
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula_dodatno" id="'.$kutije_tip[$i].'" alt="'.$kutije_opis_jezik[$i].'" title = "'.$kutije_kat_br[$i].'" data-boja="'.$kutije_boja[$i].'" data-tip="'.$kutije_tip[$i].'" data-modul ="'.$br_modula.'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$kutije_cena[$i].'" data-opis ="'.$kutije_opis[$i].'" data-kratak-opis ="'.$kutije_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/primera/slike/okviri/'.$newstring.'.png" alt = "'.$kutije_opis_jezik[$i].'" title = "'.$kutije_kat_br[$i].'" class="slika_velicine_x_modula_dodatno" title="'.$kutije_kat_br[$i].'" id="'.$br_modula_id.'"  data-boja="'.$kutije_boja[$i].'" data-tip="'.$kutije_tip[$i].'" data-modul ="'.$br_modula.'" data-kat-br = "'.$newstring[$i].'" data-cena="'.$kutije_cena[$i].'" data-opis ="'.$kutije_opis[$i].'" data-kratak-opis ="'.$kutije_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$kutije_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$kutije_opis_jezik[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Discounted price: '.number_format($uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Price: '.number_format($uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE	
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ - KUTIJE -->




</div> <!-- KRAJ LEVOG SCROLL BARA -->
</div><!-- KRAJ DIV LEVA -->
<!-- DESNI DEO STRANICE -->
<div class = "desna">
<div class = "desna_dugme_kreiranje"></div>

<?php 
/*
// PRAVI SE NIZ NOSAČA
$i=0;
while ($row=$stmt12->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_opis[$i] = $row['opis_engleski'];
	$nosaci_kat_br[$i] = $row['kat_br'];
	$nosaci_cena[$i] = $row['vp_cena_evro']/$money;
	$broj_modula[$i] = $row['mehanizam'];
	$i++;
	$brojanje_nosaci=$i;
}
echo '<ul class="podmeni_nosaci">';
echo '<li class="slika_nav_x_modula" id=1 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_opis[0].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[0].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">1 MECHANISM</p><p class = "nosac_slova_2_broj">1</p><p class = "nosac_slova_2">MECHANISM</p></a></li>';
echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_opis[1].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[1].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2 MECHANISMS</p><p class = "nosac_slova_2_broj">2</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '<li class="slika_nav_x_modula" id=3 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_opis[2].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[2].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">3 MECHANISMS</p><p class = "nosac_slova_2_broj">3</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '<li class="slika_nav_x_modula" id=4 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_opis[3].'"><a><img src = "wp-content/themes/momentous-lite/niloe/slike/'.$nosaci_kat_br[3].'.png" alt = "drawing base of support" class="slika_nav_x_modula_slika"><p class = "nosac_slova">4 MECHANISMS</p><p class = "nosac_slova_2_broj">4</p><p class = "nosac_slova_2">MECHANISMS</p></a></li>';
echo '</ul>';
*/
?>

<!-- DESNI SCROLL BAR -->
<div class="ex2">
<div class = "desna_mehanizmi"></div>
   <?php
    //polja za reseller
    echo '<div id="prava" data-prava = "'.$prava.'"></div>';
    echo '<div id="discount" data-discount = "'.$discount.'"></div>';
    ?>
</div><!-- KRAJ DESNOG SCROLL BARA -->


</div>

<?php 
/*require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="wp-content/themes/momentous-lite/primera/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/primera/skripta_primera.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/primera/jezici_kompleti.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/primera/upravljanje_primera_kompleta_eng.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/primera/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>