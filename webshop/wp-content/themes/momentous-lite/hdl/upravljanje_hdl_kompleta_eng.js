//KLONIRANJE NOSACA
var i=0;
var globalni_id = 0;
var trenutni_id = 1;
var globalni_id_mehanizma = 0;
var globalni_id_okvira = 0;
var cena_prethodnog_okvira;
var kreirano = 0;
//var evro = 120;
var currency = "EUR";

//////////////////
// NAVIGACIJA JEZICIMA
/////////////////
$('body').on('click','#british', function() {

if (confirm('Attention: All previously will be deleted. Are you sure?')) {
        window.location = "?page_id=30&lang=en";
    }
});

$('body').on('click','#serbian', function() {

if (confirm('Pažnja: Sve prethodno biće obrisano. Da li ste sigurni?')) {
        window.location = "?page_id=19&lang=sr";
    }
});

//////////////////
// KLIKOM NA SLIKU - KLONIRANJE NOSACA
//////////////////
var id_modul;
	 
$('body').on('click','.glavni_meni', function()
{
$('.nova').hide();
});

$('body').on('click','.slika_nav_x_modula', function() {
id_modul = $(this).attr('id');

//METOD SETOVANJA POTREBNIH INFORMACIJU O KOLICINAMA
var kolicina_proizvoda = 1;
var br_modula_nosac = id_modul;
var ime_slike = $(this).attr('title');
globalni_id = globalni_id + 1; // ID nosaca odnosno kompleta
// BROJ PREOSTALIH MESTA NAKON DODAVANJA - GLOBALNA PROMENLJIVA - OVAJ BROJ SE OSVEŽAVA SVAKI PUT PRILIKOM MARKIRANJA KOMPLETA

// IZRACUNAVANJE CENE MEHANIZMA
var cena_nosaca = parseFloat($(this).attr('data-cena'));
cena_nosaca_ukupno = cena_nosaca * 1;
var kratak_opis = $(this).attr('data-kratak-opis');
//var kratak_opis_engleski = $(this).attr('data-kratak-opis-engleski');

// IZRACUNAVANJE UKUPNE CENE
cena_ukupno = cena_nosaca;
if (kreirano == 0)
{
	$('<div class="export" id="srpski" title="creale list of products">CREATE<br>PRODUCT LIST<div>').hide().appendTo('.desna_dugme_kreiranje').fadeIn(1000);
	//$('<div class="export" id="english">CREATE PRODUCT LIST<div>').appendTo('#desna_dugme_kreiranje');
	kreirano = 1;
}

// KOMPLET MEHANIZAMA - SRPSKI

var rec_mehanizam;

if(br_modula_nosac > 1)
{
	rec_mehanizam = "mechanisms";
}
else{
	rec_mehanizam = "mechanism";
}

$('<div class="mesto_nosac" id="komplet_'+globalni_id+'" data-modul='+br_modula_nosac+' data-id='+globalni_id+' data-preostali-moduli='+br_modula_nosac+'>\
		<div class="naslov_tabele"><div class="naslov_tabele_tekst">'+globalni_id+'. kit - Accept '+br_modula_nosac+' '+rec_mehanizam+'</div>\
			<div class="dugmad">\
				<div class="dugme_brisi" id="dugme_'+globalni_id+'" data-id = "'+globalni_id+'" title="delete kit" data-ukupan-br-dodatih = "0"><a> x </a></div>\
				<div class="dugme_minimiziraj" id="dugme_minimiz_'+globalni_id+'" id="dugme_minimiz_'+globalni_id+'"  data-id = "'+globalni_id+'" title="minimize kit" data-klik="0"><a> - </a></div>\
			</div>\
		</div>\
		<div class="slika_nosac_'+br_modula_nosac+'_modula_klon" id="nosac_'+globalni_id+'" data-id='+globalni_id+'  data-ukupno-dodavanje = "0" data-kolicina=1 data-modul='+br_modula_nosac+' data-preostali-moduli='+br_modula_nosac+' data-kat_br = '+ime_slike+' data-cena='+cena_nosaca+'  data-kratak-opis="'+kratak_opis+'" data-br-ubacenih-mehanizama=0 style="background-image: url(wp-content/themes/momentous-lite/primera/slike/'+ime_slike+'.png)"></div>\
		<div class="kolicina_proizvoda" id="kolicina_proizvoda_'+globalni_id+'">\
				<div class="kolicina_naslov">QUANTITY</div>\
				<div class="kolicina_prikaz" id="kolicina_prikaz_'+globalni_id+'" data-id='+globalni_id+' data-prikaz-kolicina="'+kolicina_proizvoda+'">'+kolicina_proizvoda+'</div>\
				<div class="kolicina_minus" id="kolicina_minus_'+globalni_id+'" data-id='+globalni_id+' data-br-modula="'+br_modula_nosac+'">-</div>\
				<div class="kolicina_plus" id="kolicina_plus_'+globalni_id+'" data-id='+globalni_id+' data-br-modula="'+br_modula_nosac+'">+</div>\
			</div>\
		<table class="cenovnik_komplet_'+br_modula_nosac+'_modula" id="komplet_opreme_'+globalni_id+'" data-redni-broj = 1 data-cena ='+cena_nosaca_ukupno+' data-id="'+globalni_id+'" data-cena-mehanizama-ukupno = 0 data-cena-kompleta='+cena_nosaca_ukupno+' data-cena-okvira=0>\
			<tbody>\
			<tr>\
			<th colspan="5">PRODUCTS TABLE</th>\
			</tr>\
			<tr class="tabela_red_naslovi" id="tabela_red_naslovi_'+globalni_id+'">\
			<td class="kolona_naziv_naslov">DESCRIPTION</td><td class="kolona_kat_br_naslov">REFERENCE NUMBER</td><td class="kolona_kolicina_naslov">QUANTITY</td><td class="kolona_cena_naslov">PRICE</td><td class="kolona_cena_naslov">QUANTITY x PRICE</td>\
			</tr>\
			<!--<tr id="cena_nosac_'+globalni_id+'">\
			<td class="kolona_naziv">'+kratak_opis+'</td><td class="kolona_kat_br">'+ime_slike+'</td><td class="kolona_kolicina" id="kolicina_nosaca_'+globalni_id+'" data-kolicina="'+kolicina_proizvoda+'">'+kolicina_proizvoda+'</td><td class="kolona_cena" id="kolona_cena_nosaca_'+globalni_id+'" data-cena="'+cena_nosaca+'">'+Number(cena_nosaca).toLocaleString('de')+' '+currency+'</td><td class="kolona_cena" id="kolona_cena_nosaca_ukupno_'+globalni_id+'" data-cena="'+cena_nosaca_ukupno+'">'+Number(cena_nosaca_ukupno).toLocaleString('de')+' '+currency+'</td>\
			</tr>-->\
			<tr class = "cena_ukupno" id="cena_ukupno_'+globalni_id+'"><th colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_ukupno_cena" id="kolona_ukupno_'+globalni_id+'">'+Number(cena_nosaca_ukupno).toLocaleString('de')+' '+currency+'</th></tr>\
			</tbody>\
		</table>\
</div>').prependTo('.desna_mehanizmi');
$('.mesto_nosac').css("background","#ececec");
$('#komplet_'+globalni_id+'.mesto_nosac').css("background","#cde6ff");
trenutni_id = globalni_id;   

$('#komplet_opreme_'+globalni_id).attr('data-cena', cena_ukupno); 
$('#komplet_opreme_'+globalni_id).attr('data-cena-kompleta', cena_ukupno);
$('#komplet_opreme_'+globalni_id).attr('data-cena-nosaca', cena_ukupno);
cena_prethodnog_okvira = 0;
kreirano = 1;


	// TRENUTAK KADA SE SKRIVAJU I POJAVLJUJU BOJE OKVIRA SA LEVE STRANE U ZAVISNOSTI OD BROJA MEHANIZAMA
	var i = 0;
	var broj_okvira = $("#skriven").attr('data-ukupan-broj-okvira');
	var cena_okvira;
	var skracenica_okvira = "";
	//var broj_mehanizama = 
	$('.boje_okvira').show();
	if (br_modula_nosac == 4 || br_modula_nosac == 5)
	{
		while (i <= broj_okvira)
		{
		cena_okvira = $("#kolona_"+i).attr('data-cena-okvira');
			if (cena_okvira == 0)
			{
			skracenica_okvira = $('#kolona_'+i).attr('data-naziv-skracenice');
			$('.boje_okvira#skracenica_'+skracenica_okvira).hide();			
			}
		i++;
		}
	}
	
	i = 0;
	// SKRIVANJE SVIH OKVIRA KOJI SU 2,3,4 i 5 MEHANIZAMA I KOJI SU IP44 ili SA NATPISOM 
	while (i <= broj_okvira)
	{
		skracenica_okvira = $("#kolona_"+i).attr('data-naziv-skracenice');
		//alert(skracenica_okvira);
		if ( (skracenica_okvira == "ip44white" || skracenica_okvira == "ip44ivory" || skracenica_okvira == "livory" || skracenica_okvira == "lwhite")  && br_modula_nosac > 1)
		{
			
			$('.boje_okvira#skracenica_'+skracenica_okvira).hide();		
		}
		i++;
	}
	
});

//////////////////
//////KLIKOM NA SLIKU - KLONIRANJE DODATNIH PROIZVODA
//////////////////



$('body').on('click','.mehanizam_velicine_1_modula_dodatno', function() {
	
if (kreirano == 0)
{
	$('<div class="export">CREATE PRODUCT LIST<div>').appendTo('.desna_dugme_kreiranje');
	kreirano = 1;
}
	
id_modul = $(this).attr('data-modul');
//METOD SETOVANJA POTREBNIH INFORMACIJU O KOLICINAMA
var kolicina_proizvoda = 1;
var br_modula_nosac = id_modul;
var ime_slike = $(this).attr('title');
var ime_kataloski_broj = (ime_slike).replace('/', '_');
var pozicija = $(this).attr('data-pozicija');

globalni_id = globalni_id + 1; // ID nosaca odnosno kompleta
// BROJ PREOSTALIH MESTA NAKON DODAVANJA - GLOBALNA PROMENLJIVA - OVAJ BROJ SE OSVEŽAVA SVAKI PUT PRILIKOM MARKIRANJA KOMPLETA

// IZRACUNAVANJE CENE MEHANIZMA
var cena_nosaca = parseFloat($(this).attr('data-cena'));
cena_nosaca_ukupno = cena_nosaca * 1;
var kratak_opis = $(this).attr('data-kratak-opis');
//var kratak_opis_engleski = $(this).attr('data-opis');

// IZRACUNAVANJE UKUPNE CENE
cena_ukupno = cena_nosaca;

$('<div class="mesto_nosac" id="komplet_'+globalni_id+'" data-modul='+br_modula_nosac+' data-id='+globalni_id+' data-preostali-moduli='+br_modula_nosac+'>\
		<div class="naslov_tabele"><div class="naslov_tabele_tekst">'+globalni_id+'. kit - Equipment </div>\
			<div class="dugmad">\
				<div class="dugme_brisi" id="dugme_'+globalni_id+'" data-id = "'+globalni_id+'" title="delete kit" data-ukupan-br-dodatih = "0"><a> x </a></div>\
				<div class="dugme_minimiziraj" id="dugme_minimiz_'+globalni_id+'" id="dugme_minimiz_'+globalni_id+'"  data-id = "'+globalni_id+'" title="minimize kit" data-klik="0"><a> - </a></div>\
			</div>\
		</div>\
	<div class="dodatni_'+br_modula_nosac+'_modula_klon" id="dodatni_mehanizam_'+globalni_id+'" data-kolicina=1 data-id='+globalni_id+' data-modul='+br_modula_nosac+' data-preostali-moduli='+br_modula_nosac+' data-kat_br = '+ime_slike+' data-cena='+cena_nosaca+' data-kratak-opis="'+kratak_opis+'" data-br-ubacenih-mehanizama=0><img class="dodatni_'+br_modula_nosac+'_modula_klon_slika" src="wp-content/themes/momentous-lite/primera/slike/'+ime_kataloski_broj+'.png" alt = "'+kratak_opis+'" title = "'+ime_slike+'"></div>\
	<div class="kolicina_proizvoda" id="kolicina_proizvoda_'+globalni_id+'">\
			<div class="kolicina_naslov">QUANTITY</div>\
			<div class="kolicina_prikaz" id="kolicina_prikaz_'+globalni_id+'" data-id='+globalni_id+' data-prikaz-kolicina="'+kolicina_proizvoda+'">'+kolicina_proizvoda+'</div>\
			<div class="kolicina_minus" id="kolicina_minus_'+globalni_id+'" data-id='+globalni_id+' data-br-modula="'+br_modula_nosac+'">-</div>\
			<div class="kolicina_plus" id="kolicina_plus_'+globalni_id+'" data-id='+globalni_id+' data-br-modula="'+br_modula_nosac+'">+</div>\
	</div>\
		<table class="cenovnik_komplet_'+br_modula_nosac+'_modula" id="dodatni_komplet_opreme_'+globalni_id+'" data-redni-broj = 1 data-cena ='+cena_nosaca_ukupno+' data-id="'+globalni_id+'" data-cena-mehanizama-ukupno = 0 data-cena-kompleta='+cena_nosaca_ukupno+' data-cena-okvira=0>\
		<tbody>\
		<tr>\
		<th colspan="5">PRODUCTS TABLE</th>\
		</tr>\
		<tr class="tabela_red_naslovi">\
		<td class="kolona_naziv_naslov">DESCRIPTION</td><td class="kolona_kat_br_naslov">REFERENCE NUMBER</td><td class="kolona_kolicina_naslov">QUANTITY</td><td class="kolona_cena_naslov">PRICE</td><td class="kolona_cena_naslov">QUANTITY x PRICE</td>\
		</tr>\
		<tr id="cena_nosac_'+globalni_id+'">\
		<td class="kolona_naziv">'+kratak_opis+'</td><td class="kolona_kat_br">'+ime_slike+'</td><td class="kolona_kolicina" id="kolicina_nosaca_'+globalni_id+'" data-kolicina="'+kolicina_proizvoda+'">'+kolicina_proizvoda+'</td><td class="kolona_cena" id="kolona_cena_nosaca_'+globalni_id+'" data-cena="'+cena_nosaca+'">'+Number(cena_nosaca).toLocaleString('de')+' '+currency+'</td><td class="kolona_cena" id="kolona_cena_nosaca_ukupno_'+globalni_id+'" data-cena="'+cena_nosaca_ukupno+'">'+Number(cena_nosaca_ukupno).toLocaleString('de')+' '+currency+'</td>\
		</tr>\
		<tr class = "cena_ukupno" id="cena_ukupno_'+globalni_id+'"><th colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_ukupno_cena" id="kolona_ukupno_'+globalni_id+'">'+Number(cena_nosaca_ukupno).toLocaleString('de')+' '+currency+'</td></tr>\
		</tbody>\
		</table>\
		</div>\
		</div>').prependTo('.desna_mehanizmi');
		
$('.mesto_nosac').css("background","#FFFFFF");
$('#komplet_'+globalni_id+'.mesto_nosac').css("background","#cde6ff");
trenutni_id = globalni_id;   

$('#komplet_opreme_'+globalni_id).attr('data-cena', cena_ukupno); 
$('#komplet_opreme_'+globalni_id).attr('data-cena-kompleta', cena_ukupno);
$('#komplet_opreme_'+globalni_id).attr('data-cena-nosaca', cena_ukupno);
cena_prethodnog_okvira = 0;
});

//SABIRANJE I ODUZIMANJE PROIZVODA U TABELI = DUGMIĆI PLUS I MINUS 

//// DODAVANJE KOLICINE
$('body').on('click','.kolicina_plus', function(){
	
/*NESTAJANJE TABELE SA SPISKOM POIZVODA */
$('.cene_ukupno').fadeOut(300);
$('.tabela_proizvoda_potvrda').fadeOut(300);
$('.wrapper-dugmad').fadeOut(300);
	
trenutni_id = parseInt($(this).attr('data-id'));
// PRIMENA IZGLEDA POZADINE KOMPLETA
$('.mesto_nosac').css("background","#FFFFFF");
$('#komplet_'+trenutni_id).css("background","#cde6ff");

// PRIKUPLJANJE PODATAKA O NOSACU, OKVIRU I MEHANIZMIMA
var trenutni_id = $(this).attr('data-id'); 
var globalni_id_prvog_mehanizma_nosaca = $(this).attr('data-prvi-globalni');
var kolicina = parseFloat($('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina'));
var cena_nosaca = parseFloat($('#kolona_cena_nosaca_'+trenutni_id).attr('data-cena'));
if (isNaN(cena_nosaca))
	{ cena_nosaca = 0; }

// UBACIVANJE UKUPNE CENE U TABELU KAO PODATAK
var cena_okvira = parseFloat($('#okvir_'+trenutni_id).attr('data-cena-okvira'));
kolicina++;	
if (isNaN(cena_okvira))
{
	cena_okvira = 0;
}
var cena_nosaca_ukupno = parseFloat(cena_nosaca * kolicina);
var cena_okvira_ukupno = parseFloat(cena_okvira * kolicina);
cena_okvira_ukupno = cena_okvira_ukupno.toFixed(2);
//var ukupan_broj_ubacenih_mehanizama = $('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama');
var ukupan_broj_dodatih_mehanizama = $('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje');
var broj_preostalih_modula = $('#nosac_'+trenutni_id).attr('data-preostali-moduli');
var nosac_ima_modula = $('#nosac_'+trenutni_id).attr('data-modul');
var cena_mehanizma;
var cena_mehanizma_ukupno;
var cena_svih_mehanizama_ukupno = 0;

var cena_sve_ukupno;

/////////////////////////////////////////
// PETLJA ZA PRIKUPLJANJE CENE MEHANIZAMA KOD DODAVANJA KOLIČINE
/////////////////////////////////////////
var i = 1;
// PETLJA KOJA PRIKUPLJA CENE POJEDINAČNIH MEHANIZAMA, MNOŽI SA KOLIČINOM I VRAĆA IZNOS U TABELU
while (i <= ukupan_broj_dodatih_mehanizama)
{
	cena_mehanizma = parseFloat($('#cena_mehanizma_br_'+trenutni_id+i).attr('data-cena-mehanizma'));
	if (isNaN(cena_mehanizma))
	{ cena_mehanizma = 0; }
	cena_mehanizma = cena_mehanizma.toFixed(2);
	cena_mehanizma_ukupno = cena_mehanizma * kolicina;
	cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
	cena_svih_mehanizama_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_mehanizma_ukupno);
	cena_svih_mehanizama_ukupno = cena_svih_mehanizama_ukupno.toFixed(2);
	$('#kolona_kolicina_mehanizma_'+trenutni_id+i).text(kolicina);
	$('#kolona_ukupna_cena_mehanizma_'+trenutni_id+i).text(Number(cena_mehanizma_ukupno).toLocaleString('de') +' '+currency); 
	i++;
}

// PRIKUPLJANJE PODATAKA O MEHANIZMIMA
$('#kolicina_prikaz_'+trenutni_id).remove();
$('#kolicina_proizvoda_'+trenutni_id+' div:first-child').after('<div class="kolicina_prikaz" id="kolicina_prikaz_'+trenutni_id+'" data-id="'+trenutni_id+'" data-prikaz-kolicina = "'+kolicina+'">'+kolicina+'</div>');

//INKREMENETIRA SE BROJ I UKUPNA CENA NOSACA U TABELI
$('#kolicina_nosaca_'+trenutni_id).text(kolicina);
$('#kolona_cena_nosaca_ukupno_'+trenutni_id).text(Number(cena_nosaca_ukupno).toLocaleString('de') + ' '+currency);

// UBACIVANJE UKUPNE CENE U TABELU NOSACA
$('#kolona_cena_nosaca_ukupno_'+trenutni_id).attr('data-cena', cena_nosaca_ukupno);

//UBACIVANJE KOLICINE U SLIKU NOSACA
$('#nosac_'+trenutni_id).attr('data-kolicina', kolicina);
$('#dodatni_mehanizam_'+trenutni_id).attr('data-kolicina', kolicina);

//INKREMENETIRA SE BROJ I UKUPNA CENA OKVIRA U TABELI
$('#kolicina_okvira_'+trenutni_id).text(kolicina);
$('#cena_okvira_ukupno_'+trenutni_id).text(Number(cena_okvira_ukupno).toLocaleString('de') +' '+currency);

// SABIRANJE SVIH CENA U UKUPNU CENU
cena_sve_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_nosaca_ukupno) + parseFloat(cena_okvira_ukupno);
cena_sve_ukupno = cena_sve_ukupno.toFixed(2);

// INKREMENTIRA SE UKUPNA CENA U TABELI
$('#kolona_ukupno_'+trenutni_id).text(Number(cena_sve_ukupno).toLocaleString('de')+' '+currency); 
}
);

// ODUZIMANJE KLIKOM NA MINUS
$('body').on('click','.kolicina_minus', function()
{
	
/*NESTAJANJE TABELE SA SPISKOM POIZVODA */
$('.cene_ukupno').fadeOut(300);
$('.tabela_proizvoda_potvrda').fadeOut(300);
$('.wrapper-dugmad').fadeOut(300);

// PRIKUPLJANJE PODATAKA O NOSACU I MEHANIZMIMA
trenutni_id = parseInt($(this).attr('data-id'));
// MENJANJE BOJE POZADINE IZABRANOG KOMPLETA
$('.mesto_nosac').css("background","#FFFFFF");
$('#komplet_'+trenutni_id).css("background","#cde6ff");
// OSTALO
var preostali_moduli;
var cena_nosaca = parseFloat($('#kolona_cena_nosaca_'+trenutni_id).attr('data-cena'));
if (isNaN(cena_nosaca))
	{ cena_nosaca = 0; }
var cena_nosaca_ukupno;
trenutni_id = $(this).attr('data-id');
var kolicina = parseFloat($('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina'));
var cena_svih_mehanizama_ukupno = 0;
kolicina--;

// PRIKUPLJANJE PODATAKA O MEHANIZMIMA
if (kolicina>0)
{
// MENJANJE KOLICINE
$('#kolicina_prikaz_'+trenutni_id).remove();
$('#kolicina_proizvoda_'+trenutni_id+' div:first-child').after('<div class="kolicina_prikaz" id="kolicina_prikaz_'+trenutni_id+'" data-id="'+trenutni_id+'" data-prikaz-kolicina = "'+kolicina+'">'+kolicina+'</div>');

// PRIKUPLJANJE PODATAKA O OKVIRIMA I MNOZENJE SA KOLICINOM
//var cena_okvira;
var cena_okvira = parseFloat($('#okvir_'+trenutni_id).attr('data-cena-okvira'));

if (isNaN(cena_okvira))
{
	cena_okvira = 0;
}
var cena_okvira_ukupno = parseFloat(cena_okvira * kolicina);
cena_okvira_ukupno = cena_okvira_ukupno.toFixed(2);

//DEKREMENTIRA SE BROJ I UKUPNA CENA OKVIRA U TABELI
$('#kolicina_okvira_'+trenutni_id).text(kolicina);
$('#cena_okvira_ukupno_'+trenutni_id).text(Number(cena_okvira_ukupno).toLocaleString('de') + ' '+currency);

//DEKREMENETIRA SE BROJ NOSACA U TABELI
cena_nosaca_ukupno = cena_nosaca * kolicina;
$('#kolicina_nosaca_'+trenutni_id).text(kolicina);
$('#kolona_cena_nosaca_ukupno_'+trenutni_id).text(Number(cena_nosaca_ukupno).toLocaleString('de') +' '+currency);
//DEKREMENETIRA SE KOLICINA MEHANIZAMA U KOLONI TABELE
$('#kolona_kolicina_mehanizma_'+trenutni_id).text(kolicina);
//UBACIVANJE KOLICINE U SLIKU NOSACA
$('#nosac_'+trenutni_id).attr('data-kolicina', kolicina);
$('#dodatni_mehanizam_'+trenutni_id).attr('data-kolicina', kolicina);

///////////////////////////////////////
// ODUZIMANJE - PETLJA ZA PRIKUPLJANJE CENE MEHANIZAMA KOD ODUZIMANJA KOLIČINE
///////////////////////////////////////
var cena_mehanizma;
var cena_mehanizama_ukupno;
var cena_svih_mehanizama_ukupno;
var ukupan_broj_dodatih_mehanizama = $('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje');
var i = 1;

// PETLJA KOJA PRIKUPLJA CENE POJEDINAČNIH MEHANIZAMA, MNOŽI SA KOLIČINOM I VRAĆA IZNOS U TABELU
while (i <= ukupan_broj_dodatih_mehanizama)
{
	cena_mehanizma = parseFloat($('#cena_mehanizma_br_'+trenutni_id+i).attr('data-cena-mehanizma'));
	if (isNaN(cena_mehanizma))
	{
	cena_mehanizma = 0;
	}
	cena_mehanizma = cena_mehanizma.toFixed(2);
	cena_mehanizma_ukupno = cena_mehanizma * kolicina;
	cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
	cena_svih_mehanizama_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_mehanizma_ukupno);
	cena_svih_mehanizama_ukupno = cena_svih_mehanizama_ukupno.toFixed(2);
	
	$('#kolona_kolicina_mehanizma_'+trenutni_id+i).text(kolicina);
	$('#kolona_ukupna_cena_mehanizma_'+trenutni_id+i).text(Number(cena_mehanizma_ukupno).toLocaleString('de') + ' '+currency); 
	i++;
}
cena_sve_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_nosaca_ukupno) + parseFloat(cena_okvira_ukupno);
cena_sve_ukupno = cena_sve_ukupno.toFixed(2);
// INKREMENTIRA SE UKUPNA CENA U TABELI
$('#kolona_ukupno_'+trenutni_id).text(Number(cena_sve_ukupno).toLocaleString('de') + ' '+currency); 
}
}
);

//$('#komplet_opreme_'+trenutni_id+' > tbody:first-child').append('<tr id="cena_ukupno_'+trenutni_id+'"><td colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </td><td class="kolona_cena" id="kolona_ukupno_'+trenutni_id+'">'+Number(cena_ukupno).toLocaleString('de')+' '+currency+'</td></tr>');


//////////////////
// KLIKOM NA SLIKU - KLONIRANJE MEHANIZMA 2,3,4 i 7 MODULA
//////////////////
$('body').on('click','.mehanizam_velicine_1_modula', function() {


//id="okvir_slika_'+id_okvira+'" data-id = "'+id_okvira+'"

var id_okvira = $('#okvir_slika_'+trenutni_id).attr('data-id');
//alert ("id mehanizma: " + id_okvira);

var modul = $(this).attr('data-modul');
var ime_slike = $(this).attr('title');
var kolicina_kompleta = $('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina');
var preostali_moduli;
var ukupan_broj_ubacenih_mehanizama;
var ukupan_broj_dodatih_mehanizama;

var cena_nosaca = $('#komplet_opreme_'+trenutni_id).attr('data-cena');
var redni_broj_mehanizama_u_kompletu;
preostali_moduli =  parseInt($('#nosac_'+trenutni_id).attr('data-preostali-moduli'));

if (preostali_moduli == 0)
	{
		alert ("There is no free space! - Try to select or add second support!")	
	}
else
	{
		if (modul > preostali_moduli)
		{
			alert ("There is no free space! - Try to add mechanism smaller sizes (modules)!")	
		}
			else /*lokalna esle*/
		{
	
		ukupan_broj_ubacenih_mehanizama = $('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama');
		
		// dodavanje mehanizma i vracanje podatka u nosac
		ukupan_broj_dodatih_mehanizama = $('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje');
		ukupan_broj_dodatih_mehanizama++;
		$('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje', ukupan_broj_dodatih_mehanizama);
		// dodavannje ukupnog broja dodatih mehanizama da bi se kasnije iskoristilo za petlju za hide-ovanje tabele kod minimizovanja
		$('#dugme_minimiz_'+trenutni_id).attr('data-ukupan-br-dodatih', ukupan_broj_dodatih_mehanizama);
		
		var cena_nosaca_ukupno = cena_nosaca * kolicina_kompleta;	
		
		if(isNaN(ukupan_broj_ubacenih_mehanizama))
		{
			ukupan_broj_ubacenih_mehanizama = 0;
		}
		
		ukupan_broj_ubacenih_mehanizama++;
		
		
		var br_modula = $(this).attr('data-modul');
		var kat_br = $(this).attr('data-kat-br');
		kat_br = (kat_br).replace('/', '_');
		
		var cena_mehanizma = parseFloat($(this).attr('data-cena'));
		var cena_mehanizama_vec_unesenih = parseFloat($('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno'));
		
		cena_mehanizma = cena_mehanizma.toFixed(2);
		cena_mehanizma_ukupno = cena_mehanizma * kolicina_kompleta;
		cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
		
		var kratak_opis = $(this).attr('data-opis');
		//var kratak_opis_engleski = $(this).attr('data-kratak-opis-engleski');
		preostali_moduli = $('#nosac_'+trenutni_id).attr('data-preostali-moduli');
		br_modula_nosaca = $('#nosac_'+trenutni_id).attr('data-modul');
		
		// DODAVANJE SLIKE MEHANIZMA U SLIKU NOSACA
		// OVDE SAM IZMENIO DA ID UMESTO GLOBALNOG; BUDE TRENUTNI
		// A PRE SVEGA BRISANJE PA DODAVANJE OKVIRA
		
		// UZIMANJE SVIH VREDNOSTI IZ OKVIRA
			
			var okvir_src = $('#okvir_slika_'+id_okvira).attr('src');
			//alert ( " src: " + okvir_src );
			
			var okvir_class = $('#okvir_slika_'+id_okvira).attr('class');
			//alert ( " class: " + okvir_class );
			

			var okvir_data_id = $('#okvir_slika_'+id_okvira).attr('data-id');
			//alert ( " id: " + okvir_data_id );
			
			var okvir_modul = $('#okvir_slika_'+id_okvira).attr('data-modul');
			//alert ( " data modul: " + okvir_modul );
			
			var okvir_cena = $('#okvir_slika_'+id_okvira).attr('data-cena-okvira');
			//alert ( " okvir cena: " +okvir_cena );
		
		// brisanje okvira
		$('#okvir_slika_'+id_okvira+'').remove();
		//alert ( " vidi da li je obrisan ");
		
		//dodavanje mehanizma
		$('<img src = "wp-content/themes/momentous-lite/primera/slike/'+kat_br+'.png" alt="'+kratak_opis+'" class="slika_velicine_'+br_modula+'_modula_klon" id="mehanizam_'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'" data-id-mehanizma = "'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'" data-modul='+br_modula+' title='+ime_slike+' data-cena='+cena_mehanizma+' data-lokal-id="'+trenutni_id+''+ukupan_broj_ubacenih_mehanizama+'" data-trenutni='+ukupan_broj_ubacenih_mehanizama+' data-ukupan-br-ubacenih='+ukupan_broj_ubacenih_mehanizama+' data-kratak-opis="'+kratak_opis+'">').appendTo('#nosac_'+trenutni_id);
		
		// dodavanje okvira
		
		if ( okvir_src != null){
				//alert ( " okvir nije null - ubacujem okvir ");
				$('<img src = "'+okvir_src+'" class = "'+okvir_class+'" id = "okvir_slika_'+id_okvira+'" data-id = "'+okvir_data_id+'" data-modul="'+okvir_modul+'" data-cena-okvira="'+okvir_cena+'">').appendTo('#nosac_'+trenutni_id);
		}

		$('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama', ukupan_broj_ubacenih_mehanizama);
		
		//SMANJENJE BROJA PREOSTALIH MODULA
		preostali_moduli = parseInt(preostali_moduli) - parseInt(br_modula);
		preostali_moduli = parseInt(preostali_moduli);
		//UBACIVANJE BROJA PREOSTALIH MODULA U POLJE U NOSACU
		$('#nosac_'+trenutni_id).attr('data-preostali-moduli', preostali_moduli);
		preostali_moduli =  parseInt($('#nosac_'+trenutni_id).attr('data-preostali-moduli'));
		
		$('#cena_ukupno_'+trenutni_id).remove();
		
			
		// UZIMANJE INFORMACIJA O OKVIRU
		var cena_okvira = parseFloat($('#okvir_'+trenutni_id).attr('data-cena-okvira'));
			//alert ("cena okvira je: " +cena_okvira);
		if(isNaN(cena_okvira))
		{
			cena_okvira=0;
		}
		cena_okvira = cena_okvira.toFixed(2);
		
		var cena_okvira_ukupno = kolicina_kompleta * parseFloat(cena_okvira);
		cena_okvira_ukupno = cena_okvira_ukupno.toFixed(2);
		
		// ISPISIVANJE CENE MEHANIZMA U TABELI
		$('#komplet_opreme_'+trenutni_id+' > tbody:first-child').append('<tr id="vrsta_'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'">\
		<td class="kolona_naziv">'+kratak_opis+'</td>\
		<td class="kolona_kat_br_naslov">'+ime_slike+'</td>\
		<td class="kolona_kolicina" id="kolona_kolicina_mehanizma_'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'" data-kolicina = '+kolicina_kompleta+' data-lokal-id="'+trenutni_id+''+ukupan_broj_ubacenih_mehanizama+'" data-kratak-opis="'+kratak_opis+'">'+kolicina_kompleta+'</td>\
		<td class="kolona_cena" id="cena_mehanizma_br_'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'" data-cena-mehanizma = '+cena_mehanizma+'>'+Number(cena_mehanizma).toLocaleString('de')+' '+currency+'</td>\
		<td class="kolona_cena" id="kolona_ukupna_cena_mehanizma_'+trenutni_id+''+ukupan_broj_dodatih_mehanizama+'" data-ukupna-cena='+cena_mehanizma_ukupno+' data-redni-broj-mehanizma='+redni_broj_mehanizama_u_kompletu+'>'+Number(cena_mehanizma_ukupno).toLocaleString('de')+' '+currency+'</td></tr>');

		cena_mehanizama_vec_unesenih = parseFloat(cena_mehanizama_vec_unesenih) + parseFloat(cena_mehanizma_ukupno);
		$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', cena_mehanizama_vec_unesenih);
		
		// IZRACUNAVANJE UKUPNE CENE
		var cena_ukupno = parseFloat(cena_nosaca_ukupno) + parseFloat(cena_mehanizama_vec_unesenih) + parseFloat(cena_okvira_ukupno);

		if(isNaN(cena_ukupno))
		{
			alert("First, It is necessary to choose support!");
		}	
		cena_mehanizma = parseFloat(cena_mehanizma);
		cena_ukupno = parseFloat(cena_ukupno);
		cena_ukupno = cena_ukupno.toFixed(2);
		cena_mehanizma_ukupno = parseFloat(cena_mehanizma_ukupno);
		
		// UBACIVANJE UKUPNE CENE U TABELU KAO PODATAK
		$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', cena_mehanizama_vec_unesenih);
		$('#komplet_opreme_'+trenutni_id).attr('data-cena-kompleta', cena_ukupno);

		// UBACIVANJE REDA TABELE - UKUPNO
		$('#komplet_opreme_'+trenutni_id+' > tbody:first-child').append('<tr id="cena_ukupno_'+trenutni_id+'" class="cena_ukupno"><th colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_ukupno_cena" id="kolona_ukupno_'+trenutni_id+'">'+Number(cena_ukupno).toLocaleString('de')+' '+currency+'</th></tr>');

		/*zagrada od lokalne else */
		}
	}
});

//////////////////
// KLIKOM NA SLIKU - KLONIRANJE OKVIRA
//////////////////

$('body').on('click','#okvir_slika', function() {
	var broj_modula_nosaca = $('#nosac_'+trenutni_id).attr('data-modul');
	var id_okvira = $('#nosac_'+trenutni_id).attr('data-id');
	var ime_slike = $('#nosac_'+trenutni_id).attr('title');
	var skracenica = $(this).attr('data-skracenica');
	var kolicina_okvira = $('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina');
	//var kratak_opis_2 = $(this).attr('data-opis');
	
	//alert (kratak_opis_2);
	
// PREBACUJE SE UKUPAN BROJ OKVIRA ZBOG WHILE PETLJE
	var ukupan_broj_okvira = $('#skriven').attr('data-ukupan-broj-okvira');
	var niz_kat_br = [];
	var niz_modul = [];
	var niz_skracenica = [];
	var niz_cena_okvira = [];
	var niz_osnovni_opis_okvira = [];
	var cena_prethodnog_okvira = $('#'+id_okvira).attr('data-cena-okvira');

//PREBROJAVANJE I UZIMANANJE CENE MEHANIZAMA UKOLIKO POSTOJE
/////////////////////////////////////////
// ODUZIMANJE - PETLJA ZA PRIKUPLJANJE CENE MEHANIZAMA KOD ODUZIMANJA KOLIČINE
/////////////////////////////////////////
var cena_mehanizma=0;
var cena_mehanizama_ukupno;
var kolicina = kolicina_okvira;
var cena_svih_mehanizama_ukupno=0;
var ukupan_broj_ubacenih_mehanizama = $('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama');
var ukupan_broj_dodatih_mehanizama = $('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje');
var j = 1;

// PETLJA KOJA PRIKUPLJA CENE POJEDINAČNIH MEHANIZAMA, MNOŽI SA KOLIČINOM I VRAĆA IZNOS U TABELU
while (j <= ukupan_broj_dodatih_mehanizama)
{
	cena_mehanizma = parseFloat($('#cena_mehanizma_br_'+trenutni_id+j).attr('data-cena-mehanizma'));
	//alert ("cena_mehanizma 1: " +cena_mehanizma);
	if (isNaN(cena_mehanizma))
	{
	cena_mehanizma = 0;
	}
	cena_mehanizma = cena_mehanizma.toFixed(2);
	cena_mehanizma_ukupno = cena_mehanizma * kolicina;
	cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
	cena_svih_mehanizama_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_mehanizma_ukupno);
	cena_svih_mehanizama_ukupno = cena_svih_mehanizama_ukupno.toFixed(2);
	j++;
}
	
// PREBROJAVALNJE OKVIRA
var i = 0;
while (i < ukupan_broj_okvira)
{
	niz_kat_br.push($('#kolona_'+i).attr('data-kat-broj'));
	niz_modul.push($('#kolona_'+i).attr('data-broj-modula'));
	niz_skracenica.push($('#kolona_'+i).attr('data-naziv-skracenice'));
	niz_cena_okvira.push($('#kolona_'+i).attr('data-cena-okvira'));
	niz_osnovni_opis_okvira.push($('#kolona_'+i).attr('data-osnovni-opis-okvira'));

	if(niz_modul[i] == broj_modula_nosaca && niz_skracenica[i] == skracenica)
	{
			if(isNaN(cena_prethodnog_okvira))
		{
			cena_prethodnog_okvira = 0;
		}
		// POSTAVLANJE SLIKE OKVIRA
		// remove
		$('#okvir_slika_'+id_okvira+'').remove();
		$('<img src = "wp-content/themes/momentous-lite/primera/slike/okviri/'+niz_kat_br[i]+'.png" alt="shape of primera frame" class="okvir_velicine_'+niz_modul[i]+'_modula_klon" id="okvir_slika_'+id_okvira+'" data-id = "'+id_okvira+'" data-modul="'+niz_modul[i]+'" data-skracenica="'+niz_skracenica[i]+'" data-cena-okvira="'+niz_cena_okvira[i]+'">').appendTo('#nosac_'+trenutni_id);

		
		//DODAVANJE CENE U TABELU I NOVO RACUNANJE
		//PRIKUPLJANJE UKUPNE CENE I UKLANJANJE UKUPNE CENE IZ TABELE I UBACIVANJE CENE U TABELU
		var ukupna_cena_okvira = parseFloat (kolicina_okvira * niz_cena_okvira[i]);
		ukupna_cena_okvira = ukupna_cena_okvira.toFixed(2);
		var cena_nosaca = parseFloat($('#komplet_opreme_'+trenutni_id).attr('data-cena'));
		var cena_nosaca_ukupno = cena_nosaca * kolicina_okvira;
		cena_nosaca_ukupno = cena_nosaca_ukupno.toFixed(2);

		
		var cena_ukupno_nova = parseFloat(ukupna_cena_okvira) + parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_nosaca_ukupno);

		cena_ukupno_nova = parseFloat(cena_ukupno_nova);
		cena_ukupno_nova = cena_ukupno_nova.toFixed(2);
		$('#okvir_'+id_okvira).remove();
		$('#cena_ukupno_'+trenutni_id).remove();


		// UBACIVANJE UKUPNE CENE U TABELU KAO PODATAK
		$('#komplet_opreme_'+trenutni_id).attr('data-cena-kompleta', cena_ukupno_nova);
		$('#komplet_opreme_'+trenutni_id+' > tbody:first-child').append('<tr id="okvir_'+id_okvira+'" data-cena-okvira="'+niz_cena_okvira[i]+'" data-kat-br-okvira="'+niz_kat_br[i]+'" data-opis-okvira="'+niz_osnovni_opis_okvira[i]+'"><td class="kolona_naziv">'+niz_osnovni_opis_okvira[i]+'</td><td class="kolona_kat_br_naslov">'+niz_kat_br[i]+'</td><td class="kolona_kolicina" id="kolicina_okvira_'+id_okvira+'">'+kolicina_okvira+'</td><td class="kolona_cena" id="cena_okvira_'+id_okvira+'">'+Number(niz_cena_okvira[i]).toLocaleString('de')+' '+currency+'</td><td class="kolona_cena"  id="cena_okvira_ukupno_'+id_okvira+'">'+Number(ukupna_cena_okvira).toLocaleString('de')+' '+currency+'</td></tr>');
		$('#komplet_opreme_'+trenutni_id+' > tbody:first-child').append('<tr id="cena_ukupno_'+trenutni_id+'" class="cena_ukupno"><th colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_ukupno_cena" id="kolona_ukupno_'+trenutni_id+'">'+Number(cena_ukupno_nova).toLocaleString('de')+' '+currency+'</th></tr>');
		globalni_id_okvira = $('#nosac_'+trenutni_id).attr('data-id');
	
		cena_prethodnog_okvira = parseFloat(niz_cena_okvira[i]);
	}
    i++;
}

});

//////////////////
// KLIKOM NA SLIKU - BRISANJE MEHANIZMA IZ NOSAČA - 1 MODUL
//////////////////
$('body').on('click','.slika_velicine_1_modula_klon', function(){
	
	var globalni_id_kompleta = $(this).closest("div").prop("id");
	//alert ("globalni id kompleta je: " +globalni_id_kompleta);
	var id_lokal =  $(this).attr('data-lokal-id');
	//alert (id_lokal);
	//var id_vrsta = '#vrsta_'+id_lokal;
	//alert (id_vrsta);
	var br_modula = $(this).attr('data-modul');
	var ukupan_broj_ubacenih_mehanizama = $(this).attr('data-ukupan-br-ubacenih');
	trenutni_id =  $('#'+globalni_id_kompleta).attr('data-id')
	var preostali_moduli = $('#nosac_'+trenutni_id).attr('data-preostali-moduli')
	var ukupan_broj_ubacenih_mehanizama = $('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama');
	var ukupno_dodavanje_mehanizama = $('#nosac_'+trenutni_id).attr('data-ukupno-dodavanje');
	var id_mehanizma = $(this).attr('data-id-mehanizma');

	
	// BRISANJE VRSTE U TABELI KADA SE OBRISE MEHANIZAM
	i=1
	var id_vrste = 'vrsta_'+id_mehanizma;
	while (i <= ukupno_dodavanje_mehanizama)
	{
		if (id_vrste == 'vrsta_'+trenutni_id+i)
		{
			//alert ("jednako je - id vdste je: " +id_vrste);
			$('#'+id_vrste).remove();
		}
		i++;
	}
	
	ukupan_broj_ubacenih_mehanizama --;
	$('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama', ukupan_broj_ubacenih_mehanizama);

	
	//IZRACUNAVANJE CENE MEHANIZMA 
	//OVO JE KOLICINA KOMPLETA
	var kolicina = parseFloat($('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina'));
	var velicina_mehanizma_u_modulima = $(this).attr('data-modul');
	
	// UKUPAN BROJ MODULA U KOMPLETU - UNOSENJE PODATAKA O MODUL
	globalni_id_kompleta = globalni_id_kompleta.substring(6, globalni_id_kompleta.length);
	var ukupan_broj_modula = parseFloat($('#nosac_'+globalni_id_kompleta).attr('data-modul'));
	var stari_broj_modula = parseFloat($('#nosac_'+globalni_id_kompleta).attr('data-preostali-moduli'));
	var novi_broj_modula = parseFloat(stari_broj_modula + velicina_mehanizma_u_modulima);
	$('#nosac_'+globalni_id_kompleta).attr('data-preostali-moduli', novi_broj_modula);
	
	// UKUPNA CENA NOSACA 
	var cena_nosaca = $('#komplet_opreme_'+trenutni_id).attr('data-cena');
	var cena_nosaca_ukupno = cena_nosaca * kolicina;  
	
	
	// IZRACUNAVANJE UKUPNE CENE OKVIRA
	var cena_okvira = parseFloat($('#okvir_'+trenutni_id).attr('data-cena-okvira'));
	if(isNaN(cena_okvira))
	{
		cena_okvira=0;
	}
	cena_okvira = cena_okvira.toFixed(2);
	
	var cena_okvira_ukupno = kolicina * parseFloat(cena_okvira);
	cena_okvira_ukupno = cena_okvira_ukupno.toFixed(2);
	var cena_svih_mehanizama_ukupno = 0;
	
// IZRACUNAVANJE CENE MEHANIZAMA - PETLJA
	$(this).remove();
	var i = 1;
	
// PETLJA KOJA PRIKUPLJA CENE POJEDINAČNIH MEHANIZAMA, MNOŽI SA KOLIČINOM I VRAĆA IZNOS U TABELU
while (i <= ukupno_dodavanje_mehanizama)
{
	cena_mehanizma = parseFloat($('#cena_mehanizma_br_'+trenutni_id+i).attr('data-cena-mehanizma'));
	//alert ("cena_mehanizma_br"+trenutni_id+i + " je: " + cena_mehanizma);
	if (isNaN(cena_mehanizma))
	{
	cena_mehanizma = 0;
	}
	cena_mehanizma = cena_mehanizma.toFixed(2);
	cena_mehanizma_ukupno = cena_mehanizma * kolicina;
	cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
	cena_svih_mehanizama_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_mehanizma_ukupno);
	cena_svih_mehanizama_ukupno = cena_svih_mehanizama_ukupno.toFixed(2);
	
	$('#kolona_kolicina_mehanizma_'+trenutni_id+i).text(kolicina);
	$('#kolona_ukupna_cena_mehanizma_'+trenutni_id+i).text(Number(cena_mehanizma_ukupno).toLocaleString('de') + ' '+currency); 
	i++;
}
	
	//IZRACUNAVANJE NOVE CENE MEHANIZAMA
	var nova_cena_mehanizama_ukupno = cena_svih_mehanizama_ukupno;
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', nova_cena_mehanizama_ukupno);
	var nova_cena = parseFloat(nova_cena_mehanizama_ukupno) + parseFloat(cena_nosaca_ukupno) + parseFloat(cena_okvira_ukupno);
	
	$('#cena_ukupno_'+globalni_id_kompleta).remove();
	$('#komplet_opreme_'+globalni_id_kompleta+' > tbody:first-child').append('<tr id="cena_ukupno_'+globalni_id_kompleta+'" class="cena_ukupno"><th colspan="4" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_ukupno_cena" id="kolona_ukupno_'+trenutni_id+'">'+Number(nova_cena).toLocaleString('de')+' '+currency+'</th></tr>');

	//UBACIVANJE PODATAKA U KOMPLET PROIZVODA
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', nova_cena_mehanizama_ukupno);
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-kompleta', nova_cena);
	$('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama', ukupan_broj_ubacenih_mehanizama);
	
	preostali_moduli = parseInt(preostali_moduli) + parseInt(br_modula);
	preostali_moduli = parseInt(preostali_moduli);
	
	$('#nosac_'+trenutni_id).attr('data-preostali-moduli', preostali_moduli);
	
}); 

//////////////////
// KLIKOM NA SLIKU - BRISANJE MEHANIZMA IZ NOSAČA - 2 MODULA
//////////////////


$('body').on('click','.slika_velicine_2_modula_klon', function(){
	
	var globalni_id_kompleta = $(this).closest("div").prop("id");
	var id_lokal =  $(this).attr('data-lokal-id');
	var br_modula = $(this).attr('data-modul');
	trenutni_id =  $('#'+globalni_id_kompleta).attr('data-id')
	var preostali_moduli = $('#nosac_'+trenutni_id).attr('data-preostali-moduli')
	var ukupan_broj_ubacenih_mehanizama = $('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama');

	$('#vrsta_'+trenutni_id+''+ukupan_broj_ubacenih_mehanizama).remove();
	ukupan_broj_ubacenih_mehanizama--;
	$('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama', ukupan_broj_ubacenih_mehanizama);
	
	
	//id="mehanizam_'+trenutni_id+''+ukupan_broj_ubacenih_mehanizama+'" data-modul='+br_modula+' title='+ime_slike+' data-cena='+cena_mehanizma+' data-lokal-id="'+trenutni_id+''+ukupan_broj_ubacenih_mehanizama+'" data-trenutni='+trenutni_id+' data-ukupan-br-ubacenih='+ukupan_broj_ubacenih_mehanizama+' data-kratak-opis="'+kratak_opis+'">').appendTo('#nosac_'+trenutni_id);
	
	i=0;
	var inkrement;
	//var inkrement_id;
	while ((i <= ukupan_broj_ubacenih_mehanizama))
	{
		if(isNaN($('#mehanizam_'+trenutni_id+i).attr('data-ukupan-br-ubacenih')))
		{
			//niz_koji_nosi_opise_mehanizama.push($('#mehanizam_'+x+y).attr('data-kratak-opis'));
			inkrement = i + 1;
			//inkrement = "mehanizam"+trenutni_id+inkrement;
			$('#mehanizam_'+trenutni_id+inkrement).attr('id',"mehanizam_"+trenutni_id+i)
			$('#mehanizam_'+trenutni_id+i).attr('data-ukupan-br-ubacenih',i)
		}
		i++;
	}
	
	
	//IZRACUNAVANJE CENE MEHANIZMA 
	//OVO JE KOLICINA KOMPLETA
	var kolicina = parseFloat($('#kolicina_prikaz_'+trenutni_id).attr('data-prikaz-kolicina'));
	var velicina_mehanizma_u_modulima = $(this).attr('data-modul');
	
	// UKUPAN BROJ MODULA U KOMPLETU - UNOSENJE PODATAKA O MODULU
	globalni_id_kompleta = globalni_id_kompleta.substring(6, globalni_id_kompleta.length);
	var ukupan_broj_modula = parseFloat($('#nosac_'+globalni_id_kompleta).attr('data-modul'));
	var stari_broj_modula = parseFloat($('#nosac_'+globalni_id_kompleta).attr('data-preostali-moduli'));
	var novi_broj_modula = parseFloat(stari_broj_modula + velicina_mehanizma_u_modulima);
	$('#nosac_'+globalni_id_kompleta).attr('data-preostali-moduli', novi_broj_modula);
	
	// UKUPNA CENA NOSACA 
	var cena_nosaca = $('#komplet_opreme_'+trenutni_id).attr('data-cena');
	var cena_nosaca_ukupno = cena_nosaca * kolicina;  
	
	
	// IZRACUNAVANJE UKUPNE CENE OKVIRA
	var cena_okvira = parseFloat($('#okvir_'+trenutni_id).attr('data-cena-okvira'));
	if(isNaN(cena_okvira))
	{
		cena_okvira=0;
	}
	cena_okvira = cena_okvira.toFixed(2);
	
	var cena_okvira_ukupno = kolicina * parseFloat(cena_okvira);
	cena_okvira_ukupno = cena_okvira_ukupno.toFixed(2);
	var cena_svih_mehanizama_ukupno = 0;
	
// IZRACUNAVANJE CENE MEHANIZAMA - PETLJA
	$(this).remove();
	var i = 1;
	
// PETLJA KOJA PRIKUPLJA CENE POJEDINAČNIH MEHANIZAMA, MNOŽI SA KOLIČINOM I VRAĆA IZNOS U TABELU
while (i <= ukupan_broj_ubacenih_mehanizama)
{
	cena_mehanizma = parseFloat($('#cena_mehanizma_br_'+trenutni_id+i).attr('data-cena-mehanizma'));
	if (isNaN(cena_mehanizma))
	{
	cena_mehanizma = 0;
	}
	cena_mehanizma = cena_mehanizma.toFixed(2);
	cena_mehanizma_ukupno = cena_mehanizma * kolicina;
	cena_mehanizma_ukupno = cena_mehanizma_ukupno.toFixed(2);
	cena_svih_mehanizama_ukupno = parseFloat(cena_svih_mehanizama_ukupno) + parseFloat(cena_mehanizma_ukupno);
	cena_svih_mehanizama_ukupno = cena_svih_mehanizama_ukupno.toFixed(2);
	
	$('#kolona_kolicina_mehanizma_'+trenutni_id+i).text(kolicina);
	$('#kolona_ukupna_cena_mehanizma_'+trenutni_id+i).text(Number(cena_mehanizma_ukupno).toLocaleString('de') + ' '+currency); 
	i++;
}
	
	//IZRACUNAVANJE NOVE CENE MEHANIZAMA
	var nova_cena_mehanizama_ukupno = cena_svih_mehanizama_ukupno;
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', nova_cena_mehanizama_ukupno);
	var nova_cena = parseFloat(nova_cena_mehanizama_ukupno) + parseFloat(cena_nosaca_ukupno) + parseFloat(cena_okvira_ukupno);
	
	$('#cena_ukupno_'+globalni_id_kompleta).remove();
	$('#komplet_opreme_'+globalni_id_kompleta+' > tbody:first-child').append('<tr id="cena_ukupno_'+globalni_id_kompleta+'"><th colspan="3" class="kolona_ukupno">TOTAL KIT PRICE: </th><th class="kolona_cena">'+Number(nova_cena).toLocaleString('de')+' '+currency+'</th></tr>');
	
	//UBACIVANJE PODATAKA U KOMPLET PROIZVODA
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-mehanizama-ukupno', nova_cena_mehanizama_ukupno);
	$('#komplet_opreme_'+trenutni_id).attr('data-cena-kompleta', nova_cena);
	$('#nosac_'+trenutni_id).attr('data-br-ubacenih-mehanizama', ukupan_broj_ubacenih_mehanizama);
	
	preostali_moduli = parseInt(preostali_moduli) + parseInt(br_modula);
	preostali_moduli = parseInt(preostali_moduli);
	
	$('#nosac_'+trenutni_id).attr('data-preostali-moduli', preostali_moduli);
	
});

//////////////////
//MARKIRANJE KOMPLETA
//////////////////
$('body').on('click','.mesto_nosac', function() {

	trenutni_id = $(this).attr('data-id');
	trenutni_id = parseInt(trenutni_id);

	// PREUZIMA SE TRENUTNI BROJ PRAZNIH MODULA
	br_modula = $(this).children("#nosac_"+trenutni_id).attr('data-modul');
	preostali_moduli = parseInt($("#nosac_"+trenutni_id).attr('data-preostali-moduli'));
	globalni_broj_modula_nosac = $(this).attr('data-modul');
	
	$('.mesto_nosac').css("background","#ececec");
    $(this).css("background","#cde6ff");

	if (isNaN(preostali_moduli)) 
	{
	preostali_moduli = 0;
	}
	$(this).attr('data-preostali-moduli', preostali_moduli); 
	
	//OVDE UZETI CENU PRETHODNOG OKVIRA - AKO JE IMA
	var id_okvira = $('#nosac_'+trenutni_id).attr('data-id');
	cena_prethodnog_okvira = parseFloat($('#okvir_slika_'+trenutni_id).attr('data-cena-okvira'));
	
	// TRENUTAK KADA SE KLIKOM NA NOSAC BRISU I POJAVLJUJU BOJE OKVIRA SA LEVE STRANE	
	var i = 0;
	var broj_okvira = $("#skriven").attr('data-ukupan-broj-okvira');
	var cena_okvira;
	var skracenica_okvira = "";
	
	$('.boje_okvira').show();
	// SKRIVANJE SVIH OKVIRA KOJI SU 4 ili 5 MEHANIZAMA I NEMAJU CENU
	if (globalni_broj_modula_nosac == 4 || globalni_broj_modula_nosac == 5)
	{
		while (i <= broj_okvira)
		{
		cena_okvira = $("#kolona_"+i).attr('data-cena-okvira');
			if (cena_okvira == 0)
			{
			skracenica_okvira = $('#kolona_'+i).attr('data-naziv-skracenice');
			$('.boje_okvira#skracenica_'+skracenica_okvira).hide();			
			}
		i++;
		}
	}
	
	i = 0;
	// SKRIVANJE SVIH OKVIRA KOJI SU 2,3,4 i 5 MEHANIZAMA I KOJI SU IP44 ili SA NATPISOM 
	while (i <= broj_okvira)
	{
		skracenica_okvira = $("#kolona_"+i).attr('data-naziv-skracenice');
		//alert(skracenica_okvira);
		if ( (skracenica_okvira == "ip44white" || skracenica_okvira == "ip44ivory" || skracenica_okvira == "livory" || skracenica_okvira == "lwhite")  && globalni_broj_modula_nosac > 1)
		{
			
			$('.boje_okvira#skracenica_'+skracenica_okvira).hide();		
		}
		i++;
	}
	
});

	
//BRISANJE KOMPLETA KLIKOM NA DUGME BRISI
$('body').on('click','.dugme_brisi', function()
{
	
/*NESTAJANJE TABELE SA SPISKOM POIZVODA */
$('.cene_ukupno').fadeOut(300);
$('.tabela_proizvoda_potvrda').fadeOut(300);
$('.wrapper-dugmad').fadeOut(300);	
	
var trenutna_promenljiva_id;
trenutna_promenljiva_id = parseInt ($(this).attr('data-id'));

/*$('#komplet_'+trenutna_promenljiva_id+'.mesto_nosac').remove();*/
$('#komplet_'+trenutna_promenljiva_id+'.mesto_nosac').fadeOut(200, function(){ $(this).remove();});
$('#komplet_'+trenutna_promenljiva_id+'.mesto_nosac_minimiz').fadeOut(200, function(){ $(this).remove();});
$('#dugme_'+trenutna_promenljiva_id+'.dugme_brisi').remove();
$('#kolicina_proizvoda_'+trenutna_promenljiva_id+'.kolicina_proizvoda').remove();
$('#kolicina_plus_'+trenutna_promenljiva_id).remove();


$('#kolicina_prikaz_'+trenutna_promenljiva_id).remove();
$('#kolicina_minus_'+trenutna_promenljiva_id).remove();
});


// MINIMIZIRANJE KOMPLETA

$('body').on('click','.dugme_minimiziraj', function()
{
var trenutna_promenljiva_id = parseInt ($(this).attr('data-id'));
var klik = parseInt ($(this).attr('data-klik'));
var ukupan_br_dodatih = parseInt ($(this).attr('data-ukupan-br-dodatih'));

	if (parseInt ($(this).attr('data-klik')) == 0)
	{ // kada se minimizuje
	$('#nosac_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).hide();}); // nestaje nosac
	$('#komplet_opreme_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).hide();}); // nestaje cela tabela - proba
	$('#dodatni_komplet_opreme_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).hide();}); // nestaje cela dodatnog proiyvoda tabela - proba
	$('#dodatni_mehanizam_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).hide();}); // nestaje dodatni mehanizam - ako postoji
	$('#komplet_'+trenutna_promenljiva_id).attr('class', 'mesto_nosac_minimiz'); // MENJANJE KLASE DA BI SE DOBILO MAKSIMALNO SPUSTENA DIMENZIJA
	$('#kolicina_proizvoda_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).hide();}); // nestaje polje za kolicine
	var i = 1;
	while (i <= ukupan_br_dodatih)
	{
		$('#vrsta_'+trenutna_promenljiva_id+i).fadeOut(200, function(){ $(this).hide();}); // nestaju vrste mehanizama
		i++;
	}
	$('#komplet_'+trenutna_promenljiva_id).css("background-color","#ececec"); // MENJANJE KLASE DA BI SE DOBILO MAKSIMALNO SPUSTENA DIMENZIJA
	$('#dodatni_komplet_opreme_'+trenutna_promenljiva_id).animate({'top': "-27px"/*moves up*/},50);
	
	
	klik = 1; // klik postaje 1 jer je kliknuto
	$('#dugme_minimiz_'+trenutna_promenljiva_id).attr('data-klik', klik); // menja vrednost 0 ili 1 - klik ili nije klik
	
	// MARKIRANJE PRVOG SLOBODNOG KOMPLETA - ODNOSNO PRVOG SLOBODNOG KOJI NIJE MINIMIZIRAN
	
	var k = globalni_id;
	var klik;
	
	while ( k > 0)
	{
		klik = $('#dugme_minimiz_'+k).attr('data-klik');
		if( klik == 0)
		{
			//BOJENJE SVIH POZADINA U BELU BOJU A SAMO ODGOVARAJUCU U PLAVU
			$('.mesto_nosac').css("background","#fff");
			$('#komplet_'+k).css("background","#cde6ff");
			trenutna_promenljiva_id = k;
			trenutni_id = trenutna_promenljiva_id;
			return false;
		}
		k--;
	}
	}
	else
	{ // kada se maksimizuje
	$('#nosac_'+trenutna_promenljiva_id).fadeIn(200, function(){ $(this).show();}); // vraca nosac
	$('#komplet_opreme_'+trenutna_promenljiva_id).fadeIn(100, function(){ $(this).show();}); // vraca se cela tabela
	$('#dodatni_komplet_opreme_'+trenutna_promenljiva_id).fadeOut(100, function(){ $(this).show();}); // vraca se cela tabela dodatnog proizvoda
	$('#dodatni_mehanizam_'+trenutna_promenljiva_id).fadeIn(100, function(){ $(this).show();}); // vraca se dodatni mehanizam - ako postoji
	$('#komplet_'+trenutna_promenljiva_id).attr('class', 'mesto_nosac'); // MENJANJE KLASE DA BI SE DOBILO MAKSIMALNO SPUSTENA DIMENZIJA
	$('#kolicina_proizvoda_'+trenutna_promenljiva_id).fadeIn(200, function(){ $(this).show();}); // vraca polje za kolicine
	
	var i = 1;
	while (i <= ukupan_br_dodatih)
		{
			$('#vrsta_'+trenutna_promenljiva_id+i).fadeIn(200, function(){ $(this).show();}); // vracaju se vrste mehanizama
			i++;
		}

	if (screen.width > 400 ) {
            $('#komplet_opreme_'+trenutna_promenljiva_id).css({'top': "16px" /*moves down*/});
			$('#dodatni_komplet_opreme_'+trenutna_promenljiva_id).css({'top': "16px"/*moves down*/},50);
    }
	
	if (screen.width < 490 ) 
	{
		$('#komplet_opreme_'+trenutna_promenljiva_id).css({'top': "125px" /*moves down*/});
		$('#dodatni_komplet_opreme_'+trenutna_promenljiva_id).css({'top': "36px"/*moves down*/},50);
	}
	
	
	klik = 0;
	$('#dugme_minimiz_'+trenutna_promenljiva_id).attr('data-klik', klik); // menja vrednost 0 ili 1 - klik ili nije klik
	
	// bojenje svih pozadina u belo osim kliknute 
	$('.mesto_nosac').css("background","#ececec");
	$('#komplet_'+trenutna_promenljiva_id).css("background","#cde6ff");
	}
	
	// dobiajnje trenutnog_id polja zbog nastavka 
	trenutni_id = trenutna_promenljiva_id;
});


////
//PROSLEDJIVANJE NA PHP
////////////////////////
// PROSLEDJIVANJE NA  PHP //
////////////////////////


$(document).ready(function(){
$('body').on('click','.export', function() {

var niz_nosaca = [];
var niz_opis_nosaca = [];
//var niz_opis_nosaca_engleski = [];
var niz_cena_nosaca = [];
var niz_modul_nosaca = [];
var niz_br_ubacenih_meh = [];

var niz_koji_nosi_mehanizme = [];
var niz_koji_nosi_opise_mehanizama = [];
var niz_koji_nosi_cene_mehanizama = [];
var niz_koji_nosi_kolicine_mehanizama = [];
var niz_koji_nosi_module_mehanizama = [];

var niz_mehanizama = [];
var niz_opis_mehanizama = [];
//var niz_opis_mehanizama_engleski = [];
var niz_cena_mehanizama = [];
var niz_kolicina_mehanizama = [];
var niz_modul_mehanizama = [];


var niz_okvira = []
var niz_opis_okvira = [];
//var niz_opis_okvira_engleski = [];
var niz_cena_okvira = [];

var niz_ostalih_proizvoda = [];
var niz_opis_ostalih_proizvoda = [];
//var niz_opis_ostalih_proizvoda_engleski = [];
var niz_kratak_opis_ostalih_proizvoda = [];
var niz_cena_ostalih_proizvoda = [];
var niz_modul_ostalih_proizvoda = [];
var niz_kolicina_ostalih_proizvoda = [];

var niz_kolicina = [];
var x = 0;
var y;

// PODESAVANJE IZGLEDA EKRANA-STRANICA

$('.proizvodi').hide();
$('.tabela_proizvoda_potvrda').hide();
$('.nova').remove();
//$('#okviri').hide();

/*BRISANJE NASLOVA*/
$('.naslov_proizvoda').text('LIST OF PRODUCTS');
$('.bticino_levi').hide();
$('<div class="nova"></div>').appendTo('.ex1');


var broj_ubacenih;
var broj_ukupno_ubacenih;
broj_ukupno_ubacenih = 0;
while (x <= globalni_id)
{	
	niz_nosaca[x] = $('#nosac_'+x).attr('data-kat_br');
	niz_opis_nosaca[x] = $('#nosac_'+x).attr('data-kratak-opis');
	//niz_opis_nosaca_engleski[x] = $('#nosac_'+x).attr('data-kratak-opis-engleski');
	
	niz_cena_nosaca[x] = $('#nosac_'+x).attr('data-cena');	
	niz_modul_nosaca[x] = $('#nosac_'+x).attr('data-modul');
	niz_br_ubacenih_meh[x] = $('#nosac_'+x).attr('data-br-ubacenih-mehanizama');
	
	niz_okvira[x] = $('#okvir_'+x).attr('data-kat-br-okvira');
	niz_cena_okvira[x] = $('#okvir_'+x).attr('data-cena-okvira');
	niz_opis_okvira[x] = $('#okvir_'+x).attr('data-opis-okvira');
	//niz_opis_okvira_engleski[x] = $('#okvir_'+x).attr('data-kratak-opis-engleski');
	
	niz_kolicina[x] = $('#nosac_'+x).attr('data-kolicina');
	
	broj_ubacenih = $('#nosac_'+x).attr('data-ukupno-dodavanje');
	if (broj_ubacenih === undefined)
	{
		broj_ubacenih = 0;
	}
	//alert ("broj ubacenih je: " +broj_ubacenih);
	//alert ("broj ubacenih pre je: " +broj_ukupno_ubacenih);
	broj_ukupno_ubacenih = parseInt(broj_ukupno_ubacenih) + parseInt(broj_ubacenih);
	y = 0;	
		while ( y <= broj_ubacenih)
		{
			niz_koji_nosi_mehanizme.push($('#mehanizam_'+x+y).attr('title'));
			niz_koji_nosi_opise_mehanizama.push($('#mehanizam_'+x+y).attr('data-kratak-opis'));
			niz_koji_nosi_cene_mehanizama.push($('#cena_mehanizma_br_'+x+y).attr('data-cena-mehanizma'));
			niz_koji_nosi_kolicine_mehanizama.push(niz_kolicina[x]);
			niz_koji_nosi_module_mehanizama.push($('#mehanizam_'+x+y).attr('data-modul'));
			y++;
			//broj_ukupno_ubacenih++;
		}
//alert ("broj ukupno ubacenih" +niz_br_ubacenih_meh[x]);	
	x++;
}

x = 0;
while (x <= globalni_id)
{	
	niz_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-kat_br');
	niz_opis_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-kratak-opis');
	//niz_opis_ostalih_proizvoda_engleski[x] = $('#dodatni_mehanizam_'+x).attr('data-kratak-opis-engleski');
	niz_kratak_opis_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-kratak-opis');
	niz_cena_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-cena');	
	niz_modul_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-modul');
	niz_kolicina_ostalih_proizvoda[x] = $('#dodatni_mehanizam_'+x).attr('data-kolicina');
	//alert(niz_kratak_opis_ostalih_proizvoda[x]);
	x++;
}

niz_koji_nosi_mehanizme = $.grep(niz_koji_nosi_mehanizme, function(value) {
return value != undefined;
});

niz_koji_nosi_opise_mehanizama = $.grep(niz_koji_nosi_opise_mehanizama, function(value) {
return value != undefined;
});

niz_koji_nosi_cene_mehanizama = $.grep(niz_koji_nosi_cene_mehanizama, function(value) {
return value != undefined;
});

niz_koji_nosi_kolicine_mehanizama = $.grep(niz_koji_nosi_kolicine_mehanizama, function(value) {
return value != undefined;
});

niz_koji_nosi_module_mehanizama = $.grep(niz_koji_nosi_module_mehanizama, function(value) {
return value != undefined;
});

//OKVIRI I NOSACI - BRISANJE PRAZNIH MESTA
niz_okvira = $.grep(niz_okvira, function(value) {
return value != undefined;
});
niz_opis_okvira = $.grep(niz_opis_okvira, function(value) {
return value != undefined;
});
/*
niz_opis_okvira_engleski = $.grep(niz_opis_okvira_engleski, function(value) {
return value != undefined;
});*/
niz_cena_okvira = $.grep(niz_cena_okvira, function(value) {
return value != undefined;
});
niz_kolicina = $.grep(niz_kolicina, function(value) {
return value != undefined;
});
niz_nosaca = $.grep(niz_nosaca, function(value) {
return value != undefined;
});
niz_opis_nosaca = $.grep(niz_opis_nosaca, function(value) {
return value != undefined;
});
/*
niz_opis_nosaca_engleski = $.grep(niz_opis_nosaca_engleski, function(value) {
return value != undefined;
});*/
niz_cena_nosaca = $.grep(niz_cena_nosaca, function(value) {
return value != undefined;
});
niz_modul_nosaca = $.grep(niz_modul_nosaca, function(value) {
return value != undefined;
});
niz_br_ubacenih_meh = $.grep(niz_br_ubacenih_meh, function(value) {
return value != undefined;
});
niz_kolicina_ostalih_proizvoda = $.grep(niz_kolicina_ostalih_proizvoda, function(value) {
return value != undefined;
});



// DODATNI PROIZVODI - BRISANJE PRAZNIH MESTA

niz_ostalih_proizvoda = $.grep(niz_ostalih_proizvoda, function(value) {
return value != undefined;
});

niz_opis_ostalih_proizvoda = $.grep(niz_opis_ostalih_proizvoda, function(value) {
return value != undefined;
});

niz_kratak_opis_ostalih_proizvoda = $.grep(niz_kratak_opis_ostalih_proizvoda, function(value) {
return value != undefined;
});

/*
niz_opis_ostalih_proizvoda_engleski = $.grep(niz_opis_ostalih_proizvoda_engleski, function(value) {
return value != undefined;
});*/
niz_cena_ostalih_proizvoda = $.grep(niz_cena_ostalih_proizvoda, function(value) {
return value != undefined;
});
niz_modul_ostalih_proizvoda = $.grep(niz_modul_ostalih_proizvoda, function(value) {
return value != undefined;
});

niz_modul_ostalih_proizvoda = $.grep(niz_modul_ostalih_proizvoda, function(value) {
return value != undefined;
});


if (typeof niz_okvira == 'undefined' || niz_okvira.length < niz_nosaca.length)
{
alert('To complete product list - It is necessary to add all Cover Frames!');
//var div_id_naziv = $('.proizvodi').attr('id');
$('.proizvodi#okviri').show();
$('.mesto_nosac#'+trenutni_id).css("background","#FFFFFF");
$('.naslov_proizvoda').text('FRAMES');
}
else 
{
// UBACIVANJE U BAZU PODATAKA
$.ajax({
type: "POST",
url: "wp-content/themes/momentous-lite/primera/spisak_primera_kompleta_eng.php",
data: {"niz_nosaca": niz_nosaca, "niz_opis_nosaca": niz_opis_nosaca, "niz_cena_nosaca": niz_cena_nosaca, "niz_modul_nosaca": niz_modul_nosaca, "niz_okvira": niz_okvira, "niz_cena_okvira": niz_cena_okvira, "niz_opis_okvira": niz_opis_okvira, "niz_kolicina": niz_kolicina, "niz_br_ubacenih_meh":niz_br_ubacenih_meh, "niz_koji_nosi_mehanizme":niz_koji_nosi_mehanizme, "niz_koji_nosi_opise_mehanizama":niz_koji_nosi_opise_mehanizama, "niz_koji_nosi_cene_mehanizama":niz_koji_nosi_cene_mehanizama, "niz_koji_nosi_kolicine_mehanizama":niz_koji_nosi_kolicine_mehanizama, "niz_koji_nosi_module_mehanizama":niz_koji_nosi_module_mehanizama, "niz_ostalih_proizvoda":niz_ostalih_proizvoda, "niz_opis_ostalih_proizvoda":niz_opis_ostalih_proizvoda, "niz_kratak_opis_ostalih_proizvoda":niz_kratak_opis_ostalih_proizvoda,"niz_cena_ostalih_proizvoda":niz_cena_ostalih_proizvoda, "niz_modul_ostalih_proizvoda":niz_modul_ostalih_proizvoda, "niz_kolicina_ostalih_proizvoda":niz_kolicina_ostalih_proizvoda},
		
		success: function(d){
				$('.nova').empty();
				$('.tabela_proizvoda_potvrda').empty();
				$('.nova').html(d);
				return false;
            },
            error: function(d){
                alert("There was an error, it was: " + d);
            }
		});
} //kraj od else
		
});

         //Make sure you do this, otherwise the link will actually click through.
         return false;
});