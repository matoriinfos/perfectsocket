<!DOCTYPE html><!-- HTML 5 -->
<html <?php /* language_attributes(); */?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
</head>

<body <?php body_class();?>>
<?php // Get Theme Options from Database
	/*$theme_options = momentous_theme_options();*/
?>

<div id="wrapper" class="hfeed">
	<div id="header-wrap">
		<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');?>
		<header id="header" class="container clearfix" role="banner"></header>
<!--
		<div id="navigation-wrap">

		
			<div id="navigation" class="container clearfix">

				<?php // Display Social Icons in Navigation
				/*
				if ( isset( $theme_options['header_icons'] ) and $theme_options['header_icons'] == true ) : ?>

						<div id="navi-social-icons" class="social-icons-wrap clearfix">
							<?php momentous_display_social_icons(); ?>
						</div>

				<?php endif;*/ ?>


				<nav id="mainnav" class="clearfix" role="navigation">
					<?php // Display Main Navigation
					/*
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'container' => false,
							'menu_id' => 'mainnav-menu',
							'menu_class' => 'main-navigation-menu',
							'echo' => true,
							'fallback_cb' => 'momentous_default_menu',
							)
						);
				*/	?>
				</nav>
	
			</div>
-->
	</div>
</div>

<?php // Display Custom Header Image
	/*momentous_display_custom_header(); */?>

