$('.proizvodi').hide();
$('.proizvodi#prekidaci').show();
$('.glavni_meni#link_prekidaci_tasteri').css({ 'color': '#125185'});
$('.link_moduli#link_svi').css({'color':'#125185'});

// IZBOR JEZIKA
$('.dropdown-content a').click(function(){
   jezik = $(this).attr('id');
   //alert ("jezik je:" +jezik);
   $('.navigacija').hide();
   $('.navigacija#'+jezik).show();
   //NAVIGACIJA NOSACA
   $('.podmeni_nosaci').hide();
   $('.podmeni_nosaci#'+jezik).show();
   //NASLOVI NA KOMPLETA NA DESNOJ STRANI STRANICE
$('.naslov_kompleta').hide();
$('.naslov_kompleta#'+jezik).show();
	//LEVI PODMENI KOJI DELI PROIZVODE NA MODULE
	$('.levi_podmeni').hide();
	$('.levi_podmeni#'+jezik).show();
	//NASLOVI MEHANIZAMA
$('.naslov_proizvoda').hide();
$('.naslov_proizvoda#'+jezik).show();
//IMENA SVIH MEHANIZAMA NA IZABRANOM JEZIKU
$('.mehanizam_velicine_1_modula').hide();
$('.mehanizam_velicine_1_modula#'+jezik).show();
$('.mehanizam_velicine_2_modula').hide();
$('.mehanizam_velicine_2_modula#'+jezik).show();
$('.mehanizam_velicine_3_modula').hide();
$('.mehanizam_velicine_3_modula#'+jezik).show();
$('.mehanizam_velicine_4_modula').hide();
$('.mehanizam_velicine_4_modula#'+jezik).show();
$('.mehanizam_velicine_6_modula').hide();
$('.mehanizam_velicine_6_modula#'+jezik).show();
$('.mehanizam_velicine_7_modula').hide();
$('.mehanizam_velicine_7_modula#'+jezik).show();
$('.mehanizam_velicine_-_modula').hide();
$('.mehanizam_velicine_-_modula#'+jezik).show();
/*KOMLETI MEHANIZAMA*/
$('.kompleti_mehanizama').hide();
$('.kompleti_mehanizama#'+jezik).show();
// NAZIV KOLIČINA U KOMPLETU KOJI SE MENJA PO IZBORU JEZIKA
$('.kolicina_naslov').hide();
$('.kolicina_naslov#'+jezik).show();


/*
$('.mehanizam_velicine_-_modula_dodatno').hide();
$('.mehanizam_velicine_-_modula_dodatno#'+jezik).show();
$('.mehanizam_velicine_2_modula_dodatno').hide();
$('.mehanizam_velicine_2_modula_dodatno#'+jezik).show();
$('.mehanizam_velicine_3_modula_dodatno').hide();
$('.mehanizam_velicine_3_modula_dodatno#'+jezik).show();
$('.mehanizam_velicine_4_modula_dodatno').hide();
$('.mehanizam_velicine_4_modula_dodatno#'+jezik).show();
$('.mehanizam_velicine_6_modula_dodatno').hide();
$('.mehanizam_velicine_6_modula_dodatno#'+jezik).show();
$('.mehanizam_velicine_7_modula_dodatno').hide();
$('.mehanizam_velicine_7_modula_dodatno#'+jezik).show();

$('.mehanizam_velicine_22_modula').hide();
$('.mehanizam_velicine_22_modula#'+jezik).show();
$('.mehanizam_velicine_33_modula').hide();
$('.mehanizam_velicine_33_modula#'+jezik).show();
$('.mehanizam_velicine_333_modula').hide();
$('.mehanizam_velicine_333_modula#'+jezik).show();
*/
});

$('body').on('click','.glavni_meni#link_detektori_pokreta', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	$('.link_moduli').css({ 'color': 'white'});
	$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').show();
	$(this).css({'color': '#125185'});
});

// MENI NAVIGACIJA - VELIČINA - BOJA - PRELAZ MIŠEM I KLIK
$('body').on('click','.link_moduli', function() {
	$('.link_moduli').css({'color':'white'});
	$(this).css({ 'color': '#125185'});
});





$('body').on('click','.glavni_meni#link_prekidaci_tasteri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#prekidaci').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'});
	$(this).css({'color': '#125185'});

});

$('body').on('click','.glavni_meni#link_aksijalni', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.proizvodi#aksijalni').show(); // prikazuje sve prekidace
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'});
	$(this).css({'color': '#125185'});

});

$('body').on('click','.glavni_meni#link_dimeri', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	$('.link_moduli').css({ 'color': 'white'});
	$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#dimeri').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_detektori_pokreta', function() {
	$('.proizvodi').hide();
	$('.levi_podmeni').show();
	$('.link_moduli').css({ 'color': 'white'});
	$('.link_moduli#link_svi').css({ 'color': '#125185'});
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'});
	$('.proizvodi#detektori_pokreta').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_termostati_detektori', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#termostati_detektori').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_energetske_uticnice', function() {
		$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#energetske_uticnice').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_racunarske_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#racunarske_uticnice').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_tv_audio_hdmi_uticnice', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#tv_audio_hdmi_uticnice').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_maske', function() {
		$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#maske').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_zvona_svetiljke', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').show(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#zvona_svetiljke').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_ostala_oprema', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#ostala_oprema').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_vodootporno', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // prikazuje dodatni podmeni za module
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#vodootporno').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_nosaci_dozne', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#nosaci_dozne').show();
	$(this).css({'color': '#125185'});
});

$('body').on('click','.glavni_meni#link_okviri', function() {
	$('.proizvodi').hide(); // krije sve mehanizme
	$('.levi_podmeni').hide(); // ovde se sakriva .levi_podmeni
	$('.link_moduli').css({ 'color': 'white'}); // sve linkove u podmeniju boji u belo
	$('.link_moduli#link_svi').css({ 'color': '#125185'}); // link svi_moduli boji u plavo
	$('.vrsta_mehanizmi').show(); // prikazuje sve definisane mehanizme
	$('.glavni_meni').css({ 'color': 'white'}); // linkove  glavnog menija boji u belo
	$('.proizvodi#okviri').show();
	$(this).css({'color': '#125185'});
});


// PODMENI NAVIGACIJA (LINKOVI: MODUL1 i MODUL2)
$('body').on('click','.link_moduli#link_svi', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show();
});

$('body').on('click','.link_moduli#link_1', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').hide();
});

$('body').on('click','.link_moduli#link_2', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').hide();
});


//// ovde ide pocetak

/*

$('body').on('click','.link_moduli#link_2', function() {
	//$('.proizvodi').hide();
	//$('.proizvodi#prekidaci').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_2').show();
	$('.vrsta_mehanizmi#mehanizmi_modul_1').hide();
});



// SKRIVANJE I POJAVLJIVANJE DUGMETA - KREIRAJ SPECIFIKACIJU

$(function(){
    $("#eksport").hide();
    });

// MENI NAVIGACIJA - VELIČINA - BOJA - PRELAZ MIŠEM I KLIK
$('body').on('mouseover','.link', function() {
	$(this).css('cursor','pointer');
	//$(this).css({ 'color': 'red'});
});


$('body').on('click','.link#okviri', function() {
	$('.proizvodi').hide();
	$('.proizvodi#okviri').show();
	$('.link').css({'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
	
	
});


$('body').on('click','.link#prekidaci', function() {
	$(".proizvodi").hide();
	$('.proizvodi#prekidaci').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#mehanizmi_kontrole', function() {
	$(".proizvodi").hide();
	$('.proizvodi#mehanizmi_kontrole').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});


$('body').on('click','.link#dimeri', function() {
	$(".proizvodi").hide();
	$('.proizvodi#dimeri').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#termostati', function() {
	$(".proizvodi").hide();
	$('.proizvodi#termostati').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#uticnice', function() {
	$(".proizvodi").hide();
	$('.proizvodi#uticnice').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#druge_uticnice', function() {
	$(".proizvodi").hide();
	$('.proizvodi#druge_uticnice').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#zvona_sijalice', function() {
	$(".proizvodi").hide();
	$('.proizvodi#zvona_sijalice').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#zastita_maske', function() {
	$(".proizvodi").hide();
	$('.proizvodi#zastita_maske').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#racunarske_uticnice_1', function() {
	$(".proizvodi").hide();
	$('.proizvodi#racunarske_uticnice_1').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});

$('body').on('click','.link#detektori_pokreta', function() {
	$(".proizvodi").hide();
	$('.proizvodi#detektori_pokreta').show();
	$('.link').css({ 'color': 'black'});
	$('.link').css({ 'background-color': 'eeeade'});
	$(this).css({ 'color': 'white', 'background-color': '7041a3'});
});


// BRISANJE SLIKE NA KLIK - VAZI ZA MODUL1 1

var broj_preostalih_modula_1_2;
var modul_za_uklanjanje;


$('body').on('click','.modul_1_klon', function() {

	var ukupna_cena;
	var cena_modula;
	var klasa_nosaca_klona;
	globalni_id_nosac_33 = $(this).parent().attr('id');
	globalni_id_nosac = $(this).parent().parent().attr('id');
	nosac_33_klon_mali = $(this).parent().attr('class');
	//broj_preostalih_modula_1_2 = $(this).parent().attr('data-presotali-modul');
	
	//alert('globalni_id_nosac_33 je: '+globalni_id_nosac_33)
	//alert('nosac_33_klon_mali je: '+nosac_33_klon_mali)
	
	br_modula = $(this).parent().attr('data-modul');
	br_modula_33 = $(this).parent().parent().attr('data-modul');
	odgovarajuca_klasa = $(this).parent().attr('class');
	globalni_id_tekst_slika = $(this).attr('data-id-slike');	
	cena_modula = parseFloat($(this).attr('data-cena-modula'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
	modul_za_uklanjanje = $(this).attr('title');
	
	if (br_modula_33 == 33)
	{
	broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac_33+'.'+nosac_33_klon_mali).attr('data-preostali-modul'));
	//broj_preostalih_modula_2 = parseInt($('#'+globalni_id_nosac_33+'.nosac_33_klon_2').attr('data-preostali-modul'));
	broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_33_klon').attr('data-preostali-modul'));
	
	//alert('broj preostalih modula 1 je: '+broj_preostalih_modula_1)
	//alert('broj preostalih modula 2 je: '+broj_preostalih_modula_2)
	//alert('broj preostalih modula je: '+broj_preostalih_modula)
	
	broj_preostalih_modula_1 = parseInt(broj_preostalih_modula_1) + 1;
	broj_preostalih_modula = parseInt(broj_preostalih_modula) + 1;
		
	$('#'+globalni_id_nosac+'.nosac_33_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('#'+globalni_id_nosac_33+'.'+nosac_33_klon_mali).attr('data-preostali-modul', +broj_preostalih_modula_1);
	//$('#'+globalni_id_nosac+'.nosac_33_klon_1').attr('data-preostali-modul', broj_preostalih_modula_1);
	
	$('#'+globalni_id_tekst_slika+'.tekst').remove();
		
		ukupna_cena = ukupna_cena - cena_modula;
		ukupna_cena = ukupna_cena.toFixed(2);
		
		$('#'+globalni_id_nosac+'.ukupna_cena').remove();
		$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
		
		//BRISANJE MODULA IZ NIZ MODULA NIZA
		var duzina_niza_modula = niz_modula.length;
		
		var y = 0;
		while (y < duzina_niza_modula)
		{
			if ( modul_za_uklanjanje == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
		}
		
		redni_broj_modula_niza = redni_broj_modula_niza - 1;
		
		}
	else
		{
		broj_preostalih_modula = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul');
		broj_preostalih_modula = parseInt(broj_preostalih_modula) + 1;
		$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', broj_preostalih_modula);
		$('#'+globalni_id_tekst_slika+'.tekst').remove();
		
		ukupna_cena = ukupna_cena - cena_modula;
		ukupna_cena = ukupna_cena.toFixed(2);
		$('#'+globalni_id_nosac+'.ukupna_cena').remove();
		$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
		
		//PREBACIVANJE VREDNOSTI VALUE U NULU
		$('#'+globalni_id_nosac+'.nosac_2_klon').attr('value', 0);
		
		//$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
		
		//BRISANJE MODULA IZ NIZ MODULA NIZA
		var duzina_niza_modula = niz_modula.length;
		var y = 0;
		while (y < duzina_niza_modula)
		{
			if ( modul_za_uklanjanje == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
		}
		redni_broj_modula_niza = redni_broj_modula_niza - 1;
		}
	$(this).remove();
		//alert('niz modula je = '+niz_modula);
	
});

//BRISANJE SLIKE NA KLIK - VAZI ZA MODUL 2
$('body').on('click','.modul_2_klon', function() {
	globalni_id_nosac_33 = $(this).parent().attr('id');
	globalni_id_nosac = $(this).parent().parent().attr('id');
	br_modula = $(this).parent().attr('data-modul');
	br_modula_33 = $(this).parent().parent().attr('data-modul');
	odgovarajuca_klasa = $(this).parent().attr('class');
	globalni_id_tekst_slika = $(this).attr('data-id-slike');
	cena_modula = parseFloat($(this).attr('data-cena-modula'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
	modul_za_uklanjanje = $(this).attr('title');
	if (br_modula_33 == 33)
		{
		broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac_33+'.'+odgovarajuca_klasa).attr('data-preostali-modul'));
		broj_preostalih_modula_1 = parseInt(broj_preostalih_modula_1) + 2;
		$('#'+globalni_id_nosac_33+'.'+odgovarajuca_klasa).attr('data-preostali-modul', broj_preostalih_modula_1);
		$('#'+globalni_id_tekst_slika+'.tekst').remove();
		
		ukupna_cena = ukupna_cena - cena_modula;
		ukupna_cena = ukupna_cena.toFixed(2);
		$('#'+globalni_id_nosac+'.ukupna_cena').remove();
		$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
		//BRISANJE MODULA VELICINE 2 IZ NIZ MODULA NIZA
		var duzina_niza_modula = niz_modula.length;
		
		var y = 0;
		while (y < duzina_niza_modula)
		{
			if ( modul_za_uklanjanje == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
		}
		redni_broj_modula_niza = redni_broj_modula_niza - 1;
		}
		
		
	else
	{
	
	broj_preostalih_modula = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) + 2;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', broj_preostalih_modula);
	$('#'+globalni_id_tekst_slika+'.tekst').remove();
	
	//BRISANJE MODULA IZ NIZ MODULA NIZA
		var duzina_niza_modula = niz_modula.length;
		var y = 0;
		while (y < duzina_niza_modula)
		{
			if ( modul_za_uklanjanje == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
		}
		redni_broj_modula_niza = redni_broj_modula_niza - 1;
		//alert('niz modula je = '+niz_modula);
	}
	$(this).remove();
		
	//$('<div class="nova_klasa">Preostali moduli posle brisanja: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac');
	//$('<div class="nova_klasa2">ID nosac je: '+globalni_id_nosac+'</div>').appendTo('.mesto_nosac');
});

//PROMENA IZGLEDA KURSORA KADA SE PREĐE PREKO SLIKE - VAZI ZA MODULE I KLONOVE
$( ".modul_2").hover(function() {
  $(this).css('cursor','pointer');
});

$( ".modul_1").hover(function() {
  $(this).css('cursor','pointer');
});

$('body').on('mouseover','.modul_1_klon', function() {
	$(this).css('cursor','pointer');
});

$('body').on('mouseover','.modul_2_klon', function() {
	$(this).css('cursor','pointer');
});

//POJAVLJIVANJE OPISA KADA SE PREDJE MISEM PREKO MODULA 1
$('body').on('mouseover','.modul_1', function() {	
	var opis_slike = $(this).attr('value');
	var kataloski_broj_tekst = "kataloški broj: ";
	var kat_br = $(this).attr('id');
	var cena = $(this).attr('data-cena');
	
	$('div.novo').html("Opis proizvoda: "+opis_slike+"<br>Kataloški broj: "+kat_br+"<br>Cena: <b>"+cena+" RSD</b>");
	$('.novo').show();
});


//POJAVLJIVANJE OPISA KADA SE PREDJE MISEM PREKO MODULA 2
$('body').on('mouseover','.modul_2', function() {
	
	
	var opis_slike = $(this).attr('value');
	var kataloski_broj_tekst = "kataloški broj: ";
	var kat_br = $(this).attr('id');
	var cena = $(this).attr('data-cena');
	
	$('div.novo').html("Opis proizvoda: "+opis_slike+"<br>Kataloški broj: "+kat_br+"<br>Cena: <b>"+cena+" RSD</b>");
	$('.novo').show();
});


// NESTANAK OPISA NA MOUSEOUT ZA OBA MODULA
$('body').on('mouseout','.modul_2', function() {
	$('.novo').hide();
});

$('body').on('mouseout','.modul_1', function() {
	$('.novo').hide();
});


//POMERANJE DIV-OKVIRA U LEVO
$('body').on('mouseover','.okvir_desno', function() {
	$(this).css('cursor','pointer');
	$(this).css("background-color","#F2F2F2");
});

$('body').on('mouseout','.okvir_desno', function() {
	$(this).css('cursor','pointer');
	$(this).css("background-color","#A4A4A4");
});


//OTVARANJE DESNOG OKVIRA NA KLIK
$('body').on('click','.okvir_desno', function() {
$('.okvir_desno').animate({right: '0px',width: '130px'});
$('div.okvir_desno').attr('id', 'proba');
$(this).css("background-color","#F2F2F2");
$('.nosac').show();
});

//ZATVARANJE DESNOG OKVIRA NA TEKST ZATVORI

//(menjanje boje teksta zatvori)
$(document).on('mouseover','.close', function() {
$(this).css("color","red");
});
$(document).on('mouseout','.close', function() {
$(this).css("color","black");
});


$(document).on('click','.close', function() {
$('.okvir_desno').animate({right: '0px',width: '20px'});
$('div.okvir_desno').attr('id', 'proba2');
});



//POJAVLJIVANJE OPISA KADA SE PREDJE MISEM PREKO NOSACA 
$('body').on('mouseover','#nosac', function() {
	var ime_slike = $(this).attr('title');
	var src = $(this).attr('src');
	var opis_slike = $(this).attr('value');
	var kat_br = $(this).attr('title');
	var cena = $(this).attr('data-cena');
	var klasa = $(this).attr('class');
	
	$('div.novo').html("Opis proizvoda: "+opis_slike+"<br>Kataloški broj: "+kat_br+"<br>Cena: <b>"+cena+" RSD</b>");
	$('.novo').show();
});
$('body').on('mouseout','#nosac', function() {
	$('.novo').hide();
});

//KLIKOM NA NOSAC KREIRAJ NOSAC U DIV DESNA

$('body').on('click','#nosac', function() {

//$('#napomena').hide();
$('#eksport').show(1000);
//$.append('<br>');

var ukupna_cena = 0;
id_mesto_nosac = id_mesto_nosac + 1;
var ime_slike = $(this).attr('title');
var src = $(this).attr('src');
var klasa = $(this).attr('class');
var id = "klon_"+klasa;
var br_modula = $(this).attr('data-modul');
var br_modula_1 = $(this).attr('data-modul-1');
var br_modula_2 = $(this).attr('data-modul-2');

var cena_nosaca;
var cena_nosaca_33;

globalni_broj_modula = br_modula;
globalni_id_nosac = id_mesto_nosac;
globalni_broj_modula_1 = 3;
globalni_broj_modula_2 = 3;

if (br_modula != 33 )
{
cena_nosaca = $(this).attr('data-cena');
ukupna_cena = parseFloat(ukupna_cena + cena_nosaca);
ukupna_cena = ukupna_cena.toFixed(2);

$('<div class="mesto_nosac" id="'+globalni_id_nosac+'" data-id="'+globalni_id_nosac+'" data-modul="'+br_modula+'"><div class = "veliki_tekst">Komplet mehanizama broj - '+globalni_id_nosac+'</div><br><div class="nosac_'+br_modula+'_klon" id="'+globalni_id_nosac+'" data-skracenica = "0" data-modul="'+br_modula+'" data-preostali-modul="'+br_modula+'" data-kat_br = "'+ime_slike+'" style="background-image: url(livinglight/slike/okviri/'+ime_slike+'.png);"></div><div class="dugme_brisanje"><div class="tekst">obriši komplet<div></div></div>').appendTo('.desna');


//$('<div class = "nova klasa">Broj modula je: "'+br_modula+'" </div>').appendTo('.desna');;
$(".mesto_nosac").css("background","#FFFFFF");
$(".mesto_nosac_33").css("background","#FFFFFF");
$('#'+globalni_id_nosac+'.mesto_nosac').css("background","#C0C0C0");
broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));

//$('<div class="nova_klasa">Br preostalih 1: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac');
$('<div class="cena_modul_'+br_modula+'" id="'+globalni_id_nosac+'"><div class="tekst"><b>CENA KOMPLETA:</b><br></div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
$('<div class="tekst">Nosač: '+cena_nosaca+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	//$('<div class="tekst"><b>Ukupno: RSD</b></tekst></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
globalni_id_nosac_33_1 = 0;
globalni_id_nosac_33_2 = 0;

globalna_cena = parseFloat(ukupna_cena);



$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);

}




//KLONIRANJE NOSACA 33
else
{
	
	id_nosac_33_1 = id_nosac_33_2 + 1;
	id_nosac_33_2 = id_nosac_33_1 + 1;
	cena_nosaca_33 = $(this).attr('data-cena');
	ukupna_cena = parseFloat(ukupna_cena + cena_nosaca_33);
	ukupna_cena = ukupna_cena.toFixed(2);
	
	
	$('<div class="mesto_nosac_33" id="'+globalni_id_nosac+'" data-id="'+globalni_id_nosac+'" data-modul="'+br_modula+'"><div class = "veliki_tekst">Komplet mehanizama broj - '+globalni_id_nosac+'</div><br><div class="nosac_'+br_modula+'_klon" id="'+globalni_id_nosac+'" data-skracenica = "0" data-kat_br = "'+ime_slike+'" data-modul="'+br_modula+'" data-preostali-modul="6" data-cena="'+cena_nosaca_33+'" style="background-image: url(livinglight/slike/okviri/'+ime_slike+'.png);"><div class="nosac_33_klon_1" id="'+id_nosac_33_1+'" data-modul="3" data-preostali-modul="3"></div><div class="nosac_33_klon_2" id="'+id_nosac_33_2+'" data-modul="3" data-preostali-modul="3"></div></div><div class="dugme_brisanje"><div class="tekst">obriši komplet</div></div></div>').appendTo('.desna');
	$('<div class="cena_modul_33" id="'+globalni_id_nosac+'" data-cena-nosaca="'+cena_nosaca_33+'" data-cena-okvira="0" ><div class="tekst"><b>CENA KOMPLETA:</b></div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac_33');
	$('<div class="tekst_nosac" id="'+globalni_id_nosac+'" data-cena-nosaca="'+cena_nosaca_33+'">Nosač: '+cena_nosaca_33+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
	
	
	//$('<div class="tekst"><b>Ukupno: RSD</b></tekst></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
	
	globalni_id_nosac_33_1 = id_nosac_33_1;
	globalni_id_nosac_33_2 = id_nosac_33_2;

//$('<div class = "nova klasa">Broj modula je: "'+br_modula+'" </div>').appendTo('.desna');;
$('.mesto_nosac').css("background","#FFFFFF");
$('.mesto_nosac_33').css("background","#FFFFFF");
$('#'+globalni_id_nosac+'.mesto_nosac_33').css("background","#C0C0C0");	
//$('<div class="nova_klasa">Br preostalih 1: '+globalni_broj_modula_1+'</div>').appendTo('.mesto_nosac');
//$('<div class="nova_klasa">Br preostalih 2: '+globalni_broj_modula_2+'</div>').appendTo('.mesto_nosac');

}


//OVDE SE INKREMENTIRA NIZ ZA ID_NOSACA_33
	for (x = 1; x <= globalni_id_nosac; ++x) {
	}
	niz_globalni_id[x] = globalni_id_nosac;
	niz_nosaca[x] = ime_slike;

	//BRISANJE PRAZNIH POLJA U NIZU
	niz_globalni_id = jQuery.grep(niz_globalni_id, function(value) {
	return value != undefined;
	});
	//BRISANJE PRAZNIH POLJA U NIZU
	niz_nosaca = jQuery.grep(niz_nosaca, function(value) {
	return value != undefined;
	});
	
globalna_cena = parseFloat(ukupna_cena);


$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');

});

//SELEKTUJE POLJE ZA KOMPLET I BOJI GA U SIVU BOJU
//OVO TREBA DA DODELI VREDNOST GLOBALNOM_ID_NOSACU
//MARKIRANJE KOMPLETA

$('body').on('click','.mesto_nosac', function() {
	//var broj_preostalih_modula;
	//$(this).css('cursor','pointer');
	globalni_id_nosac = $(this).attr('data-id');
	//$('<div class="nova_klasa">Globalni id je: '+globalni_id_nosac+'</div>').appendTo('.mesto_nosac');
	br_modula = $(this).children("#"+globalni_id_nosac).attr('data-modul');
	//$('<div class="nova_klasa">Br modula je: '+br_modula+'</div>').appendTo('.mesto_nosac');
	broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));
	//$('<div class="nova_klasa">Br preostalih je: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac');
	
	//$('<div class="globalni_id">Globalni ID je: '+globalni_id_nosac+'</div>').appendTo('#'+globalni_id_nosac+'.nosac_2_klon');
	
	$('.mesto_nosac').css("background","#FFFFFF");
	$('.mesto_nosac_33').css("background","#FFFFFF");
    $(this).css("background","#C0C0C0");
	
	
	
	if (isNaN(broj_preostalih_modula)) 
	{
	broj_preostalih_modula = 0;
	}
	globalni_broj_modula = br_modula;
	});
	
	////////////////////////
	//SELEKTOVANJE KOMPLETA - BOJENJE U SIVO - NOSAC 3+3
	////////////////////////
$('body').on('click','.mesto_nosac_33', function() {
	//var broj_preostalih_modula;
	//$(this).css('cursor','pointer');
	globalni_id_nosac = $(this).attr('data-id');
	
	br_modula = $(this).children("#"+globalni_id_nosac).attr('data-modul');

	globalni_id_nosac_33_1 = $(this).children("#"+globalni_id_nosac).children(".nosac_33_klon_1").attr('id');
	globalni_id_nosac_33_2 = $(this).children("#"+globalni_id_nosac).children(".nosac_33_klon_2").attr('id');
	
	//$('<div class="nova_klasa">Globalni id je: '+globalni_id_nosac+'</div>').appendTo('.mesto_nosac');
	//$('<div class="nova_klasa">Br modula je: '+br_modula+'</div>').appendTo('.mesto_nosac');
	//broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));
	//$('<div class="nova_klasa">Br preostalih je: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac');
	//broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon_1').attr('data-preostali-modul'));
	broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));
	broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1').attr('data-preostali-modul'));
	broj_preostalih_modula_2 = parseInt($('#'+globalni_id_nosac_33_2+'.nosac_33_klon_2').attr('data-preostali-modul'));
	//$('<div class="nova_klasa">Br preostalih je: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac_33');
	
	
	//$('<div class="nova_klasa">Preostali lokalni 1: '+broj_preostalih_modula_1+'</div>').appendTo('.mesto_nosac_33');
	//$('<div class="nova_klasa">Preostali lokalni 2: '+broj_preostalih_modula_2+'</div>').appendTo('.mesto_nosac_33');
	//$('<div class="nova_klasa">Globalni id 33 1: '+globalni_id_nosac_33_1+'</div>').appendTo('.mesto_nosac_33');
	//$('<div class="nova_klasa">Globalni id 33 2: '+globalni_id_nosac_33_2+'</div>').appendTo('.mesto_nosac_33');
	$('.mesto_nosac_33').css("background","#FFFFFF");
	$('.mesto_nosac').css("background","#FFFFFF");
    $(this).css("background","#C0C0C0");

	
	if (isNaN(broj_preostalih_modula_1)) 
	{
	broj_preostalih_modula_1 = 0;
	}
	if (isNaN(broj_preostalih_modula_2)) 
	{
	broj_preostalih_modula_2 = 0;
	}
	globalni_broj_modula = br_modula;
	});

// DUGME ZA BRISANJE - MOUSE-OVER I BRISANJE NOSACA
var duzina_niza_okvira;

$('body').on('click','.dugme_brisanje', function() {
	
	
//PRIPREMA ZA BRISANJE NOSACA
//PRIPREMA ZA BRISANJE OKVIRA
globalni_id_nosac = $(this).parent('.mesto_nosac').attr('id');

var br_modula_brisanje = $(this).parent('.mesto_nosac').children('#'+globalni_id_nosac).attr('data-modul');
var skracenica_okvira_brisanje = $(this).parent('.mesto_nosac').children('.nosac_'+br_modula_brisanje+'_klon').attr('data-skracenica');
var kataloski_broj_nosaca_brisanje = $(this).parent('.mesto_nosac').children('.nosac_'+br_modula_brisanje+'_klon').attr('data-kat_br');
var kataloski_broj_okvira_brisanje = 'LNA480'+br_modula_brisanje+''+skracenica_okvira_brisanje;

//alert('globalni id nosac je: '+globalni_id_nosac);
//alert('kataloski broj nosaca za brisanje je: '+kataloski_broj_nosaca_brisanje);
//alert('skracenica okvira za brisanje je: '+skracenica_okvira_brisanje);
//alert('kataloski broj okvira je: '+kataloski_broj_okvira_brisanje);
	
//PRIPREMA ZA BRISANJE SVIH MODULA KADA SE KLIKNE NA DUGME OBRISI
var broj_obrisanih_modula = $('#'+globalni_id_nosac+'.nosac_'+br_modula_brisanje+'_klon > img').length;
//alert('Broj modula za brisanje = '+broj_obrisanih_modula);
	
var niz_modula_lokalno_brisanje = [];
var niz_modula_poredjenje_brisanje_kat_br = [];
var kataloski_broj_nosaca = 0;

	for (x = 1; x <= br_modula_brisanje; ++x)
	{
	niz_modula_lokalno_brisanje[x] = $(this).parent('.mesto_nosac').find('img.modul_1_klon').attr('data-id-slike');
	kataloski_broj_nosaca = $(this).parent('.mesto_nosac').find('img.modul_1_klon').attr('title');
	//alert ('kataloski broj modula je: ' +kataloski_broj_modula);
	niz_modula_poredjenje_brisanje_kat_br[x] = kataloski_broj_nosaca;
		
	//alert('Kat br slike je: '+kataloski_broj_nosaca);
	//alert('X je: '+x);
		
	$(this).parent('.mesto_nosac').find('img.modul_1_klon[data-id-slike="'+niz_modula_lokalno_brisanje[x]+'"]').remove();
	}
	
	
//OSTALO JE DA SADA OVDE PRONADJES I OBRISES KATALOSKE BROJEVE IZ NIZA
//PRAZNE SE PRAZNA MESTA U NIZOVIMA
	
	niz_modula_lokalno_brisanje = jQuery.grep(niz_modula_lokalno_brisanje, function(value) {
	return value != undefined;
	});
	
	niz_modula_poredjenje_brisanje_kat_br = jQuery.grep(niz_modula_poredjenje_brisanje_kat_br, function(value) {
	return value != undefined;
	});
	
	var duzina_niza_modula_poredjenje_brisanje_kat_br = niz_modula_poredjenje_brisanje_kat_br.length;
	var duzina_niza_modula = niz_modula.length;
	
	//alert('duzina_niza_modula_poredjenje_brisanje_kat_br je = '+duzina_niza_modula_poredjenje_brisanje_kat_br);
	//alert('duzina niza_modula je = '+duzina_niza_modula);
	
	var y = 0;
	var x = 0;
	while (x < duzina_niza_modula_poredjenje_brisanje_kat_br) 
	{
		
		//alert ('pre poredjenja y je: ' +y);
		//alert ('pre poredjenja x je: ' +x);
		y = 0;
		while (y < duzina_niza_modula)
		{
			if ( niz_modula_poredjenje_brisanje_kat_br[x] == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
			//alert ('Povecao se x' +x);
		}
		
		x = x + 1;
	}
	
	
	//
	// BRISANJE NOSACA IZ NIZA NOSACA I OKVIRA - VAZI ZA MODULE 2,3,4 i 7
	//
	
	var duzina_niza_nosaca = niz_nosaca.length;
	y = 0;
		while (y < duzina_niza_nosaca)
		{
			if ( kataloski_broj_nosaca_brisanje == niz_nosaca[y])
			{
			niz_nosaca.splice(y,1);
			break;
			}
			y = y + 1;
		}
		
	duzina_niza_okvira = niz_okvira.length;
	y = 0;
	while (y < duzina_niza_okvira)
	{
		if ( kataloski_broj_okvira_brisanje == niz_okvira[y])
		{
		niz_okvira.splice(y,1);
		
		//alert ('Isti kataloski brojevi: ' +kataloski_broj_okvira_brisanje);
		
		break;
		}
		y = y + 1;
		}
	
	
	
	//BROJ ZA UKLANJANJE NOSAČA IZ NIZA NOSAČA
	var ukloniti = globalni_id_nosac;
		
	//BRISE SE GLOBALNI ID IZ NIZA GLOBALNIH ID-EVA / TO JE USTVARI ID KOMPLETA
	niz_globalni_id = jQuery.grep(niz_globalni_id, function(value) {
	return value != ukloniti;
	return value != undefined;
	});
	
	
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA NOSACA
	niz_nosaca = jQuery.grep(niz_nosaca, function(value) {
	//return value != kataloski_broj_nosaca_brisanje;
	return value != undefined;
	});	
	
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA OKVIRA
	niz_okvira = jQuery.grep(niz_okvira, function(value) {
	//return value != kataloski_broj_nosaca_brisanje;
	return value != undefined;
	});	
	
	
	
	
	$(this).parent('.mesto_nosac').remove();
});

// DUGME ZA BRISANJE CELOG KOMPLETA - NOSAC 33 
$('body').on('click','.dugme_brisanje', function() {
	
globalni_id_nosac = $(this).parent('.mesto_nosac_33').attr('id');

//PRIPREMA ZA BRISANJE KOMPLETA 3+3


var br_modula_brisanje = '33';
var kataloski_broj_nosaca_brisanje = $(this).parent('.mesto_nosac_33').children('.nosac_33_klon').attr('data-kat_br');
var skracenica_okvira_brisanje = $(this).parent('.mesto_nosac_33').children('.nosac_33_klon').attr('data-skracenica');
var kataloski_broj_nosaca_brisanje = $(this).parent('.mesto_nosac_33').children('.nosac_33_klon').attr('data-kat_br');
var kataloski_broj_okvira_brisanje = 'LNA4826'+skracenica_okvira_brisanje;


//alert('globalni id nosac je: '+globalni_id_nosac);
//alert('kataloski broj nosaca za brisanje je: '+kataloski_broj_nosaca_brisanje);
//alert('skracenica okvira za brisanje je: '+skracenica_okvira_brisanje);
//alert('kataloski broj okvira je: '+kataloski_broj_okvira_brisanje);

//

//BRISANJE MODULA 1 U NOSACU 33


//$('<div class="mesto_nosac_33" id="'+globalni_id_nosac+'" data-id="'+globalni_id_nosac+'">Komplet mehanizama broj - '+globalni_id_nosac+'<br><div class="nosac_'+br_modula+'_klon" id="'+globalni_id_nosac+'" data-modul="'+br_modula+'" data-preostali-modul="'+br_modula+'" data-cena="'+cena_nosaca_33+'" style="background-image: url(slike/okviri/'+ime_slike+'.jpg);"><div class="nosac_'+br_modula+'_klon_1" id="'+id_nosac_33_1+'" data-modul="'+globalni_broj_modula_1+'" data-preostali-modul="'+globalni_broj_modula_1+'"></div><div class="nosac_'+br_modula+'_klon_2" id="'+id_nosac_33_2+'" data-modul="'+globalni_broj_modula_2+'" data-preostali-modul="'+globalni_broj_modula_2+'"></div></div><div class="dugme_brisanje"><div class="tekst">obriši komplet</div></div></div>').appendTo('.desna');

//var br_modula_brisanje = $(this).parent('.mesto_nosac').children('#'+globalni_id_nosac).attr('data-modul');

globalni_id_nosac_33_1 = $(this).parent('.mesto_nosac_33').children('#'+globalni_id_nosac).children('.nosac_33_klon_1').attr('id');
globalni_id_nosac_33_2 = $(this).parent('.mesto_nosac_33').children('#'+globalni_id_nosac).children('.nosac_33_klon_2').attr('id');
//alert ('id globalni 1 - je: ' +globalni_id_nosac_33_1);
//alert ('id globalni 2 - je: ' +globalni_id_nosac_33_2);

var br_modula_brisanje_1 = $(this).parent('.mesto_nosac_33').children('#'+globalni_id_nosac).children('.nosac_33_klon_1').attr('data-modul');
var br_modula_brisanje_2 = $(this).parent('.mesto_nosac_33').children('#'+globalni_id_nosac).children('.nosac_33_klon_2').attr('data-modul');

//"nosac_'+br_modula+'_klon_1"

//alert ('Broj modula za brisanje 1 - je: ' +br_modula_brisanje_1);
//alert ('Broj modula za brisanje 2 - je: ' +br_modula_brisanje_2);
	
//PRIPREMA ZA BRISANJE SVIH MODULA KADA SE KLIKNE NA DUGME OBRISI

//$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1');

var broj_obrisanih_modula_1 = $('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1 > img').length;
var broj_obrisanih_modula_2 = $('#'+globalni_id_nosac_33_2+'.nosac_33_klon_2 > img').length;
//alert ('Broj obrisanih modula 1 - je: ' +broj_obrisanih_modula_1);
//alert ('Broj obrisanih modula 2 - je: ' +broj_obrisanih_modula_2);

var br_modula_brisanje = broj_obrisanih_modula_1 + broj_obrisanih_modula_2;
//alert ('Broj obrisanih modula UKUPNO - je: ' +br_modula_brisanje);

//break;

var niz_modula_lokalno_brisanje = [];
var niz_modula_poredjenje_brisanje_kat_br = [];
var kataloski_broj_modula = 0;

//PRIPREMAM PETLJU ZA BRISANJE NOSACA 33

	for (x = 1; x <= br_modula_brisanje; ++x)
	{
	niz_modula_lokalno_brisanje[x] = $(this).parent('.mesto_nosac').find('img.modul_1_klon').attr('data-id-slike');
	
	//appendTo('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1');
	
		//	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1');
	//alert('+globalni_id_nosac_33_1 je: '+globalni_id_nosac_33_1);
	kataloski_broj_modula = $('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1').find('img.modul_1_klon').attr('title');
	//alert ('kataloski broj modula je: ' +kataloski_broj_modula);
	niz_modula_poredjenje_brisanje_kat_br[x] = kataloski_broj_modula;
		
	//alert('Kat br slike je: '+kataloski_broj_nosaca);
	//alert('X je: '+x);
		
	//$(this).parent('.mesto_nosac').find('img.modul_1_klon[data-id-slike="'+niz_modula_lokalno_brisanje[x]+'"]').remove();
	}
	
	
//OSTALO JE DA SADA OVDE PRONADJES I OBRISES KATALOSKE BROJEVE IZ NIZA
//PRAZNE SE PRAZNA MESTA U NIZOVIMA
	
	niz_modula_lokalno_brisanje = jQuery.grep(niz_modula_lokalno_brisanje, function(value) {
	return value != undefined;
	});
	
	niz_modula_poredjenje_brisanje_kat_br = jQuery.grep(niz_modula_poredjenje_brisanje_kat_br, function(value) {
	return value != undefined;
	});
	
	var duzina_niza_modula_poredjenje_brisanje_kat_br = niz_modula_poredjenje_brisanje_kat_br.length;
	var duzina_niza_modula = niz_modula.length;
	
	//alert('duzina_niza_modula_poredjenje_brisanje_kat_br je = '+duzina_niza_modula_poredjenje_brisanje_kat_br);
	//alert('duzina niza_modula je = '+duzina_niza_modula);
	
	var y = 0;
	var x = 0;
	while (x < duzina_niza_modula_poredjenje_brisanje_kat_br) 
	{
		
		//alert ('pre poredjenja y je: ' +y);
		//alert ('pre poredjenja x je: ' +x);
		y = 0;
		while (y < duzina_niza_modula)
		{
			if ( niz_modula_poredjenje_brisanje_kat_br[x] == niz_modula[y])
			{
			niz_modula.splice(y,1);
			break;
			}
			y = y + 1;
			//alert ('Povecao se x' +x);
		}
		
		x = x + 1;
	}
	
	
//
	// BRISANJE NOSACA I OKVIRA IZ NJIHOVIH NIZOVA - VAZI ZA MODUL 33
	//
	
	var duzina_niza_nosaca = niz_nosaca.length;
	y = 0;
		while (y < duzina_niza_nosaca)
		{
			if ( kataloski_broj_nosaca_brisanje == niz_nosaca[y])
			{
			niz_nosaca.splice(y,1);
			break;
			}
			y = y + 1;
		}
		
	duzina_niza_okvira = niz_okvira.length;
	y = 0;
	while (y < duzina_niza_okvira)
	{
		if ( kataloski_broj_okvira_brisanje == niz_okvira[y])
		{
		niz_okvira.splice(y,1);
		
		//alert ('Isti kataloski brojevi: ' +kataloski_broj_okvira_brisanje);
		
		break;
		}
		y = y + 1;
		}
	
	
	
	//BROJ ZA UKLANJANJE NOSAČA IZ NIZA NOSAČA
	var ukloniti = globalni_id_nosac;
		
	//BRISE SE GLOBALNI ID IZ NIZA GLOBALNIH ID-EVA / TO JE USTVARI ID KOMPLETA
	niz_globalni_id = jQuery.grep(niz_globalni_id, function(value) {
	return value != ukloniti;
	return value != undefined;
	});
	
	
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA NOSACA
	niz_nosaca = jQuery.grep(niz_nosaca, function(value) {
	//return value != kataloski_broj_nosaca_brisanje;
	return value != undefined;
	});	
	
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA OKVIRA
	niz_okvira = jQuery.grep(niz_okvira, function(value) {
	//return value != kataloski_broj_nosaca_brisanje;
	return value != undefined;
	});	
	
$(this).parent('.mesto_nosac_33').remove();
});

$('body').on('mouseover','.dugme_brisanje', function() {
	$(this).css('cursor','pointer');
	$(this).css("background-color","#F00000");
});

$('body').on('mouseout','.dugme_brisanje', function() {
	$(this).css('cursor','pointer');
	$(this).css("background-color","#FFFFFF");
});

//
//
//


// PRVO SE POJAVLJUJE POKAZIVAC PRELASKOM PREKO OKVIRA
$( ".boje_okvira").hover(function() {
  $(this).css('cursor','pointer');
});


$('body').on('mouseover','.dugme_brisanje_okvira', function() {
	$(this).css('cursor','pointer');
});

$('body').on('mouseover','.dugme_brisanje_okvira_33', function() {
	$(this).css('cursor','pointer');
});


//BRISANJE OKVIRA PRITISKOM NA DUGME - OBRISI OKVIR
// DUGME ZA BRISANJE - ZA OKVIRE 2-3-4-7

$('body').on('click','.dugme_brisanje_okvira', function() {
globalni_id_nosac = $(this).parent('.mesto_nosac').attr('id');
br_modula = $(this).parent('.mesto_nosac').attr('data-modul');
globalni_id_tekst_slika = $(this).parent('.mesto_nosac').children("#"+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').attr('id');


//alert('br modula je:' +br_modula);
//alert ('id tekst slika je: '+globalni_id_tekst_slika);

var ime_slike_okvira = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-kat_br');
var nova_cena_nosaca = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica');
var nova_putanja_nosaca = 'background-image: url(livinglight/slike/okviri/'+ime_slike_okvira+'.png);';

//
// SADA SE BRIŠE OKVIR IZ NIZA OKVIRA - KADA SE PRITISNE DUGME BRISI OKVIR 2,3,4 i 7 modula
//



var skracenica_okvira_brisanje_2 = $(this).parent('.mesto_nosac').children('.nosac_'+br_modula+'_klon').attr('data-skracenica');
var kataloski_broj_okvira_brisanje_2 = 'LNA480'+br_modula+''+skracenica_okvira_brisanje_2;
duzina_niza_okvira = niz_okvira.length;

//alert ('Duzina niza okvira je: '+duzina_niza_okvira);
//alert ('Kataloski broj okvira je: '+kataloski_broj_okvira_brisanje_3);

i = 0;
	while (i < duzina_niza_okvira)
		{
			if ( kataloski_broj_okvira_brisanje_2 == niz_okvira[i])
			{
				
			//alert ('isti kat brojevi');
			niz_okvira.splice(i,1);
			break;
			}
			//alert ('Niz okvira je: '+niz_okvira);
			i = i + 1;
			//alert ('Povecao se x' +x);
		}
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA OKVIRA
	niz_okvira = jQuery.grep(niz_okvira, function(value) {
	return value != undefined;
	});	


var id_tekst_okvira = $(this).parent('.mesto_nosac').children("#"+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').attr('id'); // UZIMANJE ID TEKSTA OKVIRA KOJI SE UBACUJE U RAČUNANJE
var stara_cena_okvira = $(this).parent('.mesto_nosac').children("#"+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').attr('data-cena-okvira'); // UZIMANJE ID TEKSTA OKVIRA KOJI SE UBACUJE U RAČUNANJE
var trenutna_ukupna_cena = $(this).parent('.mesto_nosac').children("#"+globalni_id_nosac+'.cena_modul_'+br_modula).children('.ukupna_cena').attr('data-ukupno');

ukupna_cena = trenutna_ukupna_cena - stara_cena_okvira;
ukupna_cena = ukupna_cena.toFixed(2);





//alert ('id tekst okvira je: '+id_tekst_okvira);
//alert ('stara cena okvira je: '+stara_cena_okvira);
//alert ('tenutna ukupna cena okvira je: '+trenutna_ukupna_cena);

//alert('broj modula je: '+br_modula);
//alert('nova cena nosaca je: '+nova_cena_nosaca);
//alert ('globalni ID nosaca je: '+globalni_id_nosac);

$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', 0);
$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena', nova_cena_nosaca);
$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_nosaca);
$('#'+globalni_id_nosac+' .dugme_brisanje_okvira').remove();

$(this).parent('.mesto_nosac').children('.cena_modul_'+br_modula).children('#'+id_tekst_okvira+'.tekst_okvir').remove();
$(this).parent('.mesto_nosac').children('.cena_modul_'+br_modula).children('.ukupna_cena').remove();

$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();

$(this).remove();




});

// DUGME ZA BRISANJE - ZA OKVIR 33

$('body').on('click','.dugme_brisanje_okvira_33', function() {
globalni_id_nosac = $(this).parent('.mesto_nosac_33').attr('id');
br_modula = 33;

//alert('globalni id je: '+globalni_id_nosac);

var ime_slike_okvira = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-kat_br');
var nova_cena_nosaca = $('#'+globalni_id_nosac+'.tekst').attr('data-cena-nosaca');
var nova_putanja_nosaca = 'background-image: url(livinglight/slike/okviri/'+ime_slike_okvira+'.png);';
//var trenutna_ukupna_cena = $('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno');

var id_tekst_okvira = $(this).parent('.mesto_nosac_33').children('.cena_modul_33').children('.tekst_okvir').attr('id'); // UZIMANJE ID TEKSTA OKVIRA KOJI SE UBACUJE U RAČUNANJE
var stara_cena_okvira = $(this).parent('.mesto_nosac_33').children('.cena_modul_33').children('.tekst_okvir').attr('data-cena-okvira'); // UZIMANJE ID TEKSTA OKVIRA KOJI SE UBACUJE U RAČUNANJE
var trenutna_ukupna_cena = $(this).parent('.mesto_nosac_33').children('.cena_modul_33').children('.ukupna_cena').attr('data-ukupno');


//
// SADA SE BRIŠE OKVIR IZ NIZA OKVIRA - KADA SE PRITISNE DUGME BRISE SE OKVIR 33
//

var skracenica_okvira_brisanje_3 = $(this).parent('.mesto_nosac_33').children('.nosac_33_klon').attr('data-skracenica');
var kataloski_broj_okvira_brisanje_3 = 'LNA4826'+skracenica_okvira_brisanje_3;
duzina_niza_okvira = niz_okvira.length;

//alert ('Duzina niza okvira je: '+duzina_niza_okvira);
//alert ('Kataloski broj okvira je: '+kataloski_broj_okvira_brisanje_3);

i = 0;
	while (i < duzina_niza_okvira)
		{
			if ( kataloski_broj_okvira_brisanje_3 == niz_okvira[i])
			{
				
			//alert ('isti kat brojevi');
			niz_okvira.splice(i,1);
			break;
			}
			//alert ('Niz okvira je: '+niz_okvira);
			i = i + 1;
			//alert ('Povecao se x' +x);
		}
	//BRISE SE MOGUCE PRAZNO MESTO IZ NIZA OKVIRA
	niz_okvira = jQuery.grep(niz_okvira, function(value) {
	return value != undefined;
	});	



//alert ('id_tekst_okvira je: '+id_tekst_okvira);
//alert ('id_tekst_okvira je: '+stara_cena_okvira);
//alert ('ukupna cena je: '+trenutna_ukupna_cena);

ukupna_cena = trenutna_ukupna_cena - stara_cena_okvira;
ukupna_cena = ukupna_cena.toFixed(2);

//alert('nova cena nosaca '+nova_cena_nosaca);
$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', 0);
$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena', nova_cena_nosaca);
$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_nosaca);

$(this).parent('.mesto_nosac_33').children('.cena_modul_33').children('#'+id_tekst_okvira+'.tekst_okvir').remove();
$(this).parent('.mesto_nosac_33').children('.cena_modul_33').children('.ukupna_cena').remove();

$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();
$(this).remove();
});

//// KLONIRANJE OKVIRA
//KLIK NA OKVIR U KLASI "LEVA" - KREIRA SLIKU OKVIRA U KLASI - "MESTO_NOSAC"
////
////

var cena_okvira;
$( '.boje_okvira').click(function() {
	br_modula = globalni_broj_modula;
	var ime_slike_okvira = $(this).attr('title');
	var src = $(this).attr('src');
	var id = $(this).attr('id');
	var nova_putanja_okvira;
	
	//var cena_okvira;
	var skracenica_boje_okvira = $(this).attr('data-skracenica');
	var skracenica_nosaca = $('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica');
	
if (skracenica_nosaca != skracenica_boje_okvira) // UKOLIKO JE ISTA SKRACENICA ZNACI DA JE ISTI OKVIR; PA SE IZBEGAVA PROCEDURA DOAVANJA ISTOG OKVIRA
{
//OVDE PREBACUJEM KATALOSKE BROJEVE I CENE SVIH OKVIRA
	var ukupno_okvira = $(this).parent().children('.tekst_boja_okvira').children('.cena_okvir').attr('data-ukupno');
	var count = 0;
	//var c = 1;
	
	var niz_cene_okvira = {};
	var niz_kataloski_okvira = {};
	var kataloski_broj_okvira;
	var cena_starog_okvira;
	var sad_cena_okivra;

if (br_modula == 33)
{
	//DOBIJANJE CENE OKVIRA I PRIKAZIVANJE - A KASNIJE IDE STAVLJANJE U NIZ
	kataloski_broj_okvira = 'LNA4826'+skracenica_boje_okvira;
	nova_putanja_okvira = 'background-image: url(livinglight/slike/okviri/LNA4826'+skracenica_boje_okvira+'.png)';
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', skracenica_boje_okvira); // SKRACENICA DOBIJA NOVU VREDNOST

	count = 0;
	while  (count <= ukupno_okvira)
	{
		niz_cene_okvira[count] = $('#'+count+'.cena_okvir').attr('cena-okvira')
		niz_kataloski_okvira[count] = $('#'+count+'.cena_okvir').attr('kat_br_okvira')
		if (kataloski_broj_okvira == niz_kataloski_okvira[count])
		{
			cena_okvira = niz_cene_okvira[count];
		}
		count++;
	}
	
	//KRЕIRANJE I DODAVANJE CENE OKVIRA U CENU KOMPLETA (OVO NIJE DODAVANJE CENE U NIZ)
	//KREIANJE I DODAVANJE CENE // NOSAC 3+3 MODULA
	var cena_prethodnog_okvira = $('.mesto_nosac_33').children('#'+globalni_id_nosac+'.cena_modul_33').children('.tekst_okvir').attr('data-cena-okvira'); // UZIMANJE CENE PRETHODNOG OKVIRA
	
	if (isNaN(cena_prethodnog_okvira))
	{ 
	cena_prethodnog_okvira = 0;
	}

	//OVO SE DODAJE JER CENA NOSACA IMA VREDNOST CENE_OKVIRA = 0 - DALJE OVO SE RADI (DA NE ZABORAVIM) JER SE KAO PREUZIMA CENA PRETHODNOG OKVIRA KOJA JE ISTO NULA JER JE AUTOMATSKI OBRISAN - OVO SE RADI KA SE MENJA OKVIR  
	cena_okvira = parseFloat(cena_okvira);	
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno') - cena_prethodnog_okvira + cena_okvira); //cena_okvira je uvek cena novoizabranog okvira
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	
	$('#'+globalni_id_nosac+'.mesto_nosac_33').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira', cena_okvira); //OVDE SE UBACUJE NOVA CENA OKVIRA POZADINE
	
	sad_cena_okivra = $('#'+globalni_id_nosac+'.mesto_nosac_33').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira');
	$('#'+globalni_id_nosac+'.cena_modul_33').children('.tekst_okvir').remove(); 
	
	//$('.tekst_okvir').remove();
	$('<div class="tekst_okvir" id="'+globalni_id_tekst_slika+'" data-cena-okvira="'+cena_okvira+'">Okvir 3+3: '+cena_okvira+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33'); //DODAJE SE CENA OKVIRA U CENOVNIK PORED
	
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	$('#'+globalni_id_nosac+'.dugme_brisanje_okvira_33').remove();
	$('<div class = "dugme_brisanje_okvira_33" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac_33');	

	}
else { // AKO OKVIRI NISU VELIČINE 3+3
	//
	// DODAVANJE OKVIRA U UKUPAN RAČUN PROIZVODA
	//
	
	var cena_prethodnog_okvira_drugih_velicina = $('.mesto_nosac').children('#'+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').attr('data-cena-okvira'); // UZIMANJE CENE PRETHODNOG OKVIRA	
	if (isNaN(cena_prethodnog_okvira_drugih_velicina))
	{ 
	cena_prethodnog_okvira_drugih_velicina = 0;
	}
	//alert ("cena okvira je:"+cena_prethodnog_okvira_drugih_velicina);

	// UBACIVANJE CENE OKVIRA VELIČNE 2 MODULA U RAČUN ZA KOMPLET
	if (br_modula == 2)
	{
		
	kataloski_broj_okvira = 'LNA4802'+skracenica_boje_okvira;
	nova_putanja_okvira = 'background-image: url(livinglight/slike/okviri/'+kataloski_broj_okvira+'.png)';
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', skracenica_boje_okvira); // SKRACENICA DOBIJA NOVU VREDNOST
	
			count = 0;
			while  (count <= ukupno_okvira)
				{
					niz_cene_okvira[count] = $('#'+count+'.cena_okvir').attr('cena-okvira')
					niz_kataloski_okvira[count] = $('#'+count+'.cena_okvir').attr('kat_br_okvira')
						if (kataloski_broj_okvira == niz_kataloski_okvira[count])
							{
								cena_okvira = niz_cene_okvira[count];
							}
						count++;
				}
		
		cena_okvira = parseFloat(cena_okvira);	
		ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno') - cena_prethodnog_okvira_drugih_velicina + cena_okvira); //cena_okvira je uvek cena novoizabranog okvira
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena = ukupna_cena;
		//alert("globalna cena je: "+globalna_cena);
		
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	sad_cena_okivra = $('#'+globalni_id_nosac+'.mesto_nosac').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira');
	$('#'+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').remove(); //UKLANJANJE PRETHODNE CENE OKVIRA
	
	//$('.tekst_okvir').remove();
	$('<div class="tekst_okvir" id="'+globalni_id_tekst_slika+'" data-cena-okvira="'+cena_okvira+'">Okvir '+br_modula+': '+cena_okvira+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula); //DODAJE SE CENA OKVIRA U CENOVNIK PORED

	
	
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	//$('#'+globalni_id_nosac+'.dugme_brisanje_okvira_'+br_modula).remove();
	//$('<div class = "dugme_brisanje_okvira_'+br_modula+'" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();
	$('<div class = "dugme_brisanje_okvira" id="'+globalni_id_nosac+'"><div class="tekst_obrisi_okvir">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	//<!-- Go to www.addthis.com/dashboard to customize your tools -->
	//$('<div class="addthis_sharing_toolbox"></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	}
	if (br_modula == 3)
	{
	kataloski_broj_okvira = 'LNA4803'+skracenica_boje_okvira;
	nova_putanja_okvira = 'background-image: url(livinglight/slike/okviri/'+kataloski_broj_okvira+'.png)';
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', skracenica_boje_okvira); // SKRACENICA DOBIJA NOVU VREDNOST
	
			count = 0;
			while  (count <= ukupno_okvira)
				{
					niz_cene_okvira[count] = $('#'+count+'.cena_okvir').attr('cena-okvira')
					niz_kataloski_okvira[count] = $('#'+count+'.cena_okvir').attr('kat_br_okvira')
						if (kataloski_broj_okvira == niz_kataloski_okvira[count])
							{
								cena_okvira = niz_cene_okvira[count];
							}
						count++;
				}
		
		cena_okvira = parseFloat(cena_okvira);	
		ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno') - cena_prethodnog_okvira_drugih_velicina + cena_okvira); //cena_okvira je uvek cena novoizabranog okvira
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena = ukupna_cena;
		//alert("globalna cena je: "+globalna_cena);
		
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	sad_cena_okivra = $('#'+globalni_id_nosac+'.mesto_nosac').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira');

	
	//$('<div class = "dugme_brisanje_okvira" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	$('#'+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').remove(); //UKLANJANJE PRETHODNE CENE OKVIRA
	
	//$('.tekst_okvir').remove();
	$('<div class="tekst_okvir" id="'+globalni_id_tekst_slika+'" data-cena-okvira="'+cena_okvira+'">Okvir '+br_modula+': '+cena_okvira+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula); //DODAJE SE CENA OKVIRA U CENOVNIK PORED
	//alert ("globalni id tekst slike je: ",+globalni_id_tekst_slika);
	
	
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	//$('#'+globalni_id_nosac+'.dugme_brisanje_okvira_'+br_modula).remove();
	//$('<div class = "dugme_brisanje_okvira_'+br_modula+'" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();
	$('<div class = "dugme_brisanje_okvira" id="'+globalni_id_nosac+'"><div class="tekst_obrisi_okvir">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	}
	if (br_modula == 4)
	{
	kataloski_broj_okvira = 'LNA4804'+skracenica_boje_okvira;
	nova_putanja_okvira = 'background-image: url(livinglight/slike/okviri/'+kataloski_broj_okvira+'.png)';
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', skracenica_boje_okvira); // SKRACENICA DOBIJA NOVU VREDNOST
	
			count = 0;
			while  (count <= ukupno_okvira)
				{
					niz_cene_okvira[count] = $('#'+count+'.cena_okvir').attr('cena-okvira')
					niz_kataloski_okvira[count] = $('#'+count+'.cena_okvir').attr('kat_br_okvira')
						if (kataloski_broj_okvira == niz_kataloski_okvira[count])
							{
								cena_okvira = niz_cene_okvira[count];
							}
						count++;
				}
		
		cena_okvira = parseFloat(cena_okvira);	
		ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno') - cena_prethodnog_okvira_drugih_velicina + cena_okvira); //cena_okvira je uvek cena novoizabranog okvira
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena = ukupna_cena;
		//alert("globalna cena je: "+globalna_cena);
		
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	sad_cena_okivra = $('#'+globalni_id_nosac+'.mesto_nosac').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira');
	$('#'+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').remove(); //UKLANJANJE PRETHODNE CENE OKVIRA
	
	//$('.tekst_okvir').remove();
	$('<div class="tekst_okvir" id="'+globalni_id_tekst_slika+'" data-cena-okvira="'+cena_okvira+'">Okvir '+br_modula+': '+cena_okvira+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula); //DODAJE SE CENA OKVIRA U CENOVNIK PORED
	//alert ("globalni id tekst slike je: ",+globalni_id_tekst_slika);
	
	
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	//$('#'+globalni_id_nosac+'.dugme_brisanje_okvira_'+br_modula).remove();
	//$('<div class = "dugme_brisanje_okvira_'+br_modula+'" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();
	$('<div class = "dugme_brisanje_okvira" id="'+globalni_id_nosac+'"><div class="tekst_obrisi_okvir">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	}
	if (br_modula == 7)
	{
	kataloski_broj_okvira = 'LNA4807'+skracenica_boje_okvira;
	nova_putanja_okvira = 'background-image: url(livinglight/slike/okviri/'+kataloski_broj_okvira+'.png)';
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-skracenica', skracenica_boje_okvira); // SKRACENICA DOBIJA NOVU VREDNOST
	
			count = 0;
			while  (count <= ukupno_okvira)
				{
					niz_cene_okvira[count] = $('#'+count+'.cena_okvir').attr('cena-okvira')
					niz_kataloski_okvira[count] = $('#'+count+'.cena_okvir').attr('kat_br_okvira')
						if (kataloski_broj_okvira == niz_kataloski_okvira[count])
							{
								cena_okvira = niz_cene_okvira[count];
							}
						count++;
				}
		
		cena_okvira = parseFloat(cena_okvira);	
		ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno') - cena_prethodnog_okvira_drugih_velicina + cena_okvira); //cena_okvira je uvek cena novoizabranog okvira
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena = ukupna_cena;
		//alert("globalna cena je: "+globalna_cena);
		
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	sad_cena_okivra = $('#'+globalni_id_nosac+'.mesto_nosac').children('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-cena-okvira');
	$('#'+globalni_id_nosac+'.cena_modul_'+br_modula).children('.tekst_okvir').remove(); //UKLANJANJE PRETHODNE CENE OKVIRA
	
	//$('.tekst_okvir').remove();
	$('<div class="tekst_okvir" id="'+globalni_id_tekst_slika+'" data-cena-okvira="'+cena_okvira+'">Okvir '+br_modula+': '+cena_okvira+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula); //DODAJE SE CENA OKVIRA U CENOVNIK PORED
	//alert ("globalni id tekst slike je: ",+globalni_id_tekst_slika);
	
	
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('style', nova_putanja_okvira); //OVDE SE UBACUJE SLIKA OKVIRA POZADINE
	//$('#'+globalni_id_nosac+'.dugme_brisanje_okvira_'+br_modula).remove();
	//$('<div class = "dugme_brisanje_okvira_'+br_modula+'" id="'+globalni_id_nosac+'"><div class="tekst">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	
	$('#'+globalni_id_nosac+'.dugme_brisanje_okvira').remove();
	$('<div class = "dugme_brisanje_okvira" id="'+globalni_id_nosac+'"><div class="tekst_obrisi_okvir">obriši okvir</div></div>').appendTo('#'+globalni_id_nosac+'.mesto_nosac');
	}

}
	// KREIRANJE I DODAVANJE OKVIRA U DODAVANJE NIZA
	//OVDE SE INKREMENTIRA NIZ ZA BILO KOJI OKVIR
	for (x = 1; x <= globalni_id_nosac; ++x) {
	}
	niz_okvira[x] = kataloski_broj_okvira;
	//BRISANJE PRAZNIH POLJA U NIZU OKVIRA
	niz_okvira = jQuery.grep(niz_okvira, function(value) {
	return value != undefined;
	});
	
	//alert(niz_okvira);

}// OVO JE OD PRVOG IF-a kada se porede skracenice
});

//KLIK NA SLIKU U KLASI "LEVA" - KREIRA SLIKU U KLASI - "MESTO_NOSAC"
// VAZI ZA MODUL 1 - DODAVANJE MODULA U NOSACE
$( '.modul_1').click(function() {
	
	
	if ($('.mesto_nosac, .mesto_nosac_33')[0]){
    // Do something if class exists
	
	
	
	var cena_modula;
	var ukupna_cena;
	br_modula = globalni_broj_modula;	
	broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));
	broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1').attr('data-preostali-modul'));
	broj_preostalih_modula_2 = parseInt($('#'+globalni_id_nosac_33_2+'.nosac_33_klon_2').attr('data-preostali-modul'));
	
	if (broj_preostalih_modula == 33)
	{
		broj_preostalih_modula = 6;
	}
	
	
	//alert ('broj preostalih je sada aaa: ' +broj_preostalih_modula);
	//$('<div class="nova_klasa">Preostali moduli pre bas pre: '+broj_preostalih_modula+'</div>').appendTo('.mesto_nosac');
	
	
	if ( broj_preostalih_modula == 0)
	{
	alert("Popunjena su sva mesta u ovom nosaču!");
	}
	else
	{
	poslednji_globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika) + 1;
	globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika);
	cena_modula = parseFloat($(this).attr('data-cena'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
	var ime_slike = $(this).attr('title');
	var src = $(this).attr('src');
	
	//PUNI SE NIZ MODULOM 1
	//////////////////////////
	//OVDE SE INKREMENTIRA NIZ ZA MODUL VELICINE 1
	redni_broj_modula_niza = redni_broj_modula_niza + 1;
	for (y = 1; y <= redni_broj_modula_niza; ++y) {
	}
	niz_modula[y] = ime_slike;
	//BRISANJE PRAZNIH POLJA U NIZU MODULA
	niz_modula = jQuery.grep(niz_modula, function(value) {
	return value != undefined;
	});
	
	//alert ('ima slike je: '+ime_slike);
		//alert ("niz modula je: "+niz_modula);
	
	//DODAVANJE SLIKE CENE I RACUNANJE CENE U MODULIMA
	if (br_modula == 2)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-ime-slike = "'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	if (br_modula == 3)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	if (br_modula == 4)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	if (br_modula == 7)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
	//alert ('Broj preostalih modula je: ' + broj_preostalih_modula);
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	
	//alert('a niz_modula je = '+niz_modula);
	//$('<div class="nova_klasa">Preostali moduli posle: '+$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul')+'</div>').appendTo('.mesto_nosac');
	
	if (br_modula == 33)
	{
	//alert ('Broj preostalih modula pre unosa je: ' + broj_preostalih_modula);
	globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika);
	cena_modula = parseFloat($(this).attr('data-cena'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
	
	
	if(broj_preostalih_modula_1 != 0)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1');

	broj_preostalih_modula_1 = parseInt(broj_preostalih_modula_1) - 1;
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
	
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1').attr('data-preostali-modul', +broj_preostalih_modula_1);
		
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
		
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);	
	
	} //drugi if
	else
	{
		cena_modula = parseFloat($(this).attr('data-cena'));
		if(broj_preostalih_modula_2 != 0)
		{
		$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_1_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_2+'.nosac_'+br_modula+'_klon_2');	
		
		broj_preostalih_modula_2 = parseInt(broj_preostalih_modula_2) - 1;
		broj_preostalih_modula = parseInt(broj_preostalih_modula) - 1;
		
		$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
		$('#'+globalni_id_nosac_33_2+'.nosac_'+br_modula+'_klon_2').attr('data-preostali-modul', +broj_preostalih_modula_2);

		$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
			
		ukupna_cena = ukupna_cena + cena_modula;
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena =  ukupna_cena;
		$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
		$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
		
		
		
		}
			//else 
			//{
				//alert("Popunjena su sva mesta u ovom nosaču!");				
			//}
	} // else - od drugog - ifa
	//} // else od if br_preostalih 2=0
		
	}//glavi if - za 33 
	//alert ('Broj preostalih modula posle unosa je: ' + broj_preostalih_modula);
	}
	
// ovde se paste-uje nosac 33 da se vrati na prethodni kod
	
	} else {
    alert('Potrebno je prvo dodati nosač mehanizama!');
	}
});


// DODAVANJE MODULA 2 U SVE NOSACE
$( '.modul_2').click(function() {
	
	
	if ($('.mesto_nosac, .mesto_nosac_33')[0]){
    // Do something if class exists

	
	
	var cena_modula;
	var ukupna_cena;
	br_modula = globalni_broj_modula;
	//br_modula_1 = globalni_broj_modula_1;
	//br_modula_2 = globalni_broj_modula_2;
	broj_preostalih_modula = parseInt($('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul'));
	broj_preostalih_modula_1 = parseInt($('#'+globalni_id_nosac_33_1+'.nosac_33_klon_1').attr('data-preostali-modul'));
	broj_preostalih_modula_2 = parseInt($('#'+globalni_id_nosac_33_2+'.nosac_33_klon_2').attr('data-preostali-modul'));
	
	poslednji_globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika) + 1;
	globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika);
	
	cena_modula = parseFloat($(this).attr('data-cena'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
	

if ( broj_preostalih_modula < 2)
	{
	alert("Popunjena su sva mesta u ovom nosaču!");
	}
	else
	{
	var ime_slike = $(this).attr('title');
	var src = $(this).attr('src');
	//var klasa = "modul_2_klon"
	//var id = "klon_2";
	
	
	//PUNI SE NIZ MODULOM 2
	//////////////////////////
	//OVDE SE INKREMENTIRA NIZ ZA MODUL VELICINE 1
	redni_broj_modula_niza = redni_broj_modula_niza + 1;
	for (y = 1; y <= redni_broj_modula_niza; ++y) {
	}
	niz_modula[y] = ime_slike;
	//BRISANJE PRAZNIH POLJA U NIZU MODULA
	niz_modula = jQuery.grep(niz_modula, function(value) {
	return value != undefined;
	});
	
	//alert ("niz modula je: "+niz_modula);
	
	if (br_modula == 2)
	{
	
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 2;
	
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	
	//FORMIRANJE CENE
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	if (br_modula == 3)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 2;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	//KREIANJE I DODAVANJE CENE
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}
	if (br_modula == 4)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 2;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	//KREIANJE I DODAVANJE CENE
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	
	}
	if (br_modula == 7)
	{
	$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon');
	broj_preostalih_modula = parseInt(broj_preostalih_modula) - 2;
	$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
	$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	//KREIANJE I DODAVANJE CENE
	ukupna_cena = ukupna_cena + cena_modula;
	ukupna_cena = ukupna_cena.toFixed(2);
	globalna_cena =  ukupna_cena;
	$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
	$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
	}

	}
	

	if (br_modula == 33)
	{
	
	//ukupna_cena = parseFloat(globalna_cena);
	//poslednji_globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika) + 1;
	globalni_id_tekst_slika = parseInt(poslednji_globalni_id_tekst_slika);
	cena_modula = parseFloat($(this).attr('data-cena'));
	ukupna_cena = parseFloat($('#'+globalni_id_nosac+'.ukupna_cena').attr('data-ukupno'));
		
		if(broj_preostalih_modula_1 > 1 && broj_preostalih_modula_1 != 0)
		{
		$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1');
		
		
		broj_preostalih_modula_1 = parseInt(broj_preostalih_modula_1) - 2;
		$('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1').attr('data-preostali-modul', +broj_preostalih_modula_1);
		$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
		
		//FORMIRANJE CENE
		ukupna_cena = ukupna_cena + cena_modula;
		ukupna_cena = ukupna_cena.toFixed(2);
		globalna_cena =  ukupna_cena;
		$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
		$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
		
		
		//$('<div class="nova_klasa">Br preostalih moudla u 33 1 je: '+$('#'+globalni_id_nosac_33_1+'.nosac_'+br_modula+'_klon_1').attr('data-preostali-modul')+'</div>').appendTo('.mesto_nosac_33');
		//$('#'+globalni_id_nosac+'.nosac_'+br_modula+'_klon').attr('data-preostali-modul', +broj_preostalih_modula);
		}
		else
		{
			if(broj_preostalih_modula_2 > 1 && broj_preostalih_modula_2 != 0)
			{
				
			var cena_modula;
			var ukupna_cena;
			ukupna_cena = parseFloat(globalna_cena);
			
			$('<img src="'+src+'" id="nosac_'+br_modula+'_modula" class="modul_2_klon" title="'+ime_slike+'" data-id-slike="'+globalni_id_tekst_slika+'" data-cena-modula = "'+cena_modula+'"/>').appendTo('#'+globalni_id_nosac_33_2+'.nosac_'+br_modula+'_klon_2');	
			broj_preostalih_modula_2 = parseInt(broj_preostalih_modula_2) - 2;
			$('#'+globalni_id_nosac_33_2+'.nosac_'+br_modula+'_klon_2').attr('data-preostali-modul', +broj_preostalih_modula_2);
			$('<div class="tekst" id="'+globalni_id_tekst_slika+'" data-cena-tekst = "'+cena_modula+'">Modul: '+cena_modula+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_'+br_modula);
			//$('<div class="nova_klasa">Br preostalih moudla u 33 2 je: '+$('#'+globalni_id_nosac_33_2+'.nosac_'+br_modula+'_klon_2').attr('data-preostali-modul')+'</div>').appendTo('.mesto_nosac_33');
			
			ukupna_cena = ukupna_cena + cena_modula;
			
			
			ukupna_cena = ukupna_cena.toFixed(2);
			globalna_cena =  ukupna_cena;
			$('#'+globalni_id_nosac+'.ukupna_cena').remove();	
			//ukupna_cena = Math.round(ukupna_cena * 1000) / 1000;
			$('<div class="ukupna_cena" id="'+globalni_id_nosac+'" data-ukupno="'+ukupna_cena+'" >UKUPNO: '+ukupna_cena+' RSD</div></div>').appendTo("#"+globalni_id_nosac+'.cena_modul_33');
			
			}
			else 
			{
				if(broj_preostalih_modula_1 == 0 && broj_preostalih_modula_2 == 0)
				{
					alert("Popunjena su sva mesta u ovom nosaču!");	
				}
				else
				{
				alert("U ovaj komplet, sada se može dodati samo 1 modul!");		
				}				
			}
			
		}
		
	}
	} else {// ZATVARA SE IF PETLJA ZA PROVERU DA LI UOPSTE IMA NEKOG NOSACA
     alert('Potrebno je prvo uneti nosač mehanizama!');
}
});

////////////////////////
////////////////////////
////////////////////////
// PROSLEDJIVANJE NA  PHP //
////////////////////////
////////////////////////



 $(document).ready(function(){
       $('#eksport').click(function(){
		
		
		
		$('.okvir_desno').hide(1000);
		
		
		var duzina_niza = niz_globalni_id.length;
		var duzina_niza_modula = niz_modula.length;
		duzina_niza_okvira = niz_okvira.length;
		
		//alert('niz modula je = '+niz_modula);
		
		var br_modula = globalni_broj_modula;
		var lokalni_id_nosaca  = $('.nosac_'+br_modula+'_klon').attr('id');
		var broj_modula_nosaca = $('.nosac_'+br_modula+'_klon').attr('data-modul');
		
        $.ajax({
		type: "POST",
		url: "livinglight/specifikacija.php",
		data: {"niz_nosaca": niz_nosaca, "niz_globalni_id": niz_globalni_id, "niz_modula": niz_modula, "duzina_niza_modula": duzina_niza_modula, "niz_okvira": niz_okvira, "duzina_niza": duzina_niza, "duzina_niza_okvira": duzina_niza_okvira},
		success: function(d){
                //alert("The Call was a success: " + d);
				$( ".desna" ).html(d);
				return false;
            },
            error: function(d){
                alert("There was an error, it was: " + d);
            }
		});
		
    });
         //Make sure you do this, otherwise the link will actually click through.
         return false;
});

*/
