<!DOCTYPE html>
<?php
//ini_set("default_charset", "UTF-8");
//header('Content-type: text/html; charset=UTF-8');
/*
Template Name: index_legrand
*/
?>
<html>
<head>
<title>PerfectSocket - Configurator for Wiring devices kits</title>
 <meta charset="UTF-8">
 <meta name="google-site-verification" content="TFo8xePqKYTQjtNC6Y54Lsx0iMLBXzcmZ3AcZgLbVZY" />
 <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
 <meta name="keywords" content="schneider, schrack, legrand, bticino, vimar, aling, nopallux, switches, sockets, dimmers, led, smart home, electrical, engineering, electrician, electricalengineering, electronics, engineer, construction, technology, electricity, lighting, electricalcontractor, electricalwork, electricianlife">
 <meta name="author" content="Sasa Todorovic">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
 
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/themes/fruity/01-home.php'); ?>
