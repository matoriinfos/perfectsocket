<?php

$unicID = uniqid();

$cena_svega_ukupno = $_POST["cena_svega_ukupno"];
$svega_ukupno_pdv = $_POST["svega_ukupno_pdv"];
$brojanje = $_POST["brojanje"];
$brojanje_ostalih = $_POST["brojanje_ostalih"];
$brojanje_mehanizama = $_POST["brojanje_mehanizama"];

$niz_nosaca = array ();
$niz_opis_nosaca = array ();
$niz_cena_nosaca = array ();
$niz_modul_nosaca = array ();
$niz_kolicina = array ();
$niz_cena_puta_kolicina_nosaca = array();

$niz_okvira = array ();
$niz_cena_okvira = array ();
$niz_opis_okvira = array ();
$niz_modul_okvira = array ();

$niz_koji_nosi_mehanizme = array ();
$niz_koji_nosi_opise_mehanizama = array ();
$niz_koji_nosi_cene_mehanizama = array ();
$niz_koji_nosi_kolicine_mehanizama = array ();
$niz_koji_nosi_module_mehanizama = array ();
$niz_koji_nosi_cene_mehanizama_ukupno = array ();

$niz_ostalih_proizvoda = array ();
$niz_opis_ostalih_proizvoda = array ();
$niz_cena_ostalih_proizvoda = array ();
$niz_modul_ostalih_proizvoda = array ();
$niz_kolicina_ostalih_proizvoda = array ();
$redni_broj_proizvoda = array();

//$brojanje = count($niz_nosaca);


$i = 0;
while ($i < $brojanje)
{		
	$niz_nosaca[$i] = $_POST["niz_nosaca$i"];
	$niz_opis_nosaca[$i] = $_POST["niz_opis_nosaca$i"];
	$niz_cena_nosaca[$i] = $_POST["niz_cena_nosaca$i"];
	$niz_modul_nosaca[$i] = $_POST["niz_modul_nosaca$i"];
	$niz_kolicina[$i] = $_POST["niz_kolicina$i"];
	$niz_cena_puta_kolicina_nosaca[$i] = $niz_cena_nosaca[$i] * $niz_kolicina[$i];
	
	$niz_okvira[$i] = $_POST["niz_okvira$i"];
	$niz_cena_okvira[$i] = $_POST["niz_cena_okvira$i"];
	$niz_opis_okvira[$i] = $_POST["niz_opis_okvira$i"];
	$niz_modul_okvira[$i] = $_POST["niz_modul_nosaca$i"];
	$niz_cena_okvira_ukupno[$i] = $niz_kolicina[$i] * $niz_cena_okvira[$i]; 
	//$redni_broj_proizvoda[$i] = $i;
	//$redni_broj++;
	$i++;
}

$i = 0;
while ($i < $brojanje_mehanizama)
{		
	$niz_koji_nosi_mehanizme[$i] = $_POST["niz_koji_nosi_mehanizme$i"];
	$niz_koji_nosi_opise_mehanizama[$i] = $_POST["niz_koji_nosi_opise_mehanizama$i"];
	$niz_koji_nosi_cene_mehanizama[$i] = $_POST["niz_koji_nosi_cene_mehanizama$i"];
	$niz_koji_nosi_kolicine_mehanizama[$i] = $_POST["niz_koji_nosi_kolicine_mehanizama$i"];
	$niz_koji_nosi_module_mehanizama[$i] = $_POST["niz_koji_nosi_module_mehanizama$i"];
	$niz_koji_nosi_cene_mehanizama_ukupno[$i] = $niz_koji_nosi_cene_mehanizama[$i] * $niz_koji_nosi_kolicine_mehanizama[$i];
	//$redni_broj++;
	$i++;
}

$i = 0;
while ($i < $brojanje_ostalih)
{		
	$niz_kolicina_ostalih_proizvoda[$i] = $_POST["niz_kolicina_ostalih_proizvoda$i"];
	$niz_ostalih_proizvoda[$i] = $_POST["niz_ostalih_proizvoda$i"];
	$niz_opis_ostalih_proizvoda[$i] = $_POST["niz_opis_ostalih_proizvoda$i"];
	$niz_cena_ostalih_proizvoda[$i] = $_POST["niz_cena_ostalih_proizvoda$i"];
	$niz_modul_ostalih_proizvoda[$i] = $_POST["niz_modul_ostalih_proizvoda$i"];
	$niz_cena_ostalih_proizvoda_ukupno[$i] = $niz_kolicina_ostalih_proizvoda[$i] * $niz_cena_ostalih_proizvoda[$i];
	//$redni_broj++;
	$i++;
}

// KOD ZA BAZU NA NETU
require_once('db_config.php');
// KOD ZA BAZU NA NETU
$servername = servername;
$username = username;
$password = password;
$baza = baza;

// POVEZIVANJE NA BAZU
$povezivanje = new PDO("mysql:host=$servername;dbname=$baza;", $username, $password);
$povezivanje->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);


// UBACIVANJE U BAZU NOSACA

$i=0;
$redni_broj=0;
while ($i < $brojanje)
{
$redni_broj++;	
try {
$sql = "INSERT INTO spisak_interio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES('$redni_broj','$niz_opis_nosaca[$i]','$niz_nosaca[$i]','$niz_cena_nosaca[$i]','$niz_kolicina[$i]','$niz_cena_puta_kolicina_nosaca[$i]','$niz_nosaca[$i]','.png','$niz_modul_nosaca[$i]','$unicID')";
$povezivanje->exec($sql);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}


// UBACIVANJE U BAZU OKVIRA
$i=0;
while ($i < $brojanje)
{
$redni_broj++;	
try {
$sql = "INSERT INTO spisak_interio (redni_broj, kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_opis_okvira[$i]','$niz_okvira[$i]','$niz_cena_okvira[$i]','$niz_kolicina[$i]','$niz_cena_okvira_ukupno[$i]','$niz_okvira[$i]', 'png', '$niz_modul_nosaca[$i]','$unicID')";
$povezivanje->exec($sql);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}

// UBACIVANJE U BAZU MEHANIZAMA
$i=0;
while ($i < $brojanje_mehanizama)
{
	$redni_broj++;	
try {
$sql_ostalih = "INSERT INTO spisak_interio (redni_broj,kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_koji_nosi_opise_mehanizama[$i]','$niz_koji_nosi_mehanizme[$i]','$niz_koji_nosi_cene_mehanizama[$i]','$niz_koji_nosi_kolicine_mehanizama[$i]','$niz_koji_nosi_cene_mehanizama_ukupno[$i]','$niz_koji_nosi_mehanizme[$i]', 'jpg', '$niz_koji_nosi_module_mehanizama[$i]','$unicID')";
$povezivanje->exec($sql_ostalih);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}

// UBACIVANJE U BAZU OSTALIH
$i=0;
while ($i < $brojanje_ostalih)
{
$redni_broj++;	
try {
$sql_ostalih = "INSERT INTO spisak_interio (redni_broj,kratak_opis, kat_br, jedinicna_cena, kolicina, ukupno, slika, tip_slike, broj_modula, unicid) VALUES ('$redni_broj','$niz_opis_ostalih_proizvoda[$i]','$niz_ostalih_proizvoda[$i]','$niz_cena_ostalih_proizvoda[$i]','$niz_kolicina_ostalih_proizvoda[$i]','$niz_cena_ostalih_proizvoda_ukupno[$i]','$niz_ostalih_proizvoda[$i]', 'png', '$niz_modul_ostalih_proizvoda[$i]','$unicID')";
$povezivanje->exec($sql_ostalih);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}
$i++;
}


//UBACIVANJE CENE U BAZU
try {
$sql = "INSERT INTO cena_ukupno (ukupno, ukupno_pdv, unicid) VALUES ('$cena_svega_ukupno','$svega_ukupno_pdv','$unicID')";
$povezivanje->exec($sql);
}
catch(PDOException $e)
{
echo $sql . "<br>" . $e->getMessage();
}


//POVEZIVANJE NA BAZU DA BI SE OCITAO SPISAK ZA UKUPAN SPISAK PROIZVODA
try {
    $conn = new PDO("mysql:host=$servername;dbname=$baza", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM spisak_interio order by redni_broj");
	$stmt2 = $conn->prepare("SELECT * FROM cena_ukupno");
	$stmt1->execute();
	$stmt2->execute();
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();
}

// KREIRANJE EXCEL FAJLA

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
							 
// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'PerfectSocket.com')
            ->setCellValue('B1', 'PRODUCT LIST')
            ->setCellValue('A2', 'No.')
			->setCellValue('B2', 'DESCRIPTION')
			->setCellValue('C2', 'REFERENCE')
			->setCellValue('D2', 'PRICE')
			->setCellValue('E2', 'QUANTITY')
			->setCellValue('F2', 'QUAN x PRICE');

	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	

// DIMENZIJE CELIJE						 
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			
		
// Add data

// PRAVI SE NIZ SVIH REDNIH BROJEVA
$i=0;
$redni_broj = 0;

/*
$kratak_opis = array ();
$kat_br = array ();
$jedinicna_cena = array ();
$kolicina = array ();
$ukupno = array ();
$novo_brojanje = 0;
$cena_ukupno = array ();
*/

while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
{	
	//$id_proizvoda[$i] = $row['id_proizvoda'];
	$kratak_opis[$i] = $row['kratak_opis'];
	$kat_br[$i] = $row['kat_br'];
	$jedinicna_cena[$i] = $row['jedinicna_cena'];
	$kolicina[$i] = $row['kolicina'];
	$ukupno[$i] = $row['ukupno'];
	$redni_broj_proizvoda[$i] = $row['redni_broj'];
	$i++;
	$novo_brojanje=$i;
}

$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$cena_ukupno[$i] = $row['ukupno'];
	$cena_ukupno_pdv[$i] = $row['ukupno_pdv'];
}	

$i=0;
foreach($redni_broj_proizvoda as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$k, $value);
		$i++;
}
$i=0;
foreach($kratak_opis as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$k, $value);
		$i++;
}
$i=0;
foreach($kat_br as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$k, $value);
		$i++;
}
$i=0;
foreach($jedinicna_cena as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$k, $value);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$k)->getNumberFormat()->setFormatCode('0.00');
		$i++;
}
$i=0;
foreach($kolicina as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$k, $value);
		$i++;
}
$i=0;
foreach($ukupno as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$k, $value);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$k)->getNumberFormat()->setFormatCode('0.00');
		$i++;
}

$pozicija_ukupno = $novo_brojanje+3;
$pozicija_ukupno_pdv = $novo_brojanje+4;
$objPHPExcel->getActiveSheet()->setCellValue('E'.$pozicija_ukupno, "TOTAL: ");
$objPHPExcel->getActiveSheet()->setCellValue('E'.$pozicija_ukupno_pdv, "TOTAL+TAX(20%): ");


$i=0;
foreach($cena_ukupno as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$pozicija_ukupno, $value);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$pozicija_ukupno)->getNumberFormat()->setFormatCode('0.00');
		$i++;
}

$i=0;
foreach($cena_ukupno_pdv as $value){
		$k=$i+3;
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$pozicija_ukupno_pdv, $value);
		$objPHPExcel->getActiveSheet()->getStyle('F'.$pozicija_ukupno_pdv)->getNumberFormat()->setFormatCode('0.00');
		$i++;
}


//$i=0;
for ($i = 3; $i <= $pozicija_ukupno_pdv; $i++) {
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C'.$i.':F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);   
} 

		
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('PerfectSocket.com');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="PerfectSocket - '.date('his-dmY').'.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
//exit;

//BRISANJE ZAPISA U TABELI SPECIFIKACIJA

try {

	$sql = "DELETE FROM spisak_interio where unicid = '$unicID'";

    // use exec() because no results are returned
    $povezivanje->exec($sql);
    }
	catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }
//BRISANJE ZAPISA U TABELI CENA_UKUPNO

try {

	$sql = "DELETE FROM cena_ukupno where unicid = '$unicID'";

    // use exec() because no results are returned
    $povezivanje->exec($sql);
    }
	catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }	
	
	$povezivanje = null;
    die;

?>
