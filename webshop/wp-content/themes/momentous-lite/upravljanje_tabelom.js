var id = $('.velikiNaslov#livinglight').attr('id');
var maksBrojProizvoda = $('.velikiNaslov#livinglight').attr('data-brojProizvoda');

/*UPRAVLJANJE KADA SE KLIKNE NA DUGME ZATVORI SVE*/
$('body').on('click','.dugmeMeni', function() {
	
	var dugmeID = $(this).attr('id');
	
	//alert(dugmeID);
	
	var i = 0;
	
	if (dugmeID == "LivingLightBticino")
	{
		while (i <= maksBrojProizvoda)
		{
			$('.prikazRezultataNaslov#livinglight'+i).fadeToggle();
			$('.prikazRezultataVrsta#livinglight'+i).fadeToggle();
			i++;
		}
	}
	
	if (dugmeID == "NiloeLegrand")
	{
		while (i <= maksBrojProizvoda)
		{
			$('.prikazRezultataNaslov#niloe'+i).fadeToggle();
			$('.prikazRezultataVrsta#niloe'+i).fadeToggle();
			i++;
		}
	}
	
		if (dugmeID == "InterioNopal")
	{
		while (i <= maksBrojProizvoda)
		{
			$('.prikazRezultataNaslov#interio'+i).fadeToggle();
			$('.prikazRezultataVrsta#interio'+i).fadeToggle();
			i++;
		}
	}
	
});

/*UPRAVLJANJE KADA SE KLIKNE NA NASLOV*/
$('body').on('click','.prikazRezultataNaslovMesec', function() {
	var id = $(this).attr('id');
	$('.prikazRezultataNaslov#'+id).fadeToggle();
	$('.prikazRezultataVrsta#'+id).fadeToggle();
	//alert("klik");
});

/*UPRAVLJANJE KADA SE KLIKNE NA DUGME ZATVORI SVE*/
$('body').on('click','#zatvoriSve', function() {
	//var id = $(this).attr('id');
	$('.prikazRezultataNaslov').fadeToggle();
	$('.prikazRezultataVrsta').fadeToggle();
});