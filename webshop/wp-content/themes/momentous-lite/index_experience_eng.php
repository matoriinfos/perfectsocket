﻿<!DOCTYPE html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-133685099-1');
</script>
<title>Experience - Aling-Conel - Configurator</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
<meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
<meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
<link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
<link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
<link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
<link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
<link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/experience/style_experience.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php
ini_set("default_charset", "UTF-8");
//header('Content-type: text/html; charset=UTF-8');
/*
Template Name: index_experience_eng
*/
?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php');
$discount = $_SESSION['discount'];
$prava = $_SESSION['prava'];
?>

<div class = "subMenu">
	<div class="left_container">
	<div class="bticino_logo"><img src="wp-content/themes/momentous-lite/logotipi/experience_logo.jpg"></div>
	<div class = "naslov_proizvoda"></div>
	</div>
	<div class = "right_container">
	<!-- <div class="flags"><div class="flag"><img src="wp-content/themes/momentous-lite/flags/brit.jpg" title="english" id="brit1"></div><div class="flag"><img src="wp-content/themes/momentous-lite/flags/srbDark.jpg" title="srpski" id="srb2"></div></div> -->
	<div class = "naslov_kompleta">MOJ IZBOR</div>
	</div>
</div>
<div class = "leva">
<?php 

// KOD ZA BAZA NA NETU
$servername = servername;
$username = username;
$password =password;
$dbname = baza;

//POVEZIVANJE NA BAZU
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt1 = $conn->prepare("SELECT * FROM exp_prek_mask_mod_1");
	$stmt2 = $conn->prepare("SELECT * FROM exp_prek_mask_mod_2");
	$stmt32 = $conn->prepare("SELECT * FROM exp_roletne_mod_1 ");
	$stmt33 = $conn->prepare("SELECT * FROM exp_roletne_sos_mod_2 ");
	$stmt3 = $conn->prepare("SELECT * FROM exp_kutije");
	$stmt4 = $conn->prepare("SELECT * FROM exp_boje_okvira");
	$stmt5 = $conn->prepare("SELECT * FROM exp_okviri");
	//$stmt18 = $conn->prepare("SELECT * FROM exp_boje_okvira_2");
	//$stmt19 = $conn->prepare("SELECT * FROM exp_okviri_2");
	$stmt6 = $conn->prepare("SELECT * FROM exp_dimeri_mod_1");
	$stmt7 = $conn->prepare("SELECT * FROM exp_kupatilo");
	$stmt8 = $conn->prepare("SELECT * FROM exp_det_pokreta_mod_2");
	$stmt9 = $conn->prepare("SELECT * FROM sc_visio_termostati_mod_2");
	$stmt11 = $conn->prepare("SELECT * FROM exp_prikljucnice_mod_1");
	$stmt12 = $conn->prepare("SELECT * FROM exp_prikljucnice_mod_2");
	$stmt13 = $conn->prepare("SELECT * FROM exp_tv_prikljucnice_mod_1");
	$stmt14 = $conn->prepare("SELECT * FROM exp_racunarske_prikljuc_mod_1");
	$stmt15 = $conn->prepare("SELECT * FROM exp_dr_prikljucnice_mod_1");
	$stmt16 = $conn->prepare("SELECT * FROM exp_dr_prikljucnice_mod_2");
	$stmt17 = $conn->prepare("SELECT * FROM exp_slepe_maske_mod_1");
	/*$stmt18 = $conn->prepare("SELECT * FROM sc_visio_maske_mod_2");*/
	$stmt21 = $conn->prepare("SELECT * FROM exp_racunarski_kon_mas_mod_1");
	$stmt22 = $conn->prepare("SELECT * FROM sc_visio_det_pokreta_mod_1");
	$stmt23 = $conn->prepare("SELECT * FROM sc_visio_det_pokreta_mod_2");
	$stmt24 = $conn->prepare("SELECT * FROM exp_prirubnice");
	$stmt25 = $conn->prepare("SELECT * FROM exp_indik_sijalice");
	$stmt26 = $conn->prepare("SELECT * FROM bt_living_vodootporno");
	$stmt27 = $conn->prepare("SELECT * FROM bticino_aksijalni_living_mod_1");
	$stmt28 = $conn->prepare("SELECT * FROM bticino_aksijalni_living_mod_2");
	$stmt29 = $conn->prepare("SELECT * FROM bticino_maske_klasicni_prek_ll");
	$stmt30 = $conn->prepare("SELECT * FROM bt_living_termperatura_3");
	
	$stmt41 = $conn->prepare("SELECT * FROM exp_jednop_sklopka_1");
	$stmt41->execute();
	$stmt43 = $conn->prepare("SELECT * FROM exp_naizmen_sklopka_1");
	$stmt43->execute();
	$stmt45 = $conn->prepare("SELECT * FROM exp_ukrsna_sklopka_1");
	$stmt45->execute();
	$stmt47 = $conn->prepare("SELECT * FROM exp_jednop_indik_sklopka_1");
	$stmt47->execute();
	$stmt49 = $conn->prepare("SELECT * FROM exp_dvopolna_sklopka_1");
	$stmt49->execute();
	$stmt51 = $conn->prepare("SELECT * FROM exp_taster_sklopka_1");
	$stmt51->execute();
	$stmt53 = $conn->prepare("SELECT * FROM exp_potezni_mod_1");
	$stmt53->execute();
	
	$stmt1->execute();
	$stmt2->execute();
	$stmt3->execute();
	$stmt4->execute();
	$stmt5->execute();
	$stmt6->execute();
	$stmt7->execute();
	$stmt8->execute();
	$stmt9->execute();
	$stmt11->execute();
	$stmt12->execute();
	$stmt13->execute();
	$stmt14->execute();
	$stmt15->execute();
	$stmt16->execute();
	$stmt17->execute();
	//$stmt18->execute();
	//$stmt19->execute();
	$stmt21->execute();
	$stmt22->execute();
	$stmt23->execute();
	$stmt24->execute();
	$stmt25->execute();
	$stmt26->execute();
	$stmt27->execute();
	$stmt28->execute();
	$stmt29->execute();
	$stmt30->execute();
	$stmt32->execute();
	$stmt33->execute();
	
}
catch(PDOException $e) {
echo "Error: " . $e->getMessage();
}
?>

<div class="navigacija">
<ul class="glavni">
  <li class="glavni_meni" id="link_okviri"><a>RAMOVI</a></li>
  <li class="glavni_meni" id="link_prekidac_jednopolni"><a>SKLOPKA JEDNOPOLNA</a></li>
  <li class="glavni_meni" id="link_prekidac_naizmenicni"><a>SKLOPKA NAIZMENIČNA</a></li>
  <li class="glavni_meni" id="link_prekidac_ukrsni"><a>SKLOPKA UKRSNA</a></li>
  <li class="glavni_meni" id="link_prekidac_jednopolni_nula"><a>SKLOPKA INDIKATORSKA</a></li>
  <li class="glavni_meni" id="link_prekidac_dvopolni"><a>SKLOPKA DVOPOLNA</a></li>
  <li class="glavni_meni" id="link_prekidac_taster"><a>TASTER SKLOPKA JEDNOPOLNA</a></li>
  <li class="glavni_meni" id="link_prekidac_potezni"><a>POTEZNA TASTER SKLOPKA</a></li>
  <li class="glavni_meni" id="link_roletne"><a>ROLETNE</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>ELEKTRONIKA</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_tv_uticnice"><a>TV PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>RAČUNARSKE PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_multimedijalne_uticnice"><a>MULTIMEDIJA</a></li>
  <li class="glavni_meni" id="link_maske"><a>POKLOPAC MASKA</a></li>
  <li class="glavni_meni" id="link_sijalice"><a>INDIKACIJA</a></li>
  <li class="glavni_meni" id="link_kupatilo"><a>KUPATILO</a></li>
  <li class="glavni_meni" id="link_nosaci_dozne"><a>KUTIJE</a></li>
</ul>
</div>

<div class="dropdownNovo">
  <button class="dropbtnNovo">Experience Oprema</button>
  <div class="dropdown-contentNovo">
  <li class="glavni_meni" id="link_okviri"><a>RAMOVI</a></li>
  <li class="glavni_meni" id="link_prekidac_jednopolni"><a>SKLOPKA JEDNOPOLNA</a></li>
  <li class="glavni_meni" id="link_prekidac_naizmenicni"><a>SKLOPKA NAIZMENIČNA</a></li>
  <li class="glavni_meni" id="link_prekidac_ukrsni"><a>SKLOPKA UKRSNA</a></li>
  <li class="glavni_meni" id="link_prekidac_jednopolni_nula"><a>SKLOPKA INDIKATORSKA</a></li>
  <li class="glavni_meni" id="link_prekidac_dvopolni"><a>SKLOPKA DVOPOLNA</a></li>
  <li class="glavni_meni" id="link_prekidac_potezni"><a>TASTER SKLOPKA JEDNOPOLNA</a></li>
  <li class="glavni_meni" id="link_roletne"><a>ROLETNE</a></li>
  <li class="glavni_meni" id="link_dimeri"><a>ELEKTRONIKA</a></li>
  <li class="glavni_meni" id="link_energetske_uticnice"><a>PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_tv_uticnice"><a>TV PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_racunarske_uticnice"><a>RAČUNARSKE PRIKLJUČNICE</a></li>
  <li class="glavni_meni" id="link_multimedijalne_uticnice"><a>MULTIMEDIJA</a></li>
  <li class="glavni_meni" id="link_maske"><a>POKLOPAC MASKA</a></li>
  <li class="glavni_meni" id="link_sijalice"><a>INDIKACIJA</a></li>
  <li class="glavni_meni" id="link_kupatilo"><a>KUPATILO</a></li>
  <li class="glavni_meni" id="link_nosaci_dozne"><a>KUTIJE</a></li>
  </div>
</div>

<div class="bticino_levi">
<ul class = "levi_podmeni">
<li id="naslov_podmeni"></li>
<li style="float:right"><a class="link_moduli" id="link_svi">SVI MODULI</a></li>
<li style="float:right"><a class="link_moduli" id="link_2">2 MODULA</a></li>
<li style="float:right"><a class="link_moduli" id="link_1">1 MODUL</a></li>
</ul>
</div>

<!-- LEVI SCROLL BAR -->
<div class="ex1">
<div class = "proizvodi" id = "prekidaci">
<?php
// VREDNOST EVRA U DINARIMA
$money = 1;
$currency = "RSD";

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt1->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_2_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}

?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}
?>
</div>	
</div>	<!-- KRAJ DIV PROIZVODI PREKIDACI -->

<!-- JEDNOPOLNI PREKIDAC -->


<div class = "proizvodi" id = "prekidac_jednopolni">
<div class="naslov_mehanizmi">SKLOPKA JEDNOPOLNA 16A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt41->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC JEDNOPOLNI -->


<!-- DIV PREKIDAC NAIZMENICNI -->

<div class = "proizvodi" id = "prekidac_naizmenicni">
<div class="naslov_mehanizmi">SKLOPKA NAIZMENIČNA 16A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt43->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC NAIZMENICNI -->


<!-- DIV PREKIDAC UKRSNI -->

<div class = "proizvodi" id = "prekidac_ukrsni">
<div class="naslov_mehanizmi">SKLOPKA UKRSNA 16A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt45->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC UKRSNI -->


<!-- DIV PREKIDAC JEDNOPOLNI SA NULOM -->

<div class = "proizvodi" id = "prekidac_jednopolni_nula">
<div class="naslov_mehanizmi">SKLOPKA INDIKATORSKA 16A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt47->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC JEDNOPOLNI SA NULOM -->




<!-- DIV PREKIDAC DVOPOLNI -->

<div class = "proizvodi" id = "prekidac_dvopolni">
<div class="naslov_mehanizmi">SKLOPKA DVOPOLNA 10A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt49->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_1_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC DVOPOLNI -->



<!-- DIV PREKIDAC TASTER SKLOPKA -->

<div class = "proizvodi" id = "prekidac_taster">
<div class="naslov_mehanizmi">TASTER SKLOPKA JEDNOPOLNA 16A - ISPORUKA SE TASTEROM ODVOJENO</div>
<?php

// PRAVI SE NIZ KOMPLETA MASKE I PREKIDACI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt51->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_prekidac_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_1=$i;
}

/*
$i=0;
while ($row=$stmt2->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_2_kat_br[$i] = $row['kat_br_prek_maska'];
	$prekidaci_2_kat_br[$i] = $row['kat_br_prekidac'];
	$maske_2_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_2_opis[$i] = $row['opis_prekidac_engleski'];
	$maske_2_opis[$i] = $row['opis_maska_engleski'];
	$zajednicki_opis_2[$i] = $row['kraj_opisa'];
	$prekidaci_2_cena[$i] = $row['vp_prekidac_cena']/$money;;
	$maska_2_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_2_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_2_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_maske_prekidaci_2=$i;
}*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE PREKIDACA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_maske_prekidaci_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $maske_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}
?>
</div>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
while ($i< $brojanje_kompleta_maske_prekidaci_2)
{
	$red_br = $red_br + 1;
	$string = $prekidaci_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$string_maske = $maske_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_2_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_2_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_2_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_2_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_2_opis[$i].'" data-opis-prekidac = "'.$prekidaci_2_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_2_kat_br[$i].'" data-kat-br-maska = "'.$maske_2_kat_br[$i].'" data-cena-maska="'.$maske_2_cena[$i].'" data-cena-prekidac="'.$prekidac_2_cena[$i].'" data-cena="'.$prekidaci_maske_2_cena[$i].'" data-opis-interni = "'.$prekidaci_2_opis[$i].' - '.$maske_2_opis[$i].'" data-kratak-opis = "'.$prekidaci_2_opis[$i].''.$maske_2_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_2_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_2_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_2_opis[$i].'</div>';
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE		
					
			}	
	$i++;
}*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC DVOPOLNI -->



<!-- DIV PREKIDAC POTEZNI -->

<div class = "proizvodi" id = "prekidac_potezni">
<?php
// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt53->fetch(PDO::FETCH_ASSOC))
{	
	$roletne_1_opis[$i] = $row['opis_srpski'];
	$roletne_1_kat_br[$i] = $row['kat_br'];
	$roletne_1_kratak_opis[$i] = $row['opis_srpski'];
	$roletne_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_roletni_1=$i;
}


// PRAVI SE NIZ SVIH DIMERA - VELICINE 2 MODULA
/*
$i=0;
while ($row=$stmt33->fetch(PDO::FETCH_ASSOC))
{	
	$roletne_2_opis[$i] = $row['opis_engleski'];
	$roletne_2_kat_br[$i] = $row['kat_br'];
	$roletne_2_kratak_opis[$i] = $row['opis_engleski'];
	$roletne_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_roletni_2=$i;
}*/
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE DIMERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_roletni_1)
{
	$red_br = $red_br + 1;
	$string = $roletne_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $roletne_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL

				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$roletne_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$roletne_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$roletne_1_cena[$i].'" data-kratak-opis ="'.$roletne_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" alt ="'.$roletne_1_kratak_opis[$i].'" title="'.$roletne_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$roletne_1_opis[$i].'" data-kat-br = "'.$roletne_1_kat_br[$i].'" data-cena="'.$roletne_1_cena[$i].'" data-kratak-opis ="'.$roletne_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$roletne_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($roletne_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($roletne_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($roletne_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM				
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">

<?php
/*
$i=0;
//$red_br=0;
while ($i< $brojanje_roletni_2)
{
	$red_br = $red_br + 1;
	$string = $roletne_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $roletne_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// DIMER NA ENGLESKOM - 2 MODULA
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$roletne_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$roletne_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$roletne_2_cena[$i].'" data-kratak-opis ="'.$roletne_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$roletne_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$roletne_2_kat_br[$i].'" id="'.$br_modula_id.'"  alt ="'.$roletne_2_kratak_opis[$i].'" data-modul ="'.$br_modula.'"  title="'.$roletne_2_kat_br[$i].'" data-interni-opis = "'.$roletne_2_opis[$i].'" data-kat-br = "'.$roletne_2_kat_br[$i].'" data-cena="'.$roletne_2_cena[$i].'" data-kratak-opis ="'.$roletne_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
								echo '<div class="tekst_opis">'.$roletne_2_kratak_opis[$i].'</div>';
								echo '<div class="tekst_cena">Cena: '.number_format($roletne_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';	
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
*/
?>
</div>	
</div>	<!-- KRAJ DIV PREKIDAC POTEZNI -->



<!-- ROLETNE -->

<div class = "proizvodi" id = "roletne">
<?php
// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt32->fetch(PDO::FETCH_ASSOC))
{	
	$roletne_1_opis[$i] = $row['opis_srpski'];
	$roletne_1_kat_br[$i] = $row['kat_br'];
	$roletne_1_kratak_opis[$i] = $row['opis_srpski'];
	$roletne_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_roletni_1=$i;
}


// PRAVI SE NIZ SVIH DIMERA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt33->fetch(PDO::FETCH_ASSOC))
{	
	$roletne_2_opis[$i] = $row['opis_srpski'];
	$roletne_2_kat_br[$i] = $row['kat_br'];
	$roletne_2_kratak_opis[$i] = $row['opis_srpski'];
	$roletne_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_roletni_2=$i;
}
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE DIMERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_roletni_1)
{
	$red_br = $red_br + 1;
	$string = $roletne_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $roletne_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL

				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$roletne_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$roletne_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$roletne_1_cena[$i].'" data-kratak-opis ="'.$roletne_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" alt ="'.$roletne_1_kratak_opis[$i].'" title="'.$roletne_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$roletne_1_opis[$i].'" data-kat-br = "'.$roletne_1_kat_br[$i].'" data-cena="'.$roletne_1_cena[$i].'" data-kratak-opis ="'.$roletne_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$roletne_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($roletne_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($roletne_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($roletne_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM				
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php

$i=0;
//$red_br=0;
while ($i< $brojanje_roletni_2)
{
	$red_br = $red_br + 1;
	$string = $roletne_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $roletne_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// DIMER NA ENGLESKOM - 2 MODULA
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$roletne_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$roletne_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$roletne_2_cena[$i].'" data-kratak-opis ="'.$roletne_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$roletne_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$roletne_2_kat_br[$i].'" id="'.$br_modula_id.'"  alt ="'.$roletne_2_kratak_opis[$i].'" data-modul ="'.$br_modula.'"  title="'.$roletne_2_kat_br[$i].'" data-interni-opis = "'.$roletne_2_opis[$i].'" data-kat-br = "'.$roletne_2_kat_br[$i].'" data-cena="'.$roletne_2_cena[$i].'" data-kratak-opis ="'.$roletne_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
								echo '<div class="tekst_opis">'.$roletne_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($roletne_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($roletne_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($roletne_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ DIV ROLETNE -->



<div class = "proizvodi" id = "dimeri">
<?php
// PRAVI SE NIZ SVIH DIMERA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt6->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_1_opis[$i] = $row['opis_srpski'];
	$dimeri_1_kat_br[$i] = $row['kat_br'];
	$dimeri_1_kratak_opis[$i] = $row['opis_srpski'];
	$dimeri_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_1=$i;
}


// PRAVI SE NIZ SVIH DIMERA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt8->fetch(PDO::FETCH_ASSOC))
{	
	$dimeri_2_opis[$i] = $row['opis_srpski'];
	$dimeri_2_kat_br[$i] = $row['kat_br'];
	$dimeri_2_kratak_opis[$i] = $row['opis_srpski'];
	$dimeri_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_dimera_2=$i;
}
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE DIMERA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_dimera_1)
{
	$red_br = $red_br + 1;
	$string = $dimeri_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $dimeri_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			
				// DIMER NA SRPSKOM - 1 MODUL

				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" alt ="'.$dimeri_1_kratak_opis[$i].'" title="'.$dimeri_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'" data-interni-opis = "'.$dimeri_1_opis[$i].'" data-kat-br = "'.$dimeri_1_kat_br[$i].'" data-cena="'.$dimeri_1_cena[$i].'" data-kratak-opis ="'.$dimeri_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$dimeri_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($dimeri_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($dimeri_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZMA NA SRPSKOM				
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php

$i=0;
//$red_br=0;

while ($i< $brojanje_dimera_2)
{
	$red_br = $red_br + 1;
	$string = $dimeri_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $dimeri_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// DIMER NA ENGLESKOM - 2 MODULA
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$dimeri_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$dimeri_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$dimeri_2_kat_br[$i].'" id="'.$br_modula_id.'"  alt ="'.$dimeri_2_kratak_opis[$i].'" data-modul ="'.$br_modula.'"  title="'.$dimeri_2_kat_br[$i].'" data-interni-opis = "'.$dimeri_2_opis[$i].'" data-kat-br = "'.$dimeri_2_kat_br[$i].'" data-cena="'.$dimeri_2_cena[$i].'" data-kratak-opis ="'.$dimeri_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
								echo '<div class="tekst_opis">'.$dimeri_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($dimeri_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($dimeri_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ DIV PROIZVODI DIMERI -->


<!-- DETEKTORI POKRETA -->
<div class = "proizvodi" id = "detektori_pokreta">
<?php
// PRAVI SE NIZ SVIH DETEKTORA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt22->fetch(PDO::FETCH_ASSOC))
{	
	$detektori_1_opis[$i] = $row['interni_opis'];
	$detektori_1_kat_br[$i] = $row['kat_br'];
	$detektori_1_kratak_opis[$i] = $row['opis_srpski'];
	$detektori_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_detektori_pokreta_1=$i;
}

// PRAVI SE NIZ SVIH DETEKTORA POKRETA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt23->fetch(PDO::FETCH_ASSOC))
{	
	$detektori_2_opis[$i] = $row['interni_opis'];
	$detektori_2_kat_br[$i] = $row['kat_br'];
	$detektori_2_kratak_opis[$i] = $row['opis_srpski'];
	$detektori_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_detektori_pokreta_2=$i;
}
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ISPISIVANJE DETEKTORA POKRETA - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_detektori_pokreta_1)
{
	$red_br = $red_br + 1;
	$string = $detektori_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $detektori_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 1;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
			// DETEKTORI POKRETA - SRPSKI - 1 modul
			
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$detektori_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$detektori_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$detektori_1_cena[$i].'" data-kratak-opis ="'.$detektori_1_kratak_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$detektori_1_kratak_opis[$i].'" class="slika_velicine_x_modula" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$detektori_1_kat_br[$i].'" data-interni-opis = "'.$detektori_1_opis[$i].'" data-kat-br = "'.$detektori_1_kat_br[$i].'" data-cena="'.$detektori_1_cena[$i].'" data-kratak-opis ="'.$detektori_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$detektori_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($detektori_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($detektori_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($detektori_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
while ($i< $brojanje_detektori_pokreta_2)
{
	$red_br = $red_br + 1;
	$string = $detektori_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
		$string_maske = $detektori_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// DETEKTORI POKRETA - SRPSKI - 2 MODULA
				//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$detektori_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$detektori_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$detektori_2_cena[$i].'" data-kratak-opis ="'.$detektori_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$detektori_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$detektori_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$detektori_2_kat_br[$i].'" data-interni-opis = "'.$detektori_2_opis[$i].'" data-kat-br = "'.$detektori_2_kat_br[$i].'" data-cena="'.$detektori_2_cena[$i].'" data-kratak-opis ="'.$detektori_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';	
								echo '<div class="tekst_opis">'.$detektori_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($detektori_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE			
			}	
	$i++;
}
?>
</div>
</div>	<!-- KRAJ DIV PROIZVODI - DETEKTORI POKRETA -->


<!-- TERMOSTATI I DETEKTORI GASA -->
<div class = "proizvodi" id = "termostati_detektori">

<?php

// PRAVI SE NIZ SVIH TERMOSTATA_DETEKTORA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt9->fetch(PDO::FETCH_ASSOC))
{	
	$termostati_detektori_2_opis[$i] = $row['interni_opis'];
	$termostati_detektori_2_kat_br[$i] = $row['kat_br'];
	$termostati_detektori_2_kratak_opis[$i] = $row['opis_srpski'];
	$termostati_detektori_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_termostata_detektora_2=$i;
}

// NIZOVI ZA TERMOSTATE DIMENZIJE 3 MODULA
$i=0;
while ($row=$stmt30->fetch(PDO::FETCH_ASSOC))
{	
	$termostati_3_opis[$i] = $row['interni_opis'];
	$termostati_3_kat_br[$i] = $row['kat_br'];
	$termostati_3_kratak_opis[$i] = $row['opis_srpski'];
	$termostati_3_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_termostata_3=$i;
}

//ISPISIVANJE DETEKTORA POKRETA - 2 MODULA

$i=0;
$red_br=0;
?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
while ($i< $brojanje_termostata_detektora_2)
{
	$red_br = $red_br + 1;
	$string = $termostati_detektori_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// TERMOSTATI I DETEKTORI NA SRPSKOM
					
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$termostati_detektori_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$termostati_detektori_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$termostati_detektori_2_cena[$i].'" data-kratak-opis ="'.$termostati_detektori_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$termostati_detektori_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$termostati_detektori_2_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$termostati_detektori_2_kat_br[$i].'" data-interni-opis = "'.$termostati_detektori_2_opis[$i].'" data-kat-br = "'.$termostati_detektori_2_kat_br[$i].'" data-cena="'.$termostati_detektori_2_cena[$i].'" data-kratak-opis ="'.$termostati_detektori_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$termostati_detektori_2_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$termostati_detektori_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($termostati_detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($termostati_detektori_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($termostati_detektori_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}

?>
</div>


<!-- <div class = "vrsta_mehanizmi" id="mehanizmi_modul_3"> -->


<?php
/*
$i=0;
while ($i< $brojanje_termostata_3)
{
	$red_br = $red_br + 1;
	$string = $termostati_3_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = 3 ;
				$br_modula_id = "tri";
				
				// TERMOSTATI I DETEKTORI NA SRPSKOM
					
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$termostati_3_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$termostati_3_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$termostati_3_cena[$i].'" data-kratak-opis ="'.$termostati_3_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/'.$newstring.'.jpg" alt ="'.$termostati_3_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$termostati_3_kat_br[$i].'" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$termostati_3_kat_br[$i].'" data-interni-opis = "'.$termostati_3_opis[$i].'" data-kat-br = "'.$termostati_3_kat_br[$i].'" data-cena="'.$termostati_3_cena[$i].'" data-kratak-opis ="'.$termostati_3_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$termostati_3_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$termostati_3_kratak_opis[$i].'</div>';
								echo '<div class="tekst_cena">Cena: '.number_format($termostati_3_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}

*/?>

<!-- </div> -->


</div>	<!-- KRAJ DIV PROIZVODI - TERMOSTATI DETEKTORI -->
<!-- ENERGETSKE UTICNICE -->
<div class = "proizvodi" id = "energetske_uticnice">

<?php
// PRAVI SE NIZ SVIH ENERGETSKIH UTICNICA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt11->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_1_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_1_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_1_kratak_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_energetske_uticnice_1=$i;
}
// PRAVI SE NIZ SVIH DETEKTORA POKRETA - VELICINE 2 MODULA
$i=0;
while ($row=$stmt12->fetch(PDO::FETCH_ASSOC))
{	
	$energetske_uticnice_2_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_2_kat_br[$i] = $row['kat_br'];
	$energetske_uticnice_2_kratak_opis[$i] = $row['opis_srpski'];
	$energetske_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_energetske_uticnice_2=$i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
//ENERGETSKE UTICNICE- 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_energetske_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $energetske_uticnice_1_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 1;
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
								// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
								//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';	
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$energetske_uticnice_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$energetske_uticnice_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$energetske_uticnice_1_kat_br[$i].'" data-interni-opis = "'.$energetske_uticnice_1_opis[$i].'" data-kat-br = "'.$energetske_uticnice_1_kat_br[$i].'" data-cena="'.$energetske_uticnice_1_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$new_string_maske.'</div>';
							echo '<div class="tekst_opis">'.$energetske_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($energetske_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($energetske_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
				
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">

<?php
$i=0;
//$red_br=0;
while ($i< $brojanje_energetske_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $energetske_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$string_maske = $energetske_uticnice_2_kat_br[$i];
	$new_string_maske = str_replace("_", ".", $string_maske);
	
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				//ENERGETSKE UTICNICE - 2 MODULA - srpski
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$energetske_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';	
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$energetske_uticnice_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$energetske_uticnice_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$energetske_uticnice_2_kat_br[$i].'" data-interni-opis = "'.$energetske_uticnice_2_opis[$i].'" data-kat-br = "'.$energetske_uticnice_2_kat_br[$i].'" data-cena="'.$energetske_uticnice_2_cena[$i].'" data-kratak-opis ="'.$energetske_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div div class="tekst_katbr">'.$new_string_maske.'</div>';
								echo '<div class="tekst_opis">'.$energetske_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($energetske_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($energetske_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ DIV ENERGETSKE UTICNICE -->


<!-- RACUNARSKE UTICNICE -->
<div class = "proizvodi" id = "racunarske_uticnice">

<div class = "naslov_mehanizmi" >RAČUNARSKE UTIČNICE</div>
<?php
// PRAVI SE NIZ SVIH KOMPLETIRANIH UTICNICA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt14->fetch(PDO::FETCH_ASSOC))
{	
	$racunarske_uticnice_opis[$i] = $row['opis_srpski'];
	$racunarske_uticnice_kat_br[$i] = $row['kat_br'];
	$racunarske_uticnice_kratak_opis[$i] = $row['opis_srpski'];
	$racunarske_uticnice_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_racunarskih_uticnica=$i;
}

?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE RAC KOMPLETIRANE UTICNICE - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_racunarskih_uticnica)
{
	$red_br = $red_br + 1;
	$string = $racunarske_uticnice_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $racunarske_uticnice_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 		
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}	
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$racunarske_uticnice_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';		
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$racunarske_uticnice_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$racunarske_uticnice_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$racunarske_uticnice_kat_br[$i].'" data-interni-opis = "'.$racunarske_uticnice_opis[$i].'" data-kat-br = "'.$racunarske_uticnice_kat_br[$i].'" data-cena="'.$racunarske_uticnice_cena[$i].'" data-kratak-opis ="'.$racunarske_uticnice_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$racunarske_uticnice_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($racunarske_uticnice_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($racunarske_uticnice_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
?>
</div>


<?php
// PRAVI SE NIZ SVIH RACUNARSKIH UTICNICA - VELICINE 1 MODULA

$i=0;
while ($row=$stmt21->fetch(PDO::FETCH_ASSOC))
{	
	$prekidaci_maske_1_kat_br[$i] = $row['kat_br_konektor_maska'];
	$prekidaci_1_kat_br[$i] = $row['kat_br_konektor'];
	$maske_1_kat_br[$i] = $row['kat_br_maska'];
	$prekidaci_1_opis[$i] = $row['opis_konektor_srpski'];
	$maske_1_opis[$i] = $row['opis_maska_srpski'];
	$zajednicki_opis[$i] = $row['kraj_opisa'];
	$prekidaci_1_cena[$i] = $row['vp_konektor_cena']/$money;;
	$maska_1_cena[$i] = $row['vp_maska_cena']/$money;;
	$prekidaci_maske_1_cena[$i] = $row['vp_cena_ukupno']/$money;;
	$maske_1_boja[$i] = $row['boja'];
	$i++;
	$brojanje_kompleta_konektori_maske_1=$i;
}

//ISPISIVANJE RACUNARSKIH UTICNICA - 1 MODUL
?>
<div class = "naslov_mehanizmi">RAČUNARSKE UTIČNICE - ISPORUKA SA KONEKTOROM ODVOJENO</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">
<?php
$i=0;
$red_br=0;
while ($i< $brojanje_kompleta_konektori_maske_1)
{
	// ISPRAVLJANJE KOSE CRTE U DONJU CRTU ZBOG PRETRAGE SLIKA U FOLDERIMA
	$red_br = $red_br + 1;
	$string = $maske_prekidaci_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);

			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				// MEHANIZAM NA SRPSKOM
				echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$maske_1_boja[$i].'" id="mehanizam" title="'.$prekidaci_maske_1_kat_br[$i].'"  data-kat-br = "'.$prekidaci_maske_1_kat_br[$i].'" data-modul ="'.$br_modula.'"  data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
					echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$maske_1_kat_br[$i].'.jpg" alt="'.$izabran_prekidac_opis.' '.$izabrana_maska_opis.'" class="slika_velicine_x_modula" id='.$br_modula_id.' title="'.$prekidaci_maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-maska = "'.$maske_1_opis[$i].'" data-opis-prekidac = "'.$prekidaci_1_opis[$i].'" data-kat-br-prekidac = "'.$prekidaci_1_kat_br[$i].'" data-kat-br-maska = "'.$maske_1_kat_br[$i].'" data-cena-maska="'.$maske_1_cena[$i].'" data-cena-prekidac="'.$prekidac_1_cena[$i].'" data-cena="'.$prekidaci_maske_1_cena[$i].'" data-opis-interni = "'.$prekidaci_1_opis[$i].' - '.$maske_1_opis[$i].'" data-kratak-opis = "'.$prekidaci_1_opis[$i].''.$maske_1_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$prekidaci_1_kat_br[$i].'</div>';
							echo '<div class="tekst_katbr">'.$maske_1_kat_br[$i].'</div>';
							echo '<div class="tekst_opis">'.$prekidaci_1_opis[$i].'</div>';
							echo '<div class="tekst_opis">'.$maske_1_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($prekidaci_maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($prekidaci_maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
				echo '</div>'; // kraj MEHANIZAM VELICINE				
			}	
	$i++;
}

?>
</div>
</div>	<!-- KRAJ DIV PROIZVODI - RACUNARSKE UTICNICE -->


<!-- TV UTICNICE -->
<div class = "proizvodi" id = "tv_uticnice">
<?php
// PRAVI SE NIZ SVIH TV/AUDIO/HDMI UTICNICEA - VELICINE 1 MODUL
$i=0;
while ($row=$stmt13->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_1_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_1_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_1_kratak_opis[$i] = $row['opis_srpski'];
	$tv_audio_hdmi_uticnice_1_cena[$i] = $row['vp_cena']/$money;

	$i++;
	$brojanje_tv_audio_hdmi_uticnice_1=$i;
}

// PRAVI SE NIZ SVIH TV - VELICINE 2 MODULA
/*
$i=0;
while ($row=$stmt14->fetch(PDO::FETCH_ASSOC))
{	
	$tv_audio_hdmi_uticnice_2_opis[$i] = $row['interni_opis'];
	$tv_audio_hdmi_uticnice_2_kat_br[$i] = $row['kat_br'];
	$tv_audio_hdmi_uticnice_2_kratak_opis[$i] = $row['opis_engleski'];
	$tv_audio_hdmi_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_tv_audio_hdmi_uticnice_2=$i;
}
*/
?>



<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE TV - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_tv_audio_hdmi_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_1_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 		
			{
				$br_modula = 1;
				
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}	
					// ENERGETSKE UTIČNICE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';		
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_1_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_1_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_1_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($tv_audio_hdmi_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
/*
$i=0;
//$red_br=0;
while ($i< $brojanje_tv_audio_hdmi_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $tv_audio_hdmi_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	$newKatbr = $tv_audio_hdmi_uticnice_2_kat_br[$i];
	$newKatbr = str_replace("_", "", $newKatbr); 
			{
				$br_modula = 2 ;
				
					if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// TV UTIČNICE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" data-interni-opis = "'.$tv_audio_hdmi_uticnice_2_opis[$i].'" data-kat-br = "'.$tv_audio_hdmi_uticnice_2_kat_br[$i].'" data-cena="'.$tv_audio_hdmi_uticnice_2_cena[$i].'" data-kratak-opis ="'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$tv_audio_hdmi_uticnice_2_kratak_opis[$i].'</div>';
								echo '<div class="tekst_cena">Cena: '.number_format($tv_audio_hdmi_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
*/
?>
</div>
</div> <!-- KRAJ DIV TV UTICNICE -->

<!-- MULTIMEDIJALNE UTICNICE -->
<div class = "proizvodi" id = "multimedijalne_uticnice">
<?php
// PRAVI SE NIZ SVIH ZVONA I SVETILJKI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt15->fetch(PDO::FETCH_ASSOC))
{	
	$multimedijalne_uticnice_1_opis[$i] = $row['interni_opis'];
	$multimedijalne_uticnice_1_kat_br[$i] = $row['kat_br'];
	$multimedijalne_uticnice_1_kratak_opis[$i] = $row['opis_srpski'];
	$multimedijalne_uticnice_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_multimedijalne_uticnice_1=$i;
}

// PRAVI SE NIZ SVIH TV/AUDIO/HDMI - VELICINE 2 MODULA
$i=0;
while ($row=$stmt16->fetch(PDO::FETCH_ASSOC))
{	
	$multimedijalne_uticnice_2_opis[$i] = $row['interni_opis'];
	$multimedijalne_uticnice_2_kat_br[$i] = $row['kat_br'];
	$multimedijalne_uticnice_2_kratak_opis[$i] = $row['opis_srpski'];
	$multimedijalne_uticnice_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_multimedijalne_uticnice_2=$i;
}

//ISPISIVANJE TV/AUDIO/HDMI - 1 MODUL
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
$i=0;
$red_br=0;

while ($i< $brojanje_multimedijalne_uticnice_1)
{
	$red_br = $red_br + 1;
	$string = $multimedijalne_uticnice_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$newKatbr = $multimedijalne_uticnice_1_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 	
	
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
					// BELLS AND LIGHTS - 1 MODUL
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$multimedijalne_uticnice_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$multimedijalne_uticnice_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$multimedijalne_uticnice_1_cena[$i].'" data-kratak-opis ="'.$multimedijalne_uticnice_1_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" alt ="'.$multimedijalne_uticnice_1_kratak_opis[$i].'" title="'.$multimedijalne_uticnice_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$multimedijalne_uticnice_1_kat_br[$i].'" data-interni-opis = "'.$multimedijalne_uticnice_1_opis[$i].'" data-kat-br = "'.$multimedijalne_uticnice_1_kat_br[$i].'" data-cena="'.$multimedijalne_uticnice_1_cena[$i].'" data-kratak-opis ="'.$multimedijalne_uticnice_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$multimedijalne_uticnice_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($multimedijalne_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($multimedijalne_uticnice_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($multimedijalne_uticnice_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
</div>
<?php
$i=0;

while ($i< $brojanje_multimedijalne_uticnice_2)
{
	$red_br = $red_br + 1;
	$string = $multimedijalne_uticnice_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$newKatbr = $multimedijalne_uticnice_2_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 
	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
					// ZVONA I SVETILJKE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$multimedijalne_uticnice_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$multimedijalne_uticnice_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$multimedijalne_uticnice_2_cena[$i].'" data-kratak-opis ="'.$multimedijalne_uticnice_2_kratak_opis[$i].'">';			
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" class="slika_velicine_x_modula" title="'.$multimedijalne_uticnice_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$multimedijalne_uticnice_2_kat_br[$i].'" data-interni-opis = "'.$multimedijalne_uticnice_2_opis[$i].'" data-kat-br = "'.$multimedijalne_uticnice_2_kat_br[$i].'" data-cena="'.$multimedijalne_uticnice_2_cena[$i].'" data-kratak-opis ="'.$multimedijalne_uticnice_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$multimedijalne_uticnice_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($multimedijalne_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($multimedijalne_uticnice_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($multimedijalne_uticnice_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					
			}	
	$i++;
}
?>
</div>	
</div> <!-- KRAJ ZVONA I SVETILJKE -->

<!-- MASKE -->
<div class = "proizvodi" id = "maske">
<?php

// PRAVI SE NIZ SVIH MASKI - VELICINE 1 MODUL
$i=0;
while ($row=$stmt17->fetch(PDO::FETCH_ASSOC))
{	
	$maske_1_opis[$i] = $row['interni_opis'];
	$maske_1_kat_br[$i] = $row['kat_br'];
	$maske_1_kratak_opis[$i] = $row['opis_srpski'];
	$maske_1_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_maske_1=$i;
}

// PRAVI SE NIZ SVIH MASKI - VELICINE 2 MODULA
/*
$i=0;
while ($row=$okviri->fetch(PDO::FETCH_ASSOC))
{	
	$maske_2_opis[$i] = $row['interni_opis'];
	$maske_2_kat_br[$i] = $row['kat_br'];
	$maske_2_kratak_opis[$i] = $row['opis_engleski'];
	$maske_2_cena[$i] = $row['vp_cena']/$money;
	$i++;
	$brojanje_maske_2=$i;
}
*/
?>

<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE MASKI - 1 MODUL
$i=0;
$red_br=0;
while ($i< $brojanje_maske_1)
{
	$red_br = $red_br + 1;
	$string = $maske_1_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	
	$newKatbr = $maske_1_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 
			{
				$br_modula = 1;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				// SLEPE MASKE 1 MODUL - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_1_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_1_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$maske_1_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$maske_1_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$maske_1_kat_br[$i].'" data-interni-opis = "'.$maske_1_opis[$i].'" data-kat-br = "'.$maske_1_kat_br[$i].'" data-cena="'.$maske_1_cena[$i].'" data-kratak-opis ="'.$maske_1_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$maske_1_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($maske_1_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($maske_1_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}

?>
</div>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_2">
<?php
$i=0;
while ($i< $brojanje_maske_2)
{
	$red_br = $red_br + 1;
	$string = $maske_2_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$newKatbr = $maske_2_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 
	
			{
				$br_modula = 2 ;
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				// SLEPE MASKE 2 MODULA - SRPSKI
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="mehanizam" title="'.$maske_2_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$maske_2_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$maske_2_cena[$i].'" data-kratak-opis ="'.$maske_2_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/'.$newstring.'.jpg" alt ="'.$maske_2_kratak_opis[$i].'" class="slika_velicine_x_modula" title="'.$maske_2_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$maske_2_kat_br[$i].'" data-interni-opis = "'.$maske_2_opis[$i].'" data-kat-br = "'.$maske_2_kat_br[$i].'" data-cena="'.$maske_2_cena[$i].'" data-kratak-opis ="'.$maske_2_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$maske_2_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($maske_2_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($maske_2_cena[$i], 2, ',', ' ').' '.$currency.'</div>';
								
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
		
			}	
	$i++;
}

?>
</div>
</div><!-- KRAJ MASKI -->


<!-- POČETAK KUPATILO -->
<div class = "proizvodi" id = "kupatilo">

<?php

// PRAVI SE NIZ SVE OSTALE OPREME - kupatilo
$i=0;
while ($row=$stmt7->fetch(PDO::FETCH_ASSOC))
{	
	$kupatilo_opis[$i] = $row['opis_srpski'];
	$kupatilo_kat_br[$i] = $row['kat_br'];
	$kupatilo_kratak_opis[$i] = $row['opis_srpski'];
	$kupatilo_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['mehanizam'];
	$i++;
	$brojanje_kupatilo=$i;
}

//ISPISIVANJE OPREME KUPATILO

$i=0;
$red_br=0;

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_kupatilo)
{
	$red_br = $red_br + 1;
	$string = $kupatilo_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$newKatbr = $kupatilo_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 
	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				
					if ($br_modula == "2v")
				{
					$br_modula_id = "dva_vertikalno";
				}
			
						//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					//echo '<div class = "kontejner">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-modul-class = "'.$br_modula_class.'" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$kupatilo_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$kupatilo_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$kupatilo_cena[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$newstring.'" data-interni-opis = "'.$kupatilo_opis[$i].'" data-kat-br = "'.$kupatilo_kat_br[$i].'" data-cena="'.$kupatilo_cena[$i].'" data-kratak-opis ="'.$kupatilo_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$kupatilo_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($kupatilo_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($kupatilo_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($kupatilo_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
				//	echo '</div>'; // KRAJ KONTEJNERA
			}	
	$i++;
}

?>
</div>
</div>
<!-- KRAJ - KUPATILO -->



<!-- SIJALICE -->
<div class = "proizvodi" id = "sijalice">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - VELICINE 1 MODUL
$i=0;
while ($row=$stmt25->fetch(PDO::FETCH_ASSOC))
{	
	$ostala_oprema_opis[$i] = $row['opis_srpski'];
	$ostala_oprema_kat_br[$i] = $row['kat_br'];
	$ostala_oprema_kratak_opis[$i] = $row['opis_srpski'];
	$ostala_oprema_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_ostala_oprema=$i;
}
?>

<div class = "vrsta_mehanizmi">
<?php
//ISPISIVANJE OSTALE OPREME

$i=0;
$red_br=0;
while ($i< $brojanje_ostala_oprema)
{
	$red_br = $red_br + 1;
	$string = $ostala_oprema_kat_br[$i];
    $newstring = str_replace("/", "_", $string);

	$newKatbr = $ostala_oprema_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 

	
			{
				$br_modula = $broj_modula[$i];
				
				if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
				// DODATNA OPREMA - SRPSKI
				
				
					// DODATNE LAMPE ZA MASKE

					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$ostala_oprema_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$ostala_oprema_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$ostala_oprema_cena[$i].'" data-kratak-opis ="'.$ostala_oprema_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" title="'.$ostala_oprema_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$ostala_oprema_kat_br[$i].'" data-interni-opis = "'.$ostala_oprema_opis[$i].'" data-kat-br = "'.$ostala_oprema_kat_br[$i].'" data-cena="'.$ostala_oprema_cena[$i].'" data-kratak-opis ="'.$ostala_oprema_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
							echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
							echo '<div class="tekst_opis">'.$ostala_oprema_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($ostala_oprema_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
							echo '<div class="tekst_cena">Cena: '.number_format($ostala_oprema_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
					

			}	
	$i++;
}

?>
</div>
</div> <!-- KRAJ SIJALICE -->

<!-- VODOOTPORNO -->
<div class = "proizvodi" id = "vodootporno">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME - VODOOTPORNO
$i=0;
while ($row=$stmt26->fetch(PDO::FETCH_ASSOC))
{	
	$vodootporno_opis[$i] = $row['interni_opis'];
	$vodootporno_kat_br[$i] = $row['kat_br'];
	$vodootporno_kratak_opis[$i] = $row['opis_srpski'];
	$vodootporno_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_vodootporno=$i;
}

//ISPISIVANJE OPREME VODOOTPORNO

$i=0;
$red_br=0;

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
while ($i< $brojanje_vodootporno)
{
	$red_br = $red_br + 1;
	$string = $vodootporno_kat_br[$i];
    $newstring = str_replace("/", "_", $string);	
			{
				$br_modula = $broj_modula[$i];
				
			if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
					if ($br_modula == 6)
				{
					$br_modula_id = "sest";
				}
					if ($br_modula == "-")
				{
					$br_modula_id = "nista";
				}
			
						//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
						echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$vodootporno_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/livinglight_3/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" id="'.$br_modula_id.'" data-modul ="'.$br_modula.'"  title="'.$newstring.'" data-interni-opis = "'.$vodootporno_opis[$i].'" data-kat-br = "'.$vodootporno_kat_br[$i].'" data-cena="'.$vodootporno_cena[$i].'" data-kratak-opis ="'.$vodootporno_kratak_opis[$i].'">';
							echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$vodootporno_kat_br[$i].'</div>';
								echo '<div class="tekst_opis">'.$vodootporno_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($vodootporno_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($vodootporno_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
							echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE
			}	
	$i++;
}

?>
</div>
</div>
<!-- KRAJ - VODOOTPORNO -->


<!-- DOZNE I MALTER ZA GIPS -->
<div class = "proizvodi" id = "nosaci_dozne">
<?php

// PRAVI SE NIZ SVE OSTALE OPREME
$i=0;
while ($row=$stmt3->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_dozne_opis[$i] = $row['opis_srpski'];
	$nosaci_dozne_kat_br[$i] = $row['kat_br'];
	$nosaci_dozne_kratak_opis[$i] = $row['opis_srpski'];
	$nosaci_dozne_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci_dozne=$i;
}

?>
<div class = "vrsta_mehanizmi" id="mehanizmi_modul_1">

<?php
//ISPISIVANJE OPREME - DOZNE ZA MALTER I GIPS
$i=0;
$red_br=0;
while ($i< $brojanje_nosaci_dozne)
{
	$red_br = $red_br + 1;
	$string = $nosaci_dozne_kat_br[$i];
    $newstring = str_replace("/", "_", $string);
	
	$newKatbr = $nosaci_dozne_kat_br[$i];
	$newKatbr = str_replace("_", ".", $newKatbr); 
	
	
			{
				$br_modula = $broj_modula[$i];
				
				
						if ($br_modula == 1)
				{
					$br_modula_id = "jedan";
				}
				if ($br_modula == 2)
				{
					$br_modula_id = "dva";
				}
				
				if ($br_modula == 3)
				{
					$br_modula_id = "tri";
				}
				
				if ($br_modula == 4)
				{
					$br_modula_id = "cetiri";
				}
				
				if ($br_modula == 7)
				{
					$br_modula_id = "sedam";
				}
				
				if ($br_modula == 22)
				{
					$br_modula_id = "dva_dva";
				}
				
				if ($br_modula == 333)
				{
					$br_modula_id = "tri_tri_tri";
				}
				
				if ($br_modula == 33)
				{
					$br_modula_id = "tri_tri";
				}
					//echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula">';
					echo '<div class = "mehanizam_velicine_'.$br_modula.'_modula" data-boja="'.$prekidaci_boja[$i].'" id="dodatni_mehanizam_dodatni" title="'.$nosaci_dozne_kat_br[$i].'"  data-modul ="'.$br_modula.'" data-opis-interni = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$newstring.'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';				
						echo '<img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring.'.png" class="slika_velicine_x_modula_dodatno" title="'.$nosaci_dozne_kat_br[$i].'" id="'.$br_modula_id.'"  data-modul ="'.$br_modula.'"  title="'.$nosaci_dozne_kat_br[$i].'" data-interni-opis = "'.$nosaci_dozne_opis[$i].'" data-kat-br = "'.$nosaci_dozne_kat_br[$i].'" data-cena="'.$nosaci_dozne_cena[$i].'" data-kratak-opis ="'.$nosaci_dozne_kratak_opis[$i].'">';
						echo '<div class= "podaci_mehanizama" id = "podaci_mehanizma_'.$br_modula.'_modula">'; //DIV PODACI MEHANIZMA 2 MODULA
								echo '<div class="tekst_katbr">'.$newKatbr.'</div>';
								echo '<div class="tekst_opis">'.$nosaci_dozne_kratak_opis[$i].'</div>';
                if($_SESSION['prava']=='reseller')
                {
                    echo '<div class="tekst_cena"><s>Cena: '.number_format($nosaci_dozne_cena[$i], 2, ',', ' ').' '.$currency.'</s></div>';
                    //cena za reseller
                    echo '<div class="tekst_cena">Snižena cena: '.number_format($nosaci_dozne_cena[$i] * $discount, 2, ',', ' ').' '.$currency.'</div>';

                }
                else
								echo '<div class="tekst_cena">Cena: '.number_format($nosaci_dozne_cena[$i], 2, ',', ' ').' '.$currency.'</div>'; // number_format (koristi se za prabacivanje tačke u zarez)
						echo '</div>';//KRAJ DIV PODACI MEHANIZMA
					echo '</div>'; // kraj MEHANIZAM VELICINE

			}	
	$i++;
}
?>	
</div> <!-- kraj DIV vrsta_mehanizmi-->
</div> <!-- KRAJ - nosaci_dozne -->

<!-- OKVIRI -->
<div class = "proizvodi" id = "okviri">
<?php
// PRAVI SE NIZ SVIH BOJA OKVIRA 1
$i=0;
while ($row=$stmt4->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_boje[$i] = $row['boje'];
	$okviri_nazivi[$i] = $row['naziv_srpski'];
	$okviri_skracenice_boja[$i] = $row['skracenica'];
	$i++;
	$brojanje_okviri=$i;
}

// PRAVI SE NIZ SVIH OKVIRA 1
$j=0;
while ($row=$stmt5->fetch(PDO::FETCH_ASSOC))
{	
	$okviri_kat_br[$j] = $row['kat_br'];
	$okviri_cena[$j] = $row['vp_cena']/$money;
	$okviri_interni_opis[$j] = $row['opis_srpski'];
	$okviri_modul[$j] = $row['modul'];
	$okviri_kratak_opis[$j] = $row['opis_srpski'];
	$okviri_materijal[$j] = $row['materijal'];
	$okviri_skracenice[$j] = $row['skracenica'];
	$okviri_okviri_boje[$j] = $row['boje'];
	$okviri_opis[$j] = $row['opis_srpski'];
	$j++;
	$brojanje_svi_okviri=$j;
}
?>

<div class = "vrsta_mehanizmi" id="okviri_vrsta">
<?php

//ISPISIVANJE BOJA OKVIRA -1
$i=0;
$red_br=0;
$ukupan_broj_okvira = 0;

while ($i< $brojanje_okviri)
{
	$red_br = $red_br + 1;
	$string = $okviri_boje[$i];
    $newstring = str_replace("/", "_", $string);
	

	
	
			{
					echo '<div class = "boje_okvira" id="skracenica_'.$okviri_skracenice_boja[$i].'">';
						echo '<div class = "slika_tekst">';
						echo '<img class="okvir_slika" src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$newstring.'.jpg" title="'.$okviri_nazivi[$i].'" id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'">';
						echo '<h2 id="okvir_slika" data-skracenica="'.$okviri_skracenice_boja[$i].'"><span>'.$okviri_nazivi[$i].'</span></h2>';
						echo '</div>';

						echo '<table class="podaci_okvira" id="podaci_okvira_'.$okviri_skracenice_boja[$i].'">'; //TABELA PODACI OKVIRA								
								
								echo '<tr><td class="kolona_modul">MODUL</td><td class="kolona_kat_br">KAT.BR.</td><td class = "kolona_cena_2">CENA</td></tr>';
								//PRIKAZIVANJE CENE - OPISA
								$j=0;
								while ($j < $brojanje_svi_okviri)
								{
								$newKatbr = $okviri_kat_br[$j];
								$newKatbr = str_replace("_", ".", $newKatbr); 
									if($okviri_skracenice_boja[$i] == $okviri_skracenice[$j])
									{
										$ukupan_broj_okvira++;
										if ($okviri_modul[$j] == 33)
										{
											$okviri_modul[$j] = "3+3";
											}
                                        if($_SESSION['prava']=='reseller')
                                        {
                                            echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$newKatbr.'</td><td class = "kolona_cena_2"><s>'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</s><br>'.number_format($okviri_cena[$j] * $discount, 2, ',', ' ').' '.$currency.'</td></tr>';

                                        }
                                        else
										echo '<tr><td class="kolona_modul" id="kolona_'.$j.'" data-redni-broj="'.$ј.'" data-cena-okvira="'.$okviri_cena[$j].'" data-kat-broj="'.$okviri_kat_br[$j].'" data-broj-modula = "'.$okviri_modul[$j].'" data-naziv-skracenice = "'.$okviri_skracenice[$j].'" data-osnovni-opis-okvira = "'.$okviri_opis[$j].'">'.$okviri_modul[$j].'</td><td class="kolona_kat_br">'.$newKatbr.'</td><td class = "kolona_cena_2">'.number_format($okviri_cena[$j], 2, ',', ' ').' '.$currency.'</td></tr>';
									}
									$j++;
								}
								
								  echo '<input type="hidden" id = "skriven" data-ukupan-broj-okvira="'.$brojanje_svi_okviri.'">';
								
							echo '</table>';//TABELA PODACI OKVIRA
							
					echo '</div>'; // kraj boje okvira
				
			}	
	$i++;
}

?>

</div>
</div> <!-- KRAJ class=PROIZVODI id=OKVIRI -->
</div><!-- KRAJ SKROLA U LEVOJ-->
</div>	<!-- KRAJ DIV LEVA -->
<!-- DESNI DEO STRANICE -->
<div class = "desna">
<div class = "desna_dugme_kreiranje"></div>
<?php 
// PRAVI SE NIZ NOSAČA
$i=0;
while ($row=$stmt24->fetch(PDO::FETCH_ASSOC))
{	
	$nosaci_opis[$i] = $row['opis_srpski'];
	$nosaci_kat_br[$i] = $row['kat_br'];
	$nosaci_kratak_opis[$i] = $row['opis_srpski'];
	//$nosaci_kratak_opis_srpski[$i] = $row['opis_srpski'];
	$nosaci_cena[$i] = $row['vp_cena']/$money;
	$broj_modula[$i] = $row['modul'];
	$i++;
	$brojanje_nosaci=$i;
}
 
echo '<ul class="podmeni_nosaci">';
echo '<li class="slika_nav_x_modula" id=1 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[0].'.png" id=2 title = "'.$nosaci_kat_br[0].'" data-cena = "'.$nosaci_cena[0].'" data-kratak-opis = "'.$nosaci_kratak_opis[0].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">1 MODUL</p><p class = "nosac_slova_2_broj">1</p><p class = "nosac_slova_2">MODUL</p></a></li>';
echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[1].'.png" id=2 title = "'.$nosaci_kat_br[1].'" data-cena = "'.$nosaci_cena[1].'" data-kratak-opis = "'.$nosaci_kratak_opis[1].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2 MODULA</p><p class = "nosac_slova_2_broj">2</p><p class = "nosac_slova_2">MODULA</p></a></li>';
//echo '<li class="slika_nav_x_modula" id=2 title = "'.$nosaci_kat_br[2].'" data-dimenzija = "3" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[2].'.png" id=3 title = "'.$nosaci_kat_br[2].'" data-cena = "'.$nosaci_cena[2].'" data-kratak-opis = "'.$nosaci_kratak_opis[2].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">2/3 MODULA</p><p class = "nosac_slova_2_broj">2/3</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=3 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[3].'.png" id=3 title = "'.$nosaci_kat_br[3].'" data-cena = "'.$nosaci_cena[3].'" data-kratak-opis = "'.$nosaci_kratak_opis[3].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">3 MODULA</p><p class = "nosac_slova_2_broj">3</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=4 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[4].'.png" id=4 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">4 MODULA</p><p class = "nosac_slova_2_broj">4</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=6 title = "'.$nosaci_kat_br[5].'" data-cena = "'.$nosaci_cena[5].'" data-kratak-opis = "'.$nosaci_kratak_opis[5].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[5].'.png" id=6 title = "'.$nosaci_kat_br[5].'" data-cena = "'.$nosaci_cena[5].'" data-kratak-opis = "'.$nosaci_kratak_opis[5].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">6 MODULA</p><p class = "nosac_slova_2_broj">6</p><p class = "nosac_slova_2">MODULA</p></a></li>';
echo '<li class="slika_nav_x_modula" id=7 title = "'.$nosaci_kat_br[6].'" data-cena = "'.$nosaci_cena[6].'" data-kratak-opis = "'.$nosaci_kratak_opis[6].'"><a><img src = "wp-content/themes/momentous-lite/experience/slike/okviri/'.$nosaci_kat_br[6].'.png" id=7 title = "'.$nosaci_kat_br[6].'" data-cena = "'.$nosaci_cena[6].'" data-kratak-opis = "'.$nosaci_kratak_opis[6].'" class="slika_nav_x_modula_slika"><p class = "nosac_slova">7 MODULA</p><p class = "nosac_slova_2_broj">7</p><p class = "nosac_slova_2">MODULA</p></a></li>';
/*echo '<li><a><img src = "wp-content/themes/momentous-lite/experience/slike/'.$nosaci_kat_br[4].'.jpg" class="slika_nav_x_modula" id=33 title = "'.$nosaci_kat_br[4].'" data-cena = "'.$nosaci_cena[4].'" data-kratak-opis = "'.$nosaci_kratak_opis[4].'"><br> 3+3 MODULA</a></li>';*/
echo '</ul>';
//polja za reseller
echo '<div id="prava" data-prava = "'.$prava.'"></div>';
echo '<div id="discount" data-discount = "'.$discount.'"></div>';
?>

<!-- DESNI SCROLL BAR -->
<div class="ex2">
<div class = "desna_mehanizmi"></div>
</div><!-- KRAJ DESNOG SCROLL BARA -->
</div>
<?php /*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="wp-content/themes/momentous-lite/experience/jquery-3.1.1.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/experience/skripta_experience.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/experience/upravljanje_experience_eng.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/experience/experience_jezici.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/experience/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>