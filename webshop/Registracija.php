<?php

require_once 'controller/RegistracijaController.php';


$action = isset($_REQUEST['action'])? $_REQUEST['action'] : "";

if ($_SERVER['REQUEST_METHOD']== 'POST') {


        if(isset($_POST['ime']))
        {
            $rc = new RegistracijaController();
            $rc->registrujKorisnika();
            $errors = $_SESSION['greskee'];
            $uspesno = $_SESSION['uspesno'];

            if(empty($_SESSION['greskee']))
                $greske = array();
            else
            {
                $greske = $_SESSION['greskee'];
                unset($_SESSION['greskee']);
            }

            if(empty($_SESSION['uspesno']))
                $uspesno = "";
            else
            {
                $uspesno = $_SESSION['uspesno'];
                unset($_SESSION['uspesno']);
            }

        }



}
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133685099-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-133685099-1');
    </script>
    <title>Sign up</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="p:domain_verify" content="14698ab00a50113d237550e21238ada0"/>
    <meta name="description" content="We are web service for creating kits of wiring devices. We deliver availability to all target groups: architects, designers, distributors, end-users">
    <meta name="keywords" content="switches, sockets, legrand, bticino, dimmers, led, smart home">
    <link href="http://perfectsocket.com/?page_id=30&lang=en" rel="canonical" hreflang="en">
    <link rel="alternate" hreflang="bs-BA" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="hr-HR" href="http://perfectsocket.com/?page_id=30&lang=en" />
    <link rel="alternate" hreflang="sr-RS" href="http://perfectsocket.com/?page_id=30&lang=en"/>
    <?php require_once('wp-content/themes/momentous-lite/lteme/parts/head--fruity.php'); ?>
    <link rel="stylesheet" type="text/css" href="wp-content/themes/momentous-lite/interio/style_interio.css">
    <link rel="stylesheet" type="text/css" href="css/registracija.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    ini_set("default_charset", "UTF-8");
    //header('Content-type: text/html; charset=UTF-8');
    /*
    Template Name: index_interio_eng
    */
    ?>
</head>
<body>
<?php require_once('wp-content/themes/momentous-lite/lteme/parts/header.php'); ?>

<div id="registracija">
<?php
    //prikaz gresaka

    if(!empty($greske))
    {
    echo "<h1 class='text-center' style='color: red'>Error</h1>
    <p style='color: red'>The following errors occur: <br />";
        foreach($greske as $msg)  //stampanje svake greske
        {
        echo "*  $msg <br />\n";
        }

    }
    if(!empty($uspesno))
    {
    echo "<h1 class='text-center'>Registration successful, Welcome!</h1>";
    }
    else
    {
?>

    <h3 class="text-center" style="color: #15998c">Sign up</h3>
    <form action="Registracija.php" method="post">
        <p>Name:  <br>
            <input type="text" name="ime" size="15" maxlength="20" value="<?php if(isset($_POST['ime'])) echo $_POST['ime']; ?>">
        </p>
        <p>Surname:<br>
            <input type="text" name="prezime" size="15" maxlength="40" value="<?php if(isset($_POST['prezime'])) echo $_POST['prezime']; ?>">
        </p>
        <p>Address:<br>
            <input type="text" name="adresa" size="20" maxlength="40" value="<?php if(isset($_POST['adresa'])) echo $_POST['adresa']; ?>">
        </p>
        <p>City:<br>
            <input type="text" name="grad" size="20" maxlength="40" value="<?php if(isset($_POST['grad'])) echo $_POST['grad']; ?>">
        </p>
        <p>Telephone:<br>
            <input type="text" name="telefon" size="20" maxlength="40" value="<?php if(isset($_POST['telefon'])) echo $_POST['telefon']; ?>">
        </p>
        <p>Email:<br>
            <input type="text" name="email" size="20" maxlength="40" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
        </p>
        <p>Password:<br>
            <input type="password" name="lozinka1" size="10" maxlength="20" value="<?php if(isset($_POST['lozinka1'])) echo $_POST['lozinka1']; ?>">
        </p>
        <p>Confirm password:<br>
            <input type="password" name="lozinka2" size="10" maxlength="20" value="<?php if(isset($_POST['lozinka2'])) echo $_POST['lozinka2']; ?>">
        </p>
        <p> <input class="dugme_potvrdi" type="submit" name="submit" value="Confirm"/></p>
    </form>
    <?php } ?>
</div>









<?php /*
require_once('wp-content/themes/momentous-lite/lteme/parts/footer2.php');*/
?>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery-3.1.1.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/skripta_interio.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/upravljanje_interio_eng.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/interio_jezici.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/interio/jquery.formatCurrency-1.4.0.min.js"></script>
<!-- odnosi se na navigaciju -->
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/app.min.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/header.js"></script>
<script type="text/javascript" src="../wp-content/themes/momentous-lite/lteme/js/burger.js"></script>
</body>
</html>